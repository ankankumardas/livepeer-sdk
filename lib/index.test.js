"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _ava = _interopRequireDefault(require("ava"));

var yup = _interopRequireWildcard(require("yup"));

var _index = _interopRequireWildcard(require("./index"));

var _rxjs = require("rxjs");

// clears console
// console.log('\x1Bc')

/**
 * Type validators
 */
var boolean = yup.boolean();
var number = yup.number().required().required();
var string = yup.string().strict().required();

var object = function object(x) {
  return yup.object().shape(x);
};

var array = function array(x) {
  return yup.array().of(x);
};

var oneOf = function oneOf(x) {
  return yup.mixed().oneOf(x);
};
/**
 * Pre-test
 */


var livepeer;

_ava.default.beforeEach(
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(t) {
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _index.default)();

          case 2:
            livepeer = _context.sent;

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}());
/**
 * Run tests
 */
// utils


(0, _ava.default)('should serialize profiles', function (t) {
  var a = _index.utils.serializeTranscodingProfiles(['P720p60fps16x9']);

  var b = _index.utils.serializeTranscodingProfiles(['foo']);

  t.is('a7ac137a', a);
  t.is('d435c53a', b);
}); // sdk snapshots

(0, _ava.default)('should initialize contracts',
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(t) {
    var _ref3, contracts, snap;

    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0, _index.initContracts)();

          case 2:
            _ref3 = _context2.sent;
            contracts = _ref3.contracts;
            snap = Object.keys(contracts).reduce(function (a, b) {
              var c = (0, _objectSpread2.default)({}, a); // constantly changes, so set to 0 for snapshot purposes

              c[b].query.rpc.idCounter = 0;
              return c;
            }, contracts); // TODO: unskip once contract schema is stable
            // t.skip.snapshot(snap)

            t.pass();

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x2) {
    return _ref2.apply(this, arguments);
  };
}());
(0, _ava.default)('should initialize SDK',
/*#__PURE__*/
function () {
  var _ref4 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3(t) {
    var _ref5, rpc;

    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _index.default)();

          case 2:
            _ref5 = _context3.sent;
            rpc = _ref5.rpc;
            // TODO: unskip once sdk rpc is stable
            // t.skip.snapshot(rpc)
            t.pass();

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x3) {
    return _ref4.apply(this, arguments);
  };
}()); // Bonding Manager

(0, _ava.default)('should get unbonding period',
/*#__PURE__*/
function () {
  var _ref6 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(t) {
    var res;
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return livepeer.rpc.getUnbondingPeriod();

          case 2:
            res = _context4.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x4) {
    return _ref6.apply(this, arguments);
  };
}());
(0, _ava.default)('should get number active transcoders',
/*#__PURE__*/
function () {
  var _ref7 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5(t) {
    var res;
    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return livepeer.rpc.getNumActiveTranscoders();

          case 2:
            res = _context5.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x5) {
    return _ref7.apply(this, arguments);
  };
}());
(0, _ava.default)('should get maximum earning for claims rounds',
/*#__PURE__*/
function () {
  var _ref8 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee6(t) {
    var res;
    return _regenerator.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return livepeer.rpc.getMaxEarningsClaimsRounds();

          case 2:
            res = _context6.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x6) {
    return _ref8.apply(this, arguments);
  };
}());
(0, _ava.default)('should get total bonded',
/*#__PURE__*/
function () {
  var _ref9 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee7(t) {
    var res;
    return _regenerator.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return livepeer.rpc.getTotalBonded();

          case 2:
            res = _context7.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x7) {
    return _ref9.apply(this, arguments);
  };
}());
(0, _ava.default)('should get transcoder pool max size',
/*#__PURE__*/
function () {
  var _ref10 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee8(t) {
    var res;
    return _regenerator.default.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return livepeer.rpc.getTranscoderPoolMaxSize();

          case 2:
            res = _context8.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x8) {
    return _ref10.apply(this, arguments);
  };
}()); // ENS

(0, _ava.default)('should get ENS for ETH address',
/*#__PURE__*/
function () {
  var _ref11 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee9(t) {
    var res;
    return _regenerator.default.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return livepeer.rpc.getENSName('0x96b20f67309a0750b3fc3dcbe989f347167482ff');

          case 2:
            res = _context9.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x9) {
    return _ref11.apply(this, arguments);
  };
}());
(0, _ava.default)('should get empty string when no ENS name address',
/*#__PURE__*/
function () {
  var _ref12 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee10(t) {
    var res;
    return _regenerator.default.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return livepeer.rpc.getENSName('0x0000000000000000000000000000000000000000');

          case 2:
            res = _context10.sent;
            t.true('' === res);

          case 4:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x10) {
    return _ref12.apply(this, arguments);
  };
}());
(0, _ava.default)('should get ETH address for ENS name',
/*#__PURE__*/
function () {
  var _ref13 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee11(t) {
    var res;
    return _regenerator.default.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return livepeer.rpc.getENSAddress('please.buymecoffee.eth');

          case 2:
            res = _context11.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x11) {
    return _ref13.apply(this, arguments);
  };
}());
(0, _ava.default)('should get empty string for nonexistent ENS name',
/*#__PURE__*/
function () {
  var _ref14 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee12(t) {
    var res;
    return _regenerator.default.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return livepeer.rpc.getENSAddress('donot.buymecoffee.eth');

          case 2:
            res = _context12.sent;
            t.true('' === res);

          case 4:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));

  return function (_x12) {
    return _ref14.apply(this, arguments);
  };
}()); // ETH

(0, _ava.default)('should get ETH balance',
/*#__PURE__*/
function () {
  var _ref15 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee13(t) {
    var from, res;
    return _regenerator.default.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            from = livepeer.config.defaultTx.from;
            _context13.next = 3;
            return livepeer.rpc.getEthBalance(from);

          case 3:
            res = _context13.sent;
            t.true(string.isValidSync(res));

          case 5:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));

  return function (_x13) {
    return _ref15.apply(this, arguments);
  };
}()); // Minter

(0, _ava.default)('should return a number from getInflation()',
/*#__PURE__*/
function () {
  var _ref16 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee14(t) {
    var res;
    return _regenerator.default.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.next = 2;
            return livepeer.rpc.getInflation();

          case 2:
            res = _context14.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14);
  }));

  return function (_x14) {
    return _ref16.apply(this, arguments);
  };
}());
(0, _ava.default)('should return a number from getInflationChange()',
/*#__PURE__*/
function () {
  var _ref17 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee15(t) {
    var res;
    return _regenerator.default.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.next = 2;
            return livepeer.rpc.getInflationChange();

          case 2:
            res = _context15.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15);
  }));

  return function (_x15) {
    return _ref17.apply(this, arguments);
  };
}()); // Token

(0, _ava.default)('should return a number from getTokenTotalSupply()',
/*#__PURE__*/
function () {
  var _ref18 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee16(t) {
    var res;
    return _regenerator.default.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.next = 2;
            return livepeer.rpc.getTokenTotalSupply();

          case 2:
            res = _context16.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16);
  }));

  return function (_x16) {
    return _ref18.apply(this, arguments);
  };
}());
(0, _ava.default)('should return a number from getTokenBalance()',
/*#__PURE__*/
function () {
  var _ref19 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee17(t) {
    var from, res;
    return _regenerator.default.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            from = livepeer.config.defaultTx.from;
            _context17.next = 3;
            return livepeer.rpc.getTokenBalance(from);

          case 3:
            res = _context17.sent;
            t.true(string.isValidSync(res));

          case 5:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17);
  }));

  return function (_x17) {
    return _ref19.apply(this, arguments);
  };
}());
(0, _ava.default)('should return object with correct shape from getTokenInfo()',
/*#__PURE__*/
function () {
  var _ref20 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee18(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            schema = object({
              totalSupply: string,
              balance: string
            });
            from = livepeer.config.defaultTx.from;
            _context18.next = 4;
            return livepeer.rpc.getTokenInfo(from);

          case 4:
            res = _context18.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18);
  }));

  return function (_x18) {
    return _ref20.apply(this, arguments);
  };
}()); // Faucet

(0, _ava.default)('should return object with correct shape from getFaucetInfo()',
/*#__PURE__*/
function () {
  var _ref21 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee19(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            schema = object({
              amount: string,
              wait: string,
              next: string
            });
            from = livepeer.config.defaultTx.from;
            _context19.next = 4;
            return livepeer.rpc.getFaucetInfo(from);

          case 4:
            res = _context19.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context19.stop();
        }
      }
    }, _callee19);
  }));

  return function (_x19) {
    return _ref21.apply(this, arguments);
  };
}()); // Block

(0, _ava.default)('should return object with correct shape from getBlock()',
/*#__PURE__*/
function () {
  var _ref22 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee20(t) {
    var _object;

    var schema, res;
    return _regenerator.default.wrap(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            schema = object((_object = {
              number: string,
              hash: string,
              parentHash: string,
              nonce: string,
              sha3Uncles: string,
              logsBloom: string,
              transactionsRoot: string,
              stateRoot: string,
              receiptsRoot: string,
              miner: string,
              mixHash: string,
              difficulty: string,
              totalDifficulty: string,
              extraData: string,
              size: string,
              gasLimit: string,
              gasUsed: string,
              timestamp: number,
              transactions: array(object({}))
            }, (0, _defineProperty2.default)(_object, "transactionsRoot", string), (0, _defineProperty2.default)(_object, "uncles", array(string)), _object));
            _context20.next = 3;
            return livepeer.rpc.getBlock('latest');

          case 3:
            res = _context20.sent;
            schema.validateSync(res);
            t.pass();

          case 6:
          case "end":
            return _context20.stop();
        }
      }
    }, _callee20);
  }));

  return function (_x20) {
    return _ref22.apply(this, arguments);
  };
}()); // Broadcaster

(0, _ava.default)('should return object with correct shape from getBroadcaster()',
/*#__PURE__*/
function () {
  var _ref23 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee21(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee21$(_context21) {
      while (1) {
        switch (_context21.prev = _context21.next) {
          case 0:
            schema = object({
              deposit: string,
              withdrawBlock: string
            });
            from = livepeer.config.defaultTx.from;
            _context21.next = 4;
            return livepeer.rpc.getBroadcaster(from);

          case 4:
            res = _context21.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context21.stop();
        }
      }
    }, _callee21);
  }));

  return function (_x21) {
    return _ref23.apply(this, arguments);
  };
}()); // Delgator

(0, _ava.default)('should return object with correct shape from getDelegator()',
/*#__PURE__*/
function () {
  var _ref24 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee22(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee22$(_context22) {
      while (1) {
        switch (_context22.prev = _context22.next) {
          case 0:
            schema = object({
              allowance: string,
              address: string,
              bondedAmount: string,
              delegateAddress: yup.string(),
              // can be empty ('')
              delegatedAmount: string,
              fees: string,
              lastClaimRound: string,
              pendingStake: string,
              pendingFees: string,
              startRound: string,
              status: oneOf(livepeer.constants.DELEGATOR_STATUS),
              withdrawAmount: string,
              withdrawRound: string,
              nextUnbondingLockId: string
            });
            from = livepeer.config.defaultTx.from;
            _context22.next = 4;
            return livepeer.rpc.getDelegator('please.buymecoffee.eth');

          case 4:
            res = _context22.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context22.stop();
        }
      }
    }, _callee22);
  }));

  return function (_x22) {
    return _ref24.apply(this, arguments);
  };
}());
(0, _ava.default)('should return object with correct shape from getDelegatorUnbondingLock()',
/*#__PURE__*/
function () {
  var _ref25 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee23(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee23$(_context23) {
      while (1) {
        switch (_context23.prev = _context23.next) {
          case 0:
            schema = object({
              id: string,
              delegator: string,
              amount: string,
              withdrawRound: string
            });
            from = livepeer.config.defaultTx.from;
            _context23.next = 4;
            return livepeer.rpc.getDelegatorUnbondingLock(from, '0');

          case 4:
            res = _context23.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context23.stop();
        }
      }
    }, _callee23);
  }));

  return function (_x23) {
    return _ref25.apply(this, arguments);
  };
}());
(0, _ava.default)('should return object with correct shape from getDelegatorUnbondingLocks()',
/*#__PURE__*/
function () {
  var _ref26 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee24(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee24$(_context24) {
      while (1) {
        switch (_context24.prev = _context24.next) {
          case 0:
            schema = array(object({
              id: string,
              delegator: string,
              amount: string,
              withdrawRound: string
            }));
            from = livepeer.config.defaultTx.from;
            _context24.next = 4;
            return livepeer.rpc.getDelegatorUnbondingLocks(from);

          case 4:
            res = _context24.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context24.stop();
        }
      }
    }, _callee24);
  }));

  return function (_x24) {
    return _ref26.apply(this, arguments);
  };
}()); // Transcoder

(0, _ava.default)('should return object with correct shape from getTranscoder()',
/*#__PURE__*/
function () {
  var _ref27 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee25(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee25$(_context25) {
      while (1) {
        switch (_context25.prev = _context25.next) {
          case 0:
            schema = object({
              active: boolean,
              address: string,
              feeShare: string,
              // %
              lastRewardRound: string,
              pricePerSegment: string,
              pendingRewardCut: string,
              // %
              pendingFeeShare: string,
              // %
              pendingPricePerSegment: string,
              rewardCut: string,
              // %
              status: oneOf(livepeer.constants.TRANSCODER_STATUS)
            });
            from = livepeer.config.defaultTx.from;
            _context25.next = 4;
            return livepeer.rpc.getTranscoder(from);

          case 4:
            res = _context25.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context25.stop();
        }
      }
    }, _callee25);
  }));

  return function (_x25) {
    return _ref27.apply(this, arguments);
  };
}()); // Rounds

(0, _ava.default)('should return object with correct shape from getCurrentRoundInfo()',
/*#__PURE__*/
function () {
  var _ref28 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee26(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee26$(_context26) {
      while (1) {
        switch (_context26.prev = _context26.next) {
          case 0:
            schema = object({
              id: string,
              initialized: boolean,
              startBlock: string,
              lastInitializedRound: string,
              length: string
            });
            from = livepeer.config.defaultTx.from;
            _context26.next = 4;
            return livepeer.rpc.getCurrentRoundInfo(from);

          case 4:
            res = _context26.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context26.stop();
        }
      }
    }, _callee26);
  }));

  return function (_x26) {
    return _ref28.apply(this, arguments);
  };
}()); // Jobs

(0, _ava.default)('should return object with correct shape from getJob()',
/*#__PURE__*/
function () {
  var _ref29 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee27(t) {
    var schema, res, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, x, job;

    return _regenerator.default.wrap(function _callee27$(_context27) {
      while (1) {
        switch (_context27.prev = _context27.next) {
          case 0:
            schema = object({
              id: string,
              streamId: string,
              transcodingOptions: array(object({
                hash: string,
                name: string,
                bitrate: string,
                framerate: number,
                resolution: string
              })),
              transcoder: string,
              broadcaster: string
            });
            _context27.next = 3;
            return livepeer.rpc.getJobs();

          case 3:
            res = _context27.sent;
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context27.prev = 7;
            _iterator = res[Symbol.iterator]();

          case 9:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context27.next = 18;
              break;
            }

            x = _step.value;
            _context27.next = 13;
            return livepeer.rpc.getJob(x.id);

          case 13:
            job = _context27.sent;
            schema.validateSync(job);

          case 15:
            _iteratorNormalCompletion = true;
            _context27.next = 9;
            break;

          case 18:
            _context27.next = 24;
            break;

          case 20:
            _context27.prev = 20;
            _context27.t0 = _context27["catch"](7);
            _didIteratorError = true;
            _iteratorError = _context27.t0;

          case 24:
            _context27.prev = 24;
            _context27.prev = 25;

            if (!_iteratorNormalCompletion && _iterator.return != null) {
              _iterator.return();
            }

          case 27:
            _context27.prev = 27;

            if (!_didIteratorError) {
              _context27.next = 30;
              break;
            }

            throw _iteratorError;

          case 30:
            return _context27.finish(27);

          case 31:
            return _context27.finish(24);

          case 32:
            t.pass();

          case 33:
          case "end":
            return _context27.stop();
        }
      }
    }, _callee27, null, [[7, 20, 24, 32], [25,, 27, 31]]);
  }));

  return function (_x27) {
    return _ref29.apply(this, arguments);
  };
}());
(0, _ava.default)('should return number that signifies the estimated amount of gas to be used',
/*#__PURE__*/
function () {
  var _ref30 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee28(t) {
    var cases, _i, _cases, x, res;

    return _regenerator.default.wrap(function _callee28$(_context28) {
      while (1) {
        switch (_context28.prev = _context28.next) {
          case 0:
            cases = [{
              contractName: 'BondingManager',
              methodName: 'maxEarningsClaimsRounds',
              methodArgs: []
            }, {
              contractName: 'LivepeerToken',
              methodName: 'approve',
              methodArgs: [livepeer.config.contracts.BondingManager.address, 10]
            }, {
              contractName: 'Minter',
              methodName: 'currentMintedTokens',
              methodArgs: []
            }, {
              contractName: 'LivepeerToken',
              methodName: 'mintingFinished',
              methodArgs: []
            }];
            _i = 0, _cases = cases;

          case 2:
            if (!(_i < _cases.length)) {
              _context28.next = 12;
              break;
            }

            x = _cases[_i];
            _context28.next = 6;
            return livepeer.rpc.estimateGas(x.contractName, x.methodName, x.methodArgs);

          case 6:
            res = _context28.sent;
            t.true(number.isValidSync(res));
            t.true(res > 0);

          case 9:
            _i++;
            _context28.next = 2;
            break;

          case 12:
            t.pass();

          case 13:
          case "end":
            return _context28.stop();
        }
      }
    }, _callee28);
  }));

  return function (_x28) {
    return _ref30.apply(this, arguments);
  };
}());
(0, _ava.default)('should return object with correct shape from getJobsInfo()',
/*#__PURE__*/
function () {
  var _ref31 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee29(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee29$(_context29) {
      while (1) {
        switch (_context29.prev = _context29.next) {
          case 0:
            schema = object({
              total: string,
              verificationRate: string,
              verificationSlashingPeriod: string,
              finderFee: string
            });
            from = livepeer.config.defaultTx.from;
            _context29.next = 4;
            return livepeer.rpc.getJobsInfo(from);

          case 4:
            res = _context29.sent;
            schema.validateSync(res);
            t.pass();

          case 7:
          case "end":
            return _context29.stop();
        }
      }
    }, _callee29);
  }));

  return function (_x29) {
    return _ref31.apply(this, arguments);
  };
}());
(0, _ava.default)('should get many jobs from getJobs()',
/*#__PURE__*/
function () {
  var _ref32 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee30(t) {
    var schema, from, res;
    return _regenerator.default.wrap(function _callee30$(_context30) {
      while (1) {
        switch (_context30.prev = _context30.next) {
          case 0:
            schema = object({
              id: string,
              streamId: string,
              transcodingOptions: array(object({
                name: string,
                bitrate: string,
                framerate: yup.number().positive().required(),
                resolution: string
              })),
              broadcaster: string
            });
            from = livepeer.config.defaultTx.from;
            _context30.next = 4;
            return livepeer.rpc.getJobs();

          case 4:
            res = _context30.sent;
            res.forEach(function (x) {
              return schema.validateSync(x);
            });
            t.pass();

          case 7:
          case "end":
            return _context30.stop();
        }
      }
    }, _callee30);
  }));

  return function (_x30) {
    return _ref32.apply(this, arguments);
  };
}()); // Protocol

(0, _ava.default)('should return object with correct shape from getProtocol()',
/*#__PURE__*/
function () {
  var _ref33 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee31(t) {
    var schema, res;
    return _regenerator.default.wrap(function _callee31$(_context31) {
      while (1) {
        switch (_context31.prev = _context31.next) {
          case 0:
            schema = object({
              paused: boolean,
              totalTokenSupply: string,
              totalBondedToken: string,
              targetBondingRate: string,
              transcoderPoolMaxSize: string,
              maxEarningsClaimsRounds: string
            });
            _context31.next = 3;
            return livepeer.rpc.getProtocol();

          case 3:
            res = _context31.sent;
            schema.validateSync(res);
            t.pass();

          case 6:
          case "end":
            return _context31.stop();
        }
      }
    }, _callee31);
  }));

  return function (_x31) {
    return _ref33.apply(this, arguments);
  };
}()); // Minter

(0, _ava.default)('should return target bonding rate',
/*#__PURE__*/
function () {
  var _ref34 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee32(t) {
    var res;
    return _regenerator.default.wrap(function _callee32$(_context32) {
      while (1) {
        switch (_context32.prev = _context32.next) {
          case 0:
            _context32.next = 2;
            return livepeer.rpc.getTargetBondingRate();

          case 2:
            res = _context32.sent;
            t.true(string.isValidSync(res));

          case 4:
          case "end":
            return _context32.stop();
        }
      }
    }, _callee32);
  }));

  return function (_x32) {
    return _ref34.apply(this, arguments);
  };
}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC50ZXN0LmpzIl0sIm5hbWVzIjpbImJvb2xlYW4iLCJ5dXAiLCJudW1iZXIiLCJyZXF1aXJlZCIsInN0cmluZyIsInN0cmljdCIsIm9iamVjdCIsIngiLCJzaGFwZSIsImFycmF5Iiwib2YiLCJvbmVPZiIsIm1peGVkIiwibGl2ZXBlZXIiLCJ0ZXN0IiwiYmVmb3JlRWFjaCIsInQiLCJhIiwidXRpbHMiLCJzZXJpYWxpemVUcmFuc2NvZGluZ1Byb2ZpbGVzIiwiYiIsImlzIiwiY29udHJhY3RzIiwic25hcCIsIk9iamVjdCIsImtleXMiLCJyZWR1Y2UiLCJjIiwicXVlcnkiLCJycGMiLCJpZENvdW50ZXIiLCJwYXNzIiwiZ2V0VW5ib25kaW5nUGVyaW9kIiwicmVzIiwidHJ1ZSIsImlzVmFsaWRTeW5jIiwiZ2V0TnVtQWN0aXZlVHJhbnNjb2RlcnMiLCJnZXRNYXhFYXJuaW5nc0NsYWltc1JvdW5kcyIsImdldFRvdGFsQm9uZGVkIiwiZ2V0VHJhbnNjb2RlclBvb2xNYXhTaXplIiwiZ2V0RU5TTmFtZSIsImdldEVOU0FkZHJlc3MiLCJmcm9tIiwiY29uZmlnIiwiZGVmYXVsdFR4IiwiZ2V0RXRoQmFsYW5jZSIsImdldEluZmxhdGlvbiIsImdldEluZmxhdGlvbkNoYW5nZSIsImdldFRva2VuVG90YWxTdXBwbHkiLCJnZXRUb2tlbkJhbGFuY2UiLCJzY2hlbWEiLCJ0b3RhbFN1cHBseSIsImJhbGFuY2UiLCJnZXRUb2tlbkluZm8iLCJ2YWxpZGF0ZVN5bmMiLCJhbW91bnQiLCJ3YWl0IiwibmV4dCIsImdldEZhdWNldEluZm8iLCJoYXNoIiwicGFyZW50SGFzaCIsIm5vbmNlIiwic2hhM1VuY2xlcyIsImxvZ3NCbG9vbSIsInRyYW5zYWN0aW9uc1Jvb3QiLCJzdGF0ZVJvb3QiLCJyZWNlaXB0c1Jvb3QiLCJtaW5lciIsIm1peEhhc2giLCJkaWZmaWN1bHR5IiwidG90YWxEaWZmaWN1bHR5IiwiZXh0cmFEYXRhIiwic2l6ZSIsImdhc0xpbWl0IiwiZ2FzVXNlZCIsInRpbWVzdGFtcCIsInRyYW5zYWN0aW9ucyIsImdldEJsb2NrIiwiZGVwb3NpdCIsIndpdGhkcmF3QmxvY2siLCJnZXRCcm9hZGNhc3RlciIsImFsbG93YW5jZSIsImFkZHJlc3MiLCJib25kZWRBbW91bnQiLCJkZWxlZ2F0ZUFkZHJlc3MiLCJkZWxlZ2F0ZWRBbW91bnQiLCJmZWVzIiwibGFzdENsYWltUm91bmQiLCJwZW5kaW5nU3Rha2UiLCJwZW5kaW5nRmVlcyIsInN0YXJ0Um91bmQiLCJzdGF0dXMiLCJjb25zdGFudHMiLCJERUxFR0FUT1JfU1RBVFVTIiwid2l0aGRyYXdBbW91bnQiLCJ3aXRoZHJhd1JvdW5kIiwibmV4dFVuYm9uZGluZ0xvY2tJZCIsImdldERlbGVnYXRvciIsImlkIiwiZGVsZWdhdG9yIiwiZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9jayIsImdldERlbGVnYXRvclVuYm9uZGluZ0xvY2tzIiwiYWN0aXZlIiwiZmVlU2hhcmUiLCJsYXN0UmV3YXJkUm91bmQiLCJwcmljZVBlclNlZ21lbnQiLCJwZW5kaW5nUmV3YXJkQ3V0IiwicGVuZGluZ0ZlZVNoYXJlIiwicGVuZGluZ1ByaWNlUGVyU2VnbWVudCIsInJld2FyZEN1dCIsIlRSQU5TQ09ERVJfU1RBVFVTIiwiZ2V0VHJhbnNjb2RlciIsImluaXRpYWxpemVkIiwic3RhcnRCbG9jayIsImxhc3RJbml0aWFsaXplZFJvdW5kIiwibGVuZ3RoIiwiZ2V0Q3VycmVudFJvdW5kSW5mbyIsInN0cmVhbUlkIiwidHJhbnNjb2RpbmdPcHRpb25zIiwibmFtZSIsImJpdHJhdGUiLCJmcmFtZXJhdGUiLCJyZXNvbHV0aW9uIiwidHJhbnNjb2RlciIsImJyb2FkY2FzdGVyIiwiZ2V0Sm9icyIsImdldEpvYiIsImpvYiIsImNhc2VzIiwiY29udHJhY3ROYW1lIiwibWV0aG9kTmFtZSIsIm1ldGhvZEFyZ3MiLCJCb25kaW5nTWFuYWdlciIsImVzdGltYXRlR2FzIiwidG90YWwiLCJ2ZXJpZmljYXRpb25SYXRlIiwidmVyaWZpY2F0aW9uU2xhc2hpbmdQZXJpb2QiLCJmaW5kZXJGZWUiLCJnZXRKb2JzSW5mbyIsInBvc2l0aXZlIiwiZm9yRWFjaCIsInBhdXNlZCIsInRvdGFsVG9rZW5TdXBwbHkiLCJ0b3RhbEJvbmRlZFRva2VuIiwidGFyZ2V0Qm9uZGluZ1JhdGUiLCJ0cmFuc2NvZGVyUG9vbE1heFNpemUiLCJtYXhFYXJuaW5nc0NsYWltc1JvdW5kcyIsImdldFByb3RvY29sIiwiZ2V0VGFyZ2V0Qm9uZGluZ1JhdGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7O0FBSUEsSUFBTUEsT0FBTyxHQUFHQyxHQUFHLENBQUNELE9BQUosRUFBaEI7QUFDQSxJQUFNRSxNQUFNLEdBQUdELEdBQUcsQ0FDZkMsTUFEWSxHQUVaQyxRQUZZLEdBR1pBLFFBSFksRUFBZjtBQUlBLElBQU1DLE1BQU0sR0FBR0gsR0FBRyxDQUNmRyxNQURZLEdBRVpDLE1BRlksR0FHWkYsUUFIWSxFQUFmOztBQUlBLElBQU1HLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUFDLENBQUM7QUFBQSxTQUFJTixHQUFHLENBQUNLLE1BQUosR0FBYUUsS0FBYixDQUFtQkQsQ0FBbkIsQ0FBSjtBQUFBLENBQWhCOztBQUNBLElBQU1FLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUFGLENBQUM7QUFBQSxTQUFJTixHQUFHLENBQUNRLEtBQUosR0FBWUMsRUFBWixDQUFlSCxDQUFmLENBQUo7QUFBQSxDQUFmOztBQUNBLElBQU1JLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUFKLENBQUM7QUFBQSxTQUFJTixHQUFHLENBQUNXLEtBQUosR0FBWUQsS0FBWixDQUFrQkosQ0FBbEIsQ0FBSjtBQUFBLENBQWY7QUFFQTs7Ozs7QUFJQSxJQUFJTSxRQUFKOztBQUVBQyxhQUFLQyxVQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBZ0IsaUJBQU1DLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRUcscUJBRkg7O0FBQUE7QUFFZEgsWUFBQUEsUUFGYzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFoQjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBOzs7QUFJQTs7O0FBRUEsa0JBQUssMkJBQUwsRUFBa0MsVUFBQUcsQ0FBQyxFQUFJO0FBQ3JDLE1BQU1DLENBQUMsR0FBR0MsYUFBTUMsNEJBQU4sQ0FBbUMsQ0FBQyxnQkFBRCxDQUFuQyxDQUFWOztBQUNBLE1BQU1DLENBQUMsR0FBR0YsYUFBTUMsNEJBQU4sQ0FBbUMsQ0FBQyxLQUFELENBQW5DLENBQVY7O0FBQ0FILEVBQUFBLENBQUMsQ0FBQ0ssRUFBRixDQUFLLFVBQUwsRUFBaUJKLENBQWpCO0FBQ0FELEVBQUFBLENBQUMsQ0FBQ0ssRUFBRixDQUFLLFVBQUwsRUFBaUJELENBQWpCO0FBQ0QsQ0FMRCxFLENBT0E7O0FBRUEsa0JBQUssNkJBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFvQyxrQkFBTUosQ0FBTjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDTiwyQkFETTs7QUFBQTtBQUFBO0FBQzFCTSxZQUFBQSxTQUQwQixTQUMxQkEsU0FEMEI7QUFFNUJDLFlBQUFBLElBRjRCLEdBRXJCQyxNQUFNLENBQUNDLElBQVAsQ0FBWUgsU0FBWixFQUF1QkksTUFBdkIsQ0FBOEIsVUFBQ1QsQ0FBRCxFQUFJRyxDQUFKLEVBQVU7QUFDbkQsa0JBQU1PLENBQUMsbUNBQ0ZWLENBREUsQ0FBUCxDQURtRCxDQUluRDs7QUFDQVUsY0FBQUEsQ0FBQyxDQUFDUCxDQUFELENBQUQsQ0FBS1EsS0FBTCxDQUFXQyxHQUFYLENBQWVDLFNBQWYsR0FBMkIsQ0FBM0I7QUFDQSxxQkFBT0gsQ0FBUDtBQUNELGFBUFksRUFPVkwsU0FQVSxDQUZxQixFQVVsQztBQUNBOztBQUNBTixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBWmtDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXBDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUEsa0JBQUssdUJBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUE4QixrQkFBTWYsQ0FBTjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDTixxQkFETTs7QUFBQTtBQUFBO0FBQ3BCYSxZQUFBQSxHQURvQixTQUNwQkEsR0FEb0I7QUFFNUI7QUFDQTtBQUNBYixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBSjRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQTlCOztBQUFBO0FBQUE7QUFBQTtBQUFBLEssQ0FPQTs7QUFFQSxrQkFBSyw2QkFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQW9DLGtCQUFNZixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ2hCSCxRQUFRLENBQUNnQixHQUFULENBQWFHLGtCQUFiLEVBRGdCOztBQUFBO0FBQzVCQyxZQUFBQSxHQUQ0QjtBQUVsQ2pCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBRmtDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXBDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0Esa0JBQUssc0NBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUE2QyxrQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDekJILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYU8sdUJBQWIsRUFEeUI7O0FBQUE7QUFDckNILFlBQUFBLEdBRHFDO0FBRTNDakIsWUFBQUEsQ0FBQyxDQUFDa0IsSUFBRixDQUFPOUIsTUFBTSxDQUFDK0IsV0FBUCxDQUFtQkYsR0FBbkIsQ0FBUDs7QUFGMkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBN0M7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQSxrQkFBSyw4Q0FBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQXFELGtCQUFNakIsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUNqQ0gsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhUSwwQkFBYixFQURpQzs7QUFBQTtBQUM3Q0osWUFBQUEsR0FENkM7QUFFbkRqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU85QixNQUFNLENBQUMrQixXQUFQLENBQW1CRixHQUFuQixDQUFQOztBQUZtRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFyRDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBLGtCQUFLLHlCQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBZ0Msa0JBQU1qQixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ1pILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYVMsY0FBYixFQURZOztBQUFBO0FBQ3hCTCxZQUFBQSxHQUR3QjtBQUU5QmpCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBRjhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQWhDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0Esa0JBQUsscUNBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUE0QyxrQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDeEJILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYVUsd0JBQWIsRUFEd0I7O0FBQUE7QUFDcENOLFlBQUFBLEdBRG9DO0FBRTFDakIsWUFBQUEsQ0FBQyxDQUFDa0IsSUFBRixDQUFPOUIsTUFBTSxDQUFDK0IsV0FBUCxDQUFtQkYsR0FBbkIsQ0FBUDs7QUFGMEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBNUM7O0FBQUE7QUFBQTtBQUFBO0FBQUEsSyxDQUtBOztBQUVBLGtCQUFLLGdDQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBdUMsa0JBQU1qQixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ25CSCxRQUFRLENBQUNnQixHQUFULENBQWFXLFVBQWIsQ0FDaEIsNENBRGdCLENBRG1COztBQUFBO0FBQy9CUCxZQUFBQSxHQUQrQjtBQUlyQ2pCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBSnFDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXZDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0Esa0JBQUssa0RBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUF5RCxtQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDckNILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYVcsVUFBYixDQUNoQiw0Q0FEZ0IsQ0FEcUM7O0FBQUE7QUFDakRQLFlBQUFBLEdBRGlEO0FBSXZEakIsWUFBQUEsQ0FBQyxDQUFDa0IsSUFBRixDQUFPLE9BQU9ELEdBQWQ7O0FBSnVEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXpEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0Esa0JBQUsscUNBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUE0QyxtQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDeEJILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYVksYUFBYixDQUEyQix3QkFBM0IsQ0FEd0I7O0FBQUE7QUFDcENSLFlBQUFBLEdBRG9DO0FBRTFDakIsWUFBQUEsQ0FBQyxDQUFDa0IsSUFBRixDQUFPOUIsTUFBTSxDQUFDK0IsV0FBUCxDQUFtQkYsR0FBbkIsQ0FBUDs7QUFGMEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBNUM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQSxrQkFBSyxrREFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQXlELG1CQUFNakIsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUNyQ0gsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhWSxhQUFiLENBQTJCLHVCQUEzQixDQURxQzs7QUFBQTtBQUNqRFIsWUFBQUEsR0FEaUQ7QUFFdkRqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU8sT0FBT0QsR0FBZDs7QUFGdUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBekQ7O0FBQUE7QUFBQTtBQUFBO0FBQUEsSyxDQUtBOztBQUVBLGtCQUFLLHdCQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBK0IsbUJBQU1qQixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNyQjBCLFlBQUFBLElBRHFCLEdBQ1o3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQURKLENBQ3JCRixJQURxQjtBQUFBO0FBQUEsbUJBRVg3QixRQUFRLENBQUNnQixHQUFULENBQWFnQixhQUFiLENBQTJCSCxJQUEzQixDQUZXOztBQUFBO0FBRXZCVCxZQUFBQSxHQUZ1QjtBQUc3QmpCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBSDZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQS9COztBQUFBO0FBQUE7QUFBQTtBQUFBLEssQ0FNQTs7QUFFQSxrQkFBSyw0Q0FBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQW1ELG1CQUFNakIsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUMvQkgsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhaUIsWUFBYixFQUQrQjs7QUFBQTtBQUMzQ2IsWUFBQUEsR0FEMkM7QUFFakRqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU85QixNQUFNLENBQUMrQixXQUFQLENBQW1CRixHQUFuQixDQUFQOztBQUZpRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFuRDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBLGtCQUFLLGtEQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBeUQsbUJBQU1qQixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ3JDSCxRQUFRLENBQUNnQixHQUFULENBQWFrQixrQkFBYixFQURxQzs7QUFBQTtBQUNqRGQsWUFBQUEsR0FEaUQ7QUFFdkRqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU85QixNQUFNLENBQUMrQixXQUFQLENBQW1CRixHQUFuQixDQUFQOztBQUZ1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUF6RDs7QUFBQTtBQUFBO0FBQUE7QUFBQSxLLENBS0E7O0FBRUEsa0JBQUssbURBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUEwRCxtQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDdENILFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYW1CLG1CQUFiLEVBRHNDOztBQUFBO0FBQ2xEZixZQUFBQSxHQURrRDtBQUV4RGpCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBRndEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQTFEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0Esa0JBQUssK0NBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFzRCxtQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzVDMEIsWUFBQUEsSUFENEMsR0FDbkM3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQURtQixDQUM1Q0YsSUFENEM7QUFBQTtBQUFBLG1CQUVsQzdCLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYW9CLGVBQWIsQ0FBNkJQLElBQTdCLENBRmtDOztBQUFBO0FBRTlDVCxZQUFBQSxHQUY4QztBQUdwRGpCLFlBQUFBLENBQUMsQ0FBQ2tCLElBQUYsQ0FBTzlCLE1BQU0sQ0FBQytCLFdBQVAsQ0FBbUJGLEdBQW5CLENBQVA7O0FBSG9EO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXREOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUEsa0JBQUssNkRBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFvRSxtQkFBTWpCLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzVEa0MsWUFBQUEsTUFENEQsR0FDbkQ1QyxNQUFNLENBQUM7QUFDcEI2QyxjQUFBQSxXQUFXLEVBQUUvQyxNQURPO0FBRXBCZ0QsY0FBQUEsT0FBTyxFQUFFaEQ7QUFGVyxhQUFELENBRDZDO0FBSzFEc0MsWUFBQUEsSUFMMEQsR0FLakQ3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQUxpQyxDQUsxREYsSUFMMEQ7QUFBQTtBQUFBLG1CQU1oRDdCLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYXdCLFlBQWIsQ0FBMEJYLElBQTFCLENBTmdEOztBQUFBO0FBTTVEVCxZQUFBQSxHQU40RDtBQU9sRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBUmtFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXBFOztBQUFBO0FBQUE7QUFBQTtBQUFBLEssQ0FXQTs7QUFFQSxrQkFBSyw4REFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQXFFLG1CQUFNZixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUM3RGtDLFlBQUFBLE1BRDZELEdBQ3BENUMsTUFBTSxDQUFDO0FBQ3BCaUQsY0FBQUEsTUFBTSxFQUFFbkQsTUFEWTtBQUVwQm9ELGNBQUFBLElBQUksRUFBRXBELE1BRmM7QUFHcEJxRCxjQUFBQSxJQUFJLEVBQUVyRDtBQUhjLGFBQUQsQ0FEOEM7QUFNM0RzQyxZQUFBQSxJQU4yRCxHQU1sRDdCLFFBQVEsQ0FBQzhCLE1BQVQsQ0FBZ0JDLFNBTmtDLENBTTNERixJQU4yRDtBQUFBO0FBQUEsbUJBT2pEN0IsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhNkIsYUFBYixDQUEyQmhCLElBQTNCLENBUGlEOztBQUFBO0FBTzdEVCxZQUFBQSxHQVA2RDtBQVFuRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBVG1FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXJFOztBQUFBO0FBQUE7QUFBQTtBQUFBLEssQ0FZQTs7QUFFQSxrQkFBSyx5REFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQWdFLG1CQUFNZixDQUFOO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUN4RGtDLFlBQUFBLE1BRHdELEdBQy9DNUMsTUFBTTtBQUNuQkosY0FBQUEsTUFBTSxFQUFFRSxNQURXO0FBRW5CdUQsY0FBQUEsSUFBSSxFQUFFdkQsTUFGYTtBQUduQndELGNBQUFBLFVBQVUsRUFBRXhELE1BSE87QUFJbkJ5RCxjQUFBQSxLQUFLLEVBQUV6RCxNQUpZO0FBS25CMEQsY0FBQUEsVUFBVSxFQUFFMUQsTUFMTztBQU1uQjJELGNBQUFBLFNBQVMsRUFBRTNELE1BTlE7QUFPbkI0RCxjQUFBQSxnQkFBZ0IsRUFBRTVELE1BUEM7QUFRbkI2RCxjQUFBQSxTQUFTLEVBQUU3RCxNQVJRO0FBU25COEQsY0FBQUEsWUFBWSxFQUFFOUQsTUFUSztBQVVuQitELGNBQUFBLEtBQUssRUFBRS9ELE1BVlk7QUFXbkJnRSxjQUFBQSxPQUFPLEVBQUVoRSxNQVhVO0FBWW5CaUUsY0FBQUEsVUFBVSxFQUFFakUsTUFaTztBQWFuQmtFLGNBQUFBLGVBQWUsRUFBRWxFLE1BYkU7QUFjbkJtRSxjQUFBQSxTQUFTLEVBQUVuRSxNQWRRO0FBZW5Cb0UsY0FBQUEsSUFBSSxFQUFFcEUsTUFmYTtBQWdCbkJxRSxjQUFBQSxRQUFRLEVBQUVyRSxNQWhCUztBQWlCbkJzRSxjQUFBQSxPQUFPLEVBQUV0RSxNQWpCVTtBQWtCbkJ1RSxjQUFBQSxTQUFTLEVBQUV6RSxNQWxCUTtBQW1CbkIwRSxjQUFBQSxZQUFZLEVBQUVuRSxLQUFLLENBQUNILE1BQU0sQ0FBQyxFQUFELENBQVA7QUFuQkEsMEVBb0JERixNQXBCQyxvREFxQlhLLEtBQUssQ0FBQ0wsTUFBRCxDQXJCTSxZQUR5QztBQUFBO0FBQUEsbUJBd0I1Q1MsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhZ0QsUUFBYixDQUFzQixRQUF0QixDQXhCNEM7O0FBQUE7QUF3QnhENUMsWUFBQUEsR0F4QndEO0FBeUI5RGlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBMUI4RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFoRTs7QUFBQTtBQUFBO0FBQUE7QUFBQSxLLENBNkJBOztBQUVBLGtCQUFLLCtEQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBc0UsbUJBQU1mLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzlEa0MsWUFBQUEsTUFEOEQsR0FDckQ1QyxNQUFNLENBQUM7QUFDcEJ3RSxjQUFBQSxPQUFPLEVBQUUxRSxNQURXO0FBRXBCMkUsY0FBQUEsYUFBYSxFQUFFM0U7QUFGSyxhQUFELENBRCtDO0FBSzVEc0MsWUFBQUEsSUFMNEQsR0FLbkQ3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQUxtQyxDQUs1REYsSUFMNEQ7QUFBQTtBQUFBLG1CQU1sRDdCLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYW1ELGNBQWIsQ0FBNEJ0QyxJQUE1QixDQU5rRDs7QUFBQTtBQU05RFQsWUFBQUEsR0FOOEQ7QUFPcEVpQixZQUFBQSxNQUFNLENBQUNJLFlBQVAsQ0FBb0JyQixHQUFwQjtBQUNBakIsWUFBQUEsQ0FBQyxDQUFDZSxJQUFGOztBQVJvRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUF0RTs7QUFBQTtBQUFBO0FBQUE7QUFBQSxLLENBV0E7O0FBRUEsa0JBQUssNkRBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFvRSxtQkFBTWYsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDNURrQyxZQUFBQSxNQUQ0RCxHQUNuRDVDLE1BQU0sQ0FBQztBQUNwQjJFLGNBQUFBLFNBQVMsRUFBRTdFLE1BRFM7QUFFcEI4RSxjQUFBQSxPQUFPLEVBQUU5RSxNQUZXO0FBR3BCK0UsY0FBQUEsWUFBWSxFQUFFL0UsTUFITTtBQUlwQmdGLGNBQUFBLGVBQWUsRUFBRW5GLEdBQUcsQ0FBQ0csTUFBSixFQUpHO0FBSVc7QUFDL0JpRixjQUFBQSxlQUFlLEVBQUVqRixNQUxHO0FBTXBCa0YsY0FBQUEsSUFBSSxFQUFFbEYsTUFOYztBQU9wQm1GLGNBQUFBLGNBQWMsRUFBRW5GLE1BUEk7QUFRcEJvRixjQUFBQSxZQUFZLEVBQUVwRixNQVJNO0FBU3BCcUYsY0FBQUEsV0FBVyxFQUFFckYsTUFUTztBQVVwQnNGLGNBQUFBLFVBQVUsRUFBRXRGLE1BVlE7QUFXcEJ1RixjQUFBQSxNQUFNLEVBQUVoRixLQUFLLENBQUNFLFFBQVEsQ0FBQytFLFNBQVQsQ0FBbUJDLGdCQUFwQixDQVhPO0FBWXBCQyxjQUFBQSxjQUFjLEVBQUUxRixNQVpJO0FBYXBCMkYsY0FBQUEsYUFBYSxFQUFFM0YsTUFiSztBQWNwQjRGLGNBQUFBLG1CQUFtQixFQUFFNUY7QUFkRCxhQUFELENBRDZDO0FBaUIxRHNDLFlBQUFBLElBakIwRCxHQWlCakQ3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQWpCaUMsQ0FpQjFERixJQWpCMEQ7QUFBQTtBQUFBLG1CQWtCaEQ3QixRQUFRLENBQUNnQixHQUFULENBQWFvRSxZQUFiLENBQTBCLHdCQUExQixDQWxCZ0Q7O0FBQUE7QUFrQjVEaEUsWUFBQUEsR0FsQjREO0FBbUJsRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBcEJrRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFwRTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXVCQSxrQkFBSywwRUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQWlGLG1CQUFNZixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUN6RWtDLFlBQUFBLE1BRHlFLEdBQ2hFNUMsTUFBTSxDQUFDO0FBQ3BCNEYsY0FBQUEsRUFBRSxFQUFFOUYsTUFEZ0I7QUFFcEIrRixjQUFBQSxTQUFTLEVBQUUvRixNQUZTO0FBR3BCbUQsY0FBQUEsTUFBTSxFQUFFbkQsTUFIWTtBQUlwQjJGLGNBQUFBLGFBQWEsRUFBRTNGO0FBSkssYUFBRCxDQUQwRDtBQU92RXNDLFlBQUFBLElBUHVFLEdBTzlEN0IsUUFBUSxDQUFDOEIsTUFBVCxDQUFnQkMsU0FQOEMsQ0FPdkVGLElBUHVFO0FBQUE7QUFBQSxtQkFRN0Q3QixRQUFRLENBQUNnQixHQUFULENBQWF1RSx5QkFBYixDQUF1QzFELElBQXZDLEVBQTZDLEdBQTdDLENBUjZEOztBQUFBO0FBUXpFVCxZQUFBQSxHQVJ5RTtBQVMvRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBVitFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQWpGOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUEsa0JBQUssMkVBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFrRixtQkFBTWYsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDMUVrQyxZQUFBQSxNQUQwRSxHQUNqRXpDLEtBQUssQ0FDbEJILE1BQU0sQ0FBQztBQUNMNEYsY0FBQUEsRUFBRSxFQUFFOUYsTUFEQztBQUVMK0YsY0FBQUEsU0FBUyxFQUFFL0YsTUFGTjtBQUdMbUQsY0FBQUEsTUFBTSxFQUFFbkQsTUFISDtBQUlMMkYsY0FBQUEsYUFBYSxFQUFFM0Y7QUFKVixhQUFELENBRFksQ0FENEQ7QUFTeEVzQyxZQUFBQSxJQVR3RSxHQVMvRDdCLFFBQVEsQ0FBQzhCLE1BQVQsQ0FBZ0JDLFNBVCtDLENBU3hFRixJQVR3RTtBQUFBO0FBQUEsbUJBVTlEN0IsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhd0UsMEJBQWIsQ0FBd0MzRCxJQUF4QyxDQVY4RDs7QUFBQTtBQVUxRVQsWUFBQUEsR0FWMEU7QUFXaEZpQixZQUFBQSxNQUFNLENBQUNJLFlBQVAsQ0FBb0JyQixHQUFwQjtBQUNBakIsWUFBQUEsQ0FBQyxDQUFDZSxJQUFGOztBQVpnRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFsRjs7QUFBQTtBQUFBO0FBQUE7QUFBQSxLLENBZUE7O0FBRUEsa0JBQUssOERBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFxRSxtQkFBTWYsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDN0RrQyxZQUFBQSxNQUQ2RCxHQUNwRDVDLE1BQU0sQ0FBQztBQUNwQmdHLGNBQUFBLE1BQU0sRUFBRXRHLE9BRFk7QUFFcEJrRixjQUFBQSxPQUFPLEVBQUU5RSxNQUZXO0FBR3BCbUcsY0FBQUEsUUFBUSxFQUFFbkcsTUFIVTtBQUdGO0FBQ2xCb0csY0FBQUEsZUFBZSxFQUFFcEcsTUFKRztBQUtwQnFHLGNBQUFBLGVBQWUsRUFBRXJHLE1BTEc7QUFNcEJzRyxjQUFBQSxnQkFBZ0IsRUFBRXRHLE1BTkU7QUFNTTtBQUMxQnVHLGNBQUFBLGVBQWUsRUFBRXZHLE1BUEc7QUFPSztBQUN6QndHLGNBQUFBLHNCQUFzQixFQUFFeEcsTUFSSjtBQVNwQnlHLGNBQUFBLFNBQVMsRUFBRXpHLE1BVFM7QUFTRDtBQUNuQnVGLGNBQUFBLE1BQU0sRUFBRWhGLEtBQUssQ0FBQ0UsUUFBUSxDQUFDK0UsU0FBVCxDQUFtQmtCLGlCQUFwQjtBQVZPLGFBQUQsQ0FEOEM7QUFhM0RwRSxZQUFBQSxJQWIyRCxHQWFsRDdCLFFBQVEsQ0FBQzhCLE1BQVQsQ0FBZ0JDLFNBYmtDLENBYTNERixJQWIyRDtBQUFBO0FBQUEsbUJBY2pEN0IsUUFBUSxDQUFDZ0IsR0FBVCxDQUFha0YsYUFBYixDQUEyQnJFLElBQTNCLENBZGlEOztBQUFBO0FBYzdEVCxZQUFBQSxHQWQ2RDtBQWVuRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBaEJtRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFyRTs7QUFBQTtBQUFBO0FBQUE7QUFBQSxLLENBbUJBOztBQUVBLGtCQUFLLG9FQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBMkUsbUJBQU1mLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ25Fa0MsWUFBQUEsTUFEbUUsR0FDMUQ1QyxNQUFNLENBQUM7QUFDcEI0RixjQUFBQSxFQUFFLEVBQUU5RixNQURnQjtBQUVwQjRHLGNBQUFBLFdBQVcsRUFBRWhILE9BRk87QUFHcEJpSCxjQUFBQSxVQUFVLEVBQUU3RyxNQUhRO0FBSXBCOEcsY0FBQUEsb0JBQW9CLEVBQUU5RyxNQUpGO0FBS3BCK0csY0FBQUEsTUFBTSxFQUFFL0c7QUFMWSxhQUFELENBRG9EO0FBUWpFc0MsWUFBQUEsSUFSaUUsR0FReEQ3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQVJ3QyxDQVFqRUYsSUFSaUU7QUFBQTtBQUFBLG1CQVN2RDdCLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYXVGLG1CQUFiLENBQWlDMUUsSUFBakMsQ0FUdUQ7O0FBQUE7QUFTbkVULFlBQUFBLEdBVG1FO0FBVXpFaUIsWUFBQUEsTUFBTSxDQUFDSSxZQUFQLENBQW9CckIsR0FBcEI7QUFDQWpCLFlBQUFBLENBQUMsQ0FBQ2UsSUFBRjs7QUFYeUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBM0U7O0FBQUE7QUFBQTtBQUFBO0FBQUEsSyxDQWNBOztBQUVBLGtCQUFLLHVEQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFBOEQsbUJBQU1mLENBQU47QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUN0RGtDLFlBQUFBLE1BRHNELEdBQzdDNUMsTUFBTSxDQUFDO0FBQ3BCNEYsY0FBQUEsRUFBRSxFQUFFOUYsTUFEZ0I7QUFFcEJpSCxjQUFBQSxRQUFRLEVBQUVqSCxNQUZVO0FBR3BCa0gsY0FBQUEsa0JBQWtCLEVBQUU3RyxLQUFLLENBQ3ZCSCxNQUFNLENBQUM7QUFDTHFELGdCQUFBQSxJQUFJLEVBQUV2RCxNQUREO0FBRUxtSCxnQkFBQUEsSUFBSSxFQUFFbkgsTUFGRDtBQUdMb0gsZ0JBQUFBLE9BQU8sRUFBRXBILE1BSEo7QUFJTHFILGdCQUFBQSxTQUFTLEVBQUV2SCxNQUpOO0FBS0x3SCxnQkFBQUEsVUFBVSxFQUFFdEg7QUFMUCxlQUFELENBRGlCLENBSEw7QUFZcEJ1SCxjQUFBQSxVQUFVLEVBQUV2SCxNQVpRO0FBYXBCd0gsY0FBQUEsV0FBVyxFQUFFeEg7QUFiTyxhQUFELENBRHVDO0FBQUE7QUFBQSxtQkFnQjFDUyxRQUFRLENBQUNnQixHQUFULENBQWFnRyxPQUFiLEVBaEIwQzs7QUFBQTtBQWdCdEQ1RixZQUFBQSxHQWhCc0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQWlCNUNBLEdBakI0Qzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWlCakQxQixZQUFBQSxDQWpCaUQ7QUFBQTtBQUFBLG1CQWtCeENNLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYWlHLE1BQWIsQ0FBb0J2SCxDQUFDLENBQUMyRixFQUF0QixDQWxCd0M7O0FBQUE7QUFrQnBENkIsWUFBQUEsR0FsQm9EO0FBbUIxRDdFLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnlFLEdBQXBCOztBQW5CMEQ7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBOztBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFBQTtBQXFCNUQvRyxZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBckI0RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUE5RDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXdCQSxrQkFBSyw0RUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQW1GLG1CQUFNZixDQUFOO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDM0VnSCxZQUFBQSxLQUQyRSxHQUNuRSxDQUNaO0FBQ0VDLGNBQUFBLFlBQVksRUFBRSxnQkFEaEI7QUFFRUMsY0FBQUEsVUFBVSxFQUFFLHlCQUZkO0FBR0VDLGNBQUFBLFVBQVUsRUFBRTtBQUhkLGFBRFksRUFNWjtBQUNFRixjQUFBQSxZQUFZLEVBQUUsZUFEaEI7QUFFRUMsY0FBQUEsVUFBVSxFQUFFLFNBRmQ7QUFHRUMsY0FBQUEsVUFBVSxFQUFFLENBQUN0SCxRQUFRLENBQUM4QixNQUFULENBQWdCckIsU0FBaEIsQ0FBMEI4RyxjQUExQixDQUF5Q2xELE9BQTFDLEVBQW1ELEVBQW5EO0FBSGQsYUFOWSxFQVdaO0FBQ0UrQyxjQUFBQSxZQUFZLEVBQUUsUUFEaEI7QUFFRUMsY0FBQUEsVUFBVSxFQUFFLHFCQUZkO0FBR0VDLGNBQUFBLFVBQVUsRUFBRTtBQUhkLGFBWFksRUFnQlo7QUFDRUYsY0FBQUEsWUFBWSxFQUFFLGVBRGhCO0FBRUVDLGNBQUFBLFVBQVUsRUFBRSxpQkFGZDtBQUdFQyxjQUFBQSxVQUFVLEVBQUU7QUFIZCxhQWhCWSxDQURtRTtBQUFBLDZCQXVCakVILEtBdkJpRTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXVCdEV6SCxZQUFBQSxDQXZCc0U7QUFBQTtBQUFBLG1CQXdCN0RNLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYXdHLFdBQWIsQ0FDaEI5SCxDQUFDLENBQUMwSCxZQURjLEVBRWhCMUgsQ0FBQyxDQUFDMkgsVUFGYyxFQUdoQjNILENBQUMsQ0FBQzRILFVBSGMsQ0F4QjZEOztBQUFBO0FBd0J6RWxHLFlBQUFBLEdBeEJ5RTtBQTZCL0VqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU9oQyxNQUFNLENBQUNpQyxXQUFQLENBQW1CRixHQUFuQixDQUFQO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNrQixJQUFGLENBQU9ELEdBQUcsR0FBRyxDQUFiOztBQTlCK0U7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFnQ2pGakIsWUFBQUEsQ0FBQyxDQUFDZSxJQUFGOztBQWhDaUY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBbkY7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQ0Esa0JBQUssNERBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUFtRSxtQkFBTWYsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDM0RrQyxZQUFBQSxNQUQyRCxHQUNsRDVDLE1BQU0sQ0FBQztBQUNwQmdJLGNBQUFBLEtBQUssRUFBRWxJLE1BRGE7QUFFcEJtSSxjQUFBQSxnQkFBZ0IsRUFBRW5JLE1BRkU7QUFHcEJvSSxjQUFBQSwwQkFBMEIsRUFBRXBJLE1BSFI7QUFJcEJxSSxjQUFBQSxTQUFTLEVBQUVySTtBQUpTLGFBQUQsQ0FENEM7QUFPekRzQyxZQUFBQSxJQVB5RCxHQU9oRDdCLFFBQVEsQ0FBQzhCLE1BQVQsQ0FBZ0JDLFNBUGdDLENBT3pERixJQVB5RDtBQUFBO0FBQUEsbUJBUS9DN0IsUUFBUSxDQUFDZ0IsR0FBVCxDQUFhNkcsV0FBYixDQUF5QmhHLElBQXpCLENBUitDOztBQUFBO0FBUTNEVCxZQUFBQSxHQVIyRDtBQVNqRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBVmlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQW5FOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUEsa0JBQUsscUNBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQUE0QyxtQkFBTWYsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDcENrQyxZQUFBQSxNQURvQyxHQUMzQjVDLE1BQU0sQ0FBQztBQUNwQjRGLGNBQUFBLEVBQUUsRUFBRTlGLE1BRGdCO0FBRXBCaUgsY0FBQUEsUUFBUSxFQUFFakgsTUFGVTtBQUdwQmtILGNBQUFBLGtCQUFrQixFQUFFN0csS0FBSyxDQUN2QkgsTUFBTSxDQUFDO0FBQ0xpSCxnQkFBQUEsSUFBSSxFQUFFbkgsTUFERDtBQUVMb0gsZ0JBQUFBLE9BQU8sRUFBRXBILE1BRko7QUFHTHFILGdCQUFBQSxTQUFTLEVBQUV4SCxHQUFHLENBQ1hDLE1BRFEsR0FFUnlJLFFBRlEsR0FHUnhJLFFBSFEsRUFITjtBQU9MdUgsZ0JBQUFBLFVBQVUsRUFBRXRIO0FBUFAsZUFBRCxDQURpQixDQUhMO0FBY3BCd0gsY0FBQUEsV0FBVyxFQUFFeEg7QUFkTyxhQUFELENBRHFCO0FBaUJsQ3NDLFlBQUFBLElBakJrQyxHQWlCekI3QixRQUFRLENBQUM4QixNQUFULENBQWdCQyxTQWpCUyxDQWlCbENGLElBakJrQztBQUFBO0FBQUEsbUJBa0J4QjdCLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYWdHLE9BQWIsRUFsQndCOztBQUFBO0FBa0JwQzVGLFlBQUFBLEdBbEJvQztBQW1CMUNBLFlBQUFBLEdBQUcsQ0FBQzJHLE9BQUosQ0FBWSxVQUFBckksQ0FBQztBQUFBLHFCQUFJMkMsTUFBTSxDQUFDSSxZQUFQLENBQW9CL0MsQ0FBcEIsQ0FBSjtBQUFBLGFBQWI7QUFDQVMsWUFBQUEsQ0FBQyxDQUFDZSxJQUFGOztBQXBCMEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBNUM7O0FBQUE7QUFBQTtBQUFBO0FBQUEsSyxDQXVCQTs7QUFFQSxrQkFBSyw0REFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQW1FLG1CQUFNZixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUMzRGtDLFlBQUFBLE1BRDJELEdBQ2xENUMsTUFBTSxDQUFDO0FBQ3BCdUksY0FBQUEsTUFBTSxFQUFFN0ksT0FEWTtBQUVwQjhJLGNBQUFBLGdCQUFnQixFQUFFMUksTUFGRTtBQUdwQjJJLGNBQUFBLGdCQUFnQixFQUFFM0ksTUFIRTtBQUlwQjRJLGNBQUFBLGlCQUFpQixFQUFFNUksTUFKQztBQUtwQjZJLGNBQUFBLHFCQUFxQixFQUFFN0ksTUFMSDtBQU1wQjhJLGNBQUFBLHVCQUF1QixFQUFFOUk7QUFOTCxhQUFELENBRDRDO0FBQUE7QUFBQSxtQkFTL0NTLFFBQVEsQ0FBQ2dCLEdBQVQsQ0FBYXNILFdBQWIsRUFUK0M7O0FBQUE7QUFTM0RsSCxZQUFBQSxHQVQyRDtBQVVqRWlCLFlBQUFBLE1BQU0sQ0FBQ0ksWUFBUCxDQUFvQnJCLEdBQXBCO0FBQ0FqQixZQUFBQSxDQUFDLENBQUNlLElBQUY7O0FBWGlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQW5FOztBQUFBO0FBQUE7QUFBQTtBQUFBLEssQ0FjQTs7QUFFQSxrQkFBSyxtQ0FBTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQTBDLG1CQUFNZixDQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ3RCSCxRQUFRLENBQUNnQixHQUFULENBQWF1SCxvQkFBYixFQURzQjs7QUFBQTtBQUNsQ25ILFlBQUFBLEdBRGtDO0FBRXhDakIsWUFBQUEsQ0FBQyxDQUFDa0IsSUFBRixDQUFPOUIsTUFBTSxDQUFDK0IsV0FBUCxDQUFtQkYsR0FBbkIsQ0FBUDs7QUFGd0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBMUM7O0FBQUE7QUFBQTtBQUFBO0FBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgdGVzdCBmcm9tICdhdmEnXG5pbXBvcnQgKiBhcyB5dXAgZnJvbSAneXVwJ1xuaW1wb3J0IExpdmVwZWVyLCB7IHV0aWxzLCBpbml0Q29udHJhY3RzIH0gZnJvbSAnLi9pbmRleCdcbmltcG9ydCB7IEFzeW5jU3ViamVjdCB9IGZyb20gJ3J4anMnXG5cbi8vIGNsZWFycyBjb25zb2xlXG4vLyBjb25zb2xlLmxvZygnXFx4MUJjJylcblxuLyoqXG4gKiBUeXBlIHZhbGlkYXRvcnNcbiAqL1xuXG5jb25zdCBib29sZWFuID0geXVwLmJvb2xlYW4oKVxuY29uc3QgbnVtYmVyID0geXVwXG4gIC5udW1iZXIoKVxuICAucmVxdWlyZWQoKVxuICAucmVxdWlyZWQoKVxuY29uc3Qgc3RyaW5nID0geXVwXG4gIC5zdHJpbmcoKVxuICAuc3RyaWN0KClcbiAgLnJlcXVpcmVkKClcbmNvbnN0IG9iamVjdCA9IHggPT4geXVwLm9iamVjdCgpLnNoYXBlKHgpXG5jb25zdCBhcnJheSA9IHggPT4geXVwLmFycmF5KCkub2YoeClcbmNvbnN0IG9uZU9mID0geCA9PiB5dXAubWl4ZWQoKS5vbmVPZih4KVxuXG4vKipcbiAqIFByZS10ZXN0XG4gKi9cblxubGV0IGxpdmVwZWVyXG5cbnRlc3QuYmVmb3JlRWFjaChhc3luYyB0ID0+IHtcbiAgLy8gY3JlYXRlIG5ldyBzZGsgaW5zdGFuY2UgYmVmb3JlIGVhY2ggdGVzdFxuICBsaXZlcGVlciA9IGF3YWl0IExpdmVwZWVyKClcbn0pXG5cbi8qKlxuICogUnVuIHRlc3RzXG4gKi9cblxuLy8gdXRpbHNcblxudGVzdCgnc2hvdWxkIHNlcmlhbGl6ZSBwcm9maWxlcycsIHQgPT4ge1xuICBjb25zdCBhID0gdXRpbHMuc2VyaWFsaXplVHJhbnNjb2RpbmdQcm9maWxlcyhbJ1A3MjBwNjBmcHMxNng5J10pXG4gIGNvbnN0IGIgPSB1dGlscy5zZXJpYWxpemVUcmFuc2NvZGluZ1Byb2ZpbGVzKFsnZm9vJ10pXG4gIHQuaXMoJ2E3YWMxMzdhJywgYSlcbiAgdC5pcygnZDQzNWM1M2EnLCBiKVxufSlcblxuLy8gc2RrIHNuYXBzaG90c1xuXG50ZXN0KCdzaG91bGQgaW5pdGlhbGl6ZSBjb250cmFjdHMnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgeyBjb250cmFjdHMgfSA9IGF3YWl0IGluaXRDb250cmFjdHMoKVxuICBjb25zdCBzbmFwID0gT2JqZWN0LmtleXMoY29udHJhY3RzKS5yZWR1Y2UoKGEsIGIpID0+IHtcbiAgICBjb25zdCBjID0ge1xuICAgICAgLi4uYSxcbiAgICB9XG4gICAgLy8gY29uc3RhbnRseSBjaGFuZ2VzLCBzbyBzZXQgdG8gMCBmb3Igc25hcHNob3QgcHVycG9zZXNcbiAgICBjW2JdLnF1ZXJ5LnJwYy5pZENvdW50ZXIgPSAwXG4gICAgcmV0dXJuIGNcbiAgfSwgY29udHJhY3RzKVxuICAvLyBUT0RPOiB1bnNraXAgb25jZSBjb250cmFjdCBzY2hlbWEgaXMgc3RhYmxlXG4gIC8vIHQuc2tpcC5zbmFwc2hvdChzbmFwKVxuICB0LnBhc3MoKVxufSlcblxudGVzdCgnc2hvdWxkIGluaXRpYWxpemUgU0RLJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHsgcnBjIH0gPSBhd2FpdCBMaXZlcGVlcigpXG4gIC8vIFRPRE86IHVuc2tpcCBvbmNlIHNkayBycGMgaXMgc3RhYmxlXG4gIC8vIHQuc2tpcC5zbmFwc2hvdChycGMpXG4gIHQucGFzcygpXG59KVxuXG4vLyBCb25kaW5nIE1hbmFnZXJcblxudGVzdCgnc2hvdWxkIGdldCB1bmJvbmRpbmcgcGVyaW9kJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRVbmJvbmRpbmdQZXJpb2QoKVxuICB0LnRydWUoc3RyaW5nLmlzVmFsaWRTeW5jKHJlcykpXG59KVxuXG50ZXN0KCdzaG91bGQgZ2V0IG51bWJlciBhY3RpdmUgdHJhbnNjb2RlcnMnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldE51bUFjdGl2ZVRyYW5zY29kZXJzKClcbiAgdC50cnVlKHN0cmluZy5pc1ZhbGlkU3luYyhyZXMpKVxufSlcblxudGVzdCgnc2hvdWxkIGdldCBtYXhpbXVtIGVhcm5pbmcgZm9yIGNsYWltcyByb3VuZHMnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldE1heEVhcm5pbmdzQ2xhaW1zUm91bmRzKClcbiAgdC50cnVlKHN0cmluZy5pc1ZhbGlkU3luYyhyZXMpKVxufSlcblxudGVzdCgnc2hvdWxkIGdldCB0b3RhbCBib25kZWQnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldFRvdGFsQm9uZGVkKClcbiAgdC50cnVlKHN0cmluZy5pc1ZhbGlkU3luYyhyZXMpKVxufSlcblxudGVzdCgnc2hvdWxkIGdldCB0cmFuc2NvZGVyIHBvb2wgbWF4IHNpemUnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldFRyYW5zY29kZXJQb29sTWF4U2l6ZSgpXG4gIHQudHJ1ZShzdHJpbmcuaXNWYWxpZFN5bmMocmVzKSlcbn0pXG5cbi8vIEVOU1xuXG50ZXN0KCdzaG91bGQgZ2V0IEVOUyBmb3IgRVRIIGFkZHJlc3MnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldEVOU05hbWUoXG4gICAgJzB4OTZiMjBmNjczMDlhMDc1MGIzZmMzZGNiZTk4OWYzNDcxNjc0ODJmZicsXG4gIClcbiAgdC50cnVlKHN0cmluZy5pc1ZhbGlkU3luYyhyZXMpKVxufSlcblxudGVzdCgnc2hvdWxkIGdldCBlbXB0eSBzdHJpbmcgd2hlbiBubyBFTlMgbmFtZSBhZGRyZXNzJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRFTlNOYW1lKFxuICAgICcweDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAnLFxuICApXG4gIHQudHJ1ZSgnJyA9PT0gcmVzKVxufSlcblxudGVzdCgnc2hvdWxkIGdldCBFVEggYWRkcmVzcyBmb3IgRU5TIG5hbWUnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldEVOU0FkZHJlc3MoJ3BsZWFzZS5idXltZWNvZmZlZS5ldGgnKVxuICB0LnRydWUoc3RyaW5nLmlzVmFsaWRTeW5jKHJlcykpXG59KVxuXG50ZXN0KCdzaG91bGQgZ2V0IGVtcHR5IHN0cmluZyBmb3Igbm9uZXhpc3RlbnQgRU5TIG5hbWUnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldEVOU0FkZHJlc3MoJ2Rvbm90LmJ1eW1lY29mZmVlLmV0aCcpXG4gIHQudHJ1ZSgnJyA9PT0gcmVzKVxufSlcblxuLy8gRVRIXG5cbnRlc3QoJ3Nob3VsZCBnZXQgRVRIIGJhbGFuY2UnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgeyBmcm9tIH0gPSBsaXZlcGVlci5jb25maWcuZGVmYXVsdFR4XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRFdGhCYWxhbmNlKGZyb20pXG4gIHQudHJ1ZShzdHJpbmcuaXNWYWxpZFN5bmMocmVzKSlcbn0pXG5cbi8vIE1pbnRlclxuXG50ZXN0KCdzaG91bGQgcmV0dXJuIGEgbnVtYmVyIGZyb20gZ2V0SW5mbGF0aW9uKCknLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldEluZmxhdGlvbigpXG4gIHQudHJ1ZShzdHJpbmcuaXNWYWxpZFN5bmMocmVzKSlcbn0pXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gYSBudW1iZXIgZnJvbSBnZXRJbmZsYXRpb25DaGFuZ2UoKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZ2V0SW5mbGF0aW9uQ2hhbmdlKClcbiAgdC50cnVlKHN0cmluZy5pc1ZhbGlkU3luYyhyZXMpKVxufSlcblxuLy8gVG9rZW5cblxudGVzdCgnc2hvdWxkIHJldHVybiBhIG51bWJlciBmcm9tIGdldFRva2VuVG90YWxTdXBwbHkoKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZ2V0VG9rZW5Ub3RhbFN1cHBseSgpXG4gIHQudHJ1ZShzdHJpbmcuaXNWYWxpZFN5bmMocmVzKSlcbn0pXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gYSBudW1iZXIgZnJvbSBnZXRUb2tlbkJhbGFuY2UoKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCB7IGZyb20gfSA9IGxpdmVwZWVyLmNvbmZpZy5kZWZhdWx0VHhcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldFRva2VuQmFsYW5jZShmcm9tKVxuICB0LnRydWUoc3RyaW5nLmlzVmFsaWRTeW5jKHJlcykpXG59KVxuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRUb2tlbkluZm8oKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCBzY2hlbWEgPSBvYmplY3Qoe1xuICAgIHRvdGFsU3VwcGx5OiBzdHJpbmcsXG4gICAgYmFsYW5jZTogc3RyaW5nLFxuICB9KVxuICBjb25zdCB7IGZyb20gfSA9IGxpdmVwZWVyLmNvbmZpZy5kZWZhdWx0VHhcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldFRva2VuSW5mbyhmcm9tKVxuICBzY2hlbWEudmFsaWRhdGVTeW5jKHJlcylcbiAgdC5wYXNzKClcbn0pXG5cbi8vIEZhdWNldFxuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRGYXVjZXRJbmZvKCknLCBhc3luYyB0ID0+IHtcbiAgY29uc3Qgc2NoZW1hID0gb2JqZWN0KHtcbiAgICBhbW91bnQ6IHN0cmluZyxcbiAgICB3YWl0OiBzdHJpbmcsXG4gICAgbmV4dDogc3RyaW5nLFxuICB9KVxuICBjb25zdCB7IGZyb20gfSA9IGxpdmVwZWVyLmNvbmZpZy5kZWZhdWx0VHhcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldEZhdWNldEluZm8oZnJvbSlcbiAgc2NoZW1hLnZhbGlkYXRlU3luYyhyZXMpXG4gIHQucGFzcygpXG59KVxuXG4vLyBCbG9ja1xuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRCbG9jaygpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgbnVtYmVyOiBzdHJpbmcsXG4gICAgaGFzaDogc3RyaW5nLFxuICAgIHBhcmVudEhhc2g6IHN0cmluZyxcbiAgICBub25jZTogc3RyaW5nLFxuICAgIHNoYTNVbmNsZXM6IHN0cmluZyxcbiAgICBsb2dzQmxvb206IHN0cmluZyxcbiAgICB0cmFuc2FjdGlvbnNSb290OiBzdHJpbmcsXG4gICAgc3RhdGVSb290OiBzdHJpbmcsXG4gICAgcmVjZWlwdHNSb290OiBzdHJpbmcsXG4gICAgbWluZXI6IHN0cmluZyxcbiAgICBtaXhIYXNoOiBzdHJpbmcsXG4gICAgZGlmZmljdWx0eTogc3RyaW5nLFxuICAgIHRvdGFsRGlmZmljdWx0eTogc3RyaW5nLFxuICAgIGV4dHJhRGF0YTogc3RyaW5nLFxuICAgIHNpemU6IHN0cmluZyxcbiAgICBnYXNMaW1pdDogc3RyaW5nLFxuICAgIGdhc1VzZWQ6IHN0cmluZyxcbiAgICB0aW1lc3RhbXA6IG51bWJlcixcbiAgICB0cmFuc2FjdGlvbnM6IGFycmF5KG9iamVjdCh7fSkpLFxuICAgIHRyYW5zYWN0aW9uc1Jvb3Q6IHN0cmluZyxcbiAgICB1bmNsZXM6IGFycmF5KHN0cmluZyksXG4gIH0pXG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRCbG9jaygnbGF0ZXN0JylcbiAgc2NoZW1hLnZhbGlkYXRlU3luYyhyZXMpXG4gIHQucGFzcygpXG59KVxuXG4vLyBCcm9hZGNhc3RlclxuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRCcm9hZGNhc3RlcigpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgZGVwb3NpdDogc3RyaW5nLFxuICAgIHdpdGhkcmF3QmxvY2s6IHN0cmluZyxcbiAgfSlcbiAgY29uc3QgeyBmcm9tIH0gPSBsaXZlcGVlci5jb25maWcuZGVmYXVsdFR4XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRCcm9hZGNhc3Rlcihmcm9tKVxuICBzY2hlbWEudmFsaWRhdGVTeW5jKHJlcylcbiAgdC5wYXNzKClcbn0pXG5cbi8vIERlbGdhdG9yXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gb2JqZWN0IHdpdGggY29ycmVjdCBzaGFwZSBmcm9tIGdldERlbGVnYXRvcigpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgYWxsb3dhbmNlOiBzdHJpbmcsXG4gICAgYWRkcmVzczogc3RyaW5nLFxuICAgIGJvbmRlZEFtb3VudDogc3RyaW5nLFxuICAgIGRlbGVnYXRlQWRkcmVzczogeXVwLnN0cmluZygpLCAvLyBjYW4gYmUgZW1wdHkgKCcnKVxuICAgIGRlbGVnYXRlZEFtb3VudDogc3RyaW5nLFxuICAgIGZlZXM6IHN0cmluZyxcbiAgICBsYXN0Q2xhaW1Sb3VuZDogc3RyaW5nLFxuICAgIHBlbmRpbmdTdGFrZTogc3RyaW5nLFxuICAgIHBlbmRpbmdGZWVzOiBzdHJpbmcsXG4gICAgc3RhcnRSb3VuZDogc3RyaW5nLFxuICAgIHN0YXR1czogb25lT2YobGl2ZXBlZXIuY29uc3RhbnRzLkRFTEVHQVRPUl9TVEFUVVMpLFxuICAgIHdpdGhkcmF3QW1vdW50OiBzdHJpbmcsXG4gICAgd2l0aGRyYXdSb3VuZDogc3RyaW5nLFxuICAgIG5leHRVbmJvbmRpbmdMb2NrSWQ6IHN0cmluZyxcbiAgfSlcbiAgY29uc3QgeyBmcm9tIH0gPSBsaXZlcGVlci5jb25maWcuZGVmYXVsdFR4XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXREZWxlZ2F0b3IoJ3BsZWFzZS5idXltZWNvZmZlZS5ldGgnKVxuICBzY2hlbWEudmFsaWRhdGVTeW5jKHJlcylcbiAgdC5wYXNzKClcbn0pXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gb2JqZWN0IHdpdGggY29ycmVjdCBzaGFwZSBmcm9tIGdldERlbGVnYXRvclVuYm9uZGluZ0xvY2soKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCBzY2hlbWEgPSBvYmplY3Qoe1xuICAgIGlkOiBzdHJpbmcsXG4gICAgZGVsZWdhdG9yOiBzdHJpbmcsXG4gICAgYW1vdW50OiBzdHJpbmcsXG4gICAgd2l0aGRyYXdSb3VuZDogc3RyaW5nLFxuICB9KVxuICBjb25zdCB7IGZyb20gfSA9IGxpdmVwZWVyLmNvbmZpZy5kZWZhdWx0VHhcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldERlbGVnYXRvclVuYm9uZGluZ0xvY2soZnJvbSwgJzAnKVxuICBzY2hlbWEudmFsaWRhdGVTeW5jKHJlcylcbiAgdC5wYXNzKClcbn0pXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gb2JqZWN0IHdpdGggY29ycmVjdCBzaGFwZSBmcm9tIGdldERlbGVnYXRvclVuYm9uZGluZ0xvY2tzKCknLCBhc3luYyB0ID0+IHtcbiAgY29uc3Qgc2NoZW1hID0gYXJyYXkoXG4gICAgb2JqZWN0KHtcbiAgICAgIGlkOiBzdHJpbmcsXG4gICAgICBkZWxlZ2F0b3I6IHN0cmluZyxcbiAgICAgIGFtb3VudDogc3RyaW5nLFxuICAgICAgd2l0aGRyYXdSb3VuZDogc3RyaW5nLFxuICAgIH0pLFxuICApXG4gIGNvbnN0IHsgZnJvbSB9ID0gbGl2ZXBlZXIuY29uZmlnLmRlZmF1bHRUeFxuICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9ja3MoZnJvbSlcbiAgc2NoZW1hLnZhbGlkYXRlU3luYyhyZXMpXG4gIHQucGFzcygpXG59KVxuXG4vLyBUcmFuc2NvZGVyXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gb2JqZWN0IHdpdGggY29ycmVjdCBzaGFwZSBmcm9tIGdldFRyYW5zY29kZXIoKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCBzY2hlbWEgPSBvYmplY3Qoe1xuICAgIGFjdGl2ZTogYm9vbGVhbixcbiAgICBhZGRyZXNzOiBzdHJpbmcsXG4gICAgZmVlU2hhcmU6IHN0cmluZywgLy8gJVxuICAgIGxhc3RSZXdhcmRSb3VuZDogc3RyaW5nLFxuICAgIHByaWNlUGVyU2VnbWVudDogc3RyaW5nLFxuICAgIHBlbmRpbmdSZXdhcmRDdXQ6IHN0cmluZywgLy8gJVxuICAgIHBlbmRpbmdGZWVTaGFyZTogc3RyaW5nLCAvLyAlXG4gICAgcGVuZGluZ1ByaWNlUGVyU2VnbWVudDogc3RyaW5nLFxuICAgIHJld2FyZEN1dDogc3RyaW5nLCAvLyAlXG4gICAgc3RhdHVzOiBvbmVPZihsaXZlcGVlci5jb25zdGFudHMuVFJBTlNDT0RFUl9TVEFUVVMpLFxuICB9KVxuICBjb25zdCB7IGZyb20gfSA9IGxpdmVwZWVyLmNvbmZpZy5kZWZhdWx0VHhcbiAgY29uc3QgcmVzID0gYXdhaXQgbGl2ZXBlZXIucnBjLmdldFRyYW5zY29kZXIoZnJvbSlcbiAgc2NoZW1hLnZhbGlkYXRlU3luYyhyZXMpXG4gIHQucGFzcygpXG59KVxuXG4vLyBSb3VuZHNcblxudGVzdCgnc2hvdWxkIHJldHVybiBvYmplY3Qgd2l0aCBjb3JyZWN0IHNoYXBlIGZyb20gZ2V0Q3VycmVudFJvdW5kSW5mbygpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgaWQ6IHN0cmluZyxcbiAgICBpbml0aWFsaXplZDogYm9vbGVhbixcbiAgICBzdGFydEJsb2NrOiBzdHJpbmcsXG4gICAgbGFzdEluaXRpYWxpemVkUm91bmQ6IHN0cmluZyxcbiAgICBsZW5ndGg6IHN0cmluZyxcbiAgfSlcbiAgY29uc3QgeyBmcm9tIH0gPSBsaXZlcGVlci5jb25maWcuZGVmYXVsdFR4XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRDdXJyZW50Um91bmRJbmZvKGZyb20pXG4gIHNjaGVtYS52YWxpZGF0ZVN5bmMocmVzKVxuICB0LnBhc3MoKVxufSlcblxuLy8gSm9ic1xuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRKb2IoKScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCBzY2hlbWEgPSBvYmplY3Qoe1xuICAgIGlkOiBzdHJpbmcsXG4gICAgc3RyZWFtSWQ6IHN0cmluZyxcbiAgICB0cmFuc2NvZGluZ09wdGlvbnM6IGFycmF5KFxuICAgICAgb2JqZWN0KHtcbiAgICAgICAgaGFzaDogc3RyaW5nLFxuICAgICAgICBuYW1lOiBzdHJpbmcsXG4gICAgICAgIGJpdHJhdGU6IHN0cmluZyxcbiAgICAgICAgZnJhbWVyYXRlOiBudW1iZXIsXG4gICAgICAgIHJlc29sdXRpb246IHN0cmluZyxcbiAgICAgIH0pLFxuICAgICksXG4gICAgdHJhbnNjb2Rlcjogc3RyaW5nLFxuICAgIGJyb2FkY2FzdGVyOiBzdHJpbmcsXG4gIH0pXG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRKb2JzKClcbiAgZm9yIChjb25zdCB4IG9mIHJlcykge1xuICAgIGNvbnN0IGpvYiA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRKb2IoeC5pZClcbiAgICBzY2hlbWEudmFsaWRhdGVTeW5jKGpvYilcbiAgfVxuICB0LnBhc3MoKVxufSlcblxudGVzdCgnc2hvdWxkIHJldHVybiBudW1iZXIgdGhhdCBzaWduaWZpZXMgdGhlIGVzdGltYXRlZCBhbW91bnQgb2YgZ2FzIHRvIGJlIHVzZWQnLCBhc3luYyB0ID0+IHtcbiAgY29uc3QgY2FzZXMgPSBbXG4gICAge1xuICAgICAgY29udHJhY3ROYW1lOiAnQm9uZGluZ01hbmFnZXInLFxuICAgICAgbWV0aG9kTmFtZTogJ21heEVhcm5pbmdzQ2xhaW1zUm91bmRzJyxcbiAgICAgIG1ldGhvZEFyZ3M6IFtdLFxuICAgIH0sXG4gICAge1xuICAgICAgY29udHJhY3ROYW1lOiAnTGl2ZXBlZXJUb2tlbicsXG4gICAgICBtZXRob2ROYW1lOiAnYXBwcm92ZScsXG4gICAgICBtZXRob2RBcmdzOiBbbGl2ZXBlZXIuY29uZmlnLmNvbnRyYWN0cy5Cb25kaW5nTWFuYWdlci5hZGRyZXNzLCAxMF0sXG4gICAgfSxcbiAgICB7XG4gICAgICBjb250cmFjdE5hbWU6ICdNaW50ZXInLFxuICAgICAgbWV0aG9kTmFtZTogJ2N1cnJlbnRNaW50ZWRUb2tlbnMnLFxuICAgICAgbWV0aG9kQXJnczogW10sXG4gICAgfSxcbiAgICB7XG4gICAgICBjb250cmFjdE5hbWU6ICdMaXZlcGVlclRva2VuJyxcbiAgICAgIG1ldGhvZE5hbWU6ICdtaW50aW5nRmluaXNoZWQnLFxuICAgICAgbWV0aG9kQXJnczogW10sXG4gICAgfSxcbiAgXVxuICBmb3IgKGNvbnN0IHggb2YgY2FzZXMpIHtcbiAgICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZXN0aW1hdGVHYXMoXG4gICAgICB4LmNvbnRyYWN0TmFtZSxcbiAgICAgIHgubWV0aG9kTmFtZSxcbiAgICAgIHgubWV0aG9kQXJncyxcbiAgICApXG4gICAgdC50cnVlKG51bWJlci5pc1ZhbGlkU3luYyhyZXMpKVxuICAgIHQudHJ1ZShyZXMgPiAwKVxuICB9XG4gIHQucGFzcygpXG59KVxuXG50ZXN0KCdzaG91bGQgcmV0dXJuIG9iamVjdCB3aXRoIGNvcnJlY3Qgc2hhcGUgZnJvbSBnZXRKb2JzSW5mbygpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgdG90YWw6IHN0cmluZyxcbiAgICB2ZXJpZmljYXRpb25SYXRlOiBzdHJpbmcsXG4gICAgdmVyaWZpY2F0aW9uU2xhc2hpbmdQZXJpb2Q6IHN0cmluZyxcbiAgICBmaW5kZXJGZWU6IHN0cmluZyxcbiAgfSlcbiAgY29uc3QgeyBmcm9tIH0gPSBsaXZlcGVlci5jb25maWcuZGVmYXVsdFR4XG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRKb2JzSW5mbyhmcm9tKVxuICBzY2hlbWEudmFsaWRhdGVTeW5jKHJlcylcbiAgdC5wYXNzKClcbn0pXG5cbnRlc3QoJ3Nob3VsZCBnZXQgbWFueSBqb2JzIGZyb20gZ2V0Sm9icygpJywgYXN5bmMgdCA9PiB7XG4gIGNvbnN0IHNjaGVtYSA9IG9iamVjdCh7XG4gICAgaWQ6IHN0cmluZyxcbiAgICBzdHJlYW1JZDogc3RyaW5nLFxuICAgIHRyYW5zY29kaW5nT3B0aW9uczogYXJyYXkoXG4gICAgICBvYmplY3Qoe1xuICAgICAgICBuYW1lOiBzdHJpbmcsXG4gICAgICAgIGJpdHJhdGU6IHN0cmluZyxcbiAgICAgICAgZnJhbWVyYXRlOiB5dXBcbiAgICAgICAgICAubnVtYmVyKClcbiAgICAgICAgICAucG9zaXRpdmUoKVxuICAgICAgICAgIC5yZXF1aXJlZCgpLFxuICAgICAgICByZXNvbHV0aW9uOiBzdHJpbmcsXG4gICAgICB9KSxcbiAgICApLFxuICAgIGJyb2FkY2FzdGVyOiBzdHJpbmcsXG4gIH0pXG4gIGNvbnN0IHsgZnJvbSB9ID0gbGl2ZXBlZXIuY29uZmlnLmRlZmF1bHRUeFxuICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZ2V0Sm9icygpXG4gIHJlcy5mb3JFYWNoKHggPT4gc2NoZW1hLnZhbGlkYXRlU3luYyh4KSlcbiAgdC5wYXNzKClcbn0pXG5cbi8vIFByb3RvY29sXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gb2JqZWN0IHdpdGggY29ycmVjdCBzaGFwZSBmcm9tIGdldFByb3RvY29sKCknLCBhc3luYyB0ID0+IHtcbiAgY29uc3Qgc2NoZW1hID0gb2JqZWN0KHtcbiAgICBwYXVzZWQ6IGJvb2xlYW4sXG4gICAgdG90YWxUb2tlblN1cHBseTogc3RyaW5nLFxuICAgIHRvdGFsQm9uZGVkVG9rZW46IHN0cmluZyxcbiAgICB0YXJnZXRCb25kaW5nUmF0ZTogc3RyaW5nLFxuICAgIHRyYW5zY29kZXJQb29sTWF4U2l6ZTogc3RyaW5nLFxuICAgIG1heEVhcm5pbmdzQ2xhaW1zUm91bmRzOiBzdHJpbmcsXG4gIH0pXG4gIGNvbnN0IHJlcyA9IGF3YWl0IGxpdmVwZWVyLnJwYy5nZXRQcm90b2NvbCgpXG4gIHNjaGVtYS52YWxpZGF0ZVN5bmMocmVzKVxuICB0LnBhc3MoKVxufSlcblxuLy8gTWludGVyXG5cbnRlc3QoJ3Nob3VsZCByZXR1cm4gdGFyZ2V0IGJvbmRpbmcgcmF0ZScsIGFzeW5jIHQgPT4ge1xuICBjb25zdCByZXMgPSBhd2FpdCBsaXZlcGVlci5ycGMuZ2V0VGFyZ2V0Qm9uZGluZ1JhdGUoKVxuICB0LnRydWUoc3RyaW5nLmlzVmFsaWRTeW5jKHJlcykpXG59KVxuIl19