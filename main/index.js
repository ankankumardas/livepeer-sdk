"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deployContract = deployContract;
exports.getContractAt = getContractAt;
exports.initRPC = initRPC;
exports.initContracts = initContracts;
exports.default = exports.LivepeerSDK = exports.createLivepeerSDK = createLivepeerSDK;
exports.utils = exports.DEFAULTS = exports.TRANSCODER_STATUS = exports.DELEGATOR_STATUS = exports.VIDEO_PROFILES = exports.VIDEO_PROFILE_ID_SIZE = exports.ADDRESS_PAD = exports.EMPTY_ADDRESS = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectSpread4 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _ethjs = _interopRequireDefault(require("ethjs"));

var _ethjsProviderSigner = _interopRequireDefault(require("ethjs-provider-signer"));

var _ethereumjsTx = _interopRequireDefault(require("ethereumjs-tx"));

var _ethjsAbi = require("ethjs-abi");

var _ethjsEns = _interopRequireDefault(require("ethjs-ens"));

var _LivepeerToken = _interopRequireDefault(require("../etc/LivepeerToken"));

var _LivepeerTokenFaucet = _interopRequireDefault(require("../etc/LivepeerTokenFaucet"));

var _Controller = _interopRequireDefault(require("../etc/Controller"));

var _JobsManager = _interopRequireDefault(require("../etc/JobsManager"));

var _RoundsManager = _interopRequireDefault(require("../etc/RoundsManager"));

var _BondingManager = _interopRequireDefault(require("../etc/BondingManager"));

var _Minter = _interopRequireDefault(require("../etc/Minter"));

// Constants
var EMPTY_ADDRESS = '0x0000000000000000000000000000000000000000';
exports.EMPTY_ADDRESS = EMPTY_ADDRESS;
var ADDRESS_PAD = '0x000000000000000000000000';
exports.ADDRESS_PAD = ADDRESS_PAD;
var VIDEO_PROFILE_ID_SIZE = 8;
exports.VIDEO_PROFILE_ID_SIZE = VIDEO_PROFILE_ID_SIZE;
var VIDEO_PROFILES = {
  P720p60fps16x9: {
    hash: 'a7ac137a',
    name: 'P720p60fps16x9',
    bitrate: '6000k',
    framerate: 60,
    resolution: '1280x720'
  },
  P720p30fps16x9: {
    hash: '49d54ea9',
    name: 'P720p30fps16x9',
    bitrate: '4000k',
    framerate: 30,
    resolution: '1280x720'
  },
  P720p30fps4x3: {
    hash: '79332fe7',
    name: 'P720p30fps4x3',
    bitrate: '3500k',
    framerate: 30,
    resolution: '960x720'
  },
  P576p30fps16x9: {
    hash: '5ecf4b52',
    name: 'P576p30fps16x9',
    bitrate: '1500k',
    framerate: 30,
    resolution: '1024x576'
  },
  P360p30fps16x9: {
    hash: '93c717e7',
    name: 'P360p30fps16x9',
    bitrate: '1200k',
    framerate: 30,
    resolution: '640x360'
  },
  P360p30fps4x3: {
    hash: 'b60382a0',
    name: 'P360p30fps4x3',
    bitrate: '1000k',
    framerate: 30,
    resolution: '480x360'
  },
  P240p30fps16x9: {
    hash: 'c0a6517a',
    name: 'P240p30fps16x9',
    bitrate: '600k',
    framerate: 30,
    resolution: '426x240'
  },
  P240p30fps4x3: {
    hash: 'd435c53a',
    name: 'P240p30fps4x3',
    bitrate: '600k',
    framerate: 30,
    resolution: '320x240'
  },
  P144p30fps16x9: {
    hash: 'fca40bf9',
    name: 'P144p30fps16x9',
    bitrate: '400k',
    framerate: 30,
    resolution: '256x144'
  }
};
exports.VIDEO_PROFILES = VIDEO_PROFILES;
var DELEGATOR_STATUS = ['Pending', 'Bonded', 'Unbonded', 'Unbonding'];
exports.DELEGATOR_STATUS = DELEGATOR_STATUS;
DELEGATOR_STATUS.Pending = DELEGATOR_STATUS[0];
DELEGATOR_STATUS.Bonded = DELEGATOR_STATUS[1];
DELEGATOR_STATUS.Unbonded = DELEGATOR_STATUS[2];
DELEGATOR_STATUS.Unbonding = DELEGATOR_STATUS[3];
var TRANSCODER_STATUS = ['NotRegistered', 'Registered'];
exports.TRANSCODER_STATUS = TRANSCODER_STATUS;
TRANSCODER_STATUS.NotRegistered = TRANSCODER_STATUS[0];
TRANSCODER_STATUS.Registered = TRANSCODER_STATUS[1];
// Defaults
var DEFAULTS = {
  controllerAddress: '0xf96d54e490317c557a967abfa5d6e33006be69b3',
  provider: 'https://mainnet.infura.io/v3/e9dc54dbd8de4664890e641a8efa45b1',
  privateKeys: {},
  // { [publicKey: string]: privateKey }
  account: '',
  gas: 0,
  artifacts: {
    LivepeerToken: _LivepeerToken.default,
    LivepeerTokenFaucet: _LivepeerTokenFaucet.default,
    Controller: _Controller.default,
    JobsManager: _JobsManager.default,
    RoundsManager: _RoundsManager.default,
    BondingManager: _BondingManager.default,
    Minter: _Minter.default
  },
  ensRegistries: {
    // Mainnet
    '1': '0x314159265dd8dbb310642f98f50c066173c1259b',
    // Ropsten
    '3': '0x112234455c3a32fd11230c42e7bccd4a84e02010',
    // Rinkeby
    '4': '0xe7410170f87102df0055eb195163a03b7f2bff4a'
  } // Utils

};
exports.DEFAULTS = DEFAULTS;
var utils = {
  isValidAddress: function isValidAddress(x) {
    return /^0x[a-fA-F0-9]{40}$/.test(x);
  },
  resolveAddress: function () {
    var _resolveAddress = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(resolve, x) {
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!utils.isValidAddress(x)) {
                _context.next = 4;
                break;
              }

              _context.t0 = x;
              _context.next = 7;
              break;

            case 4:
              _context.next = 6;
              return resolve(x);

            case 6:
              _context.t0 = _context.sent;

            case 7:
              return _context.abrupt("return", _context.t0);

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function resolveAddress(_x, _x2) {
      return _resolveAddress.apply(this, arguments);
    }

    return resolveAddress;
  }(),
  getMethodHash: function getMethodHash(item) {
    // const sig = `${item.name}(${item.inputs.map(x => x.type).join(',')})`
    // const hash = Eth.keccak256(sig)
    // return hash
    return (0, _ethjsAbi.encodeSignature)(item);
  },
  findAbiByName: function findAbiByName(abis, name) {
    var _abis$filter = abis.filter(function (item) {
      if (item.type !== 'function') return false;
      if (item.name === name) return true;
    }),
        _abis$filter2 = (0, _slicedToArray2.default)(_abis$filter, 1),
        abi = _abis$filter2[0];

    return abi;
  },
  findAbiByHash: function findAbiByHash(abis, hash) {
    var _abis$filter3 = abis.filter(function (item) {
      if (item.type !== 'function') return false;
      return (0, _ethjsAbi.encodeSignature)(item) === hash;
    }),
        _abis$filter4 = (0, _slicedToArray2.default)(_abis$filter3, 1),
        abi = _abis$filter4[0];

    return abi;
  },
  encodeMethodParams: function encodeMethodParams(abi, params) {
    return (0, _ethjsAbi.encodeMethod)(abi, params);
  },
  decodeMethodParams: function decodeMethodParams(abi, bytecode) {
    return (0, _ethjsAbi.decodeParams)(abi.inputs.map(function (x) {
      return x.name;
    }), abi.inputs.map(function (x) {
      return x.type;
    }), "0x".concat(bytecode.substr(10)), false);
  },
  decodeContractInput: function decodeContractInput(contracts, contractAddress, input) {
    for (var key in contracts) {
      var contract = contracts[key];
      if (contract.address !== contractAddress) continue;
      var hash = input.substring(0, 10);
      var abi = utils.findAbiByHash(contract.abi, hash);
      return {
        contract: key,
        method: abi.name,
        params: Object.entries(utils.decodeMethodParams(abi, input)).reduce(function (obj, _ref) {
          var _ref2 = (0, _slicedToArray2.default)(_ref, 2),
              k = _ref2[0],
              v = _ref2[1];

          return (0, _objectSpread4.default)({}, obj, (0, _defineProperty2.default)({}, k, Array.isArray(v) ? v.map(function (_v) {
            return BN.isBN(_v) ? toString(_v) : _v;
          }) : BN.isBN(v) ? toString(v) : v));
        }, {})
      };
    }

    return {
      contract: '',
      method: '',
      params: {}
    };
  },

  /**
   * Polls for a transaction receipt
   * @ignore
   * @param {string}   txHash - the transaction hash
   * @param {Eth}      eth    - an instance of Ethjs
   * @return {Object}
   */
  getTxReceipt: function () {
    var _getTxReceipt = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee3(txHash, eth) {
      return _regenerator.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return new Promise(function (resolve, reject) {
                setTimeout(
                /*#__PURE__*/
                function () {
                  var _pollForReceipt = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee2() {
                    var receipt;
                    return _regenerator.default.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            _context2.prev = 0;
                            _context2.next = 3;
                            return eth.getTransactionReceipt(txHash);

                          case 3:
                            receipt = _context2.sent;

                            if (!receipt) {
                              _context2.next = 21;
                              break;
                            }

                            if (!(receipt.status === '0x1')) {
                              _context2.next = 9;
                              break;
                            }

                            _context2.t0 = // success
                            resolve(receipt);
                            _context2.next = 20;
                            break;

                          case 9:
                            _context2.t1 = reject;
                            _context2.t2 = Error;
                            _context2.t3 = JSON;
                            _context2.t4 = receipt;
                            _context2.next = 15;
                            return eth.getTransactionByHash(receipt.transactionHash);

                          case 15:
                            _context2.t5 = _context2.sent;
                            _context2.t6 = {
                              receipt: _context2.t4,
                              transaction: _context2.t5
                            };
                            _context2.t7 = _context2.t3.stringify.call(_context2.t3, _context2.t6, null, 2);
                            _context2.t8 = new _context2.t2(_context2.t7);
                            _context2.t0 = (0, _context2.t1)(_context2.t8);

                          case 20:
                            return _context2.abrupt("return", _context2.t0);

                          case 21:
                            setTimeout(pollForReceipt, 300);
                            _context2.next = 27;
                            break;

                          case 24:
                            _context2.prev = 24;
                            _context2.t9 = _context2["catch"](0);
                            reject(_context2.t9);

                          case 27:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2, null, [[0, 24]]);
                  }));

                  function pollForReceipt() {
                    return _pollForReceipt.apply(this, arguments);
                  }

                  return pollForReceipt;
                }(), 0);
              });

            case 2:
              return _context3.abrupt("return", _context3.sent);

            case 3:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    function getTxReceipt(_x3, _x4) {
      return _getTxReceipt.apply(this, arguments);
    }

    return getTxReceipt;
  }(),

  /**
   * Parses an encoded string of transcoding options
   * @ignore
   * @param  {string} opts - transcoding options
   * @return {Object[]}
   */
  parseTranscodingOptions: function parseTranscodingOptions(opts) {
    var profiles = Object.values(VIDEO_PROFILES);
    var validHashes = new Set(profiles.map(function (x) {
      return x.hash;
    }));
    var hashes = [];

    for (var i = 0; i < opts.length; i += VIDEO_PROFILE_ID_SIZE) {
      var hash = opts.slice(i, i + VIDEO_PROFILE_ID_SIZE);
      if (!validHashes.has(hash)) continue;
      hashes.push(hash);
    }

    return hashes.map(function (x) {
      return profiles.find(function (_ref3) {
        var hash = _ref3.hash;
        return x === hash;
      });
    });
  },

  /**
   * Serializes a list of transcoding profiles name into a hash
   * @ignore
   * @param  {string[]} name - transcoding profile name
   * @return {string}
   */
  serializeTranscodingProfiles: function serializeTranscodingProfiles(names) {
    return (0, _toConsumableArray2.default)(new Set( // dedupe profiles
    names.map(function (x) {
      return VIDEO_PROFILES[x] ? VIDEO_PROFILES[x].hash : VIDEO_PROFILES.P240p30fps4x3.hash;
    }))).join('');
  },

  /**
   * Pads an address with 0s on the left (for topic encoding)
   * @ignore
   * @param  {string} addr - an ETH address
   * @return {string}
   */
  padAddress: function padAddress(addr) {
    return ADDRESS_PAD + addr.substr(2);
  },

  /**
   * Encodes an event filter object into a topic list
   * @ignore
   * @param  {Function} event   - a contract event method
   * @param  {Object}   filters - key/value map of indexed event params
   * @return {string[]}
   */
  encodeEventTopics: function encodeEventTopics(event, filters) {
    return event.abi.inputs.reduce(function (topics, _ref4, i) {
      var indexed = _ref4.indexed,
          name = _ref4.name,
          type = _ref4.type;
      if (!indexed) return topics;
      if (!filters.hasOwnProperty(name)) return [].concat((0, _toConsumableArray2.default)(topics), [null]);
      if (type === 'address' && 'string' === typeof filters[name]) return [].concat((0, _toConsumableArray2.default)(topics), [utils.padAddress(filters[name])]);
      return [].concat((0, _toConsumableArray2.default)(topics), [filters[name]]);
    }, [event().options.defaultFilterObject.topics[0]]);
  },

  /**
   * Turns a raw event log into a result object
   * @ignore
   * @param  {Function} event  - a contract event method
   * @param  {string}   data   - bytecode from log
   * @param  {string[]} topics - list of topics for log query
   * @return {Object}
   */
  decodeEvent: function decodeEvent(event) {
    return function (_ref5) {
      var data = _ref5.data,
          topics = _ref5.topics;
      return (0, _ethjsAbi.decodeEvent)(event.abi, data, topics, false);
    };
  } // Helper functions
  // ethjs returns a Result type from rpc requests
  // these functions help with formatting those values

};
exports.utils = utils;
var BN = _ethjs.default.BN;

var toBN = function toBN(n) {
  return BN.isBN(n) ? n : new BN(n.toString(10), 10);
};

var compose = function compose() {
  for (var _len = arguments.length, fns = new Array(_len), _key = 0; _key < _len; _key++) {
    fns[_key] = arguments[_key];
  }

  return fns.reduce(function (f, g) {
    return function () {
      return f(g.apply(void 0, arguments));
    };
  });
};

var prop = function prop(k) {
  return function (x) {
    return x[k];
  };
};

var toBool = function toBool(x) {
  return !!x;
};

var toString = function toString(x) {
  return x.toString(10);
};

var toNumber = function toNumber(x) {
  return Number(x.toString(10));
};

var headToBool = compose(toBool, prop(0));
var headToString = compose(toString, prop(0));
var headToNumber = compose(toNumber, prop(0));

var invariant = function invariant(name, pos, type) {
  throw new Error("Missing argument \"".concat(name, "\" (").concat(type, ") at position ").concat(pos));
};

var formatDuration = function formatDuration(ms) {
  var seconds = (ms / 1000).toFixed(1);
  var minutes = (ms / (1000 * 60)).toFixed(1);
  var hours = (ms / (1000 * 60 * 60)).toFixed(1);
  var days = (ms / (1000 * 60 * 60 * 24)).toFixed(1);
  if (seconds < 60) return seconds + ' sec';else if (minutes < 60) return minutes + ' min';else if (hours < 24) return hours + ' hours';
  return days + ' days';
};
/**
 * Deploys contract and return instance at deployed address
 * @ignore
 * @param {*} eth
 * @param {*} args
 */


function deployContract(_x5, _x6) {
  return _deployContract.apply(this, arguments);
}
/**
 * Creates a contract instance from a specific address
 * @ignore
 * @param {Eth}    eth     - ethjs instance
 * @param {string} address -
 * @param {Object} args[0] - an object containing all relevant Livepeer Artifacts
 */


function _deployContract() {
  _deployContract = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(eth, _ref6) {
    var abi, bytecode, defaultTx, contract, txHash, receipt;
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            abi = _ref6.abi, bytecode = _ref6.bytecode, defaultTx = _ref6.defaultTx;
            contract = eth.contract(abi, bytecode, defaultTx);
            _context4.next = 4;
            return contract.new();

          case 4:
            txHash = _context4.sent;
            _context4.next = 7;
            return eth.getTransactionSuccess(txHash);

          case 7:
            receipt = _context4.sent;
            return _context4.abrupt("return", contract.at(receipt.contractAddress));

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _deployContract.apply(this, arguments);
}

function getContractAt(eth, _ref7) {
  var abi = _ref7.abi,
      bytecode = _ref7.bytecode,
      address = _ref7.address,
      defaultTx = _ref7.defaultTx;
  return eth.contract(abi, bytecode, defaultTx).at(address);
}
/**
 * Creates an instance of Eth and a default transaction object
 * @ignore
 * @return {{ et, gas: Eth, defaultTx: { from: string, gas: number } }}
 */


function initRPC(_x7) {
  return _initRPC.apply(this, arguments);
}
/**
 * Creates instances of all main Livepeer contracts
 * @ignore
 * @param {string} opts.provider  - the httpProvider for contract RPC
 * @param {Object} opts.artifacts - ...
 */


function _initRPC() {
  _initRPC = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5(_ref8) {
    var account, privateKeys, gas, provider, usePrivateKeys, ethjsProvider, eth, ens, _accounts, from;

    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            account = _ref8.account, privateKeys = _ref8.privateKeys, gas = _ref8.gas, provider = _ref8.provider;
            usePrivateKeys = 0 < Object.keys(privateKeys).length;
            ethjsProvider = 'object' === (0, _typeof2.default)(provider) && provider ? provider : usePrivateKeys ? // Use provider-signer to locally sign transactions
            new _ethjsProviderSigner.default(provider, {
              signTransaction: function signTransaction(rawTx, cb) {
                var tx = new _ethereumjsTx.default(rawTx);
                tx.sign(privateKeys[from]);
                cb(null, '0x' + tx.serialize().toString('hex'));
              },
              accounts: function accounts(cb) {
                return cb(null, _accounts);
              },
              timeout: 10 * 1000
            }) : // Use default signer
            new _ethjs.default.HttpProvider(provider || DEFAULTS.provider);
            eth = new _ethjs.default(ethjsProvider);
            _context5.t0 = _ethjsEns.default;
            _context5.t1 = eth.currentProvider;
            _context5.next = 8;
            return eth.net_version();

          case 8:
            _context5.t2 = _context5.sent;
            _context5.t3 = DEFAULTS.ensRegistries[_context5.t2];
            _context5.t4 = {
              provider: _context5.t1,
              registryAddress: _context5.t3
            };
            ens = new _context5.t0(_context5.t4);

            if (!usePrivateKeys) {
              _context5.next = 16;
              break;
            }

            _context5.t5 = Object.keys(privateKeys);
            _context5.next = 19;
            break;

          case 16:
            _context5.next = 18;
            return eth.accounts();

          case 18:
            _context5.t5 = _context5.sent;

          case 19:
            _accounts = _context5.t5;
            from = // select account by address or index
            // default to EMPTY_ADDRESS (read-only; cannot transact)
            new Set(_accounts).has(account) ? account : _accounts[account] || EMPTY_ADDRESS;
            return _context5.abrupt("return", {
              eth: eth,
              ens: ens,
              provider: provider,
              accounts: _accounts,
              defaultTx: {
                from: from,
                gas: gas
              }
            });

          case 22:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _initRPC.apply(this, arguments);
}

function initContracts() {
  return _initContracts.apply(this, arguments);
}
/**
 * Livepeer SDK main module exports
 * @namespace module~exports
 */

/**
 * Livepeer SDK factory function. Creates an instance of the Livepeer SDK -- an object with useful methods for interacting with Livepeer protocol smart contracts
 * @memberof module~exports
 * @name default
 * @param {LivepeerSDKOptions} opts - SDK configuration options
 * @return {Promise<LivepeerSDK>}
 *
 * @example
 *
 * // Here we're naming the default export "LivepeerSDK"
 * import LivepeerSDK from '@livepeer/sdk'
 *
 * // Call the factory function and await its Promise
 * LivepeerSDK().then(sdk => {
 *   // Your Livepeer SDK instance is now ready to use
 * })
 *
 */


function _initContracts() {
  _initContracts = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee6() {
    var opts,
        _opts$account,
        account,
        _opts$artifacts,
        artifacts,
        _opts$controllerAddre,
        controllerAddress,
        _opts$gas,
        gas,
        _opts$privateKeys,
        privateKeys,
        _opts$provider,
        provider,
        _ref9,
        accounts,
        defaultTx,
        ens,
        eth,
        contracts,
        hashes,
        Controller,
        _i,
        _Object$keys,
        name,
        hash,
        address,
        _iteratorNormalCompletion,
        _didIteratorError,
        _iteratorError,
        _iterator,
        _step,
        item,
        abis,
        events,
        _args6 = arguments;

    return _regenerator.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            opts = _args6.length > 0 && _args6[0] !== undefined ? _args6[0] : {};
            // Merge pass options with defaults
            _opts$account = opts.account, account = _opts$account === void 0 ? DEFAULTS.account : _opts$account, _opts$artifacts = opts.artifacts, artifacts = _opts$artifacts === void 0 ? DEFAULTS.artifacts : _opts$artifacts, _opts$controllerAddre = opts.controllerAddress, controllerAddress = _opts$controllerAddre === void 0 ? DEFAULTS.controllerAddress : _opts$controllerAddre, _opts$gas = opts.gas, gas = _opts$gas === void 0 ? DEFAULTS.gas : _opts$gas, _opts$privateKeys = opts.privateKeys, privateKeys = _opts$privateKeys === void 0 ? DEFAULTS.privateKeys : _opts$privateKeys, _opts$provider = opts.provider, provider = _opts$provider === void 0 ? DEFAULTS.provider : _opts$provider; // Instanstiate new ethjs instance with specified provider

            _context6.next = 4;
            return initRPC({
              account: account,
              gas: gas,
              privateKeys: privateKeys,
              provider: provider
            });

          case 4:
            _ref9 = _context6.sent;
            accounts = _ref9.accounts;
            defaultTx = _ref9.defaultTx;
            ens = _ref9.ens;
            eth = _ref9.eth;
            contracts = {
              LivepeerToken: null,
              LivepeerTokenFaucet: null,
              BondingManager: null,
              JobsManager: null,
              RoundsManager: null,
              Minter: null
            };
            hashes = {
              LivepeerToken: {},
              LivepeerTokenFaucet: {},
              BondingManager: {},
              JobsManager: {},
              RoundsManager: {},
              Minter: {} // Create a Controller contract instance

            };
            _context6.next = 13;
            return getContractAt(eth, (0, _objectSpread4.default)({}, artifacts.Controller, {
              defaultTx: defaultTx,
              address: controllerAddress
            }));

          case 13:
            Controller = _context6.sent;
            _i = 0, _Object$keys = Object.keys(contracts);

          case 15:
            if (!(_i < _Object$keys.length)) {
              _context6.next = 46;
              break;
            }

            name = _Object$keys[_i];
            // Get contract address from Controller
            hash = _ethjs.default.keccak256(name);
            _context6.next = 20;
            return Controller.getContract(hash);

          case 20:
            address = _context6.sent[0];
            _context6.next = 23;
            return getContractAt(eth, (0, _objectSpread4.default)({}, artifacts[name], {
              defaultTx: defaultTx,
              address: address
            }));

          case 23:
            contracts[name] = _context6.sent;
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context6.prev = 27;

            for (_iterator = contracts[name].abi[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              item = _step.value;
              hashes[name][utils.getMethodHash(item)] = item.name;
            }

            _context6.next = 35;
            break;

          case 31:
            _context6.prev = 31;
            _context6.t0 = _context6["catch"](27);
            _didIteratorError = true;
            _iteratorError = _context6.t0;

          case 35:
            _context6.prev = 35;
            _context6.prev = 36;

            if (!_iteratorNormalCompletion && _iterator.return != null) {
              _iterator.return();
            }

          case 38:
            _context6.prev = 38;

            if (!_didIteratorError) {
              _context6.next = 41;
              break;
            }

            throw _iteratorError;

          case 41:
            return _context6.finish(38);

          case 42:
            return _context6.finish(35);

          case 43:
            _i++;
            _context6.next = 15;
            break;

          case 46:
            // Add the Controller contract to the contracts object
            contracts.Controller = Controller; // Key ABIs by contract name

            abis = Object.entries(artifacts).map(function (_ref10) {
              var _ref11 = (0, _slicedToArray2.default)(_ref10, 2),
                  k = _ref11[0],
                  v = _ref11[1];

              return (0, _defineProperty2.default)({}, k, v.abi);
            }).reduce(function (a, b) {
              return (0, _objectSpread4.default)({}, a, b);
            }, {}); // Create a list of events in each contract

            events = Object.entries(abis).map(function (_ref13) {
              var _ref14 = (0, _slicedToArray2.default)(_ref13, 2),
                  contract = _ref14[0],
                  abi = _ref14[1];

              return abi.filter(function (x) {
                return x.type === 'event';
              }).map(function (abi) {
                return {
                  abi: abi,
                  contract: contract,
                  event: contracts[contract][abi.name],
                  name: abi.name
                };
              });
            }).reduce(function (a, b) {
              return b.reduce(function (events, _ref15) {
                var name = _ref15.name,
                    event = _ref15.event,
                    abi = _ref15.abi,
                    contract = _ref15.contract;
                // console.log(contract, name, abis[contract])
                event.abi = abi;
                event.contract = contract;
                return (0, _objectSpread4.default)({}, events, (0, _defineProperty2.default)({}, name, event));
              }, a);
            }, {});
            return _context6.abrupt("return", {
              abis: abis,
              accounts: accounts,
              contracts: contracts,
              defaultTx: defaultTx,
              ens: ens,
              eth: eth,
              events: events,
              hashes: hashes
            });

          case 50:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[27, 31, 35, 43], [36,, 38, 42]]);
  }));
  return _initContracts.apply(this, arguments);
}

function createLivepeerSDK(_x8) {
  return _createLivepeerSDK.apply(this, arguments);
}

function _createLivepeerSDK() {
  _createLivepeerSDK = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee73(opts) {
    var _rpc;

    var _ref16, ens, events, config, _config$contracts, BondingManager, Controller, JobsManager, LivepeerToken, LivepeerTokenFaucet, RoundsManager, Minter, resolveAddress, cache, rpc;

    return _regenerator.default.wrap(function _callee73$(_context73) {
      while (1) {
        switch (_context73.prev = _context73.next) {
          case 0:
            _context73.next = 2;
            return initContracts(opts);

          case 2:
            _ref16 = _context73.sent;
            ens = _ref16.ens;
            events = _ref16.events;
            config = (0, _objectWithoutProperties2.default)(_ref16, ["ens", "events"]);
            _config$contracts = config.contracts, BondingManager = _config$contracts.BondingManager, Controller = _config$contracts.Controller, JobsManager = _config$contracts.JobsManager, LivepeerToken = _config$contracts.LivepeerToken, LivepeerTokenFaucet = _config$contracts.LivepeerTokenFaucet, RoundsManager = _config$contracts.RoundsManager, Minter = _config$contracts.Minter;
            resolveAddress = utils.resolveAddress; // Cache

            cache = {} // previous log queries are held here to improve perf

            /**
             * "rpc" namespace of a Livepeer SDK instance
             * @namespace livepeer~rpc
             *
             * @example
             *
             * import LivepeerSDK from '@livepeer/sdk'
             *
             * LivepeerSDK().then(({ rpc }) => {
             *   // Here, we're destructuring the sdk to expose only its rpc namespace
             *   // Now, you you are able call rpc.<method-name>()
             *   // All rpc method yield Promises. Their usage is further explained below.
             * })
             *
             */
            ;
            rpc = (_rpc = {
              /**
               * Gets the ENS name for an address. This is known as a reverse lookup.
               * Unfortunately, users must explicitly set their own resolver.
               * So most of the time, this method just returns an empty string
               * More info here:
               * (https://docs.ens.domains/en/latest/userguide.html#reverse-name-resolution)
               * @memberof livepeer~rpc
               * @param {string} address - address to look up an ENS name for
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getENSName('0xd34db33f...')
               * // => string
               */
              getENSName: function () {
                var _getENSName = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee7(address) {
                  return _regenerator.default.wrap(function _callee7$(_context7) {
                    while (1) {
                      switch (_context7.prev = _context7.next) {
                        case 0:
                          _context7.prev = 0;
                          _context7.next = 3;
                          return ens.reverse(address);

                        case 3:
                          return _context7.abrupt("return", _context7.sent);

                        case 6:
                          _context7.prev = 6;
                          _context7.t0 = _context7["catch"](0);

                          // custom networks or unavailable resolvers can cause failure
                          if (_context7.t0.message !== 'ENS name not defined.') {
                            console.warn("Could not get ENS name for address \"".concat(address, "\":"), _context7.t0.message);
                          } // if there's no name, we can just resolve an empty string


                          return _context7.abrupt("return", '');

                        case 10:
                        case "end":
                          return _context7.stop();
                      }
                    }
                  }, _callee7, null, [[0, 6]]);
                }));

                function getENSName(_x9) {
                  return _getENSName.apply(this, arguments);
                }

                return getENSName;
              }(),

              /**
               * Gets the address for an ENS name
               * @memberof livepeer~rpc
               * @param {string} name - ENS name to look up an address for
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getENSAddress('vitalik.eth')
               * // => string
               */
              getENSAddress: function () {
                var _getENSAddress = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee8(name) {
                  return _regenerator.default.wrap(function _callee8$(_context8) {
                    while (1) {
                      switch (_context8.prev = _context8.next) {
                        case 0:
                          _context8.prev = 0;
                          _context8.next = 3;
                          return ens.lookup(name);

                        case 3:
                          return _context8.abrupt("return", _context8.sent);

                        case 6:
                          _context8.prev = 6;
                          _context8.t0 = _context8["catch"](0);

                          // custom networks or unavailable resolvers can cause failure
                          if (_context8.t0.message !== 'ENS name not defined.') {
                            console.warn("Could not get address for ENS name \"".concat(name, "\":"), _context8.t0.message);
                          } // if there's no name, we can just resolve an empty string


                          return _context8.abrupt("return", '');

                        case 10:
                        case "end":
                          return _context8.stop();
                      }
                    }
                  }, _callee8, null, [[0, 6]]);
                }));

                function getENSAddress(_x10) {
                  return _getENSAddress.apply(this, arguments);
                }

                return getENSAddress;
              }(),

              /**
               * Gets a block by number, hash, or keyword ('earliest' | 'latest')
               * @memberof livepeer~rpc
               * @param {string} block - Number of block to get
               *
               * @example
               *
               * await rpc.getBlock('latest')
               * // => {
               *   "number": string,
               *   "hash": string,
               *   "parentHash": string,
               *   "nonce": string,
               *   "sha3Uncles": string,
               *   "logsBloom": string,
               *   "transactionsRoot": string,
               *   "stateRoot": string,
               *   "receiptsRoot": string,
               *   "miner": string,
               *   "mixHash": string,
               *   "difficulty": string,
               *   "totalDifficulty": string,
               *   "extraData": string,
               *   "size": string,
               *   "gasLimit": string,
               *   "gasUsed": string,
               *   "timestamp": number,
               *   "transactions": Array<Transaction>,
               *   "transactionsRoot": string,
               *   "uncles": Array<Uncle>,
               * }
               */
              getBlock: function () {
                var _getBlock = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee9(id) {
                  var block;
                  return _regenerator.default.wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          if (!id.toString().startsWith('0x')) {
                            _context9.next = 6;
                            break;
                          }

                          _context9.next = 3;
                          return config.eth.getBlockByHash(id, true);

                        case 3:
                          _context9.t0 = _context9.sent;
                          _context9.next = 9;
                          break;

                        case 6:
                          _context9.next = 8;
                          return config.eth.getBlockByNumber(id, true);

                        case 8:
                          _context9.t0 = _context9.sent;

                        case 9:
                          block = _context9.t0;
                          return _context9.abrupt("return", (0, _objectSpread4.default)({}, block, {
                            difficulty: toString(block.difficulty),
                            gasLimit: toString(block.gasLimit),
                            gasUsed: toString(block.gasUsed),
                            number: toString(block.number),
                            size: toString(block.size),
                            timestamp: Number(toString(block.timestamp)),
                            totalDifficulty: toString(block.totalDifficulty)
                          }));

                        case 11:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9);
                }));

                function getBlock(_x11) {
                  return _getBlock.apply(this, arguments);
                }

                return getBlock;
              }(),

              /**
               * Gets the ETH balance for an account
               * @memberof livepeer~rpc
               * @param {string} addr - ETH account address
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getEthBalance('0xf00...')
               * // => string
               *
               */
              getEthBalance: function () {
                var _getEthBalance = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee10(addr) {
                  return _regenerator.default.wrap(function _callee10$(_context10) {
                    while (1) {
                      switch (_context10.prev = _context10.next) {
                        case 0:
                          _context10.t0 = toString;
                          _context10.t1 = config.eth;
                          _context10.next = 4;
                          return resolveAddress(rpc.getENSAddress, addr);

                        case 4:
                          _context10.t2 = _context10.sent;
                          _context10.next = 7;
                          return _context10.t1.getBalance.call(_context10.t1, _context10.t2);

                        case 7:
                          _context10.t3 = _context10.sent;
                          return _context10.abrupt("return", (0, _context10.t0)(_context10.t3));

                        case 9:
                        case "end":
                          return _context10.stop();
                      }
                    }
                  }, _callee10);
                }));

                function getEthBalance(_x12) {
                  return _getEthBalance.apply(this, arguments);
                }

                return getEthBalance;
              }(),

              /**
               * Gets the unbonding period for transcoders
               * @memberof livepeer~rpc
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getUnbondingPeriod()
               * // => string
               */
              getUnbondingPeriod: function () {
                var _getUnbondingPeriod = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee11() {
                  return _regenerator.default.wrap(function _callee11$(_context11) {
                    while (1) {
                      switch (_context11.prev = _context11.next) {
                        case 0:
                          _context11.t0 = headToString;
                          _context11.next = 3;
                          return BondingManager.unbondingPeriod();

                        case 3:
                          _context11.t1 = _context11.sent;
                          return _context11.abrupt("return", (0, _context11.t0)(_context11.t1));

                        case 5:
                        case "end":
                          return _context11.stop();
                      }
                    }
                  }, _callee11);
                }));

                function getUnbondingPeriod() {
                  return _getUnbondingPeriod.apply(this, arguments);
                }

                return getUnbondingPeriod;
              }(),

              /**
               * Gets the number of active transcoders
               * @memberof livepeer~rpc
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getNumActiveTranscoders()
               * // => string
               */
              getNumActiveTranscoders: function () {
                var _getNumActiveTranscoders = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee12() {
                  return _regenerator.default.wrap(function _callee12$(_context12) {
                    while (1) {
                      switch (_context12.prev = _context12.next) {
                        case 0:
                          _context12.t0 = headToString;
                          _context12.next = 3;
                          return BondingManager.numActiveTranscoders();

                        case 3:
                          _context12.t1 = _context12.sent;
                          return _context12.abrupt("return", (0, _context12.t0)(_context12.t1));

                        case 5:
                        case "end":
                          return _context12.stop();
                      }
                    }
                  }, _callee12);
                }));

                function getNumActiveTranscoders() {
                  return _getNumActiveTranscoders.apply(this, arguments);
                }

                return getNumActiveTranscoders;
              }(),

              /**
               * Gets the maximum earnings for claims rounds
               * @memberof livepeer~rpc
               * @return {Promise<string>}
               *
               * @example
               *
               * await rpc.getMaxEarningsClaimsRounds()
               * // => string
               */
              getMaxEarningsClaimsRounds: function () {
                var _getMaxEarningsClaimsRounds = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee13() {
                  return _regenerator.default.wrap(function _callee13$(_context13) {
                    while (1) {
                      switch (_context13.prev = _context13.next) {
                        case 0:
                          _context13.t0 = headToString;
                          _context13.next = 3;
                          return BondingManager.maxEarningsClaimsRounds();

                        case 3:
                          _context13.t1 = _context13.sent;
                          return _context13.abrupt("return", (0, _context13.t0)(_context13.t1));

                        case 5:
                        case "end":
                          return _context13.stop();
                      }
                    }
                  }, _callee13);
                }));

                function getMaxEarningsClaimsRounds() {
                  return _getMaxEarningsClaimsRounds.apply(this, arguments);
                }

                return getMaxEarningsClaimsRounds;
              }()
            }, (0, _defineProperty2.default)(_rpc, "getUnbondingPeriod", function () {
              var _getUnbondingPeriod2 = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee14() {
                return _regenerator.default.wrap(function _callee14$(_context14) {
                  while (1) {
                    switch (_context14.prev = _context14.next) {
                      case 0:
                        _context14.t0 = headToString;
                        _context14.next = 3;
                        return BondingManager.unbondingPeriod();

                      case 3:
                        _context14.t1 = _context14.sent;
                        return _context14.abrupt("return", (0, _context14.t0)(_context14.t1));

                      case 5:
                      case "end":
                        return _context14.stop();
                    }
                  }
                }, _callee14);
              }));

              function getUnbondingPeriod() {
                return _getUnbondingPeriod2.apply(this, arguments);
              }

              return getUnbondingPeriod;
            }()), (0, _defineProperty2.default)(_rpc, "getNumActiveTranscoders", function () {
              var _getNumActiveTranscoders2 = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee15() {
                return _regenerator.default.wrap(function _callee15$(_context15) {
                  while (1) {
                    switch (_context15.prev = _context15.next) {
                      case 0:
                        _context15.t0 = headToString;
                        _context15.next = 3;
                        return BondingManager.numActiveTranscoders();

                      case 3:
                        _context15.t1 = _context15.sent;
                        return _context15.abrupt("return", (0, _context15.t0)(_context15.t1));

                      case 5:
                      case "end":
                        return _context15.stop();
                    }
                  }
                }, _callee15);
              }));

              function getNumActiveTranscoders() {
                return _getNumActiveTranscoders2.apply(this, arguments);
              }

              return getNumActiveTranscoders;
            }()), (0, _defineProperty2.default)(_rpc, "getMaxEarningsClaimsRounds", function () {
              var _getMaxEarningsClaimsRounds2 = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee16() {
                return _regenerator.default.wrap(function _callee16$(_context16) {
                  while (1) {
                    switch (_context16.prev = _context16.next) {
                      case 0:
                        _context16.t0 = headToString;
                        _context16.next = 3;
                        return BondingManager.maxEarningsClaimsRounds();

                      case 3:
                        _context16.t1 = _context16.sent;
                        return _context16.abrupt("return", (0, _context16.t0)(_context16.t1));

                      case 5:
                      case "end":
                        return _context16.stop();
                    }
                  }
                }, _callee16);
              }));

              function getMaxEarningsClaimsRounds() {
                return _getMaxEarningsClaimsRounds2.apply(this, arguments);
              }

              return getMaxEarningsClaimsRounds;
            }()), (0, _defineProperty2.default)(_rpc, "getTotalBonded", function () {
              var _getTotalBonded = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee17() {
                return _regenerator.default.wrap(function _callee17$(_context17) {
                  while (1) {
                    switch (_context17.prev = _context17.next) {
                      case 0:
                        _context17.t0 = headToString;
                        _context17.next = 3;
                        return BondingManager.getTotalBonded();

                      case 3:
                        _context17.t1 = _context17.sent;
                        return _context17.abrupt("return", (0, _context17.t0)(_context17.t1));

                      case 5:
                      case "end":
                        return _context17.stop();
                    }
                  }
                }, _callee17);
              }));

              function getTotalBonded() {
                return _getTotalBonded.apply(this, arguments);
              }

              return getTotalBonded;
            }()), (0, _defineProperty2.default)(_rpc, "getTokenTotalSupply", function () {
              var _getTokenTotalSupply = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee18() {
                return _regenerator.default.wrap(function _callee18$(_context18) {
                  while (1) {
                    switch (_context18.prev = _context18.next) {
                      case 0:
                        _context18.t0 = headToString;
                        _context18.next = 3;
                        return LivepeerToken.totalSupply();

                      case 3:
                        _context18.t1 = _context18.sent;
                        return _context18.abrupt("return", (0, _context18.t0)(_context18.t1));

                      case 5:
                      case "end":
                        return _context18.stop();
                    }
                  }
                }, _callee18);
              }));

              function getTokenTotalSupply() {
                return _getTokenTotalSupply.apply(this, arguments);
              }

              return getTokenTotalSupply;
            }()), (0, _defineProperty2.default)(_rpc, "getTokenBalance", function () {
              var _getTokenBalance = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee19(addr) {
                return _regenerator.default.wrap(function _callee19$(_context19) {
                  while (1) {
                    switch (_context19.prev = _context19.next) {
                      case 0:
                        _context19.t0 = headToString;
                        _context19.t1 = LivepeerToken;
                        _context19.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context19.t2 = _context19.sent;
                        _context19.next = 7;
                        return _context19.t1.balanceOf.call(_context19.t1, _context19.t2);

                      case 7:
                        _context19.t3 = _context19.sent;
                        return _context19.abrupt("return", (0, _context19.t0)(_context19.t3));

                      case 9:
                      case "end":
                        return _context19.stop();
                    }
                  }
                }, _callee19);
              }));

              function getTokenBalance(_x13) {
                return _getTokenBalance.apply(this, arguments);
              }

              return getTokenBalance;
            }()), (0, _defineProperty2.default)(_rpc, "getTokenInfo", function () {
              var _getTokenInfo = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee20(addr) {
                return _regenerator.default.wrap(function _callee20$(_context20) {
                  while (1) {
                    switch (_context20.prev = _context20.next) {
                      case 0:
                        _context20.next = 2;
                        return rpc.getTokenTotalSupply();

                      case 2:
                        _context20.t0 = _context20.sent;
                        _context20.t1 = rpc;
                        _context20.next = 6;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 6:
                        _context20.t2 = _context20.sent;
                        _context20.next = 9;
                        return _context20.t1.getTokenBalance.call(_context20.t1, _context20.t2);

                      case 9:
                        _context20.t3 = _context20.sent;
                        return _context20.abrupt("return", {
                          totalSupply: _context20.t0,
                          balance: _context20.t3
                        });

                      case 11:
                      case "end":
                        return _context20.stop();
                    }
                  }
                }, _callee20);
              }));

              function getTokenInfo(_x14) {
                return _getTokenInfo.apply(this, arguments);
              }

              return getTokenInfo;
            }()), (0, _defineProperty2.default)(_rpc, "transferToken", function () {
              var _transferToken = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee21(to, amount) {
                var tx,
                    value,
                    balance,
                    _args21 = arguments;
                return _regenerator.default.wrap(function _callee21$(_context21) {
                  while (1) {
                    switch (_context21.prev = _context21.next) {
                      case 0:
                        tx = _args21.length > 2 && _args21[2] !== undefined ? _args21[2] : config.defaultTx;
                        value = toBN(amount); // make sure balance is higher than transfer

                        _context21.next = 4;
                        return LivepeerToken.balanceOf(tx.from);

                      case 4:
                        balance = _context21.sent[0];

                        if (balance.gte(value)) {
                          _context21.next = 7;
                          break;
                        }

                        throw new Error("Cannot transfer ".concat(toString(value), " LPT because is it greater than your current balance (").concat(balance, " LPT)."));

                      case 7:
                        _context21.t0 = utils;
                        _context21.t1 = LivepeerToken;
                        _context21.next = 11;
                        return resolveAddress(rpc.getENSAddress, to);

                      case 11:
                        _context21.t2 = _context21.sent;
                        _context21.t3 = value;
                        _context21.t4 = tx;
                        _context21.next = 16;
                        return _context21.t1.transfer.call(_context21.t1, _context21.t2, _context21.t3, _context21.t4);

                      case 16:
                        _context21.t5 = _context21.sent;
                        _context21.t6 = config.eth;
                        _context21.next = 20;
                        return _context21.t0.getTxReceipt.call(_context21.t0, _context21.t5, _context21.t6);

                      case 20:
                        return _context21.abrupt("return", _context21.sent);

                      case 21:
                      case "end":
                        return _context21.stop();
                    }
                  }
                }, _callee21);
              }));

              function transferToken(_x15, _x16) {
                return _transferToken.apply(this, arguments);
              }

              return transferToken;
            }()), (0, _defineProperty2.default)(_rpc, "getFaucetAmount", function () {
              var _getFaucetAmount = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee22() {
                return _regenerator.default.wrap(function _callee22$(_context22) {
                  while (1) {
                    switch (_context22.prev = _context22.next) {
                      case 0:
                        _context22.t0 = headToString;
                        _context22.next = 3;
                        return LivepeerTokenFaucet.requestAmount();

                      case 3:
                        _context22.t1 = _context22.sent;
                        return _context22.abrupt("return", (0, _context22.t0)(_context22.t1));

                      case 5:
                      case "end":
                        return _context22.stop();
                    }
                  }
                }, _callee22);
              }));

              function getFaucetAmount() {
                return _getFaucetAmount.apply(this, arguments);
              }

              return getFaucetAmount;
            }()), (0, _defineProperty2.default)(_rpc, "getFaucetWait", function () {
              var _getFaucetWait = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee23() {
                return _regenerator.default.wrap(function _callee23$(_context23) {
                  while (1) {
                    switch (_context23.prev = _context23.next) {
                      case 0:
                        _context23.t0 = headToString;
                        _context23.next = 3;
                        return LivepeerTokenFaucet.requestWait();

                      case 3:
                        _context23.t1 = _context23.sent;
                        return _context23.abrupt("return", (0, _context23.t0)(_context23.t1));

                      case 5:
                      case "end":
                        return _context23.stop();
                    }
                  }
                }, _callee23);
              }));

              function getFaucetWait() {
                return _getFaucetWait.apply(this, arguments);
              }

              return getFaucetWait;
            }()), (0, _defineProperty2.default)(_rpc, "getFaucetNext", function () {
              var _getFaucetNext = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee24(addr) {
                return _regenerator.default.wrap(function _callee24$(_context24) {
                  while (1) {
                    switch (_context24.prev = _context24.next) {
                      case 0:
                        _context24.t0 = headToString;
                        _context24.t1 = LivepeerTokenFaucet;
                        _context24.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context24.t2 = _context24.sent;
                        _context24.next = 7;
                        return _context24.t1.nextValidRequest.call(_context24.t1, _context24.t2);

                      case 7:
                        _context24.t3 = _context24.sent;
                        return _context24.abrupt("return", (0, _context24.t0)(_context24.t3));

                      case 9:
                      case "end":
                        return _context24.stop();
                    }
                  }
                }, _callee24);
              }));

              function getFaucetNext(_x17) {
                return _getFaucetNext.apply(this, arguments);
              }

              return getFaucetNext;
            }()), (0, _defineProperty2.default)(_rpc, "getFaucetInfo", function () {
              var _getFaucetInfo = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee25(addr) {
                return _regenerator.default.wrap(function _callee25$(_context25) {
                  while (1) {
                    switch (_context25.prev = _context25.next) {
                      case 0:
                        _context25.next = 2;
                        return rpc.getFaucetAmount();

                      case 2:
                        _context25.t0 = _context25.sent;
                        _context25.next = 5;
                        return rpc.getFaucetWait();

                      case 5:
                        _context25.t1 = _context25.sent;
                        _context25.t2 = rpc;
                        _context25.next = 9;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 9:
                        _context25.t3 = _context25.sent;
                        _context25.next = 12;
                        return _context25.t2.getFaucetNext.call(_context25.t2, _context25.t3);

                      case 12:
                        _context25.t4 = _context25.sent;
                        return _context25.abrupt("return", {
                          amount: _context25.t0,
                          wait: _context25.t1,
                          next: _context25.t4
                        });

                      case 14:
                      case "end":
                        return _context25.stop();
                    }
                  }
                }, _callee25);
              }));

              function getFaucetInfo(_x18) {
                return _getFaucetInfo.apply(this, arguments);
              }

              return getFaucetInfo;
            }()), (0, _defineProperty2.default)(_rpc, "getInflation", function () {
              var _getInflation = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee26() {
                return _regenerator.default.wrap(function _callee26$(_context26) {
                  while (1) {
                    switch (_context26.prev = _context26.next) {
                      case 0:
                        _context26.t0 = headToString;
                        _context26.next = 3;
                        return Minter.inflation();

                      case 3:
                        _context26.t1 = _context26.sent;
                        return _context26.abrupt("return", (0, _context26.t0)(_context26.t1));

                      case 5:
                      case "end":
                        return _context26.stop();
                    }
                  }
                }, _callee26);
              }));

              function getInflation() {
                return _getInflation.apply(this, arguments);
              }

              return getInflation;
            }()), (0, _defineProperty2.default)(_rpc, "getInflationChange", function () {
              var _getInflationChange = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee27() {
                return _regenerator.default.wrap(function _callee27$(_context27) {
                  while (1) {
                    switch (_context27.prev = _context27.next) {
                      case 0:
                        _context27.t0 = headToString;
                        _context27.next = 3;
                        return Minter.inflationChange();

                      case 3:
                        _context27.t1 = _context27.sent;
                        return _context27.abrupt("return", (0, _context27.t0)(_context27.t1));

                      case 5:
                      case "end":
                        return _context27.stop();
                    }
                  }
                }, _callee27);
              }));

              function getInflationChange() {
                return _getInflationChange.apply(this, arguments);
              }

              return getInflationChange;
            }()), (0, _defineProperty2.default)(_rpc, "getBroadcaster", function () {
              var _getBroadcaster = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee28(addr) {
                var address, b;
                return _regenerator.default.wrap(function _callee28$(_context28) {
                  while (1) {
                    switch (_context28.prev = _context28.next) {
                      case 0:
                        _context28.next = 2;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 2:
                        address = _context28.sent;
                        _context28.next = 5;
                        return JobsManager.broadcasters(address);

                      case 5:
                        b = _context28.sent;
                        return _context28.abrupt("return", {
                          address: address,
                          deposit: toString(b.deposit),
                          withdrawBlock: toString(b.withdrawBlock)
                        });

                      case 7:
                      case "end":
                        return _context28.stop();
                    }
                  }
                }, _callee28);
              }));

              function getBroadcaster(_x19) {
                return _getBroadcaster.apply(this, arguments);
              }

              return getBroadcaster;
            }()), (0, _defineProperty2.default)(_rpc, "getDelegatorStatus", function () {
              var _getDelegatorStatus = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee29(addr) {
                var status;
                return _regenerator.default.wrap(function _callee29$(_context29) {
                  while (1) {
                    switch (_context29.prev = _context29.next) {
                      case 0:
                        _context29.t0 = headToString;
                        _context29.t1 = BondingManager;
                        _context29.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context29.t2 = _context29.sent;
                        _context29.next = 7;
                        return _context29.t1.delegatorStatus.call(_context29.t1, _context29.t2);

                      case 7:
                        _context29.t3 = _context29.sent;
                        status = (0, _context29.t0)(_context29.t3);
                        return _context29.abrupt("return", DELEGATOR_STATUS[status]);

                      case 10:
                      case "end":
                        return _context29.stop();
                    }
                  }
                }, _callee29);
              }));

              function getDelegatorStatus(_x20) {
                return _getDelegatorStatus.apply(this, arguments);
              }

              return getDelegatorStatus;
            }()), (0, _defineProperty2.default)(_rpc, "getDelegator", function () {
              var _getDelegator = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee30(addr) {
                var address, allowance, currentRound, pendingStake, pendingFees, d, bondedAmount, fees, delegateAddress, delegatedAmount, lastClaimRound, startRound, nextUnbondingLockId, unbondingLockId, _ref17, withdrawAmount, withdrawRound, status;

                return _regenerator.default.wrap(function _callee30$(_context30) {
                  while (1) {
                    switch (_context30.prev = _context30.next) {
                      case 0:
                        _context30.next = 2;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 2:
                        address = _context30.sent;
                        _context30.t0 = headToString;
                        _context30.next = 6;
                        return LivepeerToken.allowance(address, BondingManager.address);

                      case 6:
                        _context30.t1 = _context30.sent;
                        allowance = (0, _context30.t0)(_context30.t1);
                        _context30.next = 10;
                        return rpc.getCurrentRound();

                      case 10:
                        currentRound = _context30.sent;
                        _context30.t2 = headToString;
                        _context30.next = 14;
                        return BondingManager.pendingStake(address, currentRound);

                      case 14:
                        _context30.t3 = _context30.sent;
                        pendingStake = (0, _context30.t2)(_context30.t3);
                        _context30.t4 = headToString;
                        _context30.next = 19;
                        return BondingManager.pendingFees(address, currentRound);

                      case 19:
                        _context30.t5 = _context30.sent;
                        pendingFees = (0, _context30.t4)(_context30.t5);
                        _context30.next = 23;
                        return BondingManager.getDelegator(address);

                      case 23:
                        d = _context30.sent;
                        bondedAmount = toString(d.bondedAmount);
                        fees = toString(d.fees);
                        delegateAddress = d.delegateAddress === EMPTY_ADDRESS ? '' : d.delegateAddress;
                        delegatedAmount = toString(d.delegatedAmount);
                        lastClaimRound = toString(d.lastClaimRound);
                        startRound = toString(d.startRound);
                        nextUnbondingLockId = toString(d.nextUnbondingLockId);
                        unbondingLockId = toBN(nextUnbondingLockId);

                        if (unbondingLockId.cmp(new BN(0)) > 0) {
                          unbondingLockId = unbondingLockId.sub(new BN(1));
                        }

                        _context30.next = 35;
                        return rpc.getDelegatorUnbondingLock(address, toString(unbondingLockId));

                      case 35:
                        _ref17 = _context30.sent;
                        withdrawAmount = _ref17.amount;
                        withdrawRound = _ref17.withdrawRound;

                        if (!(withdrawRound !== '0' && toBN(currentRound).cmp(toBN(withdrawRound)) < 0)) {
                          _context30.next = 42;
                          break;
                        }

                        _context30.t6 = DELEGATOR_STATUS.Unbonding;
                        _context30.next = 45;
                        break;

                      case 42:
                        _context30.next = 44;
                        return rpc.getDelegatorStatus(address);

                      case 44:
                        _context30.t6 = _context30.sent;

                      case 45:
                        status = _context30.t6;
                        return _context30.abrupt("return", {
                          address: address,
                          allowance: allowance,
                          bondedAmount: bondedAmount,
                          delegateAddress: delegateAddress,
                          delegatedAmount: delegatedAmount,
                          fees: fees,
                          lastClaimRound: lastClaimRound,
                          pendingFees: pendingFees,
                          pendingStake: pendingStake,
                          startRound: startRound,
                          status: status,
                          withdrawRound: withdrawRound,
                          withdrawAmount: withdrawAmount,
                          nextUnbondingLockId: nextUnbondingLockId
                        });

                      case 47:
                      case "end":
                        return _context30.stop();
                    }
                  }
                }, _callee30);
              }));

              function getDelegator(_x21) {
                return _getDelegator.apply(this, arguments);
              }

              return getDelegator;
            }()), (0, _defineProperty2.default)(_rpc, "getDelegatorUnbondingLocks", function () {
              var _getDelegatorUnbondingLocks = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee31(addr) {
                var _ref18, nextUnbondingLockId, unbondingLockId, result, unbond;

                return _regenerator.default.wrap(function _callee31$(_context31) {
                  while (1) {
                    switch (_context31.prev = _context31.next) {
                      case 0:
                        _context31.next = 2;
                        return rpc.getDelegator(addr);

                      case 2:
                        _ref18 = _context31.sent;
                        nextUnbondingLockId = _ref18.nextUnbondingLockId;
                        unbondingLockId = toNumber(nextUnbondingLockId);

                        if (unbondingLockId > 0) {
                          unbondingLockId -= 1;
                        }

                        result = [];

                      case 7:
                        if (!(unbondingLockId >= 0)) {
                          _context31.next = 15;
                          break;
                        }

                        _context31.next = 10;
                        return rpc.getDelegatorUnbondingLock(addr, toString(unbondingLockId));

                      case 10:
                        unbond = _context31.sent;
                        result.push(unbond);
                        unbondingLockId -= 1;
                        _context31.next = 7;
                        break;

                      case 15:
                        return _context31.abrupt("return", result);

                      case 16:
                      case "end":
                        return _context31.stop();
                    }
                  }
                }, _callee31);
              }));

              function getDelegatorUnbondingLocks(_x22) {
                return _getDelegatorUnbondingLocks.apply(this, arguments);
              }

              return getDelegatorUnbondingLocks;
            }()), (0, _defineProperty2.default)(_rpc, "getDelegatorUnbondingLock", function () {
              var _getDelegatorUnbondingLock = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee32(addr, unbondingLockId) {
                var lock, amount, withdrawRound;
                return _regenerator.default.wrap(function _callee32$(_context32) {
                  while (1) {
                    switch (_context32.prev = _context32.next) {
                      case 0:
                        _context32.next = 2;
                        return BondingManager.getDelegatorUnbondingLock(addr, unbondingLockId);

                      case 2:
                        lock = _context32.sent;
                        amount = toString(lock.amount);
                        withdrawRound = toString(lock.withdrawRound);
                        return _context32.abrupt("return", {
                          id: unbondingLockId,
                          delegator: addr,
                          amount: amount,
                          withdrawRound: withdrawRound
                        });

                      case 6:
                      case "end":
                        return _context32.stop();
                    }
                  }
                }, _callee32);
              }));

              function getDelegatorUnbondingLock(_x23, _x24) {
                return _getDelegatorUnbondingLock.apply(this, arguments);
              }

              return getDelegatorUnbondingLock;
            }()), (0, _defineProperty2.default)(_rpc, "rebond", function () {
              var _rebond = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee33(unbondingLockId) {
                var tx,
                    _args33 = arguments;
                return _regenerator.default.wrap(function _callee33$(_context33) {
                  while (1) {
                    switch (_context33.prev = _context33.next) {
                      case 0:
                        tx = _args33.length > 1 && _args33[1] !== undefined ? _args33[1] : config.defaultTx;
                        _context33.t0 = utils;
                        _context33.next = 4;
                        return BondingManager.rebond(unbondingLockId, (0, _objectSpread4.default)({}, config.defaultTx, tx));

                      case 4:
                        _context33.t1 = _context33.sent;
                        _context33.t2 = config.eth;
                        _context33.next = 8;
                        return _context33.t0.getTxReceipt.call(_context33.t0, _context33.t1, _context33.t2);

                      case 8:
                        return _context33.abrupt("return", _context33.sent);

                      case 9:
                      case "end":
                        return _context33.stop();
                    }
                  }
                }, _callee33);
              }));

              function rebond(_x25) {
                return _rebond.apply(this, arguments);
              }

              return rebond;
            }()), (0, _defineProperty2.default)(_rpc, "rebondFromUnbonded", function () {
              var _rebondFromUnbonded = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee34(addr, unbondingLockId) {
                var tx,
                    _args34 = arguments;
                return _regenerator.default.wrap(function _callee34$(_context34) {
                  while (1) {
                    switch (_context34.prev = _context34.next) {
                      case 0:
                        tx = _args34.length > 2 && _args34[2] !== undefined ? _args34[2] : config.defaultTx;
                        _context34.t0 = utils;
                        _context34.next = 4;
                        return BondingManager.rebondFromUnbonded(addr, unbondingLockId, (0, _objectSpread4.default)({}, config.defaultTx, tx));

                      case 4:
                        _context34.t1 = _context34.sent;
                        _context34.t2 = config.eth;
                        _context34.next = 8;
                        return _context34.t0.getTxReceipt.call(_context34.t0, _context34.t1, _context34.t2);

                      case 8:
                        return _context34.abrupt("return", _context34.sent);

                      case 9:
                      case "end":
                        return _context34.stop();
                    }
                  }
                }, _callee34);
              }));

              function rebondFromUnbonded(_x26, _x27) {
                return _rebondFromUnbonded.apply(this, arguments);
              }

              return rebondFromUnbonded;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoderIsActive", function () {
              var _getTranscoderIsActive = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee35(addr) {
                return _regenerator.default.wrap(function _callee35$(_context35) {
                  while (1) {
                    switch (_context35.prev = _context35.next) {
                      case 0:
                        _context35.t0 = headToBool;
                        _context35.t1 = BondingManager;
                        _context35.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context35.t2 = _context35.sent;
                        _context35.next = 7;
                        return rpc.getCurrentRound();

                      case 7:
                        _context35.t3 = _context35.sent;
                        _context35.next = 10;
                        return _context35.t1.isActiveTranscoder.call(_context35.t1, _context35.t2, _context35.t3);

                      case 10:
                        _context35.t4 = _context35.sent;
                        return _context35.abrupt("return", (0, _context35.t0)(_context35.t4));

                      case 12:
                      case "end":
                        return _context35.stop();
                    }
                  }
                }, _callee35);
              }));

              function getTranscoderIsActive(_x28) {
                return _getTranscoderIsActive.apply(this, arguments);
              }

              return getTranscoderIsActive;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoderStatus", function () {
              var _getTranscoderStatus = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee36(addr) {
                var status;
                return _regenerator.default.wrap(function _callee36$(_context36) {
                  while (1) {
                    switch (_context36.prev = _context36.next) {
                      case 0:
                        _context36.t0 = headToString;
                        _context36.t1 = BondingManager;
                        _context36.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context36.t2 = _context36.sent;
                        _context36.next = 7;
                        return _context36.t1.transcoderStatus.call(_context36.t1, _context36.t2);

                      case 7:
                        _context36.t3 = _context36.sent;
                        status = (0, _context36.t0)(_context36.t3);
                        return _context36.abrupt("return", TRANSCODER_STATUS[status]);

                      case 10:
                      case "end":
                        return _context36.stop();
                    }
                  }
                }, _callee36);
              }));

              function getTranscoderStatus(_x29) {
                return _getTranscoderStatus.apply(this, arguments);
              }

              return getTranscoderStatus;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoderTotalStake", function () {
              var _getTranscoderTotalStake = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee37(addr) {
                return _regenerator.default.wrap(function _callee37$(_context37) {
                  while (1) {
                    switch (_context37.prev = _context37.next) {
                      case 0:
                        _context37.t0 = headToString;
                        _context37.t1 = BondingManager;
                        _context37.next = 4;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 4:
                        _context37.t2 = _context37.sent;
                        _context37.next = 7;
                        return _context37.t1.transcoderTotalStake.call(_context37.t1, _context37.t2);

                      case 7:
                        _context37.t3 = _context37.sent;
                        return _context37.abrupt("return", (0, _context37.t0)(_context37.t3));

                      case 9:
                      case "end":
                        return _context37.stop();
                    }
                  }
                }, _callee37);
              }));

              function getTranscoderTotalStake(_x30) {
                return _getTranscoderTotalStake.apply(this, arguments);
              }

              return getTranscoderTotalStake;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoderPoolMaxSize", function () {
              var _getTranscoderPoolMaxSize = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee38() {
                return _regenerator.default.wrap(function _callee38$(_context38) {
                  while (1) {
                    switch (_context38.prev = _context38.next) {
                      case 0:
                        _context38.t0 = headToString;
                        _context38.next = 3;
                        return BondingManager.getTranscoderPoolMaxSize();

                      case 3:
                        _context38.t1 = _context38.sent;
                        return _context38.abrupt("return", (0, _context38.t0)(_context38.t1));

                      case 5:
                      case "end":
                        return _context38.stop();
                    }
                  }
                }, _callee38);
              }));

              function getTranscoderPoolMaxSize() {
                return _getTranscoderPoolMaxSize.apply(this, arguments);
              }

              return getTranscoderPoolMaxSize;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoder", function () {
              var _getTranscoder = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee39(addr) {
                var address, status, active, totalStake, t, feeShare, lastRewardRound, pendingFeeShare, pendingPricePerSegment, pendingRewardCut, pricePerSegment, rewardCut;
                return _regenerator.default.wrap(function _callee39$(_context39) {
                  while (1) {
                    switch (_context39.prev = _context39.next) {
                      case 0:
                        _context39.next = 2;
                        return resolveAddress(rpc.getENSAddress, addr);

                      case 2:
                        address = _context39.sent;
                        _context39.next = 5;
                        return rpc.getTranscoderStatus(address);

                      case 5:
                        status = _context39.sent;
                        _context39.next = 8;
                        return rpc.getTranscoderIsActive(address);

                      case 8:
                        active = _context39.sent;
                        _context39.next = 11;
                        return rpc.getTranscoderTotalStake(address);

                      case 11:
                        totalStake = _context39.sent;
                        _context39.next = 14;
                        return BondingManager.getTranscoder(address);

                      case 14:
                        t = _context39.sent;
                        feeShare = toString(t.feeShare);
                        lastRewardRound = toString(t.lastRewardRound);
                        pendingFeeShare = toString(t.pendingFeeShare);
                        pendingPricePerSegment = toString(t.pendingPricePerSegment);
                        pendingRewardCut = toString(t.pendingRewardCut);
                        pricePerSegment = toString(t.pricePerSegment);
                        rewardCut = toString(t.rewardCut);
                        return _context39.abrupt("return", {
                          active: active,
                          address: address,
                          feeShare: feeShare,
                          lastRewardRound: lastRewardRound,
                          pricePerSegment: pricePerSegment,
                          pendingRewardCut: pendingRewardCut,
                          pendingFeeShare: pendingFeeShare,
                          pendingPricePerSegment: pendingPricePerSegment,
                          rewardCut: rewardCut,
                          status: status,
                          totalStake: totalStake
                        });

                      case 23:
                      case "end":
                        return _context39.stop();
                    }
                  }
                }, _callee39);
              }));

              function getTranscoder(_x31) {
                return _getTranscoder.apply(this, arguments);
              }

              return getTranscoder;
            }()), (0, _defineProperty2.default)(_rpc, "getTranscoders", function () {
              var _getTranscoders = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee40() {
                var transcoders, addr, transcoder;
                return _regenerator.default.wrap(function _callee40$(_context40) {
                  while (1) {
                    switch (_context40.prev = _context40.next) {
                      case 0:
                        transcoders = [];
                        _context40.t0 = headToString;
                        _context40.next = 4;
                        return BondingManager.getFirstTranscoderInPool();

                      case 4:
                        _context40.t1 = _context40.sent;
                        addr = (0, _context40.t0)(_context40.t1);

                      case 6:
                        if (!(addr !== EMPTY_ADDRESS)) {
                          _context40.next = 18;
                          break;
                        }

                        _context40.next = 9;
                        return rpc.getTranscoder(addr);

                      case 9:
                        transcoder = _context40.sent;
                        transcoders.push(transcoder);
                        _context40.t2 = headToString;
                        _context40.next = 14;
                        return BondingManager.getNextTranscoderInPool(addr);

                      case 14:
                        _context40.t3 = _context40.sent;
                        addr = (0, _context40.t2)(_context40.t3);
                        _context40.next = 6;
                        break;

                      case 18:
                        return _context40.abrupt("return", transcoders);

                      case 19:
                      case "end":
                        return _context40.stop();
                    }
                  }
                }, _callee40);
              }));

              function getTranscoders() {
                return _getTranscoders.apply(this, arguments);
              }

              return getTranscoders;
            }()), (0, _defineProperty2.default)(_rpc, "getProtocolPaused", function () {
              var _getProtocolPaused = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee41() {
                return _regenerator.default.wrap(function _callee41$(_context41) {
                  while (1) {
                    switch (_context41.prev = _context41.next) {
                      case 0:
                        _context41.t0 = headToBool;
                        _context41.next = 3;
                        return Controller.paused();

                      case 3:
                        _context41.t1 = _context41.sent;
                        return _context41.abrupt("return", (0, _context41.t0)(_context41.t1));

                      case 5:
                      case "end":
                        return _context41.stop();
                    }
                  }
                }, _callee41);
              }));

              function getProtocolPaused() {
                return _getProtocolPaused.apply(this, arguments);
              }

              return getProtocolPaused;
            }()), (0, _defineProperty2.default)(_rpc, "getProtocol", function () {
              var _getProtocol = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee42() {
                var paused, totalTokenSupply, totalBondedToken, targetBondingRate, transcoderPoolMaxSize, maxEarningsClaimsRounds;
                return _regenerator.default.wrap(function _callee42$(_context42) {
                  while (1) {
                    switch (_context42.prev = _context42.next) {
                      case 0:
                        _context42.next = 2;
                        return rpc.getProtocolPaused();

                      case 2:
                        paused = _context42.sent;
                        _context42.next = 5;
                        return rpc.getTokenTotalSupply();

                      case 5:
                        totalTokenSupply = _context42.sent;
                        _context42.next = 8;
                        return rpc.getTotalBonded();

                      case 8:
                        totalBondedToken = _context42.sent;
                        _context42.next = 11;
                        return rpc.getTargetBondingRate();

                      case 11:
                        targetBondingRate = _context42.sent;
                        _context42.next = 14;
                        return rpc.getTranscoderPoolMaxSize();

                      case 14:
                        transcoderPoolMaxSize = _context42.sent;
                        _context42.next = 17;
                        return rpc.getMaxEarningsClaimsRounds();

                      case 17:
                        maxEarningsClaimsRounds = _context42.sent;
                        return _context42.abrupt("return", {
                          paused: paused,
                          totalTokenSupply: totalTokenSupply,
                          totalBondedToken: totalBondedToken,
                          targetBondingRate: targetBondingRate,
                          transcoderPoolMaxSize: transcoderPoolMaxSize,
                          maxEarningsClaimsRounds: maxEarningsClaimsRounds
                        });

                      case 19:
                      case "end":
                        return _context42.stop();
                    }
                  }
                }, _callee42);
              }));

              function getProtocol() {
                return _getProtocol.apply(this, arguments);
              }

              return getProtocol;
            }()), (0, _defineProperty2.default)(_rpc, "getRoundLength", function () {
              var _getRoundLength = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee43() {
                return _regenerator.default.wrap(function _callee43$(_context43) {
                  while (1) {
                    switch (_context43.prev = _context43.next) {
                      case 0:
                        _context43.t0 = headToString;
                        _context43.next = 3;
                        return RoundsManager.roundLength();

                      case 3:
                        _context43.t1 = _context43.sent;
                        return _context43.abrupt("return", (0, _context43.t0)(_context43.t1));

                      case 5:
                      case "end":
                        return _context43.stop();
                    }
                  }
                }, _callee43);
              }));

              function getRoundLength() {
                return _getRoundLength.apply(this, arguments);
              }

              return getRoundLength;
            }()), (0, _defineProperty2.default)(_rpc, "getRoundsPerYear", function () {
              var _getRoundsPerYear = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee44() {
                return _regenerator.default.wrap(function _callee44$(_context44) {
                  while (1) {
                    switch (_context44.prev = _context44.next) {
                      case 0:
                        _context44.t0 = headToString;
                        _context44.next = 3;
                        return RoundsManager.roundsPerYear();

                      case 3:
                        _context44.t1 = _context44.sent;
                        return _context44.abrupt("return", (0, _context44.t0)(_context44.t1));

                      case 5:
                      case "end":
                        return _context44.stop();
                    }
                  }
                }, _callee44);
              }));

              function getRoundsPerYear() {
                return _getRoundsPerYear.apply(this, arguments);
              }

              return getRoundsPerYear;
            }()), (0, _defineProperty2.default)(_rpc, "getCurrentRound", function () {
              var _getCurrentRound = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee45() {
                return _regenerator.default.wrap(function _callee45$(_context45) {
                  while (1) {
                    switch (_context45.prev = _context45.next) {
                      case 0:
                        _context45.t0 = headToString;
                        _context45.next = 3;
                        return RoundsManager.currentRound();

                      case 3:
                        _context45.t1 = _context45.sent;
                        return _context45.abrupt("return", (0, _context45.t0)(_context45.t1));

                      case 5:
                      case "end":
                        return _context45.stop();
                    }
                  }
                }, _callee45);
              }));

              function getCurrentRound() {
                return _getCurrentRound.apply(this, arguments);
              }

              return getCurrentRound;
            }()), (0, _defineProperty2.default)(_rpc, "getCurrentRoundIsInitialized", function () {
              var _getCurrentRoundIsInitialized = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee46() {
                return _regenerator.default.wrap(function _callee46$(_context46) {
                  while (1) {
                    switch (_context46.prev = _context46.next) {
                      case 0:
                        _context46.t0 = headToBool;
                        _context46.next = 3;
                        return RoundsManager.currentRoundInitialized();

                      case 3:
                        _context46.t1 = _context46.sent;
                        return _context46.abrupt("return", (0, _context46.t0)(_context46.t1));

                      case 5:
                      case "end":
                        return _context46.stop();
                    }
                  }
                }, _callee46);
              }));

              function getCurrentRoundIsInitialized() {
                return _getCurrentRoundIsInitialized.apply(this, arguments);
              }

              return getCurrentRoundIsInitialized;
            }()), (0, _defineProperty2.default)(_rpc, "getCurrentRoundStartBlock", function () {
              var _getCurrentRoundStartBlock = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee47() {
                return _regenerator.default.wrap(function _callee47$(_context47) {
                  while (1) {
                    switch (_context47.prev = _context47.next) {
                      case 0:
                        _context47.t0 = headToString;
                        _context47.next = 3;
                        return RoundsManager.currentRoundStartBlock();

                      case 3:
                        _context47.t1 = _context47.sent;
                        return _context47.abrupt("return", (0, _context47.t0)(_context47.t1));

                      case 5:
                      case "end":
                        return _context47.stop();
                    }
                  }
                }, _callee47);
              }));

              function getCurrentRoundStartBlock() {
                return _getCurrentRoundStartBlock.apply(this, arguments);
              }

              return getCurrentRoundStartBlock;
            }()), (0, _defineProperty2.default)(_rpc, "getLastInitializedRound", function () {
              var _getLastInitializedRound = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee48() {
                return _regenerator.default.wrap(function _callee48$(_context48) {
                  while (1) {
                    switch (_context48.prev = _context48.next) {
                      case 0:
                        _context48.t0 = headToString;
                        _context48.next = 3;
                        return RoundsManager.lastInitializedRound();

                      case 3:
                        _context48.t1 = _context48.sent;
                        return _context48.abrupt("return", (0, _context48.t0)(_context48.t1));

                      case 5:
                      case "end":
                        return _context48.stop();
                    }
                  }
                }, _callee48);
              }));

              function getLastInitializedRound() {
                return _getLastInitializedRound.apply(this, arguments);
              }

              return getLastInitializedRound;
            }()), (0, _defineProperty2.default)(_rpc, "getCurrentRoundInfo", function () {
              var _getCurrentRoundInfo = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee49() {
                var length, id, initialized, lastInitializedRound, startBlock;
                return _regenerator.default.wrap(function _callee49$(_context49) {
                  while (1) {
                    switch (_context49.prev = _context49.next) {
                      case 0:
                        _context49.next = 2;
                        return rpc.getRoundLength();

                      case 2:
                        length = _context49.sent;
                        _context49.next = 5;
                        return rpc.getCurrentRound();

                      case 5:
                        id = _context49.sent;
                        _context49.next = 8;
                        return rpc.getCurrentRoundIsInitialized();

                      case 8:
                        initialized = _context49.sent;
                        _context49.next = 11;
                        return rpc.getLastInitializedRound();

                      case 11:
                        lastInitializedRound = _context49.sent;
                        _context49.next = 14;
                        return rpc.getCurrentRoundStartBlock();

                      case 14:
                        startBlock = _context49.sent;
                        return _context49.abrupt("return", {
                          id: id,
                          initialized: initialized,
                          lastInitializedRound: lastInitializedRound,
                          length: length,
                          startBlock: startBlock
                        });

                      case 16:
                      case "end":
                        return _context49.stop();
                    }
                  }
                }, _callee49);
              }));

              function getCurrentRoundInfo() {
                return _getCurrentRoundInfo.apply(this, arguments);
              }

              return getCurrentRoundInfo;
            }()), (0, _defineProperty2.default)(_rpc, "getTotalJobs", function () {
              var _getTotalJobs = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee50() {
                return _regenerator.default.wrap(function _callee50$(_context50) {
                  while (1) {
                    switch (_context50.prev = _context50.next) {
                      case 0:
                        _context50.t0 = headToString;
                        _context50.next = 3;
                        return JobsManager.numJobs();

                      case 3:
                        _context50.t1 = _context50.sent;
                        return _context50.abrupt("return", (0, _context50.t0)(_context50.t1));

                      case 5:
                      case "end":
                        return _context50.stop();
                    }
                  }
                }, _callee50);
              }));

              function getTotalJobs() {
                return _getTotalJobs.apply(this, arguments);
              }

              return getTotalJobs;
            }()), (0, _defineProperty2.default)(_rpc, "getJobVerificationRate", function () {
              var _getJobVerificationRate = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee51() {
                return _regenerator.default.wrap(function _callee51$(_context51) {
                  while (1) {
                    switch (_context51.prev = _context51.next) {
                      case 0:
                        _context51.t0 = headToString;
                        _context51.next = 3;
                        return JobsManager.verificationRate();

                      case 3:
                        _context51.t1 = _context51.sent;
                        return _context51.abrupt("return", (0, _context51.t0)(_context51.t1));

                      case 5:
                      case "end":
                        return _context51.stop();
                    }
                  }
                }, _callee51);
              }));

              function getJobVerificationRate() {
                return _getJobVerificationRate.apply(this, arguments);
              }

              return getJobVerificationRate;
            }()), (0, _defineProperty2.default)(_rpc, "getJobVerificationPeriod", function () {
              var _getJobVerificationPeriod = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee52() {
                return _regenerator.default.wrap(function _callee52$(_context52) {
                  while (1) {
                    switch (_context52.prev = _context52.next) {
                      case 0:
                        _context52.t0 = headToString;
                        _context52.next = 3;
                        return JobsManager.verificationPeriod();

                      case 3:
                        _context52.t1 = _context52.sent;
                        return _context52.abrupt("return", (0, _context52.t0)(_context52.t1));

                      case 5:
                      case "end":
                        return _context52.stop();
                    }
                  }
                }, _callee52);
              }));

              function getJobVerificationPeriod() {
                return _getJobVerificationPeriod.apply(this, arguments);
              }

              return getJobVerificationPeriod;
            }()), (0, _defineProperty2.default)(_rpc, "getJobVerificationSlashingPeriod", function () {
              var _getJobVerificationSlashingPeriod = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee53() {
                return _regenerator.default.wrap(function _callee53$(_context53) {
                  while (1) {
                    switch (_context53.prev = _context53.next) {
                      case 0:
                        _context53.t0 = headToString;
                        _context53.next = 3;
                        return JobsManager.verificationSlashingPeriod();

                      case 3:
                        _context53.t1 = _context53.sent;
                        return _context53.abrupt("return", (0, _context53.t0)(_context53.t1));

                      case 5:
                      case "end":
                        return _context53.stop();
                    }
                  }
                }, _callee53);
              }));

              function getJobVerificationSlashingPeriod() {
                return _getJobVerificationSlashingPeriod.apply(this, arguments);
              }

              return getJobVerificationSlashingPeriod;
            }()), (0, _defineProperty2.default)(_rpc, "getJobFinderFee", function () {
              var _getJobFinderFee = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee54() {
                return _regenerator.default.wrap(function _callee54$(_context54) {
                  while (1) {
                    switch (_context54.prev = _context54.next) {
                      case 0:
                        _context54.t0 = headToString;
                        _context54.next = 3;
                        return JobsManager.finderFee();

                      case 3:
                        _context54.t1 = _context54.sent;
                        return _context54.abrupt("return", (0, _context54.t0)(_context54.t1));

                      case 5:
                      case "end":
                        return _context54.stop();
                    }
                  }
                }, _callee54);
              }));

              function getJobFinderFee() {
                return _getJobFinderFee.apply(this, arguments);
              }

              return getJobFinderFee;
            }()), (0, _defineProperty2.default)(_rpc, "getJobsInfo", function () {
              var _getJobsInfo = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee55() {
                var total, verificationRate, verificationPeriod, verificationSlashingPeriod, finderFee;
                return _regenerator.default.wrap(function _callee55$(_context55) {
                  while (1) {
                    switch (_context55.prev = _context55.next) {
                      case 0:
                        _context55.next = 2;
                        return rpc.getTotalJobs();

                      case 2:
                        total = _context55.sent;
                        _context55.next = 5;
                        return rpc.getJobVerificationRate();

                      case 5:
                        verificationRate = _context55.sent;
                        _context55.next = 8;
                        return rpc.getJobVerificationPeriod();

                      case 8:
                        verificationPeriod = _context55.sent;
                        _context55.next = 11;
                        return rpc.getJobVerificationSlashingPeriod();

                      case 11:
                        verificationSlashingPeriod = _context55.sent;
                        _context55.next = 14;
                        return rpc.getJobFinderFee();

                      case 14:
                        finderFee = _context55.sent;
                        return _context55.abrupt("return", {
                          total: total,
                          verificationRate: verificationRate,
                          verificationPeriod: verificationPeriod,
                          verificationSlashingPeriod: verificationSlashingPeriod,
                          finderFee: finderFee
                        });

                      case 16:
                      case "end":
                        return _context55.stop();
                    }
                  }
                }, _callee55);
              }));

              function getJobsInfo() {
                return _getJobsInfo.apply(this, arguments);
              }

              return getJobsInfo;
            }()), (0, _defineProperty2.default)(_rpc, "getJob", function () {
              var _getJob = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee56(id) {
                var x, _ref19, hash;

                return _regenerator.default.wrap(function _callee56$(_context56) {
                  while (1) {
                    switch (_context56.prev = _context56.next) {
                      case 0:
                        _context56.next = 2;
                        return JobsManager.getJob(id);

                      case 2:
                        x = _context56.sent;

                        if (!(x.transcoderAddress === null || x.transcoderAddress === EMPTY_ADDRESS)) {
                          _context56.next = 13;
                          break;
                        }

                        _context56.next = 6;
                        return rpc.getBlock(x.creationBlock);

                      case 6:
                        _ref19 = _context56.sent;
                        hash = _ref19.hash;
                        _context56.t0 = headToString;
                        _context56.next = 11;
                        return BondingManager.electActiveTranscoder(x.maxPricePerSegment, hash, x.creationRound);

                      case 11:
                        _context56.t1 = _context56.sent;
                        x.transcoderAddress = (0, _context56.t0)(_context56.t1);

                      case 13:
                        return _context56.abrupt("return", {
                          id: toString(id),
                          streamId: x.streamId,
                          transcodingOptions: utils.parseTranscodingOptions(x.transcodingOptions),
                          transcoder: x.transcoderAddress,
                          broadcaster: x.broadcasterAddress
                        });

                      case 14:
                      case "end":
                        return _context56.stop();
                    }
                  }
                }, _callee56);
              }));

              function getJob(_x32) {
                return _getJob.apply(this, arguments);
              }

              return getJob;
            }()), (0, _defineProperty2.default)(_rpc, "getJobs", function () {
              var _getJobs = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee57() {
                var _ref20,
                    to,
                    from,
                    _ref20$blocksAgo,
                    blocksAgo,
                    filters,
                    event,
                    head,
                    fromBlock,
                    toBlock,
                    topics,
                    params,
                    results,
                    _args57 = arguments;

                return _regenerator.default.wrap(function _callee57$(_context57) {
                  while (1) {
                    switch (_context57.prev = _context57.next) {
                      case 0:
                        _ref20 = _args57.length > 0 && _args57[0] !== undefined ? _args57[0] : {};
                        to = _ref20.to, from = _ref20.from, _ref20$blocksAgo = _ref20.blocksAgo, blocksAgo = _ref20$blocksAgo === void 0 ? 100 * 10000 : _ref20$blocksAgo, filters = (0, _objectWithoutProperties2.default)(_ref20, ["to", "from", "blocksAgo"]);
                        event = events.NewJob;
                        _context57.next = 5;
                        return config.eth.blockNumber();

                      case 5:
                        head = _context57.sent;
                        fromBlock = from || Math.max(0, head - blocksAgo);
                        toBlock = to || 'latest';
                        _context57.t0 = utils;
                        _context57.t1 = event;

                        if (!filters.broadcaster) {
                          _context57.next = 21;
                          break;
                        }

                        _context57.t3 = _objectSpread4.default;
                        _context57.t4 = {};
                        _context57.t5 = filters;
                        _context57.next = 16;
                        return resolveAddress(rpc.getENSAddress, filters.broadcaster);

                      case 16:
                        _context57.t6 = _context57.sent;
                        _context57.t7 = {
                          broadcaster: _context57.t6
                        };
                        _context57.t2 = (0, _context57.t3)(_context57.t4, _context57.t5, _context57.t7);
                        _context57.next = 22;
                        break;

                      case 21:
                        _context57.t2 = filters;

                      case 22:
                        _context57.t8 = _context57.t2;
                        topics = _context57.t0.encodeEventTopics.call(_context57.t0, _context57.t1, _context57.t8);
                        params = {
                          address: JobsManager.address,
                          fromBlock: fromBlock,
                          toBlock: toBlock,
                          topics: topics
                        };
                        _context57.next = 27;
                        return config.eth.getLogs(params);

                      case 27:
                        _context57.t9 = compose(function (x) {
                          return {
                            id: toString(x.jobId),
                            streamId: x.streamId,
                            transcodingOptions: utils.parseTranscodingOptions(x.transcodingOptions),
                            broadcaster: x.broadcaster
                          };
                        }, utils.decodeEvent(event));
                        results = _context57.sent.map(_context57.t9);
                        return _context57.abrupt("return", results.reverse());

                      case 30:
                      case "end":
                        return _context57.stop();
                    }
                  }
                }, _callee57);
              }));

              function getJobs() {
                return _getJobs.apply(this, arguments);
              }

              return getJobs;
            }()), (0, _defineProperty2.default)(_rpc, "tapFaucet", function () {
              var _tapFaucet = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee58() {
                var tx,
                    _args58 = arguments;
                return _regenerator.default.wrap(function _callee58$(_context58) {
                  while (1) {
                    switch (_context58.prev = _context58.next) {
                      case 0:
                        tx = _args58.length > 0 && _args58[0] !== undefined ? _args58[0] : config.defaultTx;
                        _context58.t0 = utils;
                        _context58.next = 4;
                        return LivepeerTokenFaucet.request(tx);

                      case 4:
                        _context58.t1 = _context58.sent;
                        _context58.t2 = config.eth;
                        _context58.next = 8;
                        return _context58.t0.getTxReceipt.call(_context58.t0, _context58.t1, _context58.t2);

                      case 8:
                        return _context58.abrupt("return", _context58.sent);

                      case 9:
                      case "end":
                        return _context58.stop();
                    }
                  }
                }, _callee58);
              }));

              function tapFaucet() {
                return _tapFaucet.apply(this, arguments);
              }

              return tapFaucet;
            }()), (0, _defineProperty2.default)(_rpc, "initializeRound", function () {
              var _initializeRound = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee59() {
                var tx,
                    _args59 = arguments;
                return _regenerator.default.wrap(function _callee59$(_context59) {
                  while (1) {
                    switch (_context59.prev = _context59.next) {
                      case 0:
                        tx = _args59.length > 0 && _args59[0] !== undefined ? _args59[0] : config.defaultTx;
                        _context59.prev = 1;
                        _context59.t0 = utils;
                        _context59.next = 5;
                        return RoundsManager.initializeRound(tx);

                      case 5:
                        _context59.t1 = _context59.sent;
                        _context59.t2 = config.eth;
                        _context59.next = 9;
                        return _context59.t0.getTxReceipt.call(_context59.t0, _context59.t1, _context59.t2);

                      case 9:
                        return _context59.abrupt("return", _context59.sent);

                      case 12:
                        _context59.prev = 12;
                        _context59.t3 = _context59["catch"](1);
                        _context59.t3.message = 'Error: initializeRound\n' + _context59.t3.message;
                        throw _context59.t3;

                      case 16:
                      case "end":
                        return _context59.stop();
                    }
                  }
                }, _callee59, null, [[1, 12]]);
              }));

              function initializeRound() {
                return _initializeRound.apply(this, arguments);
              }

              return initializeRound;
            }()), (0, _defineProperty2.default)(_rpc, "claimEarnings", function () {
              var _claimEarnings = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee60(endRound) {
                var tx,
                    _args60 = arguments;
                return _regenerator.default.wrap(function _callee60$(_context60) {
                  while (1) {
                    switch (_context60.prev = _context60.next) {
                      case 0:
                        tx = _args60.length > 1 && _args60[1] !== undefined ? _args60[1] : config.defaultTx;
                        _context60.next = 3;
                        return BondingManager.claimEarnings(endRound, (0, _objectSpread4.default)({}, config.defaultTx, tx));

                      case 3:
                        return _context60.abrupt("return", _context60.sent);

                      case 4:
                      case "end":
                        return _context60.stop();
                    }
                  }
                }, _callee60);
              }));

              function claimEarnings(_x33) {
                return _claimEarnings.apply(this, arguments);
              }

              return claimEarnings;
            }()), (0, _defineProperty2.default)(_rpc, "approveTokenBondAmount", function () {
              var _approveTokenBondAmount = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee61(amount, tx) {
                var token;
                return _regenerator.default.wrap(function _callee61$(_context61) {
                  while (1) {
                    switch (_context61.prev = _context61.next) {
                      case 0:
                        token = toBN(amount); // TODO: - check token balance

                        _context61.t0 = utils;
                        _context61.next = 4;
                        return LivepeerToken.approve(BondingManager.address, token, (0, _objectSpread4.default)({}, config.defaultTx, tx));

                      case 4:
                        _context61.t1 = _context61.sent;
                        _context61.t2 = config.eth;
                        _context61.next = 8;
                        return _context61.t0.getTxReceipt.call(_context61.t0, _context61.t1, _context61.t2);

                      case 8:
                      case "end":
                        return _context61.stop();
                    }
                  }
                }, _callee61);
              }));

              function approveTokenBondAmount(_x34, _x35) {
                return _approveTokenBondAmount.apply(this, arguments);
              }

              return approveTokenBondAmount;
            }()), (0, _defineProperty2.default)(_rpc, "bondApprovedTokenAmount", function () {
              var _bondApprovedTokenAmount = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee62(to, amount, tx) {
                var token;
                return _regenerator.default.wrap(function _callee62$(_context62) {
                  while (1) {
                    switch (_context62.prev = _context62.next) {
                      case 0:
                        token = toBN(amount); // TODO: check for existing approval / round initialization / token balance

                        _context62.t0 = utils;
                        _context62.t1 = BondingManager;
                        _context62.t2 = token;
                        _context62.next = 6;
                        return resolveAddress(rpc.getENSAddress, to);

                      case 6:
                        _context62.t3 = _context62.sent;
                        _context62.t4 = (0, _objectSpread4.default)({}, config.defaultTx, tx);
                        _context62.next = 10;
                        return _context62.t1.bond.call(_context62.t1, _context62.t2, _context62.t3, _context62.t4);

                      case 10:
                        _context62.t5 = _context62.sent;
                        _context62.t6 = config.eth;
                        _context62.next = 14;
                        return _context62.t0.getTxReceipt.call(_context62.t0, _context62.t5, _context62.t6);

                      case 14:
                        return _context62.abrupt("return", _context62.sent);

                      case 15:
                      case "end":
                        return _context62.stop();
                    }
                  }
                }, _callee62);
              }));

              function bondApprovedTokenAmount(_x36, _x37, _x38) {
                return _bondApprovedTokenAmount.apply(this, arguments);
              }

              return bondApprovedTokenAmount;
            }()), (0, _defineProperty2.default)(_rpc, "estimateGas", function () {
              var _estimateGas = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee63(contractName, methodName, methodArgs) {
                var tx,
                    gasRate,
                    contractABI,
                    methodABI,
                    encodedData,
                    _args63 = arguments;
                return _regenerator.default.wrap(function _callee63$(_context63) {
                  while (1) {
                    switch (_context63.prev = _context63.next) {
                      case 0:
                        tx = _args63.length > 3 && _args63[3] !== undefined ? _args63[3] : config.defaultTx;
                        tx.value = tx.value ? tx.value : '0';
                        gasRate = 1.2;
                        contractABI = config.abis[contractName];
                        methodABI = utils.findAbiByName(contractABI, methodName);
                        encodedData = utils.encodeMethodParams(methodABI, methodArgs);
                        _context63.t0 = Math;
                        _context63.t1 = toNumber;
                        _context63.next = 10;
                        return config.eth.estimateGas({
                          to: config.contracts[contractName].address,
                          from: config.defaultTx.from,
                          value: tx.value,
                          data: encodedData
                        });

                      case 10:
                        _context63.t2 = _context63.sent;
                        _context63.t3 = (0, _context63.t1)(_context63.t2);
                        _context63.t4 = gasRate;
                        _context63.t5 = _context63.t3 * _context63.t4;
                        return _context63.abrupt("return", _context63.t0.round.call(_context63.t0, _context63.t5));

                      case 15:
                      case "end":
                        return _context63.stop();
                    }
                  }
                }, _callee63);
              }));

              function estimateGas(_x39, _x40, _x41) {
                return _estimateGas.apply(this, arguments);
              }

              return estimateGas;
            }()), (0, _defineProperty2.default)(_rpc, "unbond", function () {
              var _unbond = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee64(amount) {
                var tx,
                    _ref21,
                    status,
                    pendingStake,
                    bondedAmount,
                    totalStake,
                    _args64 = arguments;

                return _regenerator.default.wrap(function _callee64$(_context64) {
                  while (1) {
                    switch (_context64.prev = _context64.next) {
                      case 0:
                        tx = _args64.length > 1 && _args64[1] !== undefined ? _args64[1] : config.defaultTx;
                        _context64.next = 3;
                        return rpc.getDelegator(tx.from);

                      case 3:
                        _ref21 = _context64.sent;
                        status = _ref21.status;
                        pendingStake = _ref21.pendingStake;
                        bondedAmount = _ref21.bondedAmount;
                        // pendingStake = 0 if delegator has claimed earnings through the current round
                        // In this case, bondedAmount is up to date
                        totalStake = toBN(pendingStake).cmp(toBN(bondedAmount)) < 0 ? bondedAmount : pendingStake; // Only unbond if amount doesn't exceed your current stake

                        if (!(toBN(totalStake).cmp(toBN(amount)) < 0)) {
                          _context64.next = 10;
                          break;
                        }

                        throw new Error("Cannot unbond a portion of tokens greater than your total stake of ".concat(totalStake, " LPT"));

                      case 10:
                        // Unbond total stake if a zero or negative amount is passed
                        amount = amount <= 0 ? totalStake : amount; // Can only unbond successfully when not already "Unbonded"

                        if (!(status === DELEGATOR_STATUS.Unbonded)) {
                          _context64.next = 13;
                          break;
                        }

                        throw new Error('This account is already unbonded.');

                      case 13:
                        _context64.next = 15;
                        return rpc.estimateGas('BondingManager', 'unbond', [amount]);

                      case 15:
                        tx.gas = _context64.sent;
                        _context64.t0 = utils;
                        _context64.next = 19;
                        return BondingManager.unbond(amount, (0, _objectSpread4.default)({}, config.defaultTx, tx));

                      case 19:
                        _context64.t1 = _context64.sent;
                        _context64.t2 = config.eth;
                        _context64.next = 23;
                        return _context64.t0.getTxReceipt.call(_context64.t0, _context64.t1, _context64.t2);

                      case 23:
                        return _context64.abrupt("return", _context64.sent);

                      case 24:
                      case "end":
                        return _context64.stop();
                    }
                  }
                }, _callee64);
              }));

              function unbond(_x42) {
                return _unbond.apply(this, arguments);
              }

              return unbond;
            }()), (0, _defineProperty2.default)(_rpc, "setupTranscoder", function () {
              var _setupTranscoder = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee65(rewardCut, // percentage
              feeShare, // percentage
              pricePerSegment) {
                var tx,
                    _args65 = arguments;
                return _regenerator.default.wrap(function _callee65$(_context65) {
                  while (1) {
                    switch (_context65.prev = _context65.next) {
                      case 0:
                        tx = _args65.length > 3 && _args65[3] !== undefined ? _args65[3] : config.defaultTx;
                        _context65.t0 = utils;
                        _context65.next = 4;
                        return BondingManager.transcoder(toBN(rewardCut), toBN(feeShare), toBN(pricePerSegment), tx);

                      case 4:
                        _context65.t1 = _context65.sent;
                        _context65.t2 = config.eth;
                        _context65.next = 8;
                        return _context65.t0.getTxReceipt.call(_context65.t0, _context65.t1, _context65.t2);

                      case 8:
                        return _context65.abrupt("return", _context65.sent);

                      case 9:
                      case "end":
                        return _context65.stop();
                    }
                  }
                }, _callee65);
              }));

              function setupTranscoder(_x43, _x44, _x45) {
                return _setupTranscoder.apply(this, arguments);
              }

              return setupTranscoder;
            }()), (0, _defineProperty2.default)(_rpc, "getTargetBondingRate", function () {
              var _getTargetBondingRate = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee66() {
                return _regenerator.default.wrap(function _callee66$(_context66) {
                  while (1) {
                    switch (_context66.prev = _context66.next) {
                      case 0:
                        _context66.t0 = headToString;
                        _context66.next = 3;
                        return Minter.targetBondingRate();

                      case 3:
                        _context66.t1 = _context66.sent;
                        return _context66.abrupt("return", (0, _context66.t0)(_context66.t1));

                      case 5:
                      case "end":
                        return _context66.stop();
                    }
                  }
                }, _callee66);
              }));

              function getTargetBondingRate() {
                return _getTargetBondingRate.apply(this, arguments);
              }

              return getTargetBondingRate;
            }()), (0, _defineProperty2.default)(_rpc, "deposit", function () {
              var _deposit = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee67(amount) {
                var tx,
                    balance,
                    value,
                    _args67 = arguments;
                return _regenerator.default.wrap(function _callee67$(_context67) {
                  while (1) {
                    switch (_context67.prev = _context67.next) {
                      case 0:
                        tx = _args67.length > 1 && _args67[1] !== undefined ? _args67[1] : config.defaultTx;
                        _context67.t0 = toBN;
                        _context67.next = 4;
                        return rpc.getEthBalance(tx.from);

                      case 4:
                        _context67.t1 = _context67.sent;
                        balance = (0, _context67.t0)(_context67.t1);
                        value = toBN(amount);

                        if (balance.gte(value)) {
                          _context67.next = 9;
                          break;
                        }

                        throw new Error("Cannot deposit ".concat(toString(toBN(value.toString(10))), " LPT because is it greater than your current balance (").concat(balance, " LPT)."));

                      case 9:
                        _context67.t2 = utils;
                        _context67.next = 12;
                        return JobsManager.deposit((0, _objectSpread4.default)({}, tx, {
                          value: value
                        }));

                      case 12:
                        _context67.t3 = _context67.sent;
                        _context67.t4 = config.eth;
                        _context67.next = 16;
                        return _context67.t2.getTxReceipt.call(_context67.t2, _context67.t3, _context67.t4);

                      case 16:
                        return _context67.abrupt("return", _context67.sent);

                      case 17:
                      case "end":
                        return _context67.stop();
                    }
                  }
                }, _callee67);
              }));

              function deposit(_x46) {
                return _deposit.apply(this, arguments);
              }

              return deposit;
            }()), (0, _defineProperty2.default)(_rpc, "withdraw", function () {
              var _withdraw = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee68() {
                var tx,
                    _args68 = arguments;
                return _regenerator.default.wrap(function _callee68$(_context68) {
                  while (1) {
                    switch (_context68.prev = _context68.next) {
                      case 0:
                        tx = _args68.length > 0 && _args68[0] !== undefined ? _args68[0] : config.defaultTx;
                        _context68.t0 = utils;
                        _context68.next = 4;
                        return JobsManager.withdraw(tx);

                      case 4:
                        _context68.t1 = _context68.sent;
                        _context68.t2 = config.eth;
                        _context68.next = 8;
                        return _context68.t0.getTxReceipt.call(_context68.t0, _context68.t1, _context68.t2);

                      case 8:
                        return _context68.abrupt("return", _context68.sent);

                      case 9:
                      case "end":
                        return _context68.stop();
                    }
                  }
                }, _callee68);
              }));

              function withdraw() {
                return _withdraw.apply(this, arguments);
              }

              return withdraw;
            }()), (0, _defineProperty2.default)(_rpc, "withdrawStake", function () {
              var _withdrawStake = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee69() {
                var tx,
                    unbondingLockId,
                    _ref22,
                    status,
                    withdrawAmount,
                    nextUnbondingLockId,
                    _args69 = arguments;

                return _regenerator.default.wrap(function _callee69$(_context69) {
                  while (1) {
                    switch (_context69.prev = _context69.next) {
                      case 0:
                        tx = _args69.length > 0 && _args69[0] !== undefined ? _args69[0] : config.defaultTx;
                        unbondingLockId = _args69.length > 1 && _args69[1] !== undefined ? _args69[1] : null;
                        _context69.next = 4;
                        return rpc.getDelegator(tx.from);

                      case 4:
                        _ref22 = _context69.sent;
                        status = _ref22.status;
                        withdrawAmount = _ref22.withdrawAmount;
                        nextUnbondingLockId = _ref22.nextUnbondingLockId;

                        if (!(status === DELEGATOR_STATUS.Unbonding && !unbondingLockId)) {
                          _context69.next = 12;
                          break;
                        }

                        throw new Error('Delegator must wait through unbonding period');

                      case 12:
                        if (!(withdrawAmount === '0')) {
                          _context69.next = 16;
                          break;
                        }

                        throw new Error('Delegator does not have anything to withdraw');

                      case 16:
                        unbondingLockId = toBN(nextUnbondingLockId);

                        if (unbondingLockId.cmp(new BN(0)) > 0) {
                          unbondingLockId = unbondingLockId.sub(new BN(1));
                        }

                        _context69.t0 = utils;
                        _context69.next = 21;
                        return BondingManager.withdrawStake(toString(unbondingLockId), tx);

                      case 21:
                        _context69.t1 = _context69.sent;
                        _context69.t2 = config.eth;
                        _context69.next = 25;
                        return _context69.t0.getTxReceipt.call(_context69.t0, _context69.t1, _context69.t2);

                      case 25:
                        return _context69.abrupt("return", _context69.sent);

                      case 26:
                      case "end":
                        return _context69.stop();
                    }
                  }
                }, _callee69);
              }));

              function withdrawStake() {
                return _withdrawStake.apply(this, arguments);
              }

              return withdrawStake;
            }()), (0, _defineProperty2.default)(_rpc, "withdrawStakeWithUnbondLock", function () {
              var _withdrawStakeWithUnbondLock = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee70(unbondlock) {
                var tx,
                    id,
                    amount,
                    withdrawRound,
                    currentRound,
                    unbondingLockId,
                    _args70 = arguments;
                return _regenerator.default.wrap(function _callee70$(_context70) {
                  while (1) {
                    switch (_context70.prev = _context70.next) {
                      case 0:
                        tx = _args70.length > 1 && _args70[1] !== undefined ? _args70[1] : config.defaultTx;
                        id = unbondlock.id, amount = unbondlock.amount, withdrawRound = unbondlock.withdrawRound;
                        _context70.next = 4;
                        return rpc.getCurrentRound();

                      case 4:
                        currentRound = _context70.sent;

                        if (!(withdrawRound > currentRound)) {
                          _context70.next = 9;
                          break;
                        }

                        throw new Error('Delegator must wait through unbonding period');

                      case 9:
                        if (!(amount === '0')) {
                          _context70.next = 13;
                          break;
                        }

                        throw new Error('Delegator does not have anything to withdraw');

                      case 13:
                        if (!(amount < 0)) {
                          _context70.next = 15;
                          break;
                        }

                        throw new Error('Amount cannot be negative');

                      case 15:
                        unbondingLockId = toBN(id);
                        _context70.t0 = utils;
                        _context70.next = 19;
                        return BondingManager.withdrawStake(toString(unbondingLockId), tx);

                      case 19:
                        _context70.t1 = _context70.sent;
                        _context70.t2 = config.eth;
                        _context70.next = 23;
                        return _context70.t0.getTxReceipt.call(_context70.t0, _context70.t1, _context70.t2);

                      case 23:
                        return _context70.abrupt("return", _context70.sent);

                      case 24:
                      case "end":
                        return _context70.stop();
                    }
                  }
                }, _callee70);
              }));

              function withdrawStakeWithUnbondLock(_x47) {
                return _withdrawStakeWithUnbondLock.apply(this, arguments);
              }

              return withdrawStakeWithUnbondLock;
            }()), (0, _defineProperty2.default)(_rpc, "withdrawFees", function () {
              var _withdrawFees = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee71() {
                var tx,
                    _args71 = arguments;
                return _regenerator.default.wrap(function _callee71$(_context71) {
                  while (1) {
                    switch (_context71.prev = _context71.next) {
                      case 0:
                        tx = _args71.length > 0 && _args71[0] !== undefined ? _args71[0] : config.defaultTx;
                        _context71.t0 = utils;
                        _context71.next = 4;
                        return BondingManager.withdrawFees(tx);

                      case 4:
                        _context71.t1 = _context71.sent;
                        _context71.t2 = config.eth;
                        _context71.next = 8;
                        return _context71.t0.getTxReceipt.call(_context71.t0, _context71.t1, _context71.t2);

                      case 8:
                        return _context71.abrupt("return", _context71.sent);

                      case 9:
                      case "end":
                        return _context71.stop();
                    }
                  }
                }, _callee71);
              }));

              function withdrawFees() {
                return _withdrawFees.apply(this, arguments);
              }

              return withdrawFees;
            }()), (0, _defineProperty2.default)(_rpc, "createJob", function () {
              var _createJob = (0, _asyncToGenerator2.default)(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee72(streamId) {
                var profiles,
                    maxPricePerSegment,
                    tx,
                    _args72 = arguments;
                return _regenerator.default.wrap(function _callee72$(_context72) {
                  while (1) {
                    switch (_context72.prev = _context72.next) {
                      case 0:
                        profiles = _args72.length > 1 && _args72[1] !== undefined ? _args72[1] : [// default profiles
                        'P240p30fps4x3', 'P360p30fps16x9'];
                        maxPricePerSegment = _args72.length > 2 ? _args72[2] : undefined;
                        tx = _args72.length > 3 && _args72[3] !== undefined ? _args72[3] : config.defaultTx;
                        _context72.t0 = utils;
                        _context72.next = 6;
                        return JobsManager.job(streamId, utils.serializeTranscodingProfiles(profiles), toBN(maxPricePerSegment), tx);

                      case 6:
                        _context72.t1 = _context72.sent;
                        _context72.t2 = config.eth;
                        _context72.next = 10;
                        return _context72.t0.getTxReceipt.call(_context72.t0, _context72.t1, _context72.t2);

                      case 10:
                        return _context72.abrupt("return", _context72.sent);

                      case 11:
                      case "end":
                        return _context72.stop();
                    }
                  }
                }, _callee72);
              }));

              function createJob(_x48) {
                return _createJob.apply(this, arguments);
              }

              return createJob;
            }()), _rpc);
            return _context73.abrupt("return", {
              create: createLivepeerSDK,
              config: config,
              rpc: rpc,
              utils: utils,
              events: events,
              constants: {
                ADDRESS_PAD: ADDRESS_PAD,
                EMPTY_ADDRESS: EMPTY_ADDRESS,
                DELEGATOR_STATUS: DELEGATOR_STATUS,
                TRANSCODER_STATUS: TRANSCODER_STATUS,
                VIDEO_PROFILE_ID_SIZE: VIDEO_PROFILE_ID_SIZE,
                VIDEO_PROFILES: VIDEO_PROFILES
              } // Keeping typedefs down here so they show up last in the generated API table of contents

              /**
               * ABI property descriptor
               * @typedef {Object} ABIPropDescriptor
               * @prop {boolean} constants - is the method constant?
               * @prop {Array<{ name: string, type: string }>} inputs - the method params
               * @prop {Array<{ name: string, type: string }>} outputs - method return values
               * @prop {boolean} payable - is the method payable?
               * @prop {string} stateMutability - type of state mutability
               * @prop {string} type - type of contract property
               */

              /**
               * Mostly "`truffle`-style" ABI artifacts but no bytecode/network properties required
               * @typedef {Object} ContractArtifact
               * @prop {string} name - name of the contract
               * @prop {Array<ABIPropDescriptor>} abi - lists info about contract properties
               */

              /**
               * SDK configuration options
               * @typedef {Object} LivepeerSDKOptions
               * @prop {string} [controllerAddress = '0x37dC71366Ec655093b9930bc816E16e6b587F968'] - The address of the delpoyed Controller contract
               * @prop {string} [provider = 'https://rinkeby.infura.io/srFaWg0SlljdJAoClX3B'] - The ETH http provider for rpc methods
               * @prop {number} [gas = 0] - the amount of gas to include with transactions by default
               * @prop {Object<string, ContractArtifact>} artifacts - an object containing contract name -> ContractArtifact mappings
               * @prop {Object<string, string>} privateKeys - an object containing public -> private key mappings. Should be specified if using the SDK for transactions without MetaMask (via CLI, etc)
               * @prop {string|number} account - the account that will be used for transacting and data-fetching. Can be one of the publicKeys specified in the `privateKeys` option or an index of an account available via MetaMask
               */

              /**
               * An object containing contract info and utility methods for interacting with the Livepeer protocol's smart contracts
               * @typedef {Object} LivepeerSDK
               * @prop {Object<string, any>} config - this prop is mostly for debugging purposes and could change a lot in the future. Currently, it contains the following props: `abis`, `accounts`, `contracts`, `defaultTx`, `eth`
               * @prop {Object<string, any>} constants - Exposes some constant values. Currently, it contains the following props: `ADDRESS_PAD`, `DELEGATOR_STATUS`, `EMPTY_ADDRESS`, `TRANSCODER_STATUS`, `VIDEO_PROFILES`, `VIDEO_PROFILE_ID_SIZE`
               * @prop {Function} create - same as the `createLivepeerSDK` function
               * @prop {Object<string, Object>} events - Object mapping an event name -> contract event descriptor object
               * @prop {Object<string, Function>} rpc - contains all of the rpc methods available for interacting with the Livepeer protocol
               * @prop {Object<string, Function>} utils - contains utility methods. Mostly here just because. Could possibly be removed or moved into its own module in the future
               */

              /**
               * An object containing the total token supply and a user's account balance.
               * @typedef {Object} TokenInfo
               * @prop {string} totalSupply - total supply of token available in the protocol (LPTU)
               * @prop {string} balance - user's token balance (LPTU)
               */

              /**
               * Transaction config object
               * @typedef {Object} TxConfig
               * @prop {string} from - the ETH account address to sign the transaction from
               * @prop {number} gas - the amount of gas to include in the transaction
               */

              /**
               * Transaction receipt
               * @typedef {Object} TxReceipt
               * @prop {string} transactionHash - the transaction hash
               * @prop {BN} transactionIndex - the transaction index
               * @prop {string} blockHash - the transaction block hash
               * @prop {BN} blockNumber - the transaction block number
               * @prop {BN} cumulativeGasUsed - the cumulative gas used in the transaction
               * @prop {BN} gasUsed - the gas used in the transaction
               * @prop {string} contractAddress - the contract address of the transaction method
               * @prop {Array<Log>} logs - an object containing logs that were fired during the transaction
               */

              /**
               * An object representing a contract log
               * @typedef {Object} Log
               * @prop {BN} logIndex - the log index
               * @prop {BN} blockNumber - the log block number
               * @prop {string} blockHash - the log block hash
               * @prop {string} transactionHash - the log's transaction hash
               * @prop {BN} transactionIndex - the log's transaction index
               * @prop {string} address - the log's address
               * @prop {string} data - the log's data
               * @prop {Array<string>} topics - the log's topics
               */

              /**
               * Information about the status of the LPT faucet
               * @typedef {Object} FaucetInfo
               * @prop {string} amount - the amount distributed by the faucet
               * @prop {string} wait - the faucet request cooldown time
               * @prop {string} next - the next time a valid faucet request may be made
               */

              /**
               * A Broadcaster struct
               * @typedef {Object} Broadcaster
               * @prop {string} address - the ETH address of the broadcaster
               * @prop {string} deposit - the amount of LPT the broadcaster has deposited
               * @prop {string} withdrawBlock - the next block at which a broadcaster may withdraw their deposit
               */

              /**
               * A Delegator struct
               * @typedef {Object} Delegator
               * @prop {string} allowance - the delegator's LivepeerToken approved amount for transfer
               * @prop {string} address - the delegator's ETH address
               * @prop {string} bondedAmount - The amount of LPTU a delegator has bonded
               * @prop {string} delegateAddress - the ETH address of the delegator's delegate
               * @prop {string} delegatedAmount - the amount of LPTU the delegator has delegated
               * @prop {string} fees - the amount of LPTU a delegator has collected
               * @prop {string} lastClaimRound - the last round that the delegator claimed reward and fee pool shares
               * @prop {string} pendingFees - the amount of ETH the delegator has earned up to the current round
               * @prop {string} pendingStake - the amount of token the delegator has earned up to the current round
               * @prop {string} startRound - the round the delegator becomes bonded and delegated to its delegate
               * @prop {string} status - the delegator's status
               * @prop {string} withdrawableAmount - the amount of LPTU a delegator can withdraw
               * @prop {string} withdrawRound - the round the delegator can withdraw its stake
               * @prop {string} nextUnbondingLockId - the next unbonding lock ID for the delegator
               */

              /**
               * A Transcoder struct
               * @typedef {Object} Transcoder
               * @prop {boolean} active - whether or not the transcoder is active
               * @prop {string} address - the transcoder's ETH address
               * @prop {string} rewardCut - % of block reward cut paid to transcoder by a delegator
               * @prop {string} feeShare - % of fees paid to delegators by transcoder
               * @prop {string} lastRewardRound - last round that the transcoder called reward
               * @prop {string} pendingRewardCut - pending block reward cut for next round if the transcoder is active
               * @prop {string} pendingFeeShare - pending fee share for next round if the transcoder is active
               * @prop {string} pendingPricePerSegment - pending price per segment for next round if the transcoder is active
               * @prop {string} pricePerSegment - price per segment for a stream (LPTU)
               * @prop {string} status - the transcoder's status
               * @prop {string} totalStake - total tokens delegated toward a transcoder (including their own)
               */

              /**
               * An UnbondingLock struct
               * @typedef {Object} UnbondingLock
               * @prop {string} id - the unbonding lock ID
               * @prop {string} delegator - the delegator's ETH address
               * @prop {string} amount - the amount of tokens being unbonded
               * @prop {string} withdrawRound - the round at which unbonding period is over and tokens can be withdrawn
               */

              /**
               * An object containing information about the current round
               * @typedef {Object} RoundInfo
               * @prop {string} id - the number of the current round
               * @prop {boolean} initialized - whether or not the current round is initialized
               * @prop {string} startBlock - the start block of the current round
               * @prop {string} lastInitializedRound - the last round that was initialized prior to the current
               * @prop {string} length - the length of rounds
               */

              /**
               * An object containing information about an Ethereum block
               * @typedef {Object} Block
               * @prop {string} number - block number
               * @prop {string} hash - block hash
               * @prop {string} parentHash - parent has of the block
               * @prop {string} nonce - block nonce
               * @prop {string} sha3Uncles - block sha3 uncles
               * @prop {string} logsBloom - logss bloom for the block
               * @prop {string} transactionsRoot - block transaction root hash
               * @prop {string} stateRoot - block state root hash
               * @prop {string} receiptsRoot - block receipts root hash
               * @prop {string} miner - block miner hash
               * @prop {string} mixHash - block mixHash
               * @prop {string} difficulty - difficulty int
               * @prop {string} totalDifficulty - total difficulty int
               * @prop {string} extraData - hash of extra data
               * @prop {string} size - block size
               * @prop {string} gasLimit - block gas limit
               * @prop {string} gasUsed - gas used in block
               * @prop {number} timestamp - block timestamp
               * @prop {string} transactions - block transactions hash
               * @prop {string} uncles - block uncles hash
               * @prop {Array<Transaction>} transactions - transactions in the block
               * @prop {string} transactionsRoot - root transaction hash
               * @prop {Array<Uncle>} uncles - block uncles
               */

              /**
               * An object containing overview information about the jobs in the protocol
               * @typedef {Object} JobsInfo
               * @prop {string} total - the total number of jobs created
               * @prop {boolean} verificationRate - the verification rate for jobs
               * @prop {string} verificationPeriod - the verification period for jobs
               * @prop {string} verificationSlashingPeriod - the slashing period for jobs
               * @prop {string} finderFee - the finder fee for jobs
               */

              /**
               * A Job struct
               * @typedef {Object} Job
               * @prop {string} jobId - the id of the job
               * @prop {string} streamId - the job's stream id
               * @prop {Array<TranscodingProfile>} transcodingOptions - transcoding profiles
               * @prop {string} [transcoder] - the ETH address of the assigned transcoder
               * @prop {string} broadcaster - the ETH address of the broadcaster who created the job
               */

              /**
               * A Protocol struct
               * @typedef {Object} Protocol
               * @prop {boolean} paused - the protocol paused or not
               * @prop {string} totalTokenSupply - total token supply for protocol
               * @prop {string} totalBondedToken - total bonded token for protocol
               * @prop {string} targetBondingRate - target bonding rate for protocol
               * @prop {string} transcoderPoolMaxSize - transcoder pool max size
               */

            });

          case 11:
          case "end":
            return _context73.stop();
        }
      }
    }, _callee73);
  }));
  return _createLivepeerSDK.apply(this, arguments);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6WyJFTVBUWV9BRERSRVNTIiwiQUREUkVTU19QQUQiLCJWSURFT19QUk9GSUxFX0lEX1NJWkUiLCJWSURFT19QUk9GSUxFUyIsIlA3MjBwNjBmcHMxNng5IiwiaGFzaCIsIm5hbWUiLCJiaXRyYXRlIiwiZnJhbWVyYXRlIiwicmVzb2x1dGlvbiIsIlA3MjBwMzBmcHMxNng5IiwiUDcyMHAzMGZwczR4MyIsIlA1NzZwMzBmcHMxNng5IiwiUDM2MHAzMGZwczE2eDkiLCJQMzYwcDMwZnBzNHgzIiwiUDI0MHAzMGZwczE2eDkiLCJQMjQwcDMwZnBzNHgzIiwiUDE0NHAzMGZwczE2eDkiLCJERUxFR0FUT1JfU1RBVFVTIiwiUGVuZGluZyIsIkJvbmRlZCIsIlVuYm9uZGVkIiwiVW5ib25kaW5nIiwiVFJBTlNDT0RFUl9TVEFUVVMiLCJOb3RSZWdpc3RlcmVkIiwiUmVnaXN0ZXJlZCIsIkRFRkFVTFRTIiwiY29udHJvbGxlckFkZHJlc3MiLCJwcm92aWRlciIsInByaXZhdGVLZXlzIiwiYWNjb3VudCIsImdhcyIsImFydGlmYWN0cyIsIkxpdmVwZWVyVG9rZW4iLCJMaXZlcGVlclRva2VuQXJ0aWZhY3QiLCJMaXZlcGVlclRva2VuRmF1Y2V0IiwiTGl2ZXBlZXJUb2tlbkZhdWNldEFydGlmYWN0IiwiQ29udHJvbGxlciIsIkNvbnRyb2xsZXJBcnRpZmFjdCIsIkpvYnNNYW5hZ2VyIiwiSm9ic01hbmFnZXJBcnRpZmFjdCIsIlJvdW5kc01hbmFnZXIiLCJSb3VuZHNNYW5hZ2VyQXJ0aWZhY3QiLCJCb25kaW5nTWFuYWdlciIsIkJvbmRpbmdNYW5hZ2VyQXJ0aWZhY3QiLCJNaW50ZXIiLCJNaW50ZXJBcnRpZmFjdCIsImVuc1JlZ2lzdHJpZXMiLCJ1dGlscyIsImlzVmFsaWRBZGRyZXNzIiwieCIsInRlc3QiLCJyZXNvbHZlQWRkcmVzcyIsInJlc29sdmUiLCJnZXRNZXRob2RIYXNoIiwiaXRlbSIsImZpbmRBYmlCeU5hbWUiLCJhYmlzIiwiZmlsdGVyIiwidHlwZSIsImFiaSIsImZpbmRBYmlCeUhhc2giLCJlbmNvZGVNZXRob2RQYXJhbXMiLCJwYXJhbXMiLCJkZWNvZGVNZXRob2RQYXJhbXMiLCJieXRlY29kZSIsImlucHV0cyIsIm1hcCIsInN1YnN0ciIsImRlY29kZUNvbnRyYWN0SW5wdXQiLCJjb250cmFjdHMiLCJjb250cmFjdEFkZHJlc3MiLCJpbnB1dCIsImtleSIsImNvbnRyYWN0IiwiYWRkcmVzcyIsInN1YnN0cmluZyIsIm1ldGhvZCIsIk9iamVjdCIsImVudHJpZXMiLCJyZWR1Y2UiLCJvYmoiLCJrIiwidiIsIkFycmF5IiwiaXNBcnJheSIsIl92IiwiQk4iLCJpc0JOIiwidG9TdHJpbmciLCJnZXRUeFJlY2VpcHQiLCJ0eEhhc2giLCJldGgiLCJQcm9taXNlIiwicmVqZWN0Iiwic2V0VGltZW91dCIsImdldFRyYW5zYWN0aW9uUmVjZWlwdCIsInJlY2VpcHQiLCJzdGF0dXMiLCJFcnJvciIsIkpTT04iLCJnZXRUcmFuc2FjdGlvbkJ5SGFzaCIsInRyYW5zYWN0aW9uSGFzaCIsInRyYW5zYWN0aW9uIiwic3RyaW5naWZ5IiwicG9sbEZvclJlY2VpcHQiLCJwYXJzZVRyYW5zY29kaW5nT3B0aW9ucyIsIm9wdHMiLCJwcm9maWxlcyIsInZhbHVlcyIsInZhbGlkSGFzaGVzIiwiU2V0IiwiaGFzaGVzIiwiaSIsImxlbmd0aCIsInNsaWNlIiwiaGFzIiwicHVzaCIsImZpbmQiLCJzZXJpYWxpemVUcmFuc2NvZGluZ1Byb2ZpbGVzIiwibmFtZXMiLCJqb2luIiwicGFkQWRkcmVzcyIsImFkZHIiLCJlbmNvZGVFdmVudFRvcGljcyIsImV2ZW50IiwiZmlsdGVycyIsInRvcGljcyIsImluZGV4ZWQiLCJoYXNPd25Qcm9wZXJ0eSIsIm9wdGlvbnMiLCJkZWZhdWx0RmlsdGVyT2JqZWN0IiwiZGVjb2RlRXZlbnQiLCJkYXRhIiwiRXRoIiwidG9CTiIsIm4iLCJjb21wb3NlIiwiZm5zIiwiZiIsImciLCJwcm9wIiwidG9Cb29sIiwidG9OdW1iZXIiLCJOdW1iZXIiLCJoZWFkVG9Cb29sIiwiaGVhZFRvU3RyaW5nIiwiaGVhZFRvTnVtYmVyIiwiaW52YXJpYW50IiwicG9zIiwiZm9ybWF0RHVyYXRpb24iLCJtcyIsInNlY29uZHMiLCJ0b0ZpeGVkIiwibWludXRlcyIsImhvdXJzIiwiZGF5cyIsImRlcGxveUNvbnRyYWN0IiwiZGVmYXVsdFR4IiwibmV3IiwiZ2V0VHJhbnNhY3Rpb25TdWNjZXNzIiwiYXQiLCJnZXRDb250cmFjdEF0IiwiaW5pdFJQQyIsInVzZVByaXZhdGVLZXlzIiwia2V5cyIsImV0aGpzUHJvdmlkZXIiLCJTaWduZXJQcm92aWRlciIsInNpZ25UcmFuc2FjdGlvbiIsInJhd1R4IiwiY2IiLCJ0eCIsIkV0aGVyZXVtVHgiLCJzaWduIiwiZnJvbSIsInNlcmlhbGl6ZSIsImFjY291bnRzIiwidGltZW91dCIsIkh0dHBQcm92aWRlciIsIkVOUyIsImN1cnJlbnRQcm92aWRlciIsIm5ldF92ZXJzaW9uIiwicmVnaXN0cnlBZGRyZXNzIiwiZW5zIiwiaW5pdENvbnRyYWN0cyIsImtlY2NhazI1NiIsImdldENvbnRyYWN0IiwiYSIsImIiLCJldmVudHMiLCJjcmVhdGVMaXZlcGVlclNESyIsImNvbmZpZyIsImNhY2hlIiwicnBjIiwiZ2V0RU5TTmFtZSIsInJldmVyc2UiLCJtZXNzYWdlIiwiY29uc29sZSIsIndhcm4iLCJnZXRFTlNBZGRyZXNzIiwibG9va3VwIiwiZ2V0QmxvY2siLCJpZCIsInN0YXJ0c1dpdGgiLCJnZXRCbG9ja0J5SGFzaCIsImdldEJsb2NrQnlOdW1iZXIiLCJibG9jayIsImRpZmZpY3VsdHkiLCJnYXNMaW1pdCIsImdhc1VzZWQiLCJudW1iZXIiLCJzaXplIiwidGltZXN0YW1wIiwidG90YWxEaWZmaWN1bHR5IiwiZ2V0RXRoQmFsYW5jZSIsImdldEJhbGFuY2UiLCJnZXRVbmJvbmRpbmdQZXJpb2QiLCJ1bmJvbmRpbmdQZXJpb2QiLCJnZXROdW1BY3RpdmVUcmFuc2NvZGVycyIsIm51bUFjdGl2ZVRyYW5zY29kZXJzIiwiZ2V0TWF4RWFybmluZ3NDbGFpbXNSb3VuZHMiLCJtYXhFYXJuaW5nc0NsYWltc1JvdW5kcyIsImdldFRvdGFsQm9uZGVkIiwidG90YWxTdXBwbHkiLCJiYWxhbmNlT2YiLCJnZXRUb2tlblRvdGFsU3VwcGx5IiwiZ2V0VG9rZW5CYWxhbmNlIiwiYmFsYW5jZSIsInRvIiwiYW1vdW50IiwidmFsdWUiLCJndGUiLCJ0cmFuc2ZlciIsInJlcXVlc3RBbW91bnQiLCJyZXF1ZXN0V2FpdCIsIm5leHRWYWxpZFJlcXVlc3QiLCJnZXRGYXVjZXRBbW91bnQiLCJnZXRGYXVjZXRXYWl0IiwiZ2V0RmF1Y2V0TmV4dCIsIndhaXQiLCJuZXh0IiwiaW5mbGF0aW9uIiwiaW5mbGF0aW9uQ2hhbmdlIiwiYnJvYWRjYXN0ZXJzIiwiZGVwb3NpdCIsIndpdGhkcmF3QmxvY2siLCJkZWxlZ2F0b3JTdGF0dXMiLCJhbGxvd2FuY2UiLCJnZXRDdXJyZW50Um91bmQiLCJjdXJyZW50Um91bmQiLCJwZW5kaW5nU3Rha2UiLCJwZW5kaW5nRmVlcyIsImdldERlbGVnYXRvciIsImQiLCJib25kZWRBbW91bnQiLCJmZWVzIiwiZGVsZWdhdGVBZGRyZXNzIiwiZGVsZWdhdGVkQW1vdW50IiwibGFzdENsYWltUm91bmQiLCJzdGFydFJvdW5kIiwibmV4dFVuYm9uZGluZ0xvY2tJZCIsInVuYm9uZGluZ0xvY2tJZCIsImNtcCIsInN1YiIsImdldERlbGVnYXRvclVuYm9uZGluZ0xvY2siLCJ3aXRoZHJhd0Ftb3VudCIsIndpdGhkcmF3Um91bmQiLCJnZXREZWxlZ2F0b3JTdGF0dXMiLCJyZXN1bHQiLCJ1bmJvbmQiLCJsb2NrIiwiZGVsZWdhdG9yIiwicmVib25kIiwicmVib25kRnJvbVVuYm9uZGVkIiwiaXNBY3RpdmVUcmFuc2NvZGVyIiwidHJhbnNjb2RlclN0YXR1cyIsInRyYW5zY29kZXJUb3RhbFN0YWtlIiwiZ2V0VHJhbnNjb2RlclBvb2xNYXhTaXplIiwiZ2V0VHJhbnNjb2RlclN0YXR1cyIsImdldFRyYW5zY29kZXJJc0FjdGl2ZSIsImFjdGl2ZSIsImdldFRyYW5zY29kZXJUb3RhbFN0YWtlIiwidG90YWxTdGFrZSIsImdldFRyYW5zY29kZXIiLCJ0IiwiZmVlU2hhcmUiLCJsYXN0UmV3YXJkUm91bmQiLCJwZW5kaW5nRmVlU2hhcmUiLCJwZW5kaW5nUHJpY2VQZXJTZWdtZW50IiwicGVuZGluZ1Jld2FyZEN1dCIsInByaWNlUGVyU2VnbWVudCIsInJld2FyZEN1dCIsInRyYW5zY29kZXJzIiwiZ2V0Rmlyc3RUcmFuc2NvZGVySW5Qb29sIiwidHJhbnNjb2RlciIsImdldE5leHRUcmFuc2NvZGVySW5Qb29sIiwicGF1c2VkIiwiZ2V0UHJvdG9jb2xQYXVzZWQiLCJ0b3RhbFRva2VuU3VwcGx5IiwidG90YWxCb25kZWRUb2tlbiIsImdldFRhcmdldEJvbmRpbmdSYXRlIiwidGFyZ2V0Qm9uZGluZ1JhdGUiLCJ0cmFuc2NvZGVyUG9vbE1heFNpemUiLCJyb3VuZExlbmd0aCIsInJvdW5kc1BlclllYXIiLCJjdXJyZW50Um91bmRJbml0aWFsaXplZCIsImN1cnJlbnRSb3VuZFN0YXJ0QmxvY2siLCJsYXN0SW5pdGlhbGl6ZWRSb3VuZCIsImdldFJvdW5kTGVuZ3RoIiwiZ2V0Q3VycmVudFJvdW5kSXNJbml0aWFsaXplZCIsImluaXRpYWxpemVkIiwiZ2V0TGFzdEluaXRpYWxpemVkUm91bmQiLCJnZXRDdXJyZW50Um91bmRTdGFydEJsb2NrIiwic3RhcnRCbG9jayIsIm51bUpvYnMiLCJ2ZXJpZmljYXRpb25SYXRlIiwidmVyaWZpY2F0aW9uUGVyaW9kIiwidmVyaWZpY2F0aW9uU2xhc2hpbmdQZXJpb2QiLCJmaW5kZXJGZWUiLCJnZXRUb3RhbEpvYnMiLCJ0b3RhbCIsImdldEpvYlZlcmlmaWNhdGlvblJhdGUiLCJnZXRKb2JWZXJpZmljYXRpb25QZXJpb2QiLCJnZXRKb2JWZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZCIsImdldEpvYkZpbmRlckZlZSIsImdldEpvYiIsInRyYW5zY29kZXJBZGRyZXNzIiwiY3JlYXRpb25CbG9jayIsImVsZWN0QWN0aXZlVHJhbnNjb2RlciIsIm1heFByaWNlUGVyU2VnbWVudCIsImNyZWF0aW9uUm91bmQiLCJzdHJlYW1JZCIsInRyYW5zY29kaW5nT3B0aW9ucyIsImJyb2FkY2FzdGVyIiwiYnJvYWRjYXN0ZXJBZGRyZXNzIiwiYmxvY2tzQWdvIiwiTmV3Sm9iIiwiYmxvY2tOdW1iZXIiLCJoZWFkIiwiZnJvbUJsb2NrIiwiTWF0aCIsIm1heCIsInRvQmxvY2siLCJnZXRMb2dzIiwiam9iSWQiLCJyZXN1bHRzIiwicmVxdWVzdCIsImluaXRpYWxpemVSb3VuZCIsImVuZFJvdW5kIiwiY2xhaW1FYXJuaW5ncyIsInRva2VuIiwiYXBwcm92ZSIsImJvbmQiLCJjb250cmFjdE5hbWUiLCJtZXRob2ROYW1lIiwibWV0aG9kQXJncyIsImdhc1JhdGUiLCJjb250cmFjdEFCSSIsIm1ldGhvZEFCSSIsImVuY29kZWREYXRhIiwiZXN0aW1hdGVHYXMiLCJyb3VuZCIsIndpdGhkcmF3Iiwid2l0aGRyYXdTdGFrZSIsInVuYm9uZGxvY2siLCJ3aXRoZHJhd0ZlZXMiLCJqb2IiLCJjcmVhdGUiLCJjb25zdGFudHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQU1BOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBO0FBQ08sSUFBTUEsYUFBYSxHQUFHLDRDQUF0Qjs7QUFDQSxJQUFNQyxXQUFXLEdBQUcsNEJBQXBCOztBQUNBLElBQU1DLHFCQUFxQixHQUFHLENBQTlCOztBQUNBLElBQU1DLGNBQWMsR0FBRztBQUM1QkMsRUFBQUEsY0FBYyxFQUFFO0FBQ2RDLElBQUFBLElBQUksRUFBRSxVQURRO0FBRWRDLElBQUFBLElBQUksRUFBRSxnQkFGUTtBQUdkQyxJQUFBQSxPQUFPLEVBQUUsT0FISztBQUlkQyxJQUFBQSxTQUFTLEVBQUUsRUFKRztBQUtkQyxJQUFBQSxVQUFVLEVBQUU7QUFMRSxHQURZO0FBUTVCQyxFQUFBQSxjQUFjLEVBQUU7QUFDZEwsSUFBQUEsSUFBSSxFQUFFLFVBRFE7QUFFZEMsSUFBQUEsSUFBSSxFQUFFLGdCQUZRO0FBR2RDLElBQUFBLE9BQU8sRUFBRSxPQUhLO0FBSWRDLElBQUFBLFNBQVMsRUFBRSxFQUpHO0FBS2RDLElBQUFBLFVBQVUsRUFBRTtBQUxFLEdBUlk7QUFlNUJFLEVBQUFBLGFBQWEsRUFBRTtBQUNiTixJQUFBQSxJQUFJLEVBQUUsVUFETztBQUViQyxJQUFBQSxJQUFJLEVBQUUsZUFGTztBQUdiQyxJQUFBQSxPQUFPLEVBQUUsT0FISTtBQUliQyxJQUFBQSxTQUFTLEVBQUUsRUFKRTtBQUtiQyxJQUFBQSxVQUFVLEVBQUU7QUFMQyxHQWZhO0FBc0I1QkcsRUFBQUEsY0FBYyxFQUFFO0FBQ2RQLElBQUFBLElBQUksRUFBRSxVQURRO0FBRWRDLElBQUFBLElBQUksRUFBRSxnQkFGUTtBQUdkQyxJQUFBQSxPQUFPLEVBQUUsT0FISztBQUlkQyxJQUFBQSxTQUFTLEVBQUUsRUFKRztBQUtkQyxJQUFBQSxVQUFVLEVBQUU7QUFMRSxHQXRCWTtBQTZCNUJJLEVBQUFBLGNBQWMsRUFBRTtBQUNkUixJQUFBQSxJQUFJLEVBQUUsVUFEUTtBQUVkQyxJQUFBQSxJQUFJLEVBQUUsZ0JBRlE7QUFHZEMsSUFBQUEsT0FBTyxFQUFFLE9BSEs7QUFJZEMsSUFBQUEsU0FBUyxFQUFFLEVBSkc7QUFLZEMsSUFBQUEsVUFBVSxFQUFFO0FBTEUsR0E3Qlk7QUFvQzVCSyxFQUFBQSxhQUFhLEVBQUU7QUFDYlQsSUFBQUEsSUFBSSxFQUFFLFVBRE87QUFFYkMsSUFBQUEsSUFBSSxFQUFFLGVBRk87QUFHYkMsSUFBQUEsT0FBTyxFQUFFLE9BSEk7QUFJYkMsSUFBQUEsU0FBUyxFQUFFLEVBSkU7QUFLYkMsSUFBQUEsVUFBVSxFQUFFO0FBTEMsR0FwQ2E7QUEyQzVCTSxFQUFBQSxjQUFjLEVBQUU7QUFDZFYsSUFBQUEsSUFBSSxFQUFFLFVBRFE7QUFFZEMsSUFBQUEsSUFBSSxFQUFFLGdCQUZRO0FBR2RDLElBQUFBLE9BQU8sRUFBRSxNQUhLO0FBSWRDLElBQUFBLFNBQVMsRUFBRSxFQUpHO0FBS2RDLElBQUFBLFVBQVUsRUFBRTtBQUxFLEdBM0NZO0FBa0Q1Qk8sRUFBQUEsYUFBYSxFQUFFO0FBQ2JYLElBQUFBLElBQUksRUFBRSxVQURPO0FBRWJDLElBQUFBLElBQUksRUFBRSxlQUZPO0FBR2JDLElBQUFBLE9BQU8sRUFBRSxNQUhJO0FBSWJDLElBQUFBLFNBQVMsRUFBRSxFQUpFO0FBS2JDLElBQUFBLFVBQVUsRUFBRTtBQUxDLEdBbERhO0FBeUQ1QlEsRUFBQUEsY0FBYyxFQUFFO0FBQ2RaLElBQUFBLElBQUksRUFBRSxVQURRO0FBRWRDLElBQUFBLElBQUksRUFBRSxnQkFGUTtBQUdkQyxJQUFBQSxPQUFPLEVBQUUsTUFISztBQUlkQyxJQUFBQSxTQUFTLEVBQUUsRUFKRztBQUtkQyxJQUFBQSxVQUFVLEVBQUU7QUFMRTtBQXpEWSxDQUF2Qjs7QUFrRVAsSUFBTVMsZ0JBQWdCLEdBQUcsQ0FBQyxTQUFELEVBQVksUUFBWixFQUFzQixVQUF0QixFQUFrQyxXQUFsQyxDQUF6Qjs7QUFDQUEsZ0JBQWdCLENBQUNDLE9BQWpCLEdBQTJCRCxnQkFBZ0IsQ0FBQyxDQUFELENBQTNDO0FBQ0FBLGdCQUFnQixDQUFDRSxNQUFqQixHQUEwQkYsZ0JBQWdCLENBQUMsQ0FBRCxDQUExQztBQUNBQSxnQkFBZ0IsQ0FBQ0csUUFBakIsR0FBNEJILGdCQUFnQixDQUFDLENBQUQsQ0FBNUM7QUFDQUEsZ0JBQWdCLENBQUNJLFNBQWpCLEdBQTZCSixnQkFBZ0IsQ0FBQyxDQUFELENBQTdDO0FBRUEsSUFBTUssaUJBQWlCLEdBQUcsQ0FBQyxlQUFELEVBQWtCLFlBQWxCLENBQTFCOztBQUNBQSxpQkFBaUIsQ0FBQ0MsYUFBbEIsR0FBa0NELGlCQUFpQixDQUFDLENBQUQsQ0FBbkQ7QUFDQUEsaUJBQWlCLENBQUNFLFVBQWxCLEdBQStCRixpQkFBaUIsQ0FBQyxDQUFELENBQWhEO0FBR0E7QUFDTyxJQUFNRyxRQUFRLEdBQUc7QUFDdEJDLEVBQUFBLGlCQUFpQixFQUFFLDRDQURHO0FBRXRCQyxFQUFBQSxRQUFRLEVBQUUsK0RBRlk7QUFHdEJDLEVBQUFBLFdBQVcsRUFBRSxFQUhTO0FBR0w7QUFDakJDLEVBQUFBLE9BQU8sRUFBRSxFQUphO0FBS3RCQyxFQUFBQSxHQUFHLEVBQUUsQ0FMaUI7QUFNdEJDLEVBQUFBLFNBQVMsRUFBRTtBQUNUQyxJQUFBQSxhQUFhLEVBQUVDLHNCQUROO0FBRVRDLElBQUFBLG1CQUFtQixFQUFFQyw0QkFGWjtBQUdUQyxJQUFBQSxVQUFVLEVBQUVDLG1CQUhIO0FBSVRDLElBQUFBLFdBQVcsRUFBRUMsb0JBSko7QUFLVEMsSUFBQUEsYUFBYSxFQUFFQyxzQkFMTjtBQU1UQyxJQUFBQSxjQUFjLEVBQUVDLHVCQU5QO0FBT1RDLElBQUFBLE1BQU0sRUFBRUM7QUFQQyxHQU5XO0FBZXRCQyxFQUFBQSxhQUFhLEVBQUU7QUFDYjtBQUNBLFNBQUssNENBRlE7QUFHYjtBQUNBLFNBQUssNENBSlE7QUFLYjtBQUNBLFNBQUs7QUFOUSxHQWZPLENBeUJ4Qjs7QUF6QndCLENBQWpCOztBQTBCQSxJQUFNQyxLQUFLLEdBQUc7QUFDbkJDLEVBQUFBLGNBQWMsRUFBRSx3QkFBQUMsQ0FBQztBQUFBLFdBQUksc0JBQXNCQyxJQUF0QixDQUEyQkQsQ0FBM0IsQ0FBSjtBQUFBLEdBREU7QUFFbkJFLEVBQUFBLGNBQWM7QUFBQTtBQUFBO0FBQUEsOEJBQUUsaUJBQU9DLE9BQVAsRUFBZ0JILENBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDZEYsS0FBSyxDQUFDQyxjQUFOLENBQXFCQyxDQUFyQixDQURjO0FBQUE7QUFBQTtBQUFBOztBQUFBLDRCQUNZQSxDQURaO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEscUJBQ3NCRyxPQUFPLENBQUNILENBQUQsQ0FEN0I7O0FBQUE7QUFBQTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUY7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsS0FGSztBQUluQkksRUFBQUEsYUFBYSxFQUFFLHVCQUFBQyxJQUFJLEVBQUk7QUFDckI7QUFDQTtBQUNBO0FBQ0EsV0FBTywrQkFBZ0JBLElBQWhCLENBQVA7QUFDRCxHQVRrQjtBQVVuQkMsRUFBQUEsYUFBYSxFQUFFLHVCQUFDQyxJQUFELEVBQU9uRCxJQUFQLEVBQWdCO0FBQUEsdUJBQ2ZtRCxJQUFJLENBQUNDLE1BQUwsQ0FBWSxVQUFBSCxJQUFJLEVBQUk7QUFDaEMsVUFBSUEsSUFBSSxDQUFDSSxJQUFMLEtBQWMsVUFBbEIsRUFBOEIsT0FBTyxLQUFQO0FBQzlCLFVBQUlKLElBQUksQ0FBQ2pELElBQUwsS0FBY0EsSUFBbEIsRUFBd0IsT0FBTyxJQUFQO0FBQ3pCLEtBSGEsQ0FEZTtBQUFBO0FBQUEsUUFDdEJzRCxHQURzQjs7QUFLN0IsV0FBT0EsR0FBUDtBQUNELEdBaEJrQjtBQWlCbkJDLEVBQUFBLGFBQWEsRUFBRSx1QkFBQ0osSUFBRCxFQUFPcEQsSUFBUCxFQUFnQjtBQUFBLHdCQUNmb0QsSUFBSSxDQUFDQyxNQUFMLENBQVksVUFBQUgsSUFBSSxFQUFJO0FBQ2hDLFVBQUlBLElBQUksQ0FBQ0ksSUFBTCxLQUFjLFVBQWxCLEVBQThCLE9BQU8sS0FBUDtBQUM5QixhQUFPLCtCQUFnQkosSUFBaEIsTUFBMEJsRCxJQUFqQztBQUNELEtBSGEsQ0FEZTtBQUFBO0FBQUEsUUFDdEJ1RCxHQURzQjs7QUFLN0IsV0FBT0EsR0FBUDtBQUNELEdBdkJrQjtBQXdCbkJFLEVBQUFBLGtCQUFrQixFQUFFLDRCQUFDRixHQUFELEVBQU1HLE1BQU4sRUFBaUI7QUFDbkMsV0FBTyw0QkFBYUgsR0FBYixFQUFrQkcsTUFBbEIsQ0FBUDtBQUNELEdBMUJrQjtBQTJCbkJDLEVBQUFBLGtCQUFrQixFQUFFLDRCQUFDSixHQUFELEVBQU1LLFFBQU4sRUFBbUI7QUFDckMsV0FBTyw0QkFDTEwsR0FBRyxDQUFDTSxNQUFKLENBQVdDLEdBQVgsQ0FBZSxVQUFBakIsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQzVDLElBQU47QUFBQSxLQUFoQixDQURLLEVBRUxzRCxHQUFHLENBQUNNLE1BQUosQ0FBV0MsR0FBWCxDQUFlLFVBQUFqQixDQUFDO0FBQUEsYUFBSUEsQ0FBQyxDQUFDUyxJQUFOO0FBQUEsS0FBaEIsQ0FGSyxjQUdBTSxRQUFRLENBQUNHLE1BQVQsQ0FBZ0IsRUFBaEIsQ0FIQSxHQUlMLEtBSkssQ0FBUDtBQU1ELEdBbENrQjtBQW1DbkJDLEVBQUFBLG1CQUFtQixFQUFFLDZCQUFDQyxTQUFELEVBQVlDLGVBQVosRUFBNkJDLEtBQTdCLEVBQXVDO0FBQzFELFNBQUssSUFBTUMsR0FBWCxJQUFrQkgsU0FBbEIsRUFBNkI7QUFDM0IsVUFBTUksUUFBUSxHQUFHSixTQUFTLENBQUNHLEdBQUQsQ0FBMUI7QUFDQSxVQUFJQyxRQUFRLENBQUNDLE9BQVQsS0FBcUJKLGVBQXpCLEVBQTBDO0FBQzFDLFVBQU1sRSxJQUFJLEdBQUdtRSxLQUFLLENBQUNJLFNBQU4sQ0FBZ0IsQ0FBaEIsRUFBbUIsRUFBbkIsQ0FBYjtBQUNBLFVBQU1oQixHQUFHLEdBQUdaLEtBQUssQ0FBQ2EsYUFBTixDQUFvQmEsUUFBUSxDQUFDZCxHQUE3QixFQUFrQ3ZELElBQWxDLENBQVo7QUFDQSxhQUFPO0FBQ0xxRSxRQUFBQSxRQUFRLEVBQUVELEdBREw7QUFFTEksUUFBQUEsTUFBTSxFQUFFakIsR0FBRyxDQUFDdEQsSUFGUDtBQUdMeUQsUUFBQUEsTUFBTSxFQUFFZSxNQUFNLENBQUNDLE9BQVAsQ0FBZS9CLEtBQUssQ0FBQ2dCLGtCQUFOLENBQXlCSixHQUF6QixFQUE4QlksS0FBOUIsQ0FBZixFQUFxRFEsTUFBckQsQ0FDTixVQUFDQyxHQUFELFFBQWlCO0FBQUE7QUFBQSxjQUFWQyxDQUFVO0FBQUEsY0FBUEMsQ0FBTzs7QUFDZixpREFDS0YsR0FETCxvQ0FFR0MsQ0FGSCxFQUVPRSxLQUFLLENBQUNDLE9BQU4sQ0FBY0YsQ0FBZCxJQUNEQSxDQUFDLENBQUNoQixHQUFGLENBQU0sVUFBQW1CLEVBQUU7QUFBQSxtQkFBS0MsRUFBRSxDQUFDQyxJQUFILENBQVFGLEVBQVIsSUFBY0csUUFBUSxDQUFDSCxFQUFELENBQXRCLEdBQTZCQSxFQUFsQztBQUFBLFdBQVIsQ0FEQyxHQUVEQyxFQUFFLENBQUNDLElBQUgsQ0FBUUwsQ0FBUixJQUNBTSxRQUFRLENBQUNOLENBQUQsQ0FEUixHQUVBQSxDQU5OO0FBUUQsU0FWSyxFQVdOLEVBWE07QUFISCxPQUFQO0FBaUJEOztBQUNELFdBQU87QUFBRVQsTUFBQUEsUUFBUSxFQUFFLEVBQVo7QUFBZ0JHLE1BQUFBLE1BQU0sRUFBRSxFQUF4QjtBQUE0QmQsTUFBQUEsTUFBTSxFQUFFO0FBQXBDLEtBQVA7QUFDRCxHQTVEa0I7O0FBNkRuQjs7Ozs7OztBQU9BMkIsRUFBQUEsWUFBWTtBQUFBO0FBQUE7QUFBQSw4QkFBRSxrQkFBT0MsTUFBUCxFQUFlQyxHQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUNDLElBQUlDLE9BQUosQ0FBWSxVQUFDeEMsT0FBRCxFQUFVeUMsTUFBVixFQUFxQjtBQUM1Q0MsZ0JBQUFBLFVBQVU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUFDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQ0FFZUgsR0FBRyxDQUFDSSxxQkFBSixDQUEwQkwsTUFBMUIsQ0FGZjs7QUFBQTtBQUVETSw0QkFBQUEsT0FGQzs7QUFBQSxpQ0FHSEEsT0FIRztBQUFBO0FBQUE7QUFBQTs7QUFBQSxrQ0FJRUEsT0FBTyxDQUFDQyxNQUFSLEtBQW1CLEtBSnJCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDJDQUtEO0FBQ0E3Qyw0QkFBQUEsT0FBTyxDQUFDNEMsT0FBRCxDQU5OO0FBQUE7QUFBQTs7QUFBQTtBQUFBLDJDQVFESCxNQVJDO0FBQUEsMkNBU0tLLEtBVEw7QUFBQSwyQ0FVR0MsSUFWSDtBQUFBLDJDQVlPSCxPQVpQO0FBQUE7QUFBQSxtQ0FhMEJMLEdBQUcsQ0FBQ1Msb0JBQUosQ0FDakJKLE9BQU8sQ0FBQ0ssZUFEUyxDQWIxQjs7QUFBQTtBQUFBO0FBQUE7QUFZT0wsOEJBQUFBLE9BWlA7QUFhT00sOEJBQUFBLFdBYlA7QUFBQTtBQUFBLHdEQVVRQyxTQVZSLGtDQWlCSyxJQWpCTCxFQWtCSyxDQWxCTDtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFBQTtBQXVCUFQsNEJBQUFBLFVBQVUsQ0FBQ1UsY0FBRCxFQUFpQixHQUFqQixDQUFWO0FBdkJPO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBeUJQWCw0QkFBQUEsTUFBTSxjQUFOOztBQXpCTztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBRDs7QUFBQSwyQkFBZ0JXLGNBQWhCO0FBQUE7QUFBQTs7QUFBQSx5QkFBZ0JBLGNBQWhCO0FBQUEscUJBMkJQLENBM0JPLENBQVY7QUE0QkQsZUE3QlksQ0FERDs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUY7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsS0FwRU87O0FBb0duQjs7Ozs7O0FBTUFDLEVBQUFBLHVCQUF1QixFQUFFLGlDQUFBQyxJQUFJLEVBQUk7QUFDL0IsUUFBTUMsUUFBUSxHQUFHOUIsTUFBTSxDQUFDK0IsTUFBUCxDQUFjMUcsY0FBZCxDQUFqQjtBQUNBLFFBQU0yRyxXQUFXLEdBQUcsSUFBSUMsR0FBSixDQUFRSCxRQUFRLENBQUN6QyxHQUFULENBQWEsVUFBQWpCLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUM3QyxJQUFOO0FBQUEsS0FBZCxDQUFSLENBQXBCO0FBQ0EsUUFBSTJHLE1BQU0sR0FBRyxFQUFiOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR04sSUFBSSxDQUFDTyxNQUF6QixFQUFpQ0QsQ0FBQyxJQUFJL0cscUJBQXRDLEVBQTZEO0FBQzNELFVBQU1HLElBQUksR0FBR3NHLElBQUksQ0FBQ1EsS0FBTCxDQUFXRixDQUFYLEVBQWNBLENBQUMsR0FBRy9HLHFCQUFsQixDQUFiO0FBQ0EsVUFBSSxDQUFDNEcsV0FBVyxDQUFDTSxHQUFaLENBQWdCL0csSUFBaEIsQ0FBTCxFQUE0QjtBQUM1QjJHLE1BQUFBLE1BQU0sQ0FBQ0ssSUFBUCxDQUFZaEgsSUFBWjtBQUNEOztBQUNELFdBQU8yRyxNQUFNLENBQUM3QyxHQUFQLENBQVcsVUFBQWpCLENBQUM7QUFBQSxhQUFJMEQsUUFBUSxDQUFDVSxJQUFULENBQWM7QUFBQSxZQUFHakgsSUFBSCxTQUFHQSxJQUFIO0FBQUEsZUFBYzZDLENBQUMsS0FBSzdDLElBQXBCO0FBQUEsT0FBZCxDQUFKO0FBQUEsS0FBWixDQUFQO0FBQ0QsR0FwSGtCOztBQXFIbkI7Ozs7OztBQU1Ba0gsRUFBQUEsNEJBQTRCLEVBQUUsc0NBQUFDLEtBQUssRUFBSTtBQUNyQyxXQUFPLGlDQUNGLElBQUlULEdBQUosRUFBUztBQUNWUyxJQUFBQSxLQUFLLENBQUNyRCxHQUFOLENBQVUsVUFBQWpCLENBQUM7QUFBQSxhQUNUL0MsY0FBYyxDQUFDK0MsQ0FBRCxDQUFkLEdBQ0kvQyxjQUFjLENBQUMrQyxDQUFELENBQWQsQ0FBa0I3QyxJQUR0QixHQUVJRixjQUFjLENBQUNhLGFBQWYsQ0FBNkJYLElBSHhCO0FBQUEsS0FBWCxDQURDLENBREUsRUFRTG9ILElBUkssQ0FRQSxFQVJBLENBQVA7QUFTRCxHQXJJa0I7O0FBc0luQjs7Ozs7O0FBTUFDLEVBQUFBLFVBQVUsRUFBRSxvQkFBQUMsSUFBSTtBQUFBLFdBQUkxSCxXQUFXLEdBQUcwSCxJQUFJLENBQUN2RCxNQUFMLENBQVksQ0FBWixDQUFsQjtBQUFBLEdBNUlHOztBQTZJbkI7Ozs7Ozs7QUFPQXdELEVBQUFBLGlCQUFpQixFQUFFLDJCQUFDQyxLQUFELEVBQVFDLE9BQVIsRUFBb0I7QUFDckMsV0FBT0QsS0FBSyxDQUFDakUsR0FBTixDQUFVTSxNQUFWLENBQWlCYyxNQUFqQixDQUNMLFVBQUMrQyxNQUFELFNBQWtDZCxDQUFsQyxFQUF3QztBQUFBLFVBQTdCZSxPQUE2QixTQUE3QkEsT0FBNkI7QUFBQSxVQUFwQjFILElBQW9CLFNBQXBCQSxJQUFvQjtBQUFBLFVBQWRxRCxJQUFjLFNBQWRBLElBQWM7QUFDdEMsVUFBSSxDQUFDcUUsT0FBTCxFQUFjLE9BQU9ELE1BQVA7QUFDZCxVQUFJLENBQUNELE9BQU8sQ0FBQ0csY0FBUixDQUF1QjNILElBQXZCLENBQUwsRUFBbUMsa0RBQVd5SCxNQUFYLElBQW1CLElBQW5CO0FBQ25DLFVBQUlwRSxJQUFJLEtBQUssU0FBVCxJQUFzQixhQUFhLE9BQU9tRSxPQUFPLENBQUN4SCxJQUFELENBQXJELEVBQ0Usa0RBQVd5SCxNQUFYLElBQW1CL0UsS0FBSyxDQUFDMEUsVUFBTixDQUFpQkksT0FBTyxDQUFDeEgsSUFBRCxDQUF4QixDQUFuQjtBQUNGLHdEQUFXeUgsTUFBWCxJQUFtQkQsT0FBTyxDQUFDeEgsSUFBRCxDQUExQjtBQUNELEtBUEksRUFRTCxDQUFDdUgsS0FBSyxHQUFHSyxPQUFSLENBQWdCQyxtQkFBaEIsQ0FBb0NKLE1BQXBDLENBQTJDLENBQTNDLENBQUQsQ0FSSyxDQUFQO0FBVUQsR0EvSmtCOztBQWdLbkI7Ozs7Ozs7O0FBUUFLLEVBQUFBLFdBQVcsRUFBRSxxQkFBQVAsS0FBSztBQUFBLFdBQUksaUJBQXNCO0FBQUEsVUFBbkJRLElBQW1CLFNBQW5CQSxJQUFtQjtBQUFBLFVBQWJOLE1BQWEsU0FBYkEsTUFBYTtBQUMxQyxhQUFPLDJCQUFZRixLQUFLLENBQUNqRSxHQUFsQixFQUF1QnlFLElBQXZCLEVBQTZCTixNQUE3QixFQUFxQyxLQUFyQyxDQUFQO0FBQ0QsS0FGaUI7QUFBQSxHQXhLQyxDQTZLckI7QUFDQTtBQUNBOztBQS9LcUIsQ0FBZDs7SUFnTEN4QyxFLEdBQU8rQyxjLENBQVAvQyxFOztBQUNSLElBQU1nRCxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFBQyxDQUFDO0FBQUEsU0FBS2pELEVBQUUsQ0FBQ0MsSUFBSCxDQUFRZ0QsQ0FBUixJQUFhQSxDQUFiLEdBQWlCLElBQUlqRCxFQUFKLENBQU9pRCxDQUFDLENBQUMvQyxRQUFGLENBQVcsRUFBWCxDQUFQLEVBQXVCLEVBQXZCLENBQXRCO0FBQUEsQ0FBZDs7QUFDQSxJQUFNZ0QsT0FBTyxHQUFHLFNBQVZBLE9BQVU7QUFBQSxvQ0FBSUMsR0FBSjtBQUFJQSxJQUFBQSxHQUFKO0FBQUE7O0FBQUEsU0FBWUEsR0FBRyxDQUFDMUQsTUFBSixDQUFXLFVBQUMyRCxDQUFELEVBQUlDLENBQUo7QUFBQSxXQUFVO0FBQUEsYUFBYUQsQ0FBQyxDQUFDQyxDQUFDLE1BQUQsbUJBQUQsQ0FBZDtBQUFBLEtBQVY7QUFBQSxHQUFYLENBQVo7QUFBQSxDQUFoQjs7QUFDQSxJQUFNQyxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFDM0QsQ0FBRDtBQUFBLFNBQXdCLFVBQUNoQyxDQUFEO0FBQUEsV0FBWUEsQ0FBQyxDQUFDZ0MsQ0FBRCxDQUFiO0FBQUEsR0FBeEI7QUFBQSxDQUFiOztBQUNBLElBQU00RCxNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFDNUYsQ0FBRDtBQUFBLFNBQXFCLENBQUMsQ0FBQ0EsQ0FBdkI7QUFBQSxDQUFmOztBQUNBLElBQU11QyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDdkMsQ0FBRDtBQUFBLFNBQXVCQSxDQUFDLENBQUN1QyxRQUFGLENBQVcsRUFBWCxDQUF2QjtBQUFBLENBQWpCOztBQUNBLElBQU1zRCxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDN0YsQ0FBRDtBQUFBLFNBQXVCOEYsTUFBTSxDQUFDOUYsQ0FBQyxDQUFDdUMsUUFBRixDQUFXLEVBQVgsQ0FBRCxDQUE3QjtBQUFBLENBQWpCOztBQUNBLElBQU13RCxVQUFVLEdBQUdSLE9BQU8sQ0FDeEJLLE1BRHdCLEVBRXhCRCxJQUFJLENBQUMsQ0FBRCxDQUZvQixDQUExQjtBQUlBLElBQU1LLFlBQVksR0FBR1QsT0FBTyxDQUMxQmhELFFBRDBCLEVBRTFCb0QsSUFBSSxDQUFDLENBQUQsQ0FGc0IsQ0FBNUI7QUFJQSxJQUFNTSxZQUFZLEdBQUdWLE9BQU8sQ0FDMUJNLFFBRDBCLEVBRTFCRixJQUFJLENBQUMsQ0FBRCxDQUZzQixDQUE1Qjs7QUFJQSxJQUFNTyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDOUksSUFBRCxFQUFPK0ksR0FBUCxFQUFZMUYsSUFBWixFQUFxQjtBQUNyQyxRQUFNLElBQUl3QyxLQUFKLDhCQUErQjdGLElBQS9CLGlCQUF5Q3FELElBQXpDLDJCQUE4RDBGLEdBQTlELEVBQU47QUFDRCxDQUZEOztBQUdBLElBQU1DLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQUMsRUFBRSxFQUFJO0FBQzNCLE1BQU1DLE9BQU8sR0FBRyxDQUFDRCxFQUFFLEdBQUcsSUFBTixFQUFZRSxPQUFaLENBQW9CLENBQXBCLENBQWhCO0FBQ0EsTUFBTUMsT0FBTyxHQUFHLENBQUNILEVBQUUsSUFBSSxPQUFPLEVBQVgsQ0FBSCxFQUFtQkUsT0FBbkIsQ0FBMkIsQ0FBM0IsQ0FBaEI7QUFDQSxNQUFNRSxLQUFLLEdBQUcsQ0FBQ0osRUFBRSxJQUFJLE9BQU8sRUFBUCxHQUFZLEVBQWhCLENBQUgsRUFBd0JFLE9BQXhCLENBQWdDLENBQWhDLENBQWQ7QUFDQSxNQUFNRyxJQUFJLEdBQUcsQ0FBQ0wsRUFBRSxJQUFJLE9BQU8sRUFBUCxHQUFZLEVBQVosR0FBaUIsRUFBckIsQ0FBSCxFQUE2QkUsT0FBN0IsQ0FBcUMsQ0FBckMsQ0FBYjtBQUNBLE1BQUlELE9BQU8sR0FBRyxFQUFkLEVBQWtCLE9BQU9BLE9BQU8sR0FBRyxNQUFqQixDQUFsQixLQUNLLElBQUlFLE9BQU8sR0FBRyxFQUFkLEVBQWtCLE9BQU9BLE9BQU8sR0FBRyxNQUFqQixDQUFsQixLQUNBLElBQUlDLEtBQUssR0FBRyxFQUFaLEVBQWdCLE9BQU9BLEtBQUssR0FBRyxRQUFmO0FBQ3JCLFNBQU9DLElBQUksR0FBRyxPQUFkO0FBQ0QsQ0FURDtBQVdBOzs7Ozs7OztTQU1zQkMsYzs7O0FBVXRCOzs7Ozs7Ozs7Ozs7NEJBVk8sa0JBQ0xqRSxHQURLO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVIaEMsWUFBQUEsR0FGRyxTQUVIQSxHQUZHLEVBRUVLLFFBRkYsU0FFRUEsUUFGRixFQUVZNkYsU0FGWixTQUVZQSxTQUZaO0FBSUNwRixZQUFBQSxRQUpELEdBSVlrQixHQUFHLENBQUNsQixRQUFKLENBQWFkLEdBQWIsRUFBa0JLLFFBQWxCLEVBQTRCNkYsU0FBNUIsQ0FKWjtBQUFBO0FBQUEsbUJBS2dCcEYsUUFBUSxDQUFDcUYsR0FBVCxFQUxoQjs7QUFBQTtBQUtDcEUsWUFBQUEsTUFMRDtBQUFBO0FBQUEsbUJBTWlCQyxHQUFHLENBQUNvRSxxQkFBSixDQUEwQnJFLE1BQTFCLENBTmpCOztBQUFBO0FBTUNNLFlBQUFBLE9BTkQ7QUFBQSw4Q0FPRXZCLFFBQVEsQ0FBQ3VGLEVBQVQsQ0FBWWhFLE9BQU8sQ0FBQzFCLGVBQXBCLENBUEY7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7OztBQWlCQSxTQUFTMkYsYUFBVCxDQUNMdEUsR0FESyxTQUdLO0FBQUEsTUFEUmhDLEdBQ1EsU0FEUkEsR0FDUTtBQUFBLE1BREhLLFFBQ0csU0FESEEsUUFDRztBQUFBLE1BRE9VLE9BQ1AsU0FET0EsT0FDUDtBQUFBLE1BRGdCbUYsU0FDaEIsU0FEZ0JBLFNBQ2hCO0FBQ1YsU0FBT2xFLEdBQUcsQ0FBQ2xCLFFBQUosQ0FBYWQsR0FBYixFQUFrQkssUUFBbEIsRUFBNEI2RixTQUE1QixFQUF1Q0csRUFBdkMsQ0FBMEN0RixPQUExQyxDQUFQO0FBQ0Q7QUFFRDs7Ozs7OztTQUtzQndGLE87OztBQW9EdEI7Ozs7Ozs7Ozs7OzRCQXBETztBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0xySSxZQUFBQSxPQURLLFNBQ0xBLE9BREssRUFFTEQsV0FGSyxTQUVMQSxXQUZLLEVBR0xFLEdBSEssU0FHTEEsR0FISyxFQUlMSCxRQUpLLFNBSUxBLFFBSks7QUFTQ3dJLFlBQUFBLGNBVEQsR0FTa0IsSUFBSXRGLE1BQU0sQ0FBQ3VGLElBQVAsQ0FBWXhJLFdBQVosRUFBeUJxRixNQVQvQztBQVVDb0QsWUFBQUEsYUFWRCxHQVdILG1DQUFvQjFJLFFBQXBCLEtBQWdDQSxRQUFoQyxHQUNJQSxRQURKLEdBRUl3SSxjQUFjLEdBQ2Q7QUFDQSxnQkFBSUcsNEJBQUosQ0FBbUIzSSxRQUFuQixFQUE2QjtBQUMzQjRJLGNBQUFBLGVBQWUsRUFBRSx5QkFBQ0MsS0FBRCxFQUFRQyxFQUFSLEVBQWU7QUFDOUIsb0JBQU1DLEVBQUUsR0FBRyxJQUFJQyxxQkFBSixDQUFlSCxLQUFmLENBQVg7QUFDQUUsZ0JBQUFBLEVBQUUsQ0FBQ0UsSUFBSCxDQUFRaEosV0FBVyxDQUFDaUosSUFBRCxDQUFuQjtBQUNBSixnQkFBQUEsRUFBRSxDQUFDLElBQUQsRUFBTyxPQUFPQyxFQUFFLENBQUNJLFNBQUgsR0FBZXRGLFFBQWYsQ0FBd0IsS0FBeEIsQ0FBZCxDQUFGO0FBQ0QsZUFMMEI7QUFNM0J1RixjQUFBQSxRQUFRLEVBQUUsa0JBQUFOLEVBQUU7QUFBQSx1QkFBSUEsRUFBRSxDQUFDLElBQUQsRUFBT00sU0FBUCxDQUFOO0FBQUEsZUFOZTtBQU8zQkMsY0FBQUEsT0FBTyxFQUFFLEtBQUs7QUFQYSxhQUE3QixDQUZjLEdBV2Q7QUFDQSxnQkFBSTNDLGVBQUk0QyxZQUFSLENBQXFCdEosUUFBUSxJQUFJRixRQUFRLENBQUNFLFFBQTFDLENBekJEO0FBMEJDZ0UsWUFBQUEsR0ExQkQsR0EwQk8sSUFBSTBDLGNBQUosQ0FBUWdDLGFBQVIsQ0ExQlA7QUFBQSwyQkEyQldhLGlCQTNCWDtBQUFBLDJCQTRCT3ZGLEdBQUcsQ0FBQ3dGLGVBNUJYO0FBQUE7QUFBQSxtQkE2QjJDeEYsR0FBRyxDQUFDeUYsV0FBSixFQTdCM0M7O0FBQUE7QUFBQTtBQUFBLDJCQTZCYzNKLFFBQVEsQ0FBQ3FCLGFBN0J2QjtBQUFBO0FBNEJIbkIsY0FBQUEsUUE1Qkc7QUE2QkgwSixjQUFBQSxlQTdCRztBQUFBO0FBMkJDQyxZQUFBQSxHQTNCRDs7QUFBQSxpQkErQlluQixjQS9CWjtBQUFBO0FBQUE7QUFBQTs7QUFBQSwyQkFnQ0R0RixNQUFNLENBQUN1RixJQUFQLENBQVl4SSxXQUFaLENBaENDO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsbUJBaUNLK0QsR0FBRyxDQUFDb0YsUUFBSixFQWpDTDs7QUFBQTtBQUFBOztBQUFBO0FBK0JDQSxZQUFBQSxTQS9CRDtBQWtDQ0YsWUFBQUEsSUFsQ0QsR0FtQ0g7QUFDQTtBQUNBLGdCQUFJL0QsR0FBSixDQUFRaUUsU0FBUixFQUFrQjVELEdBQWxCLENBQXNCdEYsT0FBdEIsSUFDSUEsT0FESixHQUVJa0osU0FBUSxDQUFDbEosT0FBRCxDQUFSLElBQXFCOUIsYUF2Q3RCO0FBQUEsOENBd0NFO0FBQ0w0RixjQUFBQSxHQUFHLEVBQUhBLEdBREs7QUFFTDJGLGNBQUFBLEdBQUcsRUFBSEEsR0FGSztBQUdMM0osY0FBQUEsUUFBUSxFQUFSQSxRQUhLO0FBSUxvSixjQUFBQSxRQUFRLEVBQVJBLFNBSks7QUFLTGxCLGNBQUFBLFNBQVMsRUFBRTtBQUNUZ0IsZ0JBQUFBLElBQUksRUFBSkEsSUFEUztBQUVUL0ksZ0JBQUFBLEdBQUcsRUFBSEE7QUFGUztBQUxOLGFBeENGOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7Ozs7U0EwRGV5SixhOzs7QUFnR3RCOzs7OztBQUtBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs0QkFyR087QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0w3RSxZQUFBQSxJQURLLDhEQUNFLEVBREY7QUFHTDtBQUhLLDRCQVdEQSxJQVhDLENBS0g3RSxPQUxHLEVBS0hBLE9BTEcsOEJBS09KLFFBQVEsQ0FBQ0ksT0FMaEIsb0NBV0Q2RSxJQVhDLENBTUgzRSxTQU5HLEVBTUhBLFNBTkcsZ0NBTVNOLFFBQVEsQ0FBQ00sU0FObEIsNENBV0QyRSxJQVhDLENBT0hoRixpQkFQRyxFQU9IQSxpQkFQRyxzQ0FPaUJELFFBQVEsQ0FBQ0MsaUJBUDFCLHNDQVdEZ0YsSUFYQyxDQVFINUUsR0FSRyxFQVFIQSxHQVJHLDBCQVFHTCxRQUFRLENBQUNLLEdBUlosa0NBV0Q0RSxJQVhDLENBU0g5RSxXQVRHLEVBU0hBLFdBVEcsa0NBU1dILFFBQVEsQ0FBQ0csV0FUcEIsdUNBV0Q4RSxJQVhDLENBVUgvRSxRQVZHLEVBVUhBLFFBVkcsK0JBVVFGLFFBQVEsQ0FBQ0UsUUFWakIsbUJBWUw7O0FBWks7QUFBQSxtQkFhMkN1SSxPQUFPLENBQUM7QUFDdERySSxjQUFBQSxPQUFPLEVBQVBBLE9BRHNEO0FBRXREQyxjQUFBQSxHQUFHLEVBQUhBLEdBRnNEO0FBR3RERixjQUFBQSxXQUFXLEVBQVhBLFdBSHNEO0FBSXRERCxjQUFBQSxRQUFRLEVBQVJBO0FBSnNELGFBQUQsQ0FibEQ7O0FBQUE7QUFBQTtBQWFHb0osWUFBQUEsUUFiSCxTQWFHQSxRQWJIO0FBYWFsQixZQUFBQSxTQWJiLFNBYWFBLFNBYmI7QUFhd0J5QixZQUFBQSxHQWJ4QixTQWF3QkEsR0FieEI7QUFhNkIzRixZQUFBQSxHQWI3QixTQWE2QkEsR0FiN0I7QUFtQkN0QixZQUFBQSxTQW5CRCxHQW1CYTtBQUNoQnJDLGNBQUFBLGFBQWEsRUFBRSxJQURDO0FBRWhCRSxjQUFBQSxtQkFBbUIsRUFBRSxJQUZMO0FBR2hCUSxjQUFBQSxjQUFjLEVBQUUsSUFIQTtBQUloQkosY0FBQUEsV0FBVyxFQUFFLElBSkc7QUFLaEJFLGNBQUFBLGFBQWEsRUFBRSxJQUxDO0FBTWhCSSxjQUFBQSxNQUFNLEVBQUU7QUFOUSxhQW5CYjtBQTJCQ21FLFlBQUFBLE1BM0JELEdBMkJVO0FBQ2IvRSxjQUFBQSxhQUFhLEVBQUUsRUFERjtBQUViRSxjQUFBQSxtQkFBbUIsRUFBRSxFQUZSO0FBR2JRLGNBQUFBLGNBQWMsRUFBRSxFQUhIO0FBSWJKLGNBQUFBLFdBQVcsRUFBRSxFQUpBO0FBS2JFLGNBQUFBLGFBQWEsRUFBRSxFQUxGO0FBTWJJLGNBQUFBLE1BQU0sRUFBRSxFQU5LLENBUWY7O0FBUmUsYUEzQlY7QUFBQTtBQUFBLG1CQW9Db0JxSCxhQUFhLENBQUN0RSxHQUFELGtDQUNqQzVELFNBQVMsQ0FBQ0ssVUFEdUI7QUFFcEN5SCxjQUFBQSxTQUFTLEVBQVRBLFNBRm9DO0FBR3BDbkYsY0FBQUEsT0FBTyxFQUFFaEQ7QUFIMkIsZUFwQ2pDOztBQUFBO0FBb0NDVSxZQUFBQSxVQXBDRDtBQUFBLG1DQXlDY3lDLE1BQU0sQ0FBQ3VGLElBQVAsQ0FBWS9GLFNBQVosQ0F6Q2Q7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUF5Q01oRSxZQUFBQSxJQXpDTjtBQTBDSDtBQUNNRCxZQUFBQSxJQTNDSCxHQTJDVWlJLGVBQUltRCxTQUFKLENBQWNuTCxJQUFkLENBM0NWO0FBQUE7QUFBQSxtQkE0Q29CK0IsVUFBVSxDQUFDcUosV0FBWCxDQUF1QnJMLElBQXZCLENBNUNwQjs7QUFBQTtBQTRDR3NFLFlBQUFBLE9BNUNILGtCQTRDa0QsQ0E1Q2xEO0FBQUE7QUFBQSxtQkE4Q3FCdUYsYUFBYSxDQUFDdEUsR0FBRCxrQ0FDaEM1RCxTQUFTLENBQUMxQixJQUFELENBRHVCO0FBRW5Dd0osY0FBQUEsU0FBUyxFQUFUQSxTQUZtQztBQUduQ25GLGNBQUFBLE9BQU8sRUFBUEE7QUFIbUMsZUE5Q2xDOztBQUFBO0FBOENITCxZQUFBQSxTQUFTLENBQUNoRSxJQUFELENBOUNOO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBbURILDZCQUFtQmdFLFNBQVMsQ0FBQ2hFLElBQUQsQ0FBVCxDQUFnQnNELEdBQW5DLHVIQUF3QztBQUE3QkwsY0FBQUEsSUFBNkI7QUFDdEN5RCxjQUFBQSxNQUFNLENBQUMxRyxJQUFELENBQU4sQ0FBYTBDLEtBQUssQ0FBQ00sYUFBTixDQUFvQkMsSUFBcEIsQ0FBYixJQUEwQ0EsSUFBSSxDQUFDakQsSUFBL0M7QUFDRDs7QUFyREU7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUF1REw7QUFDQWdFLFlBQUFBLFNBQVMsQ0FBQ2pDLFVBQVYsR0FBdUJBLFVBQXZCLENBeERLLENBeURMOztBQUNNb0IsWUFBQUEsSUExREQsR0EwRFFxQixNQUFNLENBQUNDLE9BQVAsQ0FBZS9DLFNBQWYsRUFDVm1DLEdBRFUsQ0FDTjtBQUFBO0FBQUEsa0JBQUVlLENBQUY7QUFBQSxrQkFBS0MsQ0FBTDs7QUFBQSx1REFBZ0JELENBQWhCLEVBQW9CQyxDQUFDLENBQUN2QixHQUF0QjtBQUFBLGFBRE0sRUFFVm9CLE1BRlUsQ0FFSCxVQUFDMkcsQ0FBRCxFQUFJQyxDQUFKO0FBQUEscURBQWdCRCxDQUFoQixFQUFzQkMsQ0FBdEI7QUFBQSxhQUZHLEVBRXlCLEVBRnpCLENBMURSLEVBNkRMOztBQUNNQyxZQUFBQSxNQTlERCxHQThEVS9HLE1BQU0sQ0FBQ0MsT0FBUCxDQUFldEIsSUFBZixFQUNaVSxHQURZLENBQ1Isa0JBQXFCO0FBQUE7QUFBQSxrQkFBbkJPLFFBQW1CO0FBQUEsa0JBQVRkLEdBQVM7O0FBQ3hCLHFCQUFPQSxHQUFHLENBQ1BGLE1BREksQ0FDRyxVQUFBUixDQUFDO0FBQUEsdUJBQUlBLENBQUMsQ0FBQ1MsSUFBRixLQUFXLE9BQWY7QUFBQSxlQURKLEVBRUpRLEdBRkksQ0FFQSxVQUFBUCxHQUFHO0FBQUEsdUJBQUs7QUFDWEEsa0JBQUFBLEdBQUcsRUFBSEEsR0FEVztBQUVYYyxrQkFBQUEsUUFBUSxFQUFSQSxRQUZXO0FBR1htRCxrQkFBQUEsS0FBSyxFQUFFdkQsU0FBUyxDQUFDSSxRQUFELENBQVQsQ0FBb0JkLEdBQUcsQ0FBQ3RELElBQXhCLENBSEk7QUFJWEEsa0JBQUFBLElBQUksRUFBRXNELEdBQUcsQ0FBQ3REO0FBSkMsaUJBQUw7QUFBQSxlQUZILENBQVA7QUFRRCxhQVZZLEVBV1owRSxNQVhZLENBWVgsVUFBQzJHLENBQUQsRUFBSUMsQ0FBSjtBQUFBLHFCQUNFQSxDQUFDLENBQUM1RyxNQUFGLENBQVMsVUFBQzZHLE1BQUQsVUFBNEM7QUFBQSxvQkFBakN2TCxJQUFpQyxVQUFqQ0EsSUFBaUM7QUFBQSxvQkFBM0J1SCxLQUEyQixVQUEzQkEsS0FBMkI7QUFBQSxvQkFBcEJqRSxHQUFvQixVQUFwQkEsR0FBb0I7QUFBQSxvQkFBZmMsUUFBZSxVQUFmQSxRQUFlO0FBQ25EO0FBQ0FtRCxnQkFBQUEsS0FBSyxDQUFDakUsR0FBTixHQUFZQSxHQUFaO0FBQ0FpRSxnQkFBQUEsS0FBSyxDQUFDbkQsUUFBTixHQUFpQkEsUUFBakI7QUFDQSx1REFBWW1ILE1BQVosb0NBQXFCdkwsSUFBckIsRUFBNEJ1SCxLQUE1QjtBQUNELGVBTEQsRUFLRzhELENBTEgsQ0FERjtBQUFBLGFBWlcsRUFtQlgsRUFuQlcsQ0E5RFY7QUFBQSw4Q0FvRkU7QUFDTGxJLGNBQUFBLElBQUksRUFBSkEsSUFESztBQUVMdUgsY0FBQUEsUUFBUSxFQUFSQSxRQUZLO0FBR0wxRyxjQUFBQSxTQUFTLEVBQVRBLFNBSEs7QUFJTHdGLGNBQUFBLFNBQVMsRUFBVEEsU0FKSztBQUtMeUIsY0FBQUEsR0FBRyxFQUFIQSxHQUxLO0FBTUwzRixjQUFBQSxHQUFHLEVBQUhBLEdBTks7QUFPTGlHLGNBQUFBLE1BQU0sRUFBTkEsTUFQSztBQVFMN0UsY0FBQUEsTUFBTSxFQUFOQTtBQVJLLGFBcEZGOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7Ozs7U0F1SGU4RSxpQjs7Ozs7Ozs0QkFBZixtQkFDTG5GLElBREs7QUFBQTs7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBR29DNkUsYUFBYSxDQUFDN0UsSUFBRCxDQUhqRDs7QUFBQTtBQUFBO0FBR0c0RSxZQUFBQSxHQUhILFVBR0dBLEdBSEg7QUFHUU0sWUFBQUEsTUFIUixVQUdRQSxNQUhSO0FBR21CRSxZQUFBQSxNQUhuQjtBQUFBLGdDQVlEQSxNQUFNLENBQUN6SCxTQVpOLEVBS0gzQixjQUxHLHFCQUtIQSxjQUxHLEVBTUhOLFVBTkcscUJBTUhBLFVBTkcsRUFPSEUsV0FQRyxxQkFPSEEsV0FQRyxFQVFITixhQVJHLHFCQVFIQSxhQVJHLEVBU0hFLG1CQVRHLHFCQVNIQSxtQkFURyxFQVVITSxhQVZHLHFCQVVIQSxhQVZHLEVBV0hJLE1BWEcscUJBV0hBLE1BWEc7QUFhR08sWUFBQUEsY0FiSCxHQWFzQkosS0FidEIsQ0FhR0ksY0FiSCxFQWVMOztBQUNNNEksWUFBQUEsS0FoQkQsR0FnQlMsRUFoQlQsQ0FpQkg7O0FBRUY7Ozs7Ozs7Ozs7Ozs7OztBQW5CSztBQWtDQ0MsWUFBQUEsR0FsQ0Q7QUFtQ0g7Ozs7Ozs7Ozs7Ozs7OztBQWVNQyxjQUFBQSxVQWxESDtBQUFBO0FBQUE7QUFBQSw0REFrRGN2SCxPQWxEZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlDQW9EYzRHLEdBQUcsQ0FBQ1ksT0FBSixDQUFZeEgsT0FBWixDQXBEZDs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFzREM7QUFDQSw4QkFBSSxhQUFJeUgsT0FBSixLQUFnQix1QkFBcEIsRUFBNkM7QUFDM0NDLDRCQUFBQSxPQUFPLENBQUNDLElBQVIsZ0RBQ3lDM0gsT0FEekMsVUFFRSxhQUFJeUgsT0FGTjtBQUlELDJCQTVERixDQTZEQzs7O0FBN0RELDREQThEUSxFQTlEUjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFrRUg7Ozs7Ozs7Ozs7O0FBV01HLGNBQUFBLGFBN0VIO0FBQUE7QUFBQTtBQUFBLDREQTZFaUJqTSxJQTdFakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQ0ErRWNpTCxHQUFHLENBQUNpQixNQUFKLENBQVdsTSxJQUFYLENBL0VkOztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQWlGQztBQUNBLDhCQUFJLGFBQUk4TCxPQUFKLEtBQWdCLHVCQUFwQixFQUE2QztBQUMzQ0MsNEJBQUFBLE9BQU8sQ0FBQ0MsSUFBUixnREFDeUNoTSxJQUR6QyxVQUVFLGFBQUk4TCxPQUZOO0FBSUQsMkJBdkZGLENBd0ZDOzs7QUF4RkQsNERBeUZRLEVBekZSOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBOztBQTZGSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQ01LLGNBQUFBLFFBN0hIO0FBQUE7QUFBQTtBQUFBLDREQTZIWUMsRUE3SFo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBOEhhQSxFQUFFLENBQUNqSCxRQUFILEdBQWNrSCxVQUFkLENBQXlCLElBQXpCLENBOUhiO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsaUNBK0hTWixNQUFNLENBQUNuRyxHQUFQLENBQVdnSCxjQUFYLENBQTBCRixFQUExQixFQUE4QixJQUE5QixDQS9IVDs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsaUNBZ0lTWCxNQUFNLENBQUNuRyxHQUFQLENBQVdpSCxnQkFBWCxDQUE0QkgsRUFBNUIsRUFBZ0MsSUFBaEMsQ0FoSVQ7O0FBQUE7QUFBQTs7QUFBQTtBQThIS0ksMEJBQUFBLEtBOUhMO0FBQUEsNEZBa0lJQSxLQWxJSjtBQW1JQ0MsNEJBQUFBLFVBQVUsRUFBRXRILFFBQVEsQ0FBQ3FILEtBQUssQ0FBQ0MsVUFBUCxDQW5JckI7QUFvSUNDLDRCQUFBQSxRQUFRLEVBQUV2SCxRQUFRLENBQUNxSCxLQUFLLENBQUNFLFFBQVAsQ0FwSW5CO0FBcUlDQyw0QkFBQUEsT0FBTyxFQUFFeEgsUUFBUSxDQUFDcUgsS0FBSyxDQUFDRyxPQUFQLENBcklsQjtBQXNJQ0MsNEJBQUFBLE1BQU0sRUFBRXpILFFBQVEsQ0FBQ3FILEtBQUssQ0FBQ0ksTUFBUCxDQXRJakI7QUF1SUNDLDRCQUFBQSxJQUFJLEVBQUUxSCxRQUFRLENBQUNxSCxLQUFLLENBQUNLLElBQVAsQ0F2SWY7QUF3SUNDLDRCQUFBQSxTQUFTLEVBQUVwRSxNQUFNLENBQUN2RCxRQUFRLENBQUNxSCxLQUFLLENBQUNNLFNBQVAsQ0FBVCxDQXhJbEI7QUF5SUNDLDRCQUFBQSxlQUFlLEVBQUU1SCxRQUFRLENBQUNxSCxLQUFLLENBQUNPLGVBQVA7QUF6STFCOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBOztBQTZJSDs7Ozs7Ozs7Ozs7O0FBWU1DLGNBQUFBLGFBekpIO0FBQUE7QUFBQTtBQUFBLDZEQXlKaUIzRixJQXpKakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQTBKTWxDLFFBMUpOO0FBQUEsMENBMkpPc0csTUFBTSxDQUFDbkcsR0EzSmQ7QUFBQTtBQUFBLGlDQTRKU3hDLGNBQWMsQ0FBQzZJLEdBQUcsQ0FBQ00sYUFBTCxFQUFvQjVFLElBQXBCLENBNUp2Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0EySmtCNEYsVUEzSmxCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFpS0g7Ozs7Ozs7Ozs7QUFVTUMsY0FBQUEsa0JBM0tIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0E0S010RSxZQTVLTjtBQUFBO0FBQUEsaUNBNEt5QnZHLGNBQWMsQ0FBQzhLLGVBQWYsRUE1S3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUErS0g7Ozs7Ozs7Ozs7QUFVTUMsY0FBQUEsdUJBekxIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0EwTE14RSxZQTFMTjtBQUFBO0FBQUEsaUNBMEx5QnZHLGNBQWMsQ0FBQ2dMLG9CQUFmLEVBMUx6Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7O0FBNkxIOzs7Ozs7Ozs7O0FBVU1DLGNBQUFBLDBCQXZNSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBd01NMUUsWUF4TU47QUFBQTtBQUFBLGlDQXdNeUJ2RyxjQUFjLENBQUNrTCx1QkFBZixFQXhNekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQXNOTTNFLFlBdE5OO0FBQUE7QUFBQSwrQkFzTnlCdkcsY0FBYyxDQUFDOEssZUFBZixFQXROekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FvT012RSxZQXBPTjtBQUFBO0FBQUEsK0JBb095QnZHLGNBQWMsQ0FBQ2dMLG9CQUFmLEVBcE96Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQWtQTXpFLFlBbFBOO0FBQUE7QUFBQSwrQkFrUHlCdkcsY0FBYyxDQUFDa0wsdUJBQWYsRUFsUHpCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBZ1FNM0UsWUFoUU47QUFBQTtBQUFBLCtCQWdReUJ2RyxjQUFjLENBQUNtTCxjQUFmLEVBaFF6Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQThRTTVFLFlBOVFOO0FBQUE7QUFBQSwrQkE4UXlCakgsYUFBYSxDQUFDOEwsV0FBZCxFQTlRekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQTRSbUJwRyxJQTVSbkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQTZSTXVCLFlBN1JOO0FBQUEsd0NBOFJPakgsYUE5UlA7QUFBQTtBQUFBLCtCQStSU21CLGNBQWMsQ0FBQzZJLEdBQUcsQ0FBQ00sYUFBTCxFQUFvQjVFLElBQXBCLENBL1J2Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0E4UnFCcUcsU0E5UnJCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREErU2dCckcsSUEvU2hCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQWlUb0JzRSxHQUFHLENBQUNnQyxtQkFBSixFQWpUcEI7O0FBQUE7QUFBQTtBQUFBLHdDQWtUZ0JoQyxHQWxUaEI7QUFBQTtBQUFBLCtCQW1UUzdJLGNBQWMsQ0FBQzZJLEdBQUcsQ0FBQ00sYUFBTCxFQUFvQjVFLElBQXBCLENBblR2Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FrVG9CdUcsZUFsVHBCOztBQUFBO0FBQUE7QUFBQTtBQWlUQ0gsMEJBQUFBLFdBalREO0FBa1RDSSwwQkFBQUEsT0FsVEQ7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREF3VkRDLEVBeFZDLEVBeVZEQyxNQXpWQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEwVkQxRCx3QkFBQUEsRUExVkMsaUVBMFZJb0IsTUFBTSxDQUFDakMsU0ExVlg7QUE0Vkt3RSx3QkFBQUEsS0E1VkwsR0E0VmEvRixJQUFJLENBQUM4RixNQUFELENBNVZqQixFQTZWRDs7QUE3VkM7QUFBQSwrQkE4VnNCcE0sYUFBYSxDQUFDK0wsU0FBZCxDQUF3QnJELEVBQUUsQ0FBQ0csSUFBM0IsQ0E5VnRCOztBQUFBO0FBOFZLcUQsd0JBQUFBLE9BOVZMLG1CQThWd0QsQ0E5VnhEOztBQUFBLDRCQStWSUEsT0FBTyxDQUFDSSxHQUFSLENBQVlELEtBQVosQ0EvVko7QUFBQTtBQUFBO0FBQUE7O0FBQUEsOEJBZ1dPLElBQUluSSxLQUFKLDJCQUNlVixRQUFRLENBQ3pCNkksS0FEeUIsQ0FEdkIsbUVBR3NESCxPQUh0RCxZQWhXUDs7QUFBQTtBQUFBLHdDQXVXWW5MLEtBdldaO0FBQUEsd0NBd1dPZixhQXhXUDtBQUFBO0FBQUEsK0JBeVdTbUIsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNkIsRUFBcEIsQ0F6V3ZCOztBQUFBO0FBQUE7QUFBQSx3Q0EwV0dFLEtBMVdIO0FBQUEsd0NBMldHM0QsRUEzV0g7QUFBQTtBQUFBLDZDQXdXcUI2RCxRQXhXckI7O0FBQUE7QUFBQTtBQUFBLHdDQTZXQ3pDLE1BQU0sQ0FBQ25HLEdBN1dSO0FBQUE7QUFBQSw2Q0F1V2tCRixZQXZXbEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBNFhNd0QsWUE1WE47QUFBQTtBQUFBLCtCQTRYeUIvRyxtQkFBbUIsQ0FBQ3NNLGFBQXBCLEVBNVh6Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQTBZTXZGLFlBMVlOO0FBQUE7QUFBQSwrQkEwWXlCL0csbUJBQW1CLENBQUN1TSxXQUFwQixFQTFZekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQXdaaUIvRyxJQXhaakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQXlaTXVCLFlBelpOO0FBQUEsd0NBMFpPL0csbUJBMVpQO0FBQUE7QUFBQSwrQkEyWlNpQixjQUFjLENBQUM2SSxHQUFHLENBQUNNLGFBQUwsRUFBb0I1RSxJQUFwQixDQTNadkI7O0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBMFoyQmdILGdCQTFaM0I7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQSthaUJoSCxJQS9hakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBaWJlc0UsR0FBRyxDQUFDMkMsZUFBSixFQWpiZjs7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQkFrYmEzQyxHQUFHLENBQUM0QyxhQUFKLEVBbGJiOztBQUFBO0FBQUE7QUFBQSx3Q0FtYmE1QyxHQW5iYjtBQUFBO0FBQUEsK0JBb2JTN0ksY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNUUsSUFBcEIsQ0FwYnZCOztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQW1iaUJtSCxhQW5iakI7O0FBQUE7QUFBQTtBQUFBO0FBaWJDVCwwQkFBQUEsTUFqYkQ7QUFrYkNVLDBCQUFBQSxJQWxiRDtBQW1iQ0MsMEJBQUFBLElBbmJEO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQW9jTTlGLFlBcGNOO0FBQUE7QUFBQSwrQkFvY3lCckcsTUFBTSxDQUFDb00sU0FBUCxFQXBjekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FrZE0vRixZQWxkTjtBQUFBO0FBQUEsK0JBa2R5QnJHLE1BQU0sQ0FBQ3FNLGVBQVAsRUFsZHpCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREFvZWtCdkgsSUFwZWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBcWVxQnZFLGNBQWMsQ0FBQzZJLEdBQUcsQ0FBQ00sYUFBTCxFQUFvQjVFLElBQXBCLENBcmVuQzs7QUFBQTtBQXFlS2hELHdCQUFBQSxPQXJlTDtBQUFBO0FBQUEsK0JBc2VlcEMsV0FBVyxDQUFDNE0sWUFBWixDQUF5QnhLLE9BQXpCLENBdGVmOztBQUFBO0FBc2VLaUgsd0JBQUFBLENBdGVMO0FBQUEsMkRBdWVNO0FBQ0xqSCwwQkFBQUEsT0FBTyxFQUFQQSxPQURLO0FBRUx5SywwQkFBQUEsT0FBTyxFQUFFM0osUUFBUSxDQUFDbUcsQ0FBQyxDQUFDd0QsT0FBSCxDQUZaO0FBR0xDLDBCQUFBQSxhQUFhLEVBQUU1SixRQUFRLENBQUNtRyxDQUFDLENBQUN5RCxhQUFIO0FBSGxCLHlCQXZlTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREF5ZnNCMUgsSUF6ZnRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQTBmY3VCLFlBMWZkO0FBQUEsd0NBMmZPdkcsY0EzZlA7QUFBQTtBQUFBLCtCQTRmU1MsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNUUsSUFBcEIsQ0E1ZnZCOztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQTJmc0IySCxlQTNmdEI7O0FBQUE7QUFBQTtBQTBmS3BKLHdCQUFBQSxNQTFmTDtBQUFBLDJEQStmTWhGLGdCQUFnQixDQUFDZ0YsTUFBRCxDQS9mdEI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBMmhCZ0J5QixJQTNoQmhCO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQTRoQnFCdkUsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNUUsSUFBcEIsQ0E1aEJuQzs7QUFBQTtBQTRoQktoRCx3QkFBQUEsT0E1aEJMO0FBQUEsd0NBNmhCaUJ1RSxZQTdoQmpCO0FBQUE7QUFBQSwrQkE4aEJPakgsYUFBYSxDQUFDc04sU0FBZCxDQUF3QjVLLE9BQXhCLEVBQWlDaEMsY0FBYyxDQUFDZ0MsT0FBaEQsQ0E5aEJQOztBQUFBO0FBQUE7QUE2aEJLNEssd0JBQUFBLFNBN2hCTDtBQUFBO0FBQUEsK0JBZ2lCMEJ0RCxHQUFHLENBQUN1RCxlQUFKLEVBaGlCMUI7O0FBQUE7QUFnaUJLQyx3QkFBQUEsWUFoaUJMO0FBQUEsd0NBaWlCb0J2RyxZQWppQnBCO0FBQUE7QUFBQSwrQkFraUJPdkcsY0FBYyxDQUFDK00sWUFBZixDQUE0Qi9LLE9BQTVCLEVBQXFDOEssWUFBckMsQ0FsaUJQOztBQUFBO0FBQUE7QUFpaUJLQyx3QkFBQUEsWUFqaUJMO0FBQUEsd0NBb2lCbUJ4RyxZQXBpQm5CO0FBQUE7QUFBQSwrQkFxaUJPdkcsY0FBYyxDQUFDZ04sV0FBZixDQUEyQmhMLE9BQTNCLEVBQW9DOEssWUFBcEMsQ0FyaUJQOztBQUFBO0FBQUE7QUFvaUJLRSx3QkFBQUEsV0FwaUJMO0FBQUE7QUFBQSwrQkF1aUJlaE4sY0FBYyxDQUFDaU4sWUFBZixDQUE0QmpMLE9BQTVCLENBdmlCZjs7QUFBQTtBQXVpQktrTCx3QkFBQUEsQ0F2aUJMO0FBd2lCS0Msd0JBQUFBLFlBeGlCTCxHQXdpQm9CckssUUFBUSxDQUFDb0ssQ0FBQyxDQUFDQyxZQUFILENBeGlCNUI7QUF5aUJLQyx3QkFBQUEsSUF6aUJMLEdBeWlCWXRLLFFBQVEsQ0FBQ29LLENBQUMsQ0FBQ0UsSUFBSCxDQXppQnBCO0FBMGlCS0Msd0JBQUFBLGVBMWlCTCxHQTJpQkNILENBQUMsQ0FBQ0csZUFBRixLQUFzQmhRLGFBQXRCLEdBQXNDLEVBQXRDLEdBQTJDNlAsQ0FBQyxDQUFDRyxlQTNpQjlDO0FBNGlCS0Msd0JBQUFBLGVBNWlCTCxHQTRpQnVCeEssUUFBUSxDQUFDb0ssQ0FBQyxDQUFDSSxlQUFILENBNWlCL0I7QUE2aUJLQyx3QkFBQUEsY0E3aUJMLEdBNmlCc0J6SyxRQUFRLENBQUNvSyxDQUFDLENBQUNLLGNBQUgsQ0E3aUI5QjtBQThpQktDLHdCQUFBQSxVQTlpQkwsR0E4aUJrQjFLLFFBQVEsQ0FBQ29LLENBQUMsQ0FBQ00sVUFBSCxDQTlpQjFCO0FBK2lCS0Msd0JBQUFBLG1CQS9pQkwsR0EraUIyQjNLLFFBQVEsQ0FBQ29LLENBQUMsQ0FBQ08sbUJBQUgsQ0EvaUJuQztBQWlqQkdDLHdCQUFBQSxlQWpqQkgsR0FpakJxQjlILElBQUksQ0FBQzZILG1CQUFELENBampCekI7O0FBa2pCRCw0QkFBSUMsZUFBZSxDQUFDQyxHQUFoQixDQUFvQixJQUFJL0ssRUFBSixDQUFPLENBQVAsQ0FBcEIsSUFBaUMsQ0FBckMsRUFBd0M7QUFDdEM4SywwQkFBQUEsZUFBZSxHQUFHQSxlQUFlLENBQUNFLEdBQWhCLENBQW9CLElBQUloTCxFQUFKLENBQU8sQ0FBUCxDQUFwQixDQUFsQjtBQUNEOztBQXBqQkE7QUFBQSwrQkF3akJTMEcsR0FBRyxDQUFDdUUseUJBQUosQ0FDUjdMLE9BRFEsRUFFUmMsUUFBUSxDQUFDNEssZUFBRCxDQUZBLENBeGpCVDs7QUFBQTtBQUFBO0FBc2pCU0ksd0JBQUFBLGNBdGpCVCxVQXNqQkNwQyxNQXRqQkQ7QUF1akJDcUMsd0JBQUFBLGFBdmpCRCxVQXVqQkNBLGFBdmpCRDs7QUFBQSw4QkE2akJDQSxhQUFhLEtBQUssR0FBbEIsSUFBeUJuSSxJQUFJLENBQUNrSCxZQUFELENBQUosQ0FBbUJhLEdBQW5CLENBQXVCL0gsSUFBSSxDQUFDbUksYUFBRCxDQUEzQixJQUE4QyxDQTdqQnhFO0FBQUE7QUFBQTtBQUFBOztBQUFBLHdDQThqQkt4UCxnQkFBZ0IsQ0FBQ0ksU0E5akJ0QjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBLCtCQStqQlcySyxHQUFHLENBQUMwRSxrQkFBSixDQUF1QmhNLE9BQXZCLENBL2pCWDs7QUFBQTtBQUFBOztBQUFBO0FBNGpCS3VCLHdCQUFBQSxNQTVqQkw7QUFBQSwyREFpa0JNO0FBQ0x2QiwwQkFBQUEsT0FBTyxFQUFQQSxPQURLO0FBRUw0SywwQkFBQUEsU0FBUyxFQUFUQSxTQUZLO0FBR0xPLDBCQUFBQSxZQUFZLEVBQVpBLFlBSEs7QUFJTEUsMEJBQUFBLGVBQWUsRUFBZkEsZUFKSztBQUtMQywwQkFBQUEsZUFBZSxFQUFmQSxlQUxLO0FBTUxGLDBCQUFBQSxJQUFJLEVBQUpBLElBTks7QUFPTEcsMEJBQUFBLGNBQWMsRUFBZEEsY0FQSztBQVFMUCwwQkFBQUEsV0FBVyxFQUFYQSxXQVJLO0FBU0xELDBCQUFBQSxZQUFZLEVBQVpBLFlBVEs7QUFVTFMsMEJBQUFBLFVBQVUsRUFBVkEsVUFWSztBQVdMakssMEJBQUFBLE1BQU0sRUFBTkEsTUFYSztBQVlMd0ssMEJBQUFBLGFBQWEsRUFBYkEsYUFaSztBQWFMRCwwQkFBQUEsY0FBYyxFQUFkQSxjQWJLO0FBY0xMLDBCQUFBQSxtQkFBbUIsRUFBbkJBO0FBZEsseUJBamtCTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREFtbUJEekksSUFubUJDO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQXFtQm1Dc0UsR0FBRyxDQUFDMkQsWUFBSixDQUFpQmpJLElBQWpCLENBcm1CbkM7O0FBQUE7QUFBQTtBQXFtQkt5SSx3QkFBQUEsbUJBcm1CTCxVQXFtQktBLG1CQXJtQkw7QUF1bUJHQyx3QkFBQUEsZUF2bUJILEdBdW1CcUJ0SCxRQUFRLENBQUNxSCxtQkFBRCxDQXZtQjdCOztBQXdtQkQsNEJBQUlDLGVBQWUsR0FBRyxDQUF0QixFQUF5QjtBQUN2QkEsMEJBQUFBLGVBQWUsSUFBSSxDQUFuQjtBQUNEOztBQUVHTyx3QkFBQUEsTUE1bUJILEdBNG1CWSxFQTVtQlo7O0FBQUE7QUFBQSw4QkE4bUJNUCxlQUFlLElBQUksQ0E5bUJ6QjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBLCtCQSttQnNCcEUsR0FBRyxDQUFDdUUseUJBQUosQ0FDbkI3SSxJQURtQixFQUVuQmxDLFFBQVEsQ0FBQzRLLGVBQUQsQ0FGVyxDQS9tQnRCOztBQUFBO0FBK21CT1Esd0JBQUFBLE1BL21CUDtBQW1uQkNELHdCQUFBQSxNQUFNLENBQUN2SixJQUFQLENBQVl3SixNQUFaO0FBQ0FSLHdCQUFBQSxlQUFlLElBQUksQ0FBbkI7QUFwbkJEO0FBQUE7O0FBQUE7QUFBQSwyREF1bkJNTyxNQXZuQk47O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBMG9CRGpKLElBMW9CQyxFQTJvQkQwSSxlQTNvQkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQkE2b0JrQjFOLGNBQWMsQ0FBQzZOLHlCQUFmLENBQ2pCN0ksSUFEaUIsRUFFakIwSSxlQUZpQixDQTdvQmxCOztBQUFBO0FBNm9CS1Msd0JBQUFBLElBN29CTDtBQWlwQkt6Qyx3QkFBQUEsTUFqcEJMLEdBaXBCYzVJLFFBQVEsQ0FBQ3FMLElBQUksQ0FBQ3pDLE1BQU4sQ0FqcEJ0QjtBQWtwQktxQyx3QkFBQUEsYUFscEJMLEdBa3BCcUJqTCxRQUFRLENBQUNxTCxJQUFJLENBQUNKLGFBQU4sQ0FscEI3QjtBQUFBLDJEQW1wQk07QUFDTGhFLDBCQUFBQSxFQUFFLEVBQUUyRCxlQURDO0FBRUxVLDBCQUFBQSxTQUFTLEVBQUVwSixJQUZOO0FBR0wwRywwQkFBQUEsTUFBTSxFQUFOQSxNQUhLO0FBSUxxQywwQkFBQUEsYUFBYSxFQUFiQTtBQUpLLHlCQW5wQk47O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBeXJCREwsZUF6ckJDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMHJCRDFGLHdCQUFBQSxFQTFyQkMsaUVBMHJCSW9CLE1BQU0sQ0FBQ2pDLFNBMXJCWDtBQUFBLHdDQTRyQlk5RyxLQTVyQlo7QUFBQTtBQUFBLCtCQTZyQk9MLGNBQWMsQ0FBQ3FPLE1BQWYsQ0FBc0JYLGVBQXRCLGtDQUNEdEUsTUFBTSxDQUFDakMsU0FETixFQUVEYSxFQUZDLEVBN3JCUDs7QUFBQTtBQUFBO0FBQUEsd0NBaXNCQ29CLE1BQU0sQ0FBQ25HLEdBanNCUjtBQUFBO0FBQUEsNkNBNHJCa0JGLFlBNXJCbEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREFtdUJEaUMsSUFudUJDLEVBb3VCRDBJLGVBcHVCQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXF1QkQxRix3QkFBQUEsRUFydUJDLGlFQXF1QklvQixNQUFNLENBQUNqQyxTQXJ1Qlg7QUFBQSx3Q0F1dUJZOUcsS0F2dUJaO0FBQUE7QUFBQSwrQkF3dUJPTCxjQUFjLENBQUNzTyxrQkFBZixDQUFrQ3RKLElBQWxDLEVBQXdDMEksZUFBeEMsa0NBQ0R0RSxNQUFNLENBQUNqQyxTQUROLEVBRURhLEVBRkMsRUF4dUJQOztBQUFBO0FBQUE7QUFBQSx3Q0E0dUJDb0IsTUFBTSxDQUFDbkcsR0E1dUJSO0FBQUE7QUFBQSw2Q0F1dUJrQkYsWUF2dUJsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQTJ2QnlCaUMsSUEzdkJ6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBNHZCTXNCLFVBNXZCTjtBQUFBLHdDQTZ2Qk90RyxjQTd2QlA7QUFBQTtBQUFBLCtCQTh2QlNTLGNBQWMsQ0FBQzZJLEdBQUcsQ0FBQ00sYUFBTCxFQUFvQjVFLElBQXBCLENBOXZCdkI7O0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBK3ZCU3NFLEdBQUcsQ0FBQ3VELGVBQUosRUEvdkJUOztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQTZ2QnNCMEIsa0JBN3ZCdEI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQSt3QnVCdkosSUEvd0J2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FneEJjdUIsWUFoeEJkO0FBQUEsd0NBaXhCT3ZHLGNBanhCUDtBQUFBO0FBQUEsK0JBa3hCU1MsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNUUsSUFBcEIsQ0FseEJ2Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FpeEJzQndKLGdCQWp4QnRCOztBQUFBO0FBQUE7QUFneEJLakwsd0JBQUFBLE1BaHhCTDtBQUFBLDJEQXF4Qk0zRSxpQkFBaUIsQ0FBQzJFLE1BQUQsQ0FyeEJ2Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREFteUIyQnlCLElBbnlCM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQW95Qk11QixZQXB5Qk47QUFBQSx3Q0FxeUJPdkcsY0FyeUJQO0FBQUE7QUFBQSwrQkFzeUJTUyxjQUFjLENBQUM2SSxHQUFHLENBQUNNLGFBQUwsRUFBb0I1RSxJQUFwQixDQXR5QnZCOztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQXF5QnNCeUosb0JBcnlCdEI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FzekJNbEksWUF0ekJOO0FBQUE7QUFBQSwrQkFzekJ5QnZHLGNBQWMsQ0FBQzBPLHdCQUFmLEVBdHpCekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQWcxQmlCMUosSUFoMUJqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQWkxQnFCdkUsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNUUsSUFBcEIsQ0FqMUJuQzs7QUFBQTtBQWkxQktoRCx3QkFBQUEsT0FqMUJMO0FBQUE7QUFBQSwrQkFrMUJvQnNILEdBQUcsQ0FBQ3FGLG1CQUFKLENBQXdCM00sT0FBeEIsQ0FsMUJwQjs7QUFBQTtBQWsxQkt1Qix3QkFBQUEsTUFsMUJMO0FBQUE7QUFBQSwrQkFtMUJvQitGLEdBQUcsQ0FBQ3NGLHFCQUFKLENBQTBCNU0sT0FBMUIsQ0FuMUJwQjs7QUFBQTtBQW0xQks2TSx3QkFBQUEsTUFuMUJMO0FBQUE7QUFBQSwrQkFvMUJ3QnZGLEdBQUcsQ0FBQ3dGLHVCQUFKLENBQTRCOU0sT0FBNUIsQ0FwMUJ4Qjs7QUFBQTtBQW8xQksrTSx3QkFBQUEsVUFwMUJMO0FBQUE7QUFBQSwrQkFxMUJlL08sY0FBYyxDQUFDZ1AsYUFBZixDQUE2QmhOLE9BQTdCLENBcjFCZjs7QUFBQTtBQXExQktpTix3QkFBQUEsQ0FyMUJMO0FBczFCS0Msd0JBQUFBLFFBdDFCTCxHQXMxQmdCcE0sUUFBUSxDQUFDbU0sQ0FBQyxDQUFDQyxRQUFILENBdDFCeEI7QUF1MUJLQyx3QkFBQUEsZUF2MUJMLEdBdTFCdUJyTSxRQUFRLENBQUNtTSxDQUFDLENBQUNFLGVBQUgsQ0F2MUIvQjtBQXcxQktDLHdCQUFBQSxlQXgxQkwsR0F3MUJ1QnRNLFFBQVEsQ0FBQ21NLENBQUMsQ0FBQ0csZUFBSCxDQXgxQi9CO0FBeTFCS0Msd0JBQUFBLHNCQXoxQkwsR0F5MUI4QnZNLFFBQVEsQ0FBQ21NLENBQUMsQ0FBQ0ksc0JBQUgsQ0F6MUJ0QztBQTAxQktDLHdCQUFBQSxnQkExMUJMLEdBMDFCd0J4TSxRQUFRLENBQUNtTSxDQUFDLENBQUNLLGdCQUFILENBMTFCaEM7QUEyMUJLQyx3QkFBQUEsZUEzMUJMLEdBMjFCdUJ6TSxRQUFRLENBQUNtTSxDQUFDLENBQUNNLGVBQUgsQ0EzMUIvQjtBQTQxQktDLHdCQUFBQSxTQTUxQkwsR0E0MUJpQjFNLFFBQVEsQ0FBQ21NLENBQUMsQ0FBQ08sU0FBSCxDQTUxQnpCO0FBQUEsMkRBNjFCTTtBQUNMWCwwQkFBQUEsTUFBTSxFQUFOQSxNQURLO0FBRUw3TSwwQkFBQUEsT0FBTyxFQUFQQSxPQUZLO0FBR0xrTiwwQkFBQUEsUUFBUSxFQUFSQSxRQUhLO0FBSUxDLDBCQUFBQSxlQUFlLEVBQWZBLGVBSks7QUFLTEksMEJBQUFBLGVBQWUsRUFBZkEsZUFMSztBQU1MRCwwQkFBQUEsZ0JBQWdCLEVBQWhCQSxnQkFOSztBQU9MRiwwQkFBQUEsZUFBZSxFQUFmQSxlQVBLO0FBUUxDLDBCQUFBQSxzQkFBc0IsRUFBdEJBLHNCQVJLO0FBU0xHLDBCQUFBQSxTQUFTLEVBQVRBLFNBVEs7QUFVTGpNLDBCQUFBQSxNQUFNLEVBQU5BLE1BVks7QUFXTHdMLDBCQUFBQSxVQUFVLEVBQVZBO0FBWEsseUJBNzFCTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF1M0JLVSx3QkFBQUEsV0F2M0JMLEdBdTNCbUIsRUF2M0JuQjtBQUFBLHdDQXczQlVsSixZQXgzQlY7QUFBQTtBQUFBLCtCQXczQjZCdkcsY0FBYyxDQUFDMFAsd0JBQWYsRUF4M0I3Qjs7QUFBQTtBQUFBO0FBdzNCRzFLLHdCQUFBQSxJQXgzQkg7O0FBQUE7QUFBQSw4QkF5M0JNQSxJQUFJLEtBQUszSCxhQXozQmY7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSwrQkEwM0IwQmlNLEdBQUcsQ0FBQzBGLGFBQUosQ0FBa0JoSyxJQUFsQixDQTEzQjFCOztBQUFBO0FBMDNCTzJLLHdCQUFBQSxVQTEzQlA7QUEyM0JDRix3QkFBQUEsV0FBVyxDQUFDL0ssSUFBWixDQUFpQmlMLFVBQWpCO0FBMzNCRCx3Q0E0M0JRcEosWUE1M0JSO0FBQUE7QUFBQSwrQkE0M0IyQnZHLGNBQWMsQ0FBQzRQLHVCQUFmLENBQXVDNUssSUFBdkMsQ0E1M0IzQjs7QUFBQTtBQUFBO0FBNDNCQ0Esd0JBQUFBLElBNTNCRDtBQUFBO0FBQUE7O0FBQUE7QUFBQSwyREE4M0JNeUssV0E5M0JOOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0E0NEJNbkosVUE1NEJOO0FBQUE7QUFBQSwrQkE0NEJ1QjVHLFVBQVUsQ0FBQ21RLE1BQVgsRUE1NEJ2Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQkFpNkJvQnZHLEdBQUcsQ0FBQ3dHLGlCQUFKLEVBajZCcEI7O0FBQUE7QUFpNkJLRCx3QkFBQUEsTUFqNkJMO0FBQUE7QUFBQSwrQkFrNkI4QnZHLEdBQUcsQ0FBQ2dDLG1CQUFKLEVBbDZCOUI7O0FBQUE7QUFrNkJLeUUsd0JBQUFBLGdCQWw2Qkw7QUFBQTtBQUFBLCtCQW02QjhCekcsR0FBRyxDQUFDNkIsY0FBSixFQW42QjlCOztBQUFBO0FBbTZCSzZFLHdCQUFBQSxnQkFuNkJMO0FBQUE7QUFBQSwrQkFvNkIrQjFHLEdBQUcsQ0FBQzJHLG9CQUFKLEVBcDZCL0I7O0FBQUE7QUFvNkJLQyx3QkFBQUEsaUJBcDZCTDtBQUFBO0FBQUEsK0JBcTZCbUM1RyxHQUFHLENBQUNvRix3QkFBSixFQXI2Qm5DOztBQUFBO0FBcTZCS3lCLHdCQUFBQSxxQkFyNkJMO0FBQUE7QUFBQSwrQkFzNkJxQzdHLEdBQUcsQ0FBQzJCLDBCQUFKLEVBdDZCckM7O0FBQUE7QUFzNkJLQyx3QkFBQUEsdUJBdDZCTDtBQUFBLDJEQXU2Qk07QUFDTDJFLDBCQUFBQSxNQUFNLEVBQU5BLE1BREs7QUFFTEUsMEJBQUFBLGdCQUFnQixFQUFoQkEsZ0JBRks7QUFHTEMsMEJBQUFBLGdCQUFnQixFQUFoQkEsZ0JBSEs7QUFJTEUsMEJBQUFBLGlCQUFpQixFQUFqQkEsaUJBSks7QUFLTEMsMEJBQUFBLHFCQUFxQixFQUFyQkEscUJBTEs7QUFNTGpGLDBCQUFBQSx1QkFBdUIsRUFBdkJBO0FBTksseUJBdjZCTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBNDdCTTNFLFlBNTdCTjtBQUFBO0FBQUEsK0JBNDdCeUJ6RyxhQUFhLENBQUNzUSxXQUFkLEVBNTdCekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0EwOEJNN0osWUExOEJOO0FBQUE7QUFBQSwrQkEwOEJ5QnpHLGFBQWEsQ0FBQ3VRLGFBQWQsRUExOEJ6Qjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQXc5Qk05SixZQXg5Qk47QUFBQTtBQUFBLCtCQXc5QnlCekcsYUFBYSxDQUFDZ04sWUFBZCxFQXg5QnpCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBcytCTXhHLFVBdCtCTjtBQUFBO0FBQUEsK0JBcytCdUJ4RyxhQUFhLENBQUN3USx1QkFBZCxFQXQrQnZCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBby9CTS9KLFlBcC9CTjtBQUFBO0FBQUEsK0JBby9CeUJ6RyxhQUFhLENBQUN5USxzQkFBZCxFQXAvQnpCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBa2dDTWhLLFlBbGdDTjtBQUFBO0FBQUEsK0JBa2dDeUJ6RyxhQUFhLENBQUMwUSxvQkFBZCxFQWxnQ3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQXNoQ29CbEgsR0FBRyxDQUFDbUgsY0FBSixFQXRoQ3BCOztBQUFBO0FBc2hDS2xNLHdCQUFBQSxNQXRoQ0w7QUFBQTtBQUFBLCtCQXVoQ2dCK0UsR0FBRyxDQUFDdUQsZUFBSixFQXZoQ2hCOztBQUFBO0FBdWhDSzlDLHdCQUFBQSxFQXZoQ0w7QUFBQTtBQUFBLCtCQXdoQ3lCVCxHQUFHLENBQUNvSCw0QkFBSixFQXhoQ3pCOztBQUFBO0FBd2hDS0Msd0JBQUFBLFdBeGhDTDtBQUFBO0FBQUEsK0JBeWhDa0NySCxHQUFHLENBQUNzSCx1QkFBSixFQXpoQ2xDOztBQUFBO0FBeWhDS0osd0JBQUFBLG9CQXpoQ0w7QUFBQTtBQUFBLCtCQTBoQ3dCbEgsR0FBRyxDQUFDdUgseUJBQUosRUExaEN4Qjs7QUFBQTtBQTBoQ0tDLHdCQUFBQSxVQTFoQ0w7QUFBQSwyREEyaENNO0FBQ0wvRywwQkFBQUEsRUFBRSxFQUFGQSxFQURLO0FBRUw0RywwQkFBQUEsV0FBVyxFQUFYQSxXQUZLO0FBR0xILDBCQUFBQSxvQkFBb0IsRUFBcEJBLG9CQUhLO0FBSUxqTSwwQkFBQUEsTUFBTSxFQUFOQSxNQUpLO0FBS0x1TSwwQkFBQUEsVUFBVSxFQUFWQTtBQUxLLHlCQTNoQ047O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQWlqQ012SyxZQWpqQ047QUFBQTtBQUFBLCtCQWlqQ3lCM0csV0FBVyxDQUFDbVIsT0FBWixFQWpqQ3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBK2pDTXhLLFlBL2pDTjtBQUFBO0FBQUEsK0JBK2pDeUIzRyxXQUFXLENBQUNvUixnQkFBWixFQS9qQ3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBNmtDTXpLLFlBN2tDTjtBQUFBO0FBQUEsK0JBNmtDeUIzRyxXQUFXLENBQUNxUixrQkFBWixFQTdrQ3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBMmxDTTFLLFlBM2xDTjtBQUFBO0FBQUEsK0JBMmxDeUIzRyxXQUFXLENBQUNzUiwwQkFBWixFQTNsQ3pCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBeW1DTTNLLFlBem1DTjtBQUFBO0FBQUEsK0JBeW1DeUIzRyxXQUFXLENBQUN1UixTQUFaLEVBem1DekI7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBNm5DbUI3SCxHQUFHLENBQUM4SCxZQUFKLEVBN25DbkI7O0FBQUE7QUE2bkNLQyx3QkFBQUEsS0E3bkNMO0FBQUE7QUFBQSwrQkE4bkM4Qi9ILEdBQUcsQ0FBQ2dJLHNCQUFKLEVBOW5DOUI7O0FBQUE7QUE4bkNLTix3QkFBQUEsZ0JBOW5DTDtBQUFBO0FBQUEsK0JBK25DZ0MxSCxHQUFHLENBQUNpSSx3QkFBSixFQS9uQ2hDOztBQUFBO0FBK25DS04sd0JBQUFBLGtCQS9uQ0w7QUFBQTtBQUFBLCtCQWdvQ3dDM0gsR0FBRyxDQUFDa0ksZ0NBQUosRUFob0N4Qzs7QUFBQTtBQWdvQ0tOLHdCQUFBQSwwQkFob0NMO0FBQUE7QUFBQSwrQkFpb0N1QjVILEdBQUcsQ0FBQ21JLGVBQUosRUFqb0N2Qjs7QUFBQTtBQWlvQ0tOLHdCQUFBQSxTQWpvQ0w7QUFBQSwyREFrb0NNO0FBQ0xFLDBCQUFBQSxLQUFLLEVBQUxBLEtBREs7QUFFTEwsMEJBQUFBLGdCQUFnQixFQUFoQkEsZ0JBRks7QUFHTEMsMEJBQUFBLGtCQUFrQixFQUFsQkEsa0JBSEs7QUFJTEMsMEJBQUFBLDBCQUEwQixFQUExQkEsMEJBSks7QUFLTEMsMEJBQUFBLFNBQVMsRUFBVEE7QUFMSyx5QkFsb0NOOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQTRwQ1VwSCxFQTVwQ1Y7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBNnBDZW5LLFdBQVcsQ0FBQzhSLE1BQVosQ0FBbUIzSCxFQUFuQixDQTdwQ2Y7O0FBQUE7QUE2cENLeEosd0JBQUFBLENBN3BDTDs7QUFBQSw4QkFncUNDQSxDQUFDLENBQUNvUixpQkFBRixLQUF3QixJQUF4QixJQUNBcFIsQ0FBQyxDQUFDb1IsaUJBQUYsS0FBd0J0VSxhQWpxQ3pCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsK0JBbXFDd0JpTSxHQUFHLENBQUNRLFFBQUosQ0FBYXZKLENBQUMsQ0FBQ3FSLGFBQWYsQ0FucUN4Qjs7QUFBQTtBQUFBO0FBbXFDU2xVLHdCQUFBQSxJQW5xQ1QsVUFtcUNTQSxJQW5xQ1Q7QUFBQSx3Q0FvcUN1QjZJLFlBcHFDdkI7QUFBQTtBQUFBLCtCQXFxQ1N2RyxjQUFjLENBQUM2UixxQkFBZixDQUNKdFIsQ0FBQyxDQUFDdVIsa0JBREUsRUFFSnBVLElBRkksRUFHSjZDLENBQUMsQ0FBQ3dSLGFBSEUsQ0FycUNUOztBQUFBO0FBQUE7QUFvcUNDeFIsd0JBQUFBLENBQUMsQ0FBQ29SLGlCQXBxQ0g7O0FBQUE7QUFBQSwyREE0cUNNO0FBQ0w1SCwwQkFBQUEsRUFBRSxFQUFFakgsUUFBUSxDQUFDaUgsRUFBRCxDQURQO0FBRUxpSSwwQkFBQUEsUUFBUSxFQUFFelIsQ0FBQyxDQUFDeVIsUUFGUDtBQUdMQywwQkFBQUEsa0JBQWtCLEVBQUU1UixLQUFLLENBQUMwRCx1QkFBTixDQUE4QnhELENBQUMsQ0FBQzBSLGtCQUFoQyxDQUhmO0FBSUx0QywwQkFBQUEsVUFBVSxFQUFFcFAsQ0FBQyxDQUFDb1IsaUJBSlQ7QUFLTE8sMEJBQUFBLFdBQVcsRUFBRTNSLENBQUMsQ0FBQzRSO0FBTFYseUJBNXFDTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrRkF3c0NDLEVBeHNDRDtBQW9zQ0QxRyx3QkFBQUEsRUFwc0NDLFVBb3NDREEsRUFwc0NDLEVBcXNDRHRELElBcnNDQyxVQXFzQ0RBLElBcnNDQyw0QkFzc0NEaUssU0F0c0NDLEVBc3NDREEsU0F0c0NDLGlDQXNzQ1csTUFBTSxLQXRzQ2pCLHFCQXVzQ0VqTixPQXZzQ0Y7QUF5c0NLRCx3QkFBQUEsS0F6c0NMLEdBeXNDYWdFLE1BQU0sQ0FBQ21KLE1BenNDcEI7QUFBQTtBQUFBLCtCQTBzQ2tCakosTUFBTSxDQUFDbkcsR0FBUCxDQUFXcVAsV0FBWCxFQTFzQ2xCOztBQUFBO0FBMHNDS0Msd0JBQUFBLElBMXNDTDtBQTJzQ0tDLHdCQUFBQSxTQTNzQ0wsR0Eyc0NpQnJLLElBQUksSUFBSXNLLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBWUgsSUFBSSxHQUFHSCxTQUFuQixDQTNzQ3pCO0FBNHNDS08sd0JBQUFBLE9BNXNDTCxHQTRzQ2VsSCxFQUFFLElBQUksUUE1c0NyQjtBQUFBLHdDQTZzQ2NwTCxLQTdzQ2Q7QUFBQSx3Q0E4c0NDNkUsS0E5c0NEOztBQUFBLDZCQStzQ0NDLE9BQU8sQ0FBQytNLFdBL3NDVDtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsd0NBaXRDVS9NLE9BanRDVjtBQUFBO0FBQUEsK0JBa3RDMEIxRSxjQUFjLENBQy9CNkksR0FBRyxDQUFDTSxhQUQyQixFQUUvQnpFLE9BQU8sQ0FBQytNLFdBRnVCLENBbHRDeEM7O0FBQUE7QUFBQTtBQUFBO0FBa3RDT0EsMEJBQUFBLFdBbHRDUDtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsd0NBdXRDSy9NLE9BdnRDTDs7QUFBQTtBQUFBO0FBNnNDS0Msd0JBQUFBLE1BN3NDTCxpQkE2c0NvQkgsaUJBN3NDcEI7QUF5dENLN0Qsd0JBQUFBLE1BenRDTCxHQXl0Q2M7QUFDYlksMEJBQUFBLE9BQU8sRUFBRXBDLFdBQVcsQ0FBQ29DLE9BRFI7QUFFYndRLDBCQUFBQSxTQUFTLEVBQVRBLFNBRmE7QUFHYkcsMEJBQUFBLE9BQU8sRUFBUEEsT0FIYTtBQUlidk4sMEJBQUFBLE1BQU0sRUFBTkE7QUFKYSx5QkF6dENkO0FBQUE7QUFBQSwrQkFpdUNRZ0UsTUFBTSxDQUFDbkcsR0FBUCxDQUFXMlAsT0FBWCxDQUFtQnhSLE1BQW5CLENBanVDUjs7QUFBQTtBQUFBLHdDQWt1Q0cwRSxPQUFPLENBQ0wsVUFBQXZGLENBQUM7QUFBQSxpQ0FBSztBQUNKd0osNEJBQUFBLEVBQUUsRUFBRWpILFFBQVEsQ0FBQ3ZDLENBQUMsQ0FBQ3NTLEtBQUgsQ0FEUjtBQUVKYiw0QkFBQUEsUUFBUSxFQUFFelIsQ0FBQyxDQUFDeVIsUUFGUjtBQUdKQyw0QkFBQUEsa0JBQWtCLEVBQUU1UixLQUFLLENBQUMwRCx1QkFBTixDQUNsQnhELENBQUMsQ0FBQzBSLGtCQURnQixDQUhoQjtBQU1KQyw0QkFBQUEsV0FBVyxFQUFFM1IsQ0FBQyxDQUFDMlI7QUFOWCwyQkFBTDtBQUFBLHlCQURJLEVBU0w3UixLQUFLLENBQUNvRixXQUFOLENBQWtCUCxLQUFsQixDQVRLLENBbHVDVjtBQSt0Q0s0Tix3QkFBQUEsT0EvdENMLG1CQWl1Q29DdFIsR0FqdUNwQztBQUFBLDJEQSt1Q01zUixPQUFPLENBQUN0SixPQUFSLEVBL3VDTjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQSt3Q2F4Qix3QkFBQUEsRUEvd0NiLGlFQSt3Q2tCb0IsTUFBTSxDQUFDakMsU0Evd0N6QjtBQUFBLHdDQXd4Q1k5RyxLQXh4Q1o7QUFBQTtBQUFBLCtCQXl4Q09iLG1CQUFtQixDQUFDdVQsT0FBcEIsQ0FBNEIvSyxFQUE1QixDQXp4Q1A7O0FBQUE7QUFBQTtBQUFBLHdDQTB4Q0NvQixNQUFNLENBQUNuRyxHQTF4Q1I7QUFBQTtBQUFBLDZDQXd4Q2tCRixZQXh4Q2xCOztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEyekNtQmlGLHdCQUFBQSxFQTN6Q25CLGlFQTJ6Q3dCb0IsTUFBTSxDQUFDakMsU0EzekMvQjtBQUFBO0FBQUEsd0NBOHpDYzlHLEtBOXpDZDtBQUFBO0FBQUEsK0JBK3pDU1AsYUFBYSxDQUFDa1QsZUFBZCxDQUE4QmhMLEVBQTlCLENBL3pDVDs7QUFBQTtBQUFBO0FBQUEsd0NBZzBDR29CLE1BQU0sQ0FBQ25HLEdBaDBDVjtBQUFBO0FBQUEsNkNBOHpDb0JGLFlBOXpDcEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFtMENDLHNDQUFJMEcsT0FBSixHQUFjLDZCQUE2QixjQUFJQSxPQUEvQztBQW4wQ0Q7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBcTFDRHdKLFFBcjFDQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXMxQ0RqTCx3QkFBQUEsRUF0MUNDLGlFQXMxQ0lvQixNQUFNLENBQUNqQyxTQXQxQ1g7QUFBQTtBQUFBLCtCQXcxQ1luSCxjQUFjLENBQUNrVCxhQUFmLENBQTZCRCxRQUE3QixrQ0FDUjdKLE1BQU0sQ0FBQ2pDLFNBREMsRUFFUmEsRUFGUSxFQXgxQ1o7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREErMUNEMEQsTUEvMUNDLEVBZzJDRDFELEVBaDJDQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrMkNLbUwsd0JBQUFBLEtBbDJDTCxHQWsyQ2F2TixJQUFJLENBQUM4RixNQUFELENBbDJDakIsRUFtMkNEOztBQW4yQ0Msd0NBbzJDS3JMLEtBcDJDTDtBQUFBO0FBQUEsK0JBcTJDT2YsYUFBYSxDQUFDOFQsT0FBZCxDQUFzQnBULGNBQWMsQ0FBQ2dDLE9BQXJDLEVBQThDbVIsS0FBOUMsa0NBQ0QvSixNQUFNLENBQUNqQyxTQUROLEVBRURhLEVBRkMsRUFyMkNQOztBQUFBO0FBQUE7QUFBQSx3Q0F5MkNDb0IsTUFBTSxDQUFDbkcsR0F6MkNSO0FBQUE7QUFBQSw2Q0FvMkNXRixZQXAyQ1g7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBODJDRDBJLEVBOTJDQyxFQSsyQ0RDLE1BLzJDQyxFQWczQ0QxRCxFQWgzQ0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBazNDS21MLHdCQUFBQSxLQWwzQ0wsR0FrM0Nhdk4sSUFBSSxDQUFDOEYsTUFBRCxDQWwzQ2pCLEVBbTNDRDs7QUFuM0NDLHdDQW8zQ1lyTCxLQXAzQ1o7QUFBQSx3Q0FxM0NPTCxjQXIzQ1A7QUFBQSx3Q0FzM0NHbVQsS0F0M0NIO0FBQUE7QUFBQSwrQkF1M0NTMVMsY0FBYyxDQUFDNkksR0FBRyxDQUFDTSxhQUFMLEVBQW9CNkIsRUFBcEIsQ0F2M0N2Qjs7QUFBQTtBQUFBO0FBQUEsd0VBeTNDUXJDLE1BQU0sQ0FBQ2pDLFNBejNDZixFQTAzQ1FhLEVBMTNDUjtBQUFBO0FBQUEsNkNBcTNDc0JxTCxJQXIzQ3RCOztBQUFBO0FBQUE7QUFBQSx3Q0E2M0NDakssTUFBTSxDQUFDbkcsR0E3M0NSO0FBQUE7QUFBQSw2Q0FvM0NrQkYsWUFwM0NsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQTI1Q0R1USxZQTM1Q0MsRUE0NUNEQyxVQTU1Q0MsRUE2NUNEQyxVQTc1Q0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTg1Q0R4TCx3QkFBQUEsRUE5NUNDLGlFQTg1Q0lvQixNQUFNLENBQUNqQyxTQTk1Q1g7QUFnNkNEYSx3QkFBQUEsRUFBRSxDQUFDMkQsS0FBSCxHQUFXM0QsRUFBRSxDQUFDMkQsS0FBSCxHQUFXM0QsRUFBRSxDQUFDMkQsS0FBZCxHQUFzQixHQUFqQztBQUNNOEgsd0JBQUFBLE9BajZDTCxHQWk2Q2UsR0FqNkNmO0FBazZDS0Msd0JBQUFBLFdBbDZDTCxHQWs2Q21CdEssTUFBTSxDQUFDdEksSUFBUCxDQUFZd1MsWUFBWixDQWw2Q25CO0FBbTZDS0ssd0JBQUFBLFNBbjZDTCxHQW02Q2lCdFQsS0FBSyxDQUFDUSxhQUFOLENBQW9CNlMsV0FBcEIsRUFBaUNILFVBQWpDLENBbjZDakI7QUFvNkNLSyx3QkFBQUEsV0FwNkNMLEdBbzZDbUJ2VCxLQUFLLENBQUNjLGtCQUFOLENBQXlCd1MsU0FBekIsRUFBb0NILFVBQXBDLENBcDZDbkI7QUFBQSx3Q0FxNkNNZixJQXI2Q047QUFBQSx3Q0FzNkNDck0sUUF0NkNEO0FBQUE7QUFBQSwrQkF1NkNTZ0QsTUFBTSxDQUFDbkcsR0FBUCxDQUFXNFEsV0FBWCxDQUF1QjtBQUMzQnBJLDBCQUFBQSxFQUFFLEVBQUVyQyxNQUFNLENBQUN6SCxTQUFQLENBQWlCMlIsWUFBakIsRUFBK0J0UixPQURSO0FBRTNCbUcsMEJBQUFBLElBQUksRUFBRWlCLE1BQU0sQ0FBQ2pDLFNBQVAsQ0FBaUJnQixJQUZJO0FBRzNCd0QsMEJBQUFBLEtBQUssRUFBRTNELEVBQUUsQ0FBQzJELEtBSGlCO0FBSTNCakcsMEJBQUFBLElBQUksRUFBRWtPO0FBSnFCLHlCQUF2QixDQXY2Q1Q7O0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBNjZDS0gsT0E3NkNMO0FBQUE7QUFBQSx5RUFxNkNXSyxLQXI2Q1g7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkRBODhDVXBJLE1BOThDVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBODhDMEIxRCx3QkFBQUEsRUE5OEMxQixpRUE4OEMrQm9CLE1BQU0sQ0FBQ2pDLFNBOThDdEM7QUFBQTtBQUFBLCtCQSs4Q29EbUMsR0FBRyxDQUFDMkQsWUFBSixDQUNuRGpGLEVBQUUsQ0FBQ0csSUFEZ0QsQ0EvOENwRDs7QUFBQTtBQUFBO0FBKzhDTzVFLHdCQUFBQSxNQS84Q1AsVUErOENPQSxNQS84Q1A7QUErOENld0osd0JBQUFBLFlBLzhDZixVQSs4Q2VBLFlBLzhDZjtBQSs4QzZCSSx3QkFBQUEsWUEvOEM3QixVQSs4QzZCQSxZQS84QzdCO0FBazlDRDtBQUNBO0FBQ000Qix3QkFBQUEsVUFwOUNMLEdBcTlDQ25KLElBQUksQ0FBQ21ILFlBQUQsQ0FBSixDQUFtQlksR0FBbkIsQ0FBdUIvSCxJQUFJLENBQUN1SCxZQUFELENBQTNCLElBQTZDLENBQTdDLEdBQ0lBLFlBREosR0FFSUosWUF2OUNMLEVBdzlDRDs7QUF4OUNDLDhCQXk5Q0duSCxJQUFJLENBQUNtSixVQUFELENBQUosQ0FBaUJwQixHQUFqQixDQUFxQi9ILElBQUksQ0FBQzhGLE1BQUQsQ0FBekIsSUFBcUMsQ0F6OUN4QztBQUFBO0FBQUE7QUFBQTs7QUFBQSw4QkEwOUNPLElBQUlsSSxLQUFKLDhFQUNrRXVMLFVBRGxFLFVBMTlDUDs7QUFBQTtBQSs5Q0Q7QUFDQXJELHdCQUFBQSxNQUFNLEdBQUdBLE1BQU0sSUFBSSxDQUFWLEdBQWNxRCxVQUFkLEdBQTJCckQsTUFBcEMsQ0FoK0NDLENBaytDRDs7QUFsK0NDLDhCQW0rQ0duSSxNQUFNLEtBQUtoRixnQkFBZ0IsQ0FBQ0csUUFuK0MvQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw4QkFvK0NPLElBQUk4RSxLQUFKLENBQVUsbUNBQVYsQ0FwK0NQOztBQUFBO0FBQUE7QUFBQSwrQkF1K0NjOEYsR0FBRyxDQUFDdUssV0FBSixDQUFnQixnQkFBaEIsRUFBa0MsUUFBbEMsRUFBNEMsQ0FBQ25JLE1BQUQsQ0FBNUMsQ0F2K0NkOztBQUFBO0FBdStDRDFELHdCQUFBQSxFQUFFLENBQUM1SSxHQXYrQ0Y7QUFBQSx3Q0F5K0NZaUIsS0F6K0NaO0FBQUE7QUFBQSwrQkEwK0NPTCxjQUFjLENBQUNrTyxNQUFmLENBQXNCeEMsTUFBdEIsa0NBQ0R0QyxNQUFNLENBQUNqQyxTQUROLEVBRURhLEVBRkMsRUExK0NQOztBQUFBO0FBQUE7QUFBQSx3Q0E4K0NDb0IsTUFBTSxDQUFDbkcsR0E5K0NSO0FBQUE7QUFBQSw2Q0F5K0NrQkYsWUF6K0NsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQW1oRER5TSxTQW5oREMsRUFtaERrQjtBQUNuQk4sY0FBQUEsUUFwaERDLEVBb2hEaUI7QUFDbEJLLGNBQUFBLGVBcmhEQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXNoRER2SCx3QkFBQUEsRUF0aERDLGlFQXNoRElvQixNQUFNLENBQUNqQyxTQXRoRFg7QUFBQSx3Q0F5aERZOUcsS0F6aERaO0FBQUE7QUFBQSwrQkEwaERPTCxjQUFjLENBQUMyUCxVQUFmLENBQ0ovSixJQUFJLENBQUM0SixTQUFELENBREEsRUFFSjVKLElBQUksQ0FBQ3NKLFFBQUQsQ0FGQSxFQUdKdEosSUFBSSxDQUFDMkosZUFBRCxDQUhBLEVBSUp2SCxFQUpJLENBMWhEUDs7QUFBQTtBQUFBO0FBQUEsd0NBZ2lEQ29CLE1BQU0sQ0FBQ25HLEdBaGlEUjtBQUFBO0FBQUEsNkNBeWhEa0JGLFlBemhEbEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBK2lETXdELFlBL2lETjtBQUFBO0FBQUEsK0JBK2lEeUJyRyxNQUFNLENBQUNnUSxpQkFBUCxFQS9pRHpCOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREFnbERXeEUsTUFobERYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdsRG1CMUQsd0JBQUFBLEVBaGxEbkIsaUVBZ2xEd0JvQixNQUFNLENBQUNqQyxTQWhsRC9CO0FBQUEsd0NBa2xEZXZCLElBbGxEZjtBQUFBO0FBQUEsK0JBa2xEMEIwRCxHQUFHLENBQUNxQixhQUFKLENBQWtCM0MsRUFBRSxDQUFDRyxJQUFyQixDQWxsRDFCOztBQUFBO0FBQUE7QUFrbERLcUQsd0JBQUFBLE9BbGxETDtBQW1sREtHLHdCQUFBQSxLQW5sREwsR0FtbERhL0YsSUFBSSxDQUFDOEYsTUFBRCxDQW5sRGpCOztBQUFBLDRCQW9sRElGLE9BQU8sQ0FBQ0ksR0FBUixDQUFZRCxLQUFaLENBcGxESjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw4QkFxbERPLElBQUluSSxLQUFKLDBCQUNjVixRQUFRLENBQ3hCOEMsSUFBSSxDQUFDK0YsS0FBSyxDQUFDN0ksUUFBTixDQUFlLEVBQWYsQ0FBRCxDQURvQixDQUR0QixtRUFHc0QwSSxPQUh0RCxZQXJsRFA7O0FBQUE7QUFBQSx3Q0E0bERZbkwsS0E1bERaO0FBQUE7QUFBQSwrQkE2bERPVCxXQUFXLENBQUM2TSxPQUFaLGlDQUNEekUsRUFEQztBQUVKMkQsMEJBQUFBLEtBQUssRUFBTEE7QUFGSSwyQkE3bERQOztBQUFBO0FBQUE7QUFBQSx3Q0FpbURDdkMsTUFBTSxDQUFDbkcsR0FqbURSO0FBQUE7QUFBQSw2Q0E0bERrQkYsWUE1bERsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa29EWWlGLHdCQUFBQSxFQWxvRFosaUVBa29EaUJvQixNQUFNLENBQUNqQyxTQWxvRHhCO0FBQUEsd0NBbW9EWTlHLEtBbm9EWjtBQUFBO0FBQUEsK0JBb29ET1QsV0FBVyxDQUFDbVUsUUFBWixDQUFxQi9MLEVBQXJCLENBcG9EUDs7QUFBQTtBQUFBO0FBQUEsd0NBcW9EQ29CLE1BQU0sQ0FBQ25HLEdBcm9EUjtBQUFBO0FBQUEsNkNBbW9Ea0JGLFlBbm9EbEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBb3JERGlGLHdCQUFBQSxFQXByREMsaUVBb3JESW9CLE1BQU0sQ0FBQ2pDLFNBcHJEWDtBQXFyRER1Ryx3QkFBQUEsZUFyckRDLGlFQXFyRGlCLElBcnJEakI7QUFBQTtBQUFBLCtCQTJyRFNwRSxHQUFHLENBQUMyRCxZQUFKLENBQWlCakYsRUFBRSxDQUFDRyxJQUFwQixDQTNyRFQ7O0FBQUE7QUFBQTtBQXdyREM1RSx3QkFBQUEsTUF4ckRELFVBd3JEQ0EsTUF4ckREO0FBeXJEQ3VLLHdCQUFBQSxjQXpyREQsVUF5ckRDQSxjQXpyREQ7QUEwckRDTCx3QkFBQUEsbUJBMXJERCxVQTByRENBLG1CQTFyREQ7O0FBQUEsOEJBNnJER2xLLE1BQU0sS0FBS2hGLGdCQUFnQixDQUFDSSxTQUE1QixJQUF5QyxDQUFDK08sZUE3ckQ3QztBQUFBO0FBQUE7QUFBQTs7QUFBQSw4QkE4ckRPLElBQUlsSyxLQUFKLENBQVUsOENBQVYsQ0E5ckRQOztBQUFBO0FBQUEsOEJBK3JEVXNLLGNBQWMsS0FBSyxHQS9yRDdCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDhCQWdzRE8sSUFBSXRLLEtBQUosQ0FBVSw4Q0FBVixDQWhzRFA7O0FBQUE7QUFrc0RDa0ssd0JBQUFBLGVBQWUsR0FBRzlILElBQUksQ0FBQzZILG1CQUFELENBQXRCOztBQUNBLDRCQUFJQyxlQUFlLENBQUNDLEdBQWhCLENBQW9CLElBQUkvSyxFQUFKLENBQU8sQ0FBUCxDQUFwQixJQUFpQyxDQUFyQyxFQUF3QztBQUN0QzhLLDBCQUFBQSxlQUFlLEdBQUdBLGVBQWUsQ0FBQ0UsR0FBaEIsQ0FBb0IsSUFBSWhMLEVBQUosQ0FBTyxDQUFQLENBQXBCLENBQWxCO0FBQ0Q7O0FBcnNERix3Q0Fzc0RjdkMsS0F0c0RkO0FBQUE7QUFBQSwrQkF1c0RTTCxjQUFjLENBQUNnVSxhQUFmLENBQTZCbFIsUUFBUSxDQUFDNEssZUFBRCxDQUFyQyxFQUF3RDFGLEVBQXhELENBdnNEVDs7QUFBQTtBQUFBO0FBQUEsd0NBd3NER29CLE1BQU0sQ0FBQ25HLEdBeHNEVjtBQUFBO0FBQUEsNkNBc3NEb0JGLFlBdHNEcEI7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyREE2dUREa1IsVUE3dURDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTh1RERqTSx3QkFBQUEsRUE5dURDLGlFQTh1RElvQixNQUFNLENBQUNqQyxTQTl1RFg7QUFndkRPNEMsd0JBQUFBLEVBaHZEUCxHQWd2RHFDa0ssVUFodkRyQyxDQWd2RE9sSyxFQWh2RFAsRUFndkRXMkIsTUFodkRYLEdBZ3ZEcUN1SSxVQWh2RHJDLENBZ3ZEV3ZJLE1BaHZEWCxFQWd2RG1CcUMsYUFodkRuQixHQWd2RHFDa0csVUFodkRyQyxDQWd2RG1CbEcsYUFodkRuQjtBQUFBO0FBQUEsK0JBa3ZEMEJ6RSxHQUFHLENBQUN1RCxlQUFKLEVBbHZEMUI7O0FBQUE7QUFrdkRLQyx3QkFBQUEsWUFsdkRMOztBQUFBLDhCQXF2REdpQixhQUFhLEdBQUdqQixZQXJ2RG5CO0FBQUE7QUFBQTtBQUFBOztBQUFBLDhCQXN2RE8sSUFBSXRKLEtBQUosQ0FBVSw4Q0FBVixDQXR2RFA7O0FBQUE7QUFBQSw4QkF1dkRVa0ksTUFBTSxLQUFLLEdBdnZEckI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsOEJBd3ZETyxJQUFJbEksS0FBSixDQUFVLDhDQUFWLENBeHZEUDs7QUFBQTtBQUFBLDhCQXl2RFVrSSxNQUFNLEdBQUcsQ0F6dkRuQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw4QkEwdkRPLElBQUlsSSxLQUFKLENBQVUsMkJBQVYsQ0ExdkRQOztBQUFBO0FBNnZER2tLLHdCQUFBQSxlQTd2REgsR0E2dkRxQjlILElBQUksQ0FBQ21FLEVBQUQsQ0E3dkR6QjtBQUFBLHdDQSt2RFkxSixLQS92RFo7QUFBQTtBQUFBLCtCQWd3RE9MLGNBQWMsQ0FBQ2dVLGFBQWYsQ0FBNkJsUixRQUFRLENBQUM0SyxlQUFELENBQXJDLEVBQXdEMUYsRUFBeEQsQ0Fod0RQOztBQUFBO0FBQUE7QUFBQSx3Q0Fpd0RDb0IsTUFBTSxDQUFDbkcsR0Fqd0RSO0FBQUE7QUFBQSw2Q0ErdkRrQkYsWUEvdkRsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa3lEZ0JpRix3QkFBQUEsRUFseURoQixpRUFreURxQm9CLE1BQU0sQ0FBQ2pDLFNBbHlENUI7QUFBQSx3Q0FteURZOUcsS0FueURaO0FBQUE7QUFBQSwrQkFveURPTCxjQUFjLENBQUNrVSxZQUFmLENBQTRCbE0sRUFBNUIsQ0FweURQOztBQUFBO0FBQUE7QUFBQSx3Q0FxeURDb0IsTUFBTSxDQUFDbkcsR0FyeURSO0FBQUE7QUFBQSw2Q0FteURrQkYsWUFueURsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJEQTAwRERpUCxRQTEwREM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMjBERC9OLHdCQUFBQSxRQTMwREMsaUVBMjBEeUIsQ0FDeEI7QUFDQSx1Q0FGd0IsRUFHeEIsZ0JBSHdCLENBMzBEekI7QUFnMURENk4sd0JBQUFBLGtCQWgxREM7QUFpMUREOUosd0JBQUFBLEVBajFEQyxpRUFpMURJb0IsTUFBTSxDQUFDakMsU0FqMURYO0FBQUEsd0NBMjFEWTlHLEtBMzFEWjtBQUFBO0FBQUEsK0JBNDFET1QsV0FBVyxDQUFDdVUsR0FBWixDQUNKbkMsUUFESSxFQUVKM1IsS0FBSyxDQUFDdUUsNEJBQU4sQ0FBbUNYLFFBQW5DLENBRkksRUFHSjJCLElBQUksQ0FBQ2tNLGtCQUFELENBSEEsRUFJSjlKLEVBSkksQ0E1MURQOztBQUFBO0FBQUE7QUFBQSx3Q0FrMkRDb0IsTUFBTSxDQUFDbkcsR0FsMkRSO0FBQUE7QUFBQSw2Q0EyMURrQkYsWUEzMURsQjs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsK0NBeTJERTtBQUNMcVIsY0FBQUEsTUFBTSxFQUFFakwsaUJBREg7QUFFTEMsY0FBQUEsTUFBTSxFQUFOQSxNQUZLO0FBR0xFLGNBQUFBLEdBQUcsRUFBSEEsR0FISztBQUlMakosY0FBQUEsS0FBSyxFQUFMQSxLQUpLO0FBS0w2SSxjQUFBQSxNQUFNLEVBQU5BLE1BTEs7QUFNTG1MLGNBQUFBLFNBQVMsRUFBRTtBQUNUL1csZ0JBQUFBLFdBQVcsRUFBWEEsV0FEUztBQUVURCxnQkFBQUEsYUFBYSxFQUFiQSxhQUZTO0FBR1RrQixnQkFBQUEsZ0JBQWdCLEVBQWhCQSxnQkFIUztBQUlUSyxnQkFBQUEsaUJBQWlCLEVBQWpCQSxpQkFKUztBQUtUckIsZ0JBQUFBLHFCQUFxQixFQUFyQkEscUJBTFM7QUFNVEMsZ0JBQUFBLGNBQWMsRUFBZEE7QUFOUyxlQU5OLENBZ0JQOztBQUVBOzs7Ozs7Ozs7OztBQVdBOzs7Ozs7O0FBT0E7Ozs7Ozs7Ozs7O0FBV0E7Ozs7Ozs7Ozs7O0FBV0E7Ozs7Ozs7QUFPQTs7Ozs7OztBQU9BOzs7Ozs7Ozs7Ozs7O0FBYUE7Ozs7Ozs7Ozs7Ozs7QUFhQTs7Ozs7Ozs7QUFRQTs7Ozs7Ozs7QUFRQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTs7Ozs7Ozs7O0FBU0E7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7QUF4Tk8sYUF6MkRGOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRXRoIGZyb20gJ2V0aGpzJ1xuaW1wb3J0IFNpZ25lclByb3ZpZGVyIGZyb20gJ2V0aGpzLXByb3ZpZGVyLXNpZ25lcidcbmltcG9ydCBFdGhlcmV1bVR4IGZyb20gJ2V0aGVyZXVtanMtdHgnXG5pbXBvcnQge1xuICBkZWNvZGVQYXJhbXMsXG4gIGRlY29kZUV2ZW50LFxuICBlbmNvZGVNZXRob2QsXG4gIGVuY29kZVNpZ25hdHVyZSxcbn0gZnJvbSAnZXRoanMtYWJpJ1xuaW1wb3J0IEVOUyBmcm9tICdldGhqcy1lbnMnXG5pbXBvcnQgTGl2ZXBlZXJUb2tlbkFydGlmYWN0IGZyb20gJy4uL2V0Yy9MaXZlcGVlclRva2VuJ1xuaW1wb3J0IExpdmVwZWVyVG9rZW5GYXVjZXRBcnRpZmFjdCBmcm9tICcuLi9ldGMvTGl2ZXBlZXJUb2tlbkZhdWNldCdcbmltcG9ydCBDb250cm9sbGVyQXJ0aWZhY3QgZnJvbSAnLi4vZXRjL0NvbnRyb2xsZXInXG5pbXBvcnQgSm9ic01hbmFnZXJBcnRpZmFjdCBmcm9tICcuLi9ldGMvSm9ic01hbmFnZXInXG5pbXBvcnQgUm91bmRzTWFuYWdlckFydGlmYWN0IGZyb20gJy4uL2V0Yy9Sb3VuZHNNYW5hZ2VyJ1xuaW1wb3J0IEJvbmRpbmdNYW5hZ2VyQXJ0aWZhY3QgZnJvbSAnLi4vZXRjL0JvbmRpbmdNYW5hZ2VyJ1xuaW1wb3J0IE1pbnRlckFydGlmYWN0IGZyb20gJy4uL2V0Yy9NaW50ZXInXG5cbi8vIENvbnN0YW50c1xuZXhwb3J0IGNvbnN0IEVNUFRZX0FERFJFU1MgPSAnMHgwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwJ1xuZXhwb3J0IGNvbnN0IEFERFJFU1NfUEFEID0gJzB4MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwJ1xuZXhwb3J0IGNvbnN0IFZJREVPX1BST0ZJTEVfSURfU0laRSA9IDhcbmV4cG9ydCBjb25zdCBWSURFT19QUk9GSUxFUyA9IHtcbiAgUDcyMHA2MGZwczE2eDk6IHtcbiAgICBoYXNoOiAnYTdhYzEzN2EnLFxuICAgIG5hbWU6ICdQNzIwcDYwZnBzMTZ4OScsXG4gICAgYml0cmF0ZTogJzYwMDBrJyxcbiAgICBmcmFtZXJhdGU6IDYwLFxuICAgIHJlc29sdXRpb246ICcxMjgweDcyMCcsXG4gIH0sXG4gIFA3MjBwMzBmcHMxNng5OiB7XG4gICAgaGFzaDogJzQ5ZDU0ZWE5JyxcbiAgICBuYW1lOiAnUDcyMHAzMGZwczE2eDknLFxuICAgIGJpdHJhdGU6ICc0MDAwaycsXG4gICAgZnJhbWVyYXRlOiAzMCxcbiAgICByZXNvbHV0aW9uOiAnMTI4MHg3MjAnLFxuICB9LFxuICBQNzIwcDMwZnBzNHgzOiB7XG4gICAgaGFzaDogJzc5MzMyZmU3JyxcbiAgICBuYW1lOiAnUDcyMHAzMGZwczR4MycsXG4gICAgYml0cmF0ZTogJzM1MDBrJyxcbiAgICBmcmFtZXJhdGU6IDMwLFxuICAgIHJlc29sdXRpb246ICc5NjB4NzIwJyxcbiAgfSxcbiAgUDU3NnAzMGZwczE2eDk6IHtcbiAgICBoYXNoOiAnNWVjZjRiNTInLFxuICAgIG5hbWU6ICdQNTc2cDMwZnBzMTZ4OScsXG4gICAgYml0cmF0ZTogJzE1MDBrJyxcbiAgICBmcmFtZXJhdGU6IDMwLFxuICAgIHJlc29sdXRpb246ICcxMDI0eDU3NicsXG4gIH0sXG4gIFAzNjBwMzBmcHMxNng5OiB7XG4gICAgaGFzaDogJzkzYzcxN2U3JyxcbiAgICBuYW1lOiAnUDM2MHAzMGZwczE2eDknLFxuICAgIGJpdHJhdGU6ICcxMjAwaycsXG4gICAgZnJhbWVyYXRlOiAzMCxcbiAgICByZXNvbHV0aW9uOiAnNjQweDM2MCcsXG4gIH0sXG4gIFAzNjBwMzBmcHM0eDM6IHtcbiAgICBoYXNoOiAnYjYwMzgyYTAnLFxuICAgIG5hbWU6ICdQMzYwcDMwZnBzNHgzJyxcbiAgICBiaXRyYXRlOiAnMTAwMGsnLFxuICAgIGZyYW1lcmF0ZTogMzAsXG4gICAgcmVzb2x1dGlvbjogJzQ4MHgzNjAnLFxuICB9LFxuICBQMjQwcDMwZnBzMTZ4OToge1xuICAgIGhhc2g6ICdjMGE2NTE3YScsXG4gICAgbmFtZTogJ1AyNDBwMzBmcHMxNng5JyxcbiAgICBiaXRyYXRlOiAnNjAwaycsXG4gICAgZnJhbWVyYXRlOiAzMCxcbiAgICByZXNvbHV0aW9uOiAnNDI2eDI0MCcsXG4gIH0sXG4gIFAyNDBwMzBmcHM0eDM6IHtcbiAgICBoYXNoOiAnZDQzNWM1M2EnLFxuICAgIG5hbWU6ICdQMjQwcDMwZnBzNHgzJyxcbiAgICBiaXRyYXRlOiAnNjAwaycsXG4gICAgZnJhbWVyYXRlOiAzMCxcbiAgICByZXNvbHV0aW9uOiAnMzIweDI0MCcsXG4gIH0sXG4gIFAxNDRwMzBmcHMxNng5OiB7XG4gICAgaGFzaDogJ2ZjYTQwYmY5JyxcbiAgICBuYW1lOiAnUDE0NHAzMGZwczE2eDknLFxuICAgIGJpdHJhdGU6ICc0MDBrJyxcbiAgICBmcmFtZXJhdGU6IDMwLFxuICAgIHJlc29sdXRpb246ICcyNTZ4MTQ0JyxcbiAgfSxcbn1cblxuY29uc3QgREVMRUdBVE9SX1NUQVRVUyA9IFsnUGVuZGluZycsICdCb25kZWQnLCAnVW5ib25kZWQnLCAnVW5ib25kaW5nJ11cbkRFTEVHQVRPUl9TVEFUVVMuUGVuZGluZyA9IERFTEVHQVRPUl9TVEFUVVNbMF1cbkRFTEVHQVRPUl9TVEFUVVMuQm9uZGVkID0gREVMRUdBVE9SX1NUQVRVU1sxXVxuREVMRUdBVE9SX1NUQVRVUy5VbmJvbmRlZCA9IERFTEVHQVRPUl9TVEFUVVNbMl1cbkRFTEVHQVRPUl9TVEFUVVMuVW5ib25kaW5nID0gREVMRUdBVE9SX1NUQVRVU1szXVxuZXhwb3J0IHsgREVMRUdBVE9SX1NUQVRVUyB9XG5jb25zdCBUUkFOU0NPREVSX1NUQVRVUyA9IFsnTm90UmVnaXN0ZXJlZCcsICdSZWdpc3RlcmVkJ11cblRSQU5TQ09ERVJfU1RBVFVTLk5vdFJlZ2lzdGVyZWQgPSBUUkFOU0NPREVSX1NUQVRVU1swXVxuVFJBTlNDT0RFUl9TVEFUVVMuUmVnaXN0ZXJlZCA9IFRSQU5TQ09ERVJfU1RBVFVTWzFdXG5leHBvcnQgeyBUUkFOU0NPREVSX1NUQVRVUyB9XG5cbi8vIERlZmF1bHRzXG5leHBvcnQgY29uc3QgREVGQVVMVFMgPSB7XG4gIGNvbnRyb2xsZXJBZGRyZXNzOiAnMHhmOTZkNTRlNDkwMzE3YzU1N2E5NjdhYmZhNWQ2ZTMzMDA2YmU2OWIzJyxcbiAgcHJvdmlkZXI6ICdodHRwczovL21haW5uZXQuaW5mdXJhLmlvL3YzL2U5ZGM1NGRiZDhkZTQ2NjQ4OTBlNjQxYThlZmE0NWIxJyxcbiAgcHJpdmF0ZUtleXM6IHt9LCAvLyB7IFtwdWJsaWNLZXk6IHN0cmluZ106IHByaXZhdGVLZXkgfVxuICBhY2NvdW50OiAnJyxcbiAgZ2FzOiAwLFxuICBhcnRpZmFjdHM6IHtcbiAgICBMaXZlcGVlclRva2VuOiBMaXZlcGVlclRva2VuQXJ0aWZhY3QsXG4gICAgTGl2ZXBlZXJUb2tlbkZhdWNldDogTGl2ZXBlZXJUb2tlbkZhdWNldEFydGlmYWN0LFxuICAgIENvbnRyb2xsZXI6IENvbnRyb2xsZXJBcnRpZmFjdCxcbiAgICBKb2JzTWFuYWdlcjogSm9ic01hbmFnZXJBcnRpZmFjdCxcbiAgICBSb3VuZHNNYW5hZ2VyOiBSb3VuZHNNYW5hZ2VyQXJ0aWZhY3QsXG4gICAgQm9uZGluZ01hbmFnZXI6IEJvbmRpbmdNYW5hZ2VyQXJ0aWZhY3QsXG4gICAgTWludGVyOiBNaW50ZXJBcnRpZmFjdCxcbiAgfSxcbiAgZW5zUmVnaXN0cmllczoge1xuICAgIC8vIE1haW5uZXRcbiAgICAnMSc6ICcweDMxNDE1OTI2NWRkOGRiYjMxMDY0MmY5OGY1MGMwNjYxNzNjMTI1OWInLFxuICAgIC8vIFJvcHN0ZW5cbiAgICAnMyc6ICcweDExMjIzNDQ1NWMzYTMyZmQxMTIzMGM0MmU3YmNjZDRhODRlMDIwMTAnLFxuICAgIC8vIFJpbmtlYnlcbiAgICAnNCc6ICcweGU3NDEwMTcwZjg3MTAyZGYwMDU1ZWIxOTUxNjNhMDNiN2YyYmZmNGEnLFxuICB9LFxufVxuXG4vLyBVdGlsc1xuZXhwb3J0IGNvbnN0IHV0aWxzID0ge1xuICBpc1ZhbGlkQWRkcmVzczogeCA9PiAvXjB4W2EtZkEtRjAtOV17NDB9JC8udGVzdCh4KSxcbiAgcmVzb2x2ZUFkZHJlc3M6IGFzeW5jIChyZXNvbHZlLCB4KSA9PlxuICAgIHV0aWxzLmlzVmFsaWRBZGRyZXNzKHgpID8geCA6IGF3YWl0IHJlc29sdmUoeCksXG4gIGdldE1ldGhvZEhhc2g6IGl0ZW0gPT4ge1xuICAgIC8vIGNvbnN0IHNpZyA9IGAke2l0ZW0ubmFtZX0oJHtpdGVtLmlucHV0cy5tYXAoeCA9PiB4LnR5cGUpLmpvaW4oJywnKX0pYFxuICAgIC8vIGNvbnN0IGhhc2ggPSBFdGgua2VjY2FrMjU2KHNpZylcbiAgICAvLyByZXR1cm4gaGFzaFxuICAgIHJldHVybiBlbmNvZGVTaWduYXR1cmUoaXRlbSlcbiAgfSxcbiAgZmluZEFiaUJ5TmFtZTogKGFiaXMsIG5hbWUpID0+IHtcbiAgICBjb25zdCBbYWJpXSA9IGFiaXMuZmlsdGVyKGl0ZW0gPT4ge1xuICAgICAgaWYgKGl0ZW0udHlwZSAhPT0gJ2Z1bmN0aW9uJykgcmV0dXJuIGZhbHNlXG4gICAgICBpZiAoaXRlbS5uYW1lID09PSBuYW1lKSByZXR1cm4gdHJ1ZVxuICAgIH0pXG4gICAgcmV0dXJuIGFiaVxuICB9LFxuICBmaW5kQWJpQnlIYXNoOiAoYWJpcywgaGFzaCkgPT4ge1xuICAgIGNvbnN0IFthYmldID0gYWJpcy5maWx0ZXIoaXRlbSA9PiB7XG4gICAgICBpZiAoaXRlbS50eXBlICE9PSAnZnVuY3Rpb24nKSByZXR1cm4gZmFsc2VcbiAgICAgIHJldHVybiBlbmNvZGVTaWduYXR1cmUoaXRlbSkgPT09IGhhc2hcbiAgICB9KVxuICAgIHJldHVybiBhYmlcbiAgfSxcbiAgZW5jb2RlTWV0aG9kUGFyYW1zOiAoYWJpLCBwYXJhbXMpID0+IHtcbiAgICByZXR1cm4gZW5jb2RlTWV0aG9kKGFiaSwgcGFyYW1zKVxuICB9LFxuICBkZWNvZGVNZXRob2RQYXJhbXM6IChhYmksIGJ5dGVjb2RlKSA9PiB7XG4gICAgcmV0dXJuIGRlY29kZVBhcmFtcyhcbiAgICAgIGFiaS5pbnB1dHMubWFwKHggPT4geC5uYW1lKSxcbiAgICAgIGFiaS5pbnB1dHMubWFwKHggPT4geC50eXBlKSxcbiAgICAgIGAweCR7Ynl0ZWNvZGUuc3Vic3RyKDEwKX1gLFxuICAgICAgZmFsc2UsXG4gICAgKVxuICB9LFxuICBkZWNvZGVDb250cmFjdElucHV0OiAoY29udHJhY3RzLCBjb250cmFjdEFkZHJlc3MsIGlucHV0KSA9PiB7XG4gICAgZm9yIChjb25zdCBrZXkgaW4gY29udHJhY3RzKSB7XG4gICAgICBjb25zdCBjb250cmFjdCA9IGNvbnRyYWN0c1trZXldXG4gICAgICBpZiAoY29udHJhY3QuYWRkcmVzcyAhPT0gY29udHJhY3RBZGRyZXNzKSBjb250aW51ZVxuICAgICAgY29uc3QgaGFzaCA9IGlucHV0LnN1YnN0cmluZygwLCAxMClcbiAgICAgIGNvbnN0IGFiaSA9IHV0aWxzLmZpbmRBYmlCeUhhc2goY29udHJhY3QuYWJpLCBoYXNoKVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgY29udHJhY3Q6IGtleSxcbiAgICAgICAgbWV0aG9kOiBhYmkubmFtZSxcbiAgICAgICAgcGFyYW1zOiBPYmplY3QuZW50cmllcyh1dGlscy5kZWNvZGVNZXRob2RQYXJhbXMoYWJpLCBpbnB1dCkpLnJlZHVjZShcbiAgICAgICAgICAob2JqLCBbaywgdl0pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIC4uLm9iaixcbiAgICAgICAgICAgICAgW2tdOiBBcnJheS5pc0FycmF5KHYpXG4gICAgICAgICAgICAgICAgPyB2Lm1hcChfdiA9PiAoQk4uaXNCTihfdikgPyB0b1N0cmluZyhfdikgOiBfdikpXG4gICAgICAgICAgICAgICAgOiBCTi5pc0JOKHYpXG4gICAgICAgICAgICAgICAgPyB0b1N0cmluZyh2KVxuICAgICAgICAgICAgICAgIDogdixcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHt9LFxuICAgICAgICApLFxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBjb250cmFjdDogJycsIG1ldGhvZDogJycsIHBhcmFtczoge30gfVxuICB9LFxuICAvKipcbiAgICogUG9sbHMgZm9yIGEgdHJhbnNhY3Rpb24gcmVjZWlwdFxuICAgKiBAaWdub3JlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSAgIHR4SGFzaCAtIHRoZSB0cmFuc2FjdGlvbiBoYXNoXG4gICAqIEBwYXJhbSB7RXRofSAgICAgIGV0aCAgICAtIGFuIGluc3RhbmNlIG9mIEV0aGpzXG4gICAqIEByZXR1cm4ge09iamVjdH1cbiAgICovXG4gIGdldFR4UmVjZWlwdDogYXN5bmMgKHR4SGFzaCwgZXRoKSA9PiB7XG4gICAgcmV0dXJuIGF3YWl0IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHNldFRpbWVvdXQoYXN5bmMgZnVuY3Rpb24gcG9sbEZvclJlY2VpcHQoKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgY29uc3QgcmVjZWlwdCA9IGF3YWl0IGV0aC5nZXRUcmFuc2FjdGlvblJlY2VpcHQodHhIYXNoKVxuICAgICAgICAgIGlmIChyZWNlaXB0KSB7XG4gICAgICAgICAgICByZXR1cm4gcmVjZWlwdC5zdGF0dXMgPT09ICcweDEnXG4gICAgICAgICAgICAgID8gLy8gc3VjY2Vzc1xuICAgICAgICAgICAgICAgIHJlc29sdmUocmVjZWlwdClcbiAgICAgICAgICAgICAgOiAvLyBmYWlsXG4gICAgICAgICAgICAgICAgcmVqZWN0KFxuICAgICAgICAgICAgICAgICAgbmV3IEVycm9yKFxuICAgICAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShcbiAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWNlaXB0LFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNhY3Rpb246IGF3YWl0IGV0aC5nZXRUcmFuc2FjdGlvbkJ5SGFzaChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmVjZWlwdC50cmFuc2FjdGlvbkhhc2gsXG4gICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAyLFxuICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgfVxuICAgICAgICAgIHNldFRpbWVvdXQocG9sbEZvclJlY2VpcHQsIDMwMClcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgcmVqZWN0KGVycilcbiAgICAgICAgfVxuICAgICAgfSwgMClcbiAgICB9KVxuICB9LFxuICAvKipcbiAgICogUGFyc2VzIGFuIGVuY29kZWQgc3RyaW5nIG9mIHRyYW5zY29kaW5nIG9wdGlvbnNcbiAgICogQGlnbm9yZVxuICAgKiBAcGFyYW0gIHtzdHJpbmd9IG9wdHMgLSB0cmFuc2NvZGluZyBvcHRpb25zXG4gICAqIEByZXR1cm4ge09iamVjdFtdfVxuICAgKi9cbiAgcGFyc2VUcmFuc2NvZGluZ09wdGlvbnM6IG9wdHMgPT4ge1xuICAgIGNvbnN0IHByb2ZpbGVzID0gT2JqZWN0LnZhbHVlcyhWSURFT19QUk9GSUxFUylcbiAgICBjb25zdCB2YWxpZEhhc2hlcyA9IG5ldyBTZXQocHJvZmlsZXMubWFwKHggPT4geC5oYXNoKSlcbiAgICBsZXQgaGFzaGVzID0gW11cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IG9wdHMubGVuZ3RoOyBpICs9IFZJREVPX1BST0ZJTEVfSURfU0laRSkge1xuICAgICAgY29uc3QgaGFzaCA9IG9wdHMuc2xpY2UoaSwgaSArIFZJREVPX1BST0ZJTEVfSURfU0laRSlcbiAgICAgIGlmICghdmFsaWRIYXNoZXMuaGFzKGhhc2gpKSBjb250aW51ZVxuICAgICAgaGFzaGVzLnB1c2goaGFzaClcbiAgICB9XG4gICAgcmV0dXJuIGhhc2hlcy5tYXAoeCA9PiBwcm9maWxlcy5maW5kKCh7IGhhc2ggfSkgPT4geCA9PT0gaGFzaCkpXG4gIH0sXG4gIC8qKlxuICAgKiBTZXJpYWxpemVzIGEgbGlzdCBvZiB0cmFuc2NvZGluZyBwcm9maWxlcyBuYW1lIGludG8gYSBoYXNoXG4gICAqIEBpZ25vcmVcbiAgICogQHBhcmFtICB7c3RyaW5nW119IG5hbWUgLSB0cmFuc2NvZGluZyBwcm9maWxlIG5hbWVcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgc2VyaWFsaXplVHJhbnNjb2RpbmdQcm9maWxlczogbmFtZXMgPT4ge1xuICAgIHJldHVybiBbXG4gICAgICAuLi5uZXcgU2V0KCAvLyBkZWR1cGUgcHJvZmlsZXNcbiAgICAgICAgbmFtZXMubWFwKHggPT5cbiAgICAgICAgICBWSURFT19QUk9GSUxFU1t4XVxuICAgICAgICAgICAgPyBWSURFT19QUk9GSUxFU1t4XS5oYXNoXG4gICAgICAgICAgICA6IFZJREVPX1BST0ZJTEVTLlAyNDBwMzBmcHM0eDMuaGFzaCxcbiAgICAgICAgKSxcbiAgICAgICksXG4gICAgXS5qb2luKCcnKVxuICB9LFxuICAvKipcbiAgICogUGFkcyBhbiBhZGRyZXNzIHdpdGggMHMgb24gdGhlIGxlZnQgKGZvciB0b3BpYyBlbmNvZGluZylcbiAgICogQGlnbm9yZVxuICAgKiBAcGFyYW0gIHtzdHJpbmd9IGFkZHIgLSBhbiBFVEggYWRkcmVzc1xuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqL1xuICBwYWRBZGRyZXNzOiBhZGRyID0+IEFERFJFU1NfUEFEICsgYWRkci5zdWJzdHIoMiksXG4gIC8qKlxuICAgKiBFbmNvZGVzIGFuIGV2ZW50IGZpbHRlciBvYmplY3QgaW50byBhIHRvcGljIGxpc3RcbiAgICogQGlnbm9yZVxuICAgKiBAcGFyYW0gIHtGdW5jdGlvbn0gZXZlbnQgICAtIGEgY29udHJhY3QgZXZlbnQgbWV0aG9kXG4gICAqIEBwYXJhbSAge09iamVjdH0gICBmaWx0ZXJzIC0ga2V5L3ZhbHVlIG1hcCBvZiBpbmRleGVkIGV2ZW50IHBhcmFtc1xuICAgKiBAcmV0dXJuIHtzdHJpbmdbXX1cbiAgICovXG4gIGVuY29kZUV2ZW50VG9waWNzOiAoZXZlbnQsIGZpbHRlcnMpID0+IHtcbiAgICByZXR1cm4gZXZlbnQuYWJpLmlucHV0cy5yZWR1Y2UoXG4gICAgICAodG9waWNzLCB7IGluZGV4ZWQsIG5hbWUsIHR5cGUgfSwgaSkgPT4ge1xuICAgICAgICBpZiAoIWluZGV4ZWQpIHJldHVybiB0b3BpY3NcbiAgICAgICAgaWYgKCFmaWx0ZXJzLmhhc093blByb3BlcnR5KG5hbWUpKSByZXR1cm4gWy4uLnRvcGljcywgbnVsbF1cbiAgICAgICAgaWYgKHR5cGUgPT09ICdhZGRyZXNzJyAmJiAnc3RyaW5nJyA9PT0gdHlwZW9mIGZpbHRlcnNbbmFtZV0pXG4gICAgICAgICAgcmV0dXJuIFsuLi50b3BpY3MsIHV0aWxzLnBhZEFkZHJlc3MoZmlsdGVyc1tuYW1lXSldXG4gICAgICAgIHJldHVybiBbLi4udG9waWNzLCBmaWx0ZXJzW25hbWVdXVxuICAgICAgfSxcbiAgICAgIFtldmVudCgpLm9wdGlvbnMuZGVmYXVsdEZpbHRlck9iamVjdC50b3BpY3NbMF1dLFxuICAgIClcbiAgfSxcbiAgLyoqXG4gICAqIFR1cm5zIGEgcmF3IGV2ZW50IGxvZyBpbnRvIGEgcmVzdWx0IG9iamVjdFxuICAgKiBAaWdub3JlXG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBldmVudCAgLSBhIGNvbnRyYWN0IGV2ZW50IG1ldGhvZFxuICAgKiBAcGFyYW0gIHtzdHJpbmd9ICAgZGF0YSAgIC0gYnl0ZWNvZGUgZnJvbSBsb2dcbiAgICogQHBhcmFtICB7c3RyaW5nW119IHRvcGljcyAtIGxpc3Qgb2YgdG9waWNzIGZvciBsb2cgcXVlcnlcbiAgICogQHJldHVybiB7T2JqZWN0fVxuICAgKi9cbiAgZGVjb2RlRXZlbnQ6IGV2ZW50ID0+ICh7IGRhdGEsIHRvcGljcyB9KSA9PiB7XG4gICAgcmV0dXJuIGRlY29kZUV2ZW50KGV2ZW50LmFiaSwgZGF0YSwgdG9waWNzLCBmYWxzZSlcbiAgfSxcbn1cblxuLy8gSGVscGVyIGZ1bmN0aW9uc1xuLy8gZXRoanMgcmV0dXJucyBhIFJlc3VsdCB0eXBlIGZyb20gcnBjIHJlcXVlc3RzXG4vLyB0aGVzZSBmdW5jdGlvbnMgaGVscCB3aXRoIGZvcm1hdHRpbmcgdGhvc2UgdmFsdWVzXG5jb25zdCB7IEJOIH0gPSBFdGhcbmNvbnN0IHRvQk4gPSBuID0+IChCTi5pc0JOKG4pID8gbiA6IG5ldyBCTihuLnRvU3RyaW5nKDEwKSwgMTApKVxuY29uc3QgY29tcG9zZSA9ICguLi5mbnMpID0+IGZucy5yZWR1Y2UoKGYsIGcpID0+ICguLi5hcmdzKSA9PiBmKGcoLi4uYXJncykpKVxuY29uc3QgcHJvcCA9IChrOiBzdHJpbmcgfCBudW1iZXIpID0+ICh4KTogYW55ID0+IHhba11cbmNvbnN0IHRvQm9vbCA9ICh4OiBhbnkpOiBib29sZWFuID0+ICEheFxuY29uc3QgdG9TdHJpbmcgPSAoeDogRXRoLkJOKTogc3RyaW5nID0+IHgudG9TdHJpbmcoMTApXG5jb25zdCB0b051bWJlciA9ICh4OiBFdGguQk4pOiBzdHJpbmcgPT4gTnVtYmVyKHgudG9TdHJpbmcoMTApKVxuY29uc3QgaGVhZFRvQm9vbCA9IGNvbXBvc2UoXG4gIHRvQm9vbCxcbiAgcHJvcCgwKSxcbilcbmNvbnN0IGhlYWRUb1N0cmluZyA9IGNvbXBvc2UoXG4gIHRvU3RyaW5nLFxuICBwcm9wKDApLFxuKVxuY29uc3QgaGVhZFRvTnVtYmVyID0gY29tcG9zZShcbiAgdG9OdW1iZXIsXG4gIHByb3AoMCksXG4pXG5jb25zdCBpbnZhcmlhbnQgPSAobmFtZSwgcG9zLCB0eXBlKSA9PiB7XG4gIHRocm93IG5ldyBFcnJvcihgTWlzc2luZyBhcmd1bWVudCBcIiR7bmFtZX1cIiAoJHt0eXBlfSkgYXQgcG9zaXRpb24gJHtwb3N9YClcbn1cbmNvbnN0IGZvcm1hdER1cmF0aW9uID0gbXMgPT4ge1xuICBjb25zdCBzZWNvbmRzID0gKG1zIC8gMTAwMCkudG9GaXhlZCgxKVxuICBjb25zdCBtaW51dGVzID0gKG1zIC8gKDEwMDAgKiA2MCkpLnRvRml4ZWQoMSlcbiAgY29uc3QgaG91cnMgPSAobXMgLyAoMTAwMCAqIDYwICogNjApKS50b0ZpeGVkKDEpXG4gIGNvbnN0IGRheXMgPSAobXMgLyAoMTAwMCAqIDYwICogNjAgKiAyNCkpLnRvRml4ZWQoMSlcbiAgaWYgKHNlY29uZHMgPCA2MCkgcmV0dXJuIHNlY29uZHMgKyAnIHNlYydcbiAgZWxzZSBpZiAobWludXRlcyA8IDYwKSByZXR1cm4gbWludXRlcyArICcgbWluJ1xuICBlbHNlIGlmIChob3VycyA8IDI0KSByZXR1cm4gaG91cnMgKyAnIGhvdXJzJ1xuICByZXR1cm4gZGF5cyArICcgZGF5cydcbn1cblxuLyoqXG4gKiBEZXBsb3lzIGNvbnRyYWN0IGFuZCByZXR1cm4gaW5zdGFuY2UgYXQgZGVwbG95ZWQgYWRkcmVzc1xuICogQGlnbm9yZVxuICogQHBhcmFtIHsqfSBldGhcbiAqIEBwYXJhbSB7Kn0gYXJnc1xuICovXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZGVwbG95Q29udHJhY3QoXG4gIGV0aCxcbiAgeyBhYmksIGJ5dGVjb2RlLCBkZWZhdWx0VHggfSxcbik6IFByb21pc2U8Q29udHJhY3Q+IHtcbiAgY29uc3QgY29udHJhY3QgPSBldGguY29udHJhY3QoYWJpLCBieXRlY29kZSwgZGVmYXVsdFR4KVxuICBjb25zdCB0eEhhc2ggPSBhd2FpdCBjb250cmFjdC5uZXcoKVxuICBjb25zdCByZWNlaXB0ID0gYXdhaXQgZXRoLmdldFRyYW5zYWN0aW9uU3VjY2Vzcyh0eEhhc2gpXG4gIHJldHVybiBjb250cmFjdC5hdChyZWNlaXB0LmNvbnRyYWN0QWRkcmVzcylcbn1cblxuLyoqXG4gKiBDcmVhdGVzIGEgY29udHJhY3QgaW5zdGFuY2UgZnJvbSBhIHNwZWNpZmljIGFkZHJlc3NcbiAqIEBpZ25vcmVcbiAqIEBwYXJhbSB7RXRofSAgICBldGggICAgIC0gZXRoanMgaW5zdGFuY2VcbiAqIEBwYXJhbSB7c3RyaW5nfSBhZGRyZXNzIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBhcmdzWzBdIC0gYW4gb2JqZWN0IGNvbnRhaW5pbmcgYWxsIHJlbGV2YW50IExpdmVwZWVyIEFydGlmYWN0c1xuICovXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q29udHJhY3RBdChcbiAgZXRoLFxuICB7IGFiaSwgYnl0ZWNvZGUsIGFkZHJlc3MsIGRlZmF1bHRUeCB9LFxuKTogQ29udHJhY3Qge1xuICByZXR1cm4gZXRoLmNvbnRyYWN0KGFiaSwgYnl0ZWNvZGUsIGRlZmF1bHRUeCkuYXQoYWRkcmVzcylcbn1cblxuLyoqXG4gKiBDcmVhdGVzIGFuIGluc3RhbmNlIG9mIEV0aCBhbmQgYSBkZWZhdWx0IHRyYW5zYWN0aW9uIG9iamVjdFxuICogQGlnbm9yZVxuICogQHJldHVybiB7eyBldCwgZ2FzOiBFdGgsIGRlZmF1bHRUeDogeyBmcm9tOiBzdHJpbmcsIGdhczogbnVtYmVyIH0gfX1cbiAqL1xuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGluaXRSUEMoe1xuICBhY2NvdW50LFxuICBwcml2YXRlS2V5cyxcbiAgZ2FzLFxuICBwcm92aWRlcixcbn0pOiBQcm9taXNlPHtcbiAgZXRoOiBFdGgsXG4gIGRlZmF1bHRUeDogeyBmcm9tOiBzdHJpbmcsIGdhczogbnVtYmVyIH0sXG59PiB7XG4gIGNvbnN0IHVzZVByaXZhdGVLZXlzID0gMCA8IE9iamVjdC5rZXlzKHByaXZhdGVLZXlzKS5sZW5ndGhcbiAgY29uc3QgZXRoanNQcm92aWRlciA9XG4gICAgJ29iamVjdCcgPT09IHR5cGVvZiBwcm92aWRlciAmJiBwcm92aWRlclxuICAgICAgPyBwcm92aWRlclxuICAgICAgOiB1c2VQcml2YXRlS2V5c1xuICAgICAgPyAvLyBVc2UgcHJvdmlkZXItc2lnbmVyIHRvIGxvY2FsbHkgc2lnbiB0cmFuc2FjdGlvbnNcbiAgICAgICAgbmV3IFNpZ25lclByb3ZpZGVyKHByb3ZpZGVyLCB7XG4gICAgICAgICAgc2lnblRyYW5zYWN0aW9uOiAocmF3VHgsIGNiKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB0eCA9IG5ldyBFdGhlcmV1bVR4KHJhd1R4KVxuICAgICAgICAgICAgdHguc2lnbihwcml2YXRlS2V5c1tmcm9tXSlcbiAgICAgICAgICAgIGNiKG51bGwsICcweCcgKyB0eC5zZXJpYWxpemUoKS50b1N0cmluZygnaGV4JykpXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhY2NvdW50czogY2IgPT4gY2IobnVsbCwgYWNjb3VudHMpLFxuICAgICAgICAgIHRpbWVvdXQ6IDEwICogMTAwMCxcbiAgICAgICAgfSlcbiAgICAgIDogLy8gVXNlIGRlZmF1bHQgc2lnbmVyXG4gICAgICAgIG5ldyBFdGguSHR0cFByb3ZpZGVyKHByb3ZpZGVyIHx8IERFRkFVTFRTLnByb3ZpZGVyKVxuICBjb25zdCBldGggPSBuZXcgRXRoKGV0aGpzUHJvdmlkZXIpXG4gIGNvbnN0IGVucyA9IG5ldyBFTlMoe1xuICAgIHByb3ZpZGVyOiBldGguY3VycmVudFByb3ZpZGVyLFxuICAgIHJlZ2lzdHJ5QWRkcmVzczogREVGQVVMVFMuZW5zUmVnaXN0cmllc1thd2FpdCBldGgubmV0X3ZlcnNpb24oKV0sXG4gIH0pXG4gIGNvbnN0IGFjY291bnRzID0gdXNlUHJpdmF0ZUtleXNcbiAgICA/IE9iamVjdC5rZXlzKHByaXZhdGVLZXlzKVxuICAgIDogYXdhaXQgZXRoLmFjY291bnRzKClcbiAgY29uc3QgZnJvbSA9XG4gICAgLy8gc2VsZWN0IGFjY291bnQgYnkgYWRkcmVzcyBvciBpbmRleFxuICAgIC8vIGRlZmF1bHQgdG8gRU1QVFlfQUREUkVTUyAocmVhZC1vbmx5OyBjYW5ub3QgdHJhbnNhY3QpXG4gICAgbmV3IFNldChhY2NvdW50cykuaGFzKGFjY291bnQpXG4gICAgICA/IGFjY291bnRcbiAgICAgIDogYWNjb3VudHNbYWNjb3VudF0gfHwgRU1QVFlfQUREUkVTU1xuICByZXR1cm4ge1xuICAgIGV0aCxcbiAgICBlbnMsXG4gICAgcHJvdmlkZXIsXG4gICAgYWNjb3VudHMsXG4gICAgZGVmYXVsdFR4OiB7XG4gICAgICBmcm9tLFxuICAgICAgZ2FzLFxuICAgIH0sXG4gIH1cbn1cblxuLyoqXG4gKiBDcmVhdGVzIGluc3RhbmNlcyBvZiBhbGwgbWFpbiBMaXZlcGVlciBjb250cmFjdHNcbiAqIEBpZ25vcmVcbiAqIEBwYXJhbSB7c3RyaW5nfSBvcHRzLnByb3ZpZGVyICAtIHRoZSBodHRwUHJvdmlkZXIgZm9yIGNvbnRyYWN0IFJQQ1xuICogQHBhcmFtIHtPYmplY3R9IG9wdHMuYXJ0aWZhY3RzIC0gLi4uXG4gKi9cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBpbml0Q29udHJhY3RzKFxuICBvcHRzID0ge30sXG4pOiBQcm9taXNlPE9iamVjdDxzdHJpbmcsIENvbnRyYWN0Pj4ge1xuICAvLyBNZXJnZSBwYXNzIG9wdGlvbnMgd2l0aCBkZWZhdWx0c1xuICBjb25zdCB7XG4gICAgYWNjb3VudCA9IERFRkFVTFRTLmFjY291bnQsXG4gICAgYXJ0aWZhY3RzID0gREVGQVVMVFMuYXJ0aWZhY3RzLFxuICAgIGNvbnRyb2xsZXJBZGRyZXNzID0gREVGQVVMVFMuY29udHJvbGxlckFkZHJlc3MsXG4gICAgZ2FzID0gREVGQVVMVFMuZ2FzLFxuICAgIHByaXZhdGVLZXlzID0gREVGQVVMVFMucHJpdmF0ZUtleXMsXG4gICAgcHJvdmlkZXIgPSBERUZBVUxUUy5wcm92aWRlcixcbiAgfSA9IG9wdHNcbiAgLy8gSW5zdGFuc3RpYXRlIG5ldyBldGhqcyBpbnN0YW5jZSB3aXRoIHNwZWNpZmllZCBwcm92aWRlclxuICBjb25zdCB7IGFjY291bnRzLCBkZWZhdWx0VHgsIGVucywgZXRoIH0gPSBhd2FpdCBpbml0UlBDKHtcbiAgICBhY2NvdW50LFxuICAgIGdhcyxcbiAgICBwcml2YXRlS2V5cyxcbiAgICBwcm92aWRlcixcbiAgfSlcbiAgY29uc3QgY29udHJhY3RzID0ge1xuICAgIExpdmVwZWVyVG9rZW46IG51bGwsXG4gICAgTGl2ZXBlZXJUb2tlbkZhdWNldDogbnVsbCxcbiAgICBCb25kaW5nTWFuYWdlcjogbnVsbCxcbiAgICBKb2JzTWFuYWdlcjogbnVsbCxcbiAgICBSb3VuZHNNYW5hZ2VyOiBudWxsLFxuICAgIE1pbnRlcjogbnVsbCxcbiAgfVxuICBjb25zdCBoYXNoZXMgPSB7XG4gICAgTGl2ZXBlZXJUb2tlbjoge30sXG4gICAgTGl2ZXBlZXJUb2tlbkZhdWNldDoge30sXG4gICAgQm9uZGluZ01hbmFnZXI6IHt9LFxuICAgIEpvYnNNYW5hZ2VyOiB7fSxcbiAgICBSb3VuZHNNYW5hZ2VyOiB7fSxcbiAgICBNaW50ZXI6IHt9LFxuICB9XG4gIC8vIENyZWF0ZSBhIENvbnRyb2xsZXIgY29udHJhY3QgaW5zdGFuY2VcbiAgY29uc3QgQ29udHJvbGxlciA9IGF3YWl0IGdldENvbnRyYWN0QXQoZXRoLCB7XG4gICAgLi4uYXJ0aWZhY3RzLkNvbnRyb2xsZXIsXG4gICAgZGVmYXVsdFR4LFxuICAgIGFkZHJlc3M6IGNvbnRyb2xsZXJBZGRyZXNzLFxuICB9KVxuICBmb3IgKGNvbnN0IG5hbWUgb2YgT2JqZWN0LmtleXMoY29udHJhY3RzKSkge1xuICAgIC8vIEdldCBjb250cmFjdCBhZGRyZXNzIGZyb20gQ29udHJvbGxlclxuICAgIGNvbnN0IGhhc2ggPSBFdGgua2VjY2FrMjU2KG5hbWUpXG4gICAgY29uc3QgYWRkcmVzcyA9IChhd2FpdCBDb250cm9sbGVyLmdldENvbnRyYWN0KGhhc2gpKVswXVxuICAgIC8vIENyZWF0ZSBjb250cmFjdCBpbnN0YW5jZVxuICAgIGNvbnRyYWN0c1tuYW1lXSA9IGF3YWl0IGdldENvbnRyYWN0QXQoZXRoLCB7XG4gICAgICAuLi5hcnRpZmFjdHNbbmFtZV0sXG4gICAgICBkZWZhdWx0VHgsXG4gICAgICBhZGRyZXNzLFxuICAgIH0pXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGNvbnRyYWN0c1tuYW1lXS5hYmkpIHtcbiAgICAgIGhhc2hlc1tuYW1lXVt1dGlscy5nZXRNZXRob2RIYXNoKGl0ZW0pXSA9IGl0ZW0ubmFtZVxuICAgIH1cbiAgfVxuICAvLyBBZGQgdGhlIENvbnRyb2xsZXIgY29udHJhY3QgdG8gdGhlIGNvbnRyYWN0cyBvYmplY3RcbiAgY29udHJhY3RzLkNvbnRyb2xsZXIgPSBDb250cm9sbGVyXG4gIC8vIEtleSBBQklzIGJ5IGNvbnRyYWN0IG5hbWVcbiAgY29uc3QgYWJpcyA9IE9iamVjdC5lbnRyaWVzKGFydGlmYWN0cylcbiAgICAubWFwKChbaywgdl0pID0+ICh7IFtrXTogdi5hYmkgfSkpXG4gICAgLnJlZHVjZSgoYSwgYikgPT4gKHsgLi4uYSwgLi4uYiB9KSwge30pXG4gIC8vIENyZWF0ZSBhIGxpc3Qgb2YgZXZlbnRzIGluIGVhY2ggY29udHJhY3RcbiAgY29uc3QgZXZlbnRzID0gT2JqZWN0LmVudHJpZXMoYWJpcylcbiAgICAubWFwKChbY29udHJhY3QsIGFiaV0pID0+IHtcbiAgICAgIHJldHVybiBhYmlcbiAgICAgICAgLmZpbHRlcih4ID0+IHgudHlwZSA9PT0gJ2V2ZW50JylcbiAgICAgICAgLm1hcChhYmkgPT4gKHtcbiAgICAgICAgICBhYmksXG4gICAgICAgICAgY29udHJhY3QsXG4gICAgICAgICAgZXZlbnQ6IGNvbnRyYWN0c1tjb250cmFjdF1bYWJpLm5hbWVdLFxuICAgICAgICAgIG5hbWU6IGFiaS5uYW1lLFxuICAgICAgICB9KSlcbiAgICB9KVxuICAgIC5yZWR1Y2UoXG4gICAgICAoYSwgYikgPT5cbiAgICAgICAgYi5yZWR1Y2UoKGV2ZW50cywgeyBuYW1lLCBldmVudCwgYWJpLCBjb250cmFjdCB9KSA9PiB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coY29udHJhY3QsIG5hbWUsIGFiaXNbY29udHJhY3RdKVxuICAgICAgICAgIGV2ZW50LmFiaSA9IGFiaVxuICAgICAgICAgIGV2ZW50LmNvbnRyYWN0ID0gY29udHJhY3RcbiAgICAgICAgICByZXR1cm4geyAuLi5ldmVudHMsIFtuYW1lXTogZXZlbnQgfVxuICAgICAgICB9LCBhKSxcbiAgICAgIHt9LFxuICAgIClcblxuICByZXR1cm4ge1xuICAgIGFiaXMsXG4gICAgYWNjb3VudHMsXG4gICAgY29udHJhY3RzLFxuICAgIGRlZmF1bHRUeCxcbiAgICBlbnMsXG4gICAgZXRoLFxuICAgIGV2ZW50cyxcbiAgICBoYXNoZXMsXG4gIH1cbn1cblxuLyoqXG4gKiBMaXZlcGVlciBTREsgbWFpbiBtb2R1bGUgZXhwb3J0c1xuICogQG5hbWVzcGFjZSBtb2R1bGV+ZXhwb3J0c1xuICovXG5cbi8qKlxuICogTGl2ZXBlZXIgU0RLIGZhY3RvcnkgZnVuY3Rpb24uIENyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgdGhlIExpdmVwZWVyIFNESyAtLSBhbiBvYmplY3Qgd2l0aCB1c2VmdWwgbWV0aG9kcyBmb3IgaW50ZXJhY3Rpbmcgd2l0aCBMaXZlcGVlciBwcm90b2NvbCBzbWFydCBjb250cmFjdHNcbiAqIEBtZW1iZXJvZiBtb2R1bGV+ZXhwb3J0c1xuICogQG5hbWUgZGVmYXVsdFxuICogQHBhcmFtIHtMaXZlcGVlclNES09wdGlvbnN9IG9wdHMgLSBTREsgY29uZmlndXJhdGlvbiBvcHRpb25zXG4gKiBAcmV0dXJuIHtQcm9taXNlPExpdmVwZWVyU0RLPn1cbiAqXG4gKiBAZXhhbXBsZVxuICpcbiAqIC8vIEhlcmUgd2UncmUgbmFtaW5nIHRoZSBkZWZhdWx0IGV4cG9ydCBcIkxpdmVwZWVyU0RLXCJcbiAqIGltcG9ydCBMaXZlcGVlclNESyBmcm9tICdAbGl2ZXBlZXIvc2RrJ1xuICpcbiAqIC8vIENhbGwgdGhlIGZhY3RvcnkgZnVuY3Rpb24gYW5kIGF3YWl0IGl0cyBQcm9taXNlXG4gKiBMaXZlcGVlclNESygpLnRoZW4oc2RrID0+IHtcbiAqICAgLy8gWW91ciBMaXZlcGVlciBTREsgaW5zdGFuY2UgaXMgbm93IHJlYWR5IHRvIHVzZVxuICogfSlcbiAqXG4gKi9cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBjcmVhdGVMaXZlcGVlclNESyhcbiAgb3B0czogTGl2ZXBlZXJTREtPcHRpb25zLFxuKTogUHJvbWlzZTxMaXZlcGVlclNESz4ge1xuICBjb25zdCB7IGVucywgZXZlbnRzLCAuLi5jb25maWcgfSA9IGF3YWl0IGluaXRDb250cmFjdHMob3B0cylcbiAgY29uc3Qge1xuICAgIEJvbmRpbmdNYW5hZ2VyLFxuICAgIENvbnRyb2xsZXIsXG4gICAgSm9ic01hbmFnZXIsXG4gICAgTGl2ZXBlZXJUb2tlbixcbiAgICBMaXZlcGVlclRva2VuRmF1Y2V0LFxuICAgIFJvdW5kc01hbmFnZXIsXG4gICAgTWludGVyLFxuICB9ID0gY29uZmlnLmNvbnRyYWN0c1xuICBjb25zdCB7IHJlc29sdmVBZGRyZXNzIH0gPSB1dGlsc1xuXG4gIC8vIENhY2hlXG4gIGNvbnN0IGNhY2hlID0ge1xuICAgIC8vIHByZXZpb3VzIGxvZyBxdWVyaWVzIGFyZSBoZWxkIGhlcmUgdG8gaW1wcm92ZSBwZXJmXG4gIH1cbiAgLyoqXG4gICAqIFwicnBjXCIgbmFtZXNwYWNlIG9mIGEgTGl2ZXBlZXIgU0RLIGluc3RhbmNlXG4gICAqIEBuYW1lc3BhY2UgbGl2ZXBlZXJ+cnBjXG4gICAqXG4gICAqIEBleGFtcGxlXG4gICAqXG4gICAqIGltcG9ydCBMaXZlcGVlclNESyBmcm9tICdAbGl2ZXBlZXIvc2RrJ1xuICAgKlxuICAgKiBMaXZlcGVlclNESygpLnRoZW4oKHsgcnBjIH0pID0+IHtcbiAgICogICAvLyBIZXJlLCB3ZSdyZSBkZXN0cnVjdHVyaW5nIHRoZSBzZGsgdG8gZXhwb3NlIG9ubHkgaXRzIHJwYyBuYW1lc3BhY2VcbiAgICogICAvLyBOb3csIHlvdSB5b3UgYXJlIGFibGUgY2FsbCBycGMuPG1ldGhvZC1uYW1lPigpXG4gICAqICAgLy8gQWxsIHJwYyBtZXRob2QgeWllbGQgUHJvbWlzZXMuIFRoZWlyIHVzYWdlIGlzIGZ1cnRoZXIgZXhwbGFpbmVkIGJlbG93LlxuICAgKiB9KVxuICAgKlxuICAgKi9cbiAgY29uc3QgcnBjID0ge1xuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIEVOUyBuYW1lIGZvciBhbiBhZGRyZXNzLiBUaGlzIGlzIGtub3duIGFzIGEgcmV2ZXJzZSBsb29rdXAuXG4gICAgICogVW5mb3J0dW5hdGVseSwgdXNlcnMgbXVzdCBleHBsaWNpdGx5IHNldCB0aGVpciBvd24gcmVzb2x2ZXIuXG4gICAgICogU28gbW9zdCBvZiB0aGUgdGltZSwgdGhpcyBtZXRob2QganVzdCByZXR1cm5zIGFuIGVtcHR5IHN0cmluZ1xuICAgICAqIE1vcmUgaW5mbyBoZXJlOlxuICAgICAqIChodHRwczovL2RvY3MuZW5zLmRvbWFpbnMvZW4vbGF0ZXN0L3VzZXJndWlkZS5odG1sI3JldmVyc2UtbmFtZS1yZXNvbHV0aW9uKVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gYWRkcmVzcyAtIGFkZHJlc3MgdG8gbG9vayB1cCBhbiBFTlMgbmFtZSBmb3JcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEVOU05hbWUoJzB4ZDM0ZGIzM2YuLi4nKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldEVOU05hbWUoYWRkcmVzczogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHJldHVybiBhd2FpdCBlbnMucmV2ZXJzZShhZGRyZXNzKVxuICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIC8vIGN1c3RvbSBuZXR3b3JrcyBvciB1bmF2YWlsYWJsZSByZXNvbHZlcnMgY2FuIGNhdXNlIGZhaWx1cmVcbiAgICAgICAgaWYgKGVyci5tZXNzYWdlICE9PSAnRU5TIG5hbWUgbm90IGRlZmluZWQuJykge1xuICAgICAgICAgIGNvbnNvbGUud2FybihcbiAgICAgICAgICAgIGBDb3VsZCBub3QgZ2V0IEVOUyBuYW1lIGZvciBhZGRyZXNzIFwiJHthZGRyZXNzfVwiOmAsXG4gICAgICAgICAgICBlcnIubWVzc2FnZSxcbiAgICAgICAgICApXG4gICAgICAgIH1cbiAgICAgICAgLy8gaWYgdGhlcmUncyBubyBuYW1lLCB3ZSBjYW4ganVzdCByZXNvbHZlIGFuIGVtcHR5IHN0cmluZ1xuICAgICAgICByZXR1cm4gJydcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgYWRkcmVzcyBmb3IgYW4gRU5TIG5hbWVcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgLSBFTlMgbmFtZSB0byBsb29rIHVwIGFuIGFkZHJlc3MgZm9yXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRFTlNBZGRyZXNzKCd2aXRhbGlrLmV0aCcpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0RU5TQWRkcmVzcyhuYW1lOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgdHJ5IHtcbiAgICAgICAgcmV0dXJuIGF3YWl0IGVucy5sb29rdXAobmFtZSlcbiAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAvLyBjdXN0b20gbmV0d29ya3Mgb3IgdW5hdmFpbGFibGUgcmVzb2x2ZXJzIGNhbiBjYXVzZSBmYWlsdXJlXG4gICAgICAgIGlmIChlcnIubWVzc2FnZSAhPT0gJ0VOUyBuYW1lIG5vdCBkZWZpbmVkLicpIHtcbiAgICAgICAgICBjb25zb2xlLndhcm4oXG4gICAgICAgICAgICBgQ291bGQgbm90IGdldCBhZGRyZXNzIGZvciBFTlMgbmFtZSBcIiR7bmFtZX1cIjpgLFxuICAgICAgICAgICAgZXJyLm1lc3NhZ2UsXG4gICAgICAgICAgKVxuICAgICAgICB9XG4gICAgICAgIC8vIGlmIHRoZXJlJ3Mgbm8gbmFtZSwgd2UgY2FuIGp1c3QgcmVzb2x2ZSBhbiBlbXB0eSBzdHJpbmdcbiAgICAgICAgcmV0dXJuICcnXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgYSBibG9jayBieSBudW1iZXIsIGhhc2gsIG9yIGtleXdvcmQgKCdlYXJsaWVzdCcgfCAnbGF0ZXN0JylcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGJsb2NrIC0gTnVtYmVyIG9mIGJsb2NrIHRvIGdldFxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRCbG9jaygnbGF0ZXN0JylcbiAgICAgKiAvLyA9PiB7XG4gICAgICogICBcIm51bWJlclwiOiBzdHJpbmcsXG4gICAgICogICBcImhhc2hcIjogc3RyaW5nLFxuICAgICAqICAgXCJwYXJlbnRIYXNoXCI6IHN0cmluZyxcbiAgICAgKiAgIFwibm9uY2VcIjogc3RyaW5nLFxuICAgICAqICAgXCJzaGEzVW5jbGVzXCI6IHN0cmluZyxcbiAgICAgKiAgIFwibG9nc0Jsb29tXCI6IHN0cmluZyxcbiAgICAgKiAgIFwidHJhbnNhY3Rpb25zUm9vdFwiOiBzdHJpbmcsXG4gICAgICogICBcInN0YXRlUm9vdFwiOiBzdHJpbmcsXG4gICAgICogICBcInJlY2VpcHRzUm9vdFwiOiBzdHJpbmcsXG4gICAgICogICBcIm1pbmVyXCI6IHN0cmluZyxcbiAgICAgKiAgIFwibWl4SGFzaFwiOiBzdHJpbmcsXG4gICAgICogICBcImRpZmZpY3VsdHlcIjogc3RyaW5nLFxuICAgICAqICAgXCJ0b3RhbERpZmZpY3VsdHlcIjogc3RyaW5nLFxuICAgICAqICAgXCJleHRyYURhdGFcIjogc3RyaW5nLFxuICAgICAqICAgXCJzaXplXCI6IHN0cmluZyxcbiAgICAgKiAgIFwiZ2FzTGltaXRcIjogc3RyaW5nLFxuICAgICAqICAgXCJnYXNVc2VkXCI6IHN0cmluZyxcbiAgICAgKiAgIFwidGltZXN0YW1wXCI6IG51bWJlcixcbiAgICAgKiAgIFwidHJhbnNhY3Rpb25zXCI6IEFycmF5PFRyYW5zYWN0aW9uPixcbiAgICAgKiAgIFwidHJhbnNhY3Rpb25zUm9vdFwiOiBzdHJpbmcsXG4gICAgICogICBcInVuY2xlc1wiOiBBcnJheTxVbmNsZT4sXG4gICAgICogfVxuICAgICAqL1xuICAgIGFzeW5jIGdldEJsb2NrKGlkOiBzdHJpbmcpOiBQcm9taXNlPEJsb2NrPiB7XG4gICAgICBjb25zdCBibG9jayA9IGlkLnRvU3RyaW5nKCkuc3RhcnRzV2l0aCgnMHgnKVxuICAgICAgICA/IGF3YWl0IGNvbmZpZy5ldGguZ2V0QmxvY2tCeUhhc2goaWQsIHRydWUpXG4gICAgICAgIDogYXdhaXQgY29uZmlnLmV0aC5nZXRCbG9ja0J5TnVtYmVyKGlkLCB0cnVlKVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uYmxvY2ssXG4gICAgICAgIGRpZmZpY3VsdHk6IHRvU3RyaW5nKGJsb2NrLmRpZmZpY3VsdHkpLFxuICAgICAgICBnYXNMaW1pdDogdG9TdHJpbmcoYmxvY2suZ2FzTGltaXQpLFxuICAgICAgICBnYXNVc2VkOiB0b1N0cmluZyhibG9jay5nYXNVc2VkKSxcbiAgICAgICAgbnVtYmVyOiB0b1N0cmluZyhibG9jay5udW1iZXIpLFxuICAgICAgICBzaXplOiB0b1N0cmluZyhibG9jay5zaXplKSxcbiAgICAgICAgdGltZXN0YW1wOiBOdW1iZXIodG9TdHJpbmcoYmxvY2sudGltZXN0YW1wKSksXG4gICAgICAgIHRvdGFsRGlmZmljdWx0eTogdG9TdHJpbmcoYmxvY2sudG90YWxEaWZmaWN1bHR5KSxcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgRVRIIGJhbGFuY2UgZm9yIGFuIGFjY291bnRcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGFkZHIgLSBFVEggYWNjb3VudCBhZGRyZXNzXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRFdGhCYWxhbmNlKCcweGYwMC4uLicpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICpcbiAgICAgKi9cbiAgICBhc3luYyBnZXRFdGhCYWxhbmNlKGFkZHI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gdG9TdHJpbmcoXG4gICAgICAgIGF3YWl0IGNvbmZpZy5ldGguZ2V0QmFsYW5jZShcbiAgICAgICAgICBhd2FpdCByZXNvbHZlQWRkcmVzcyhycGMuZ2V0RU5TQWRkcmVzcywgYWRkciksXG4gICAgICAgICksXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIHVuYm9uZGluZyBwZXJpb2QgZm9yIHRyYW5zY29kZXJzXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0VW5ib25kaW5nUGVyaW9kKClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRVbmJvbmRpbmdQZXJpb2QoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgQm9uZGluZ01hbmFnZXIudW5ib25kaW5nUGVyaW9kKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIG51bWJlciBvZiBhY3RpdmUgdHJhbnNjb2RlcnNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXROdW1BY3RpdmVUcmFuc2NvZGVycygpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0TnVtQWN0aXZlVHJhbnNjb2RlcnMoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgQm9uZGluZ01hbmFnZXIubnVtQWN0aXZlVHJhbnNjb2RlcnMoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgbWF4aW11bSBlYXJuaW5ncyBmb3IgY2xhaW1zIHJvdW5kc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldE1heEVhcm5pbmdzQ2xhaW1zUm91bmRzKClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRNYXhFYXJuaW5nc0NsYWltc1JvdW5kcygpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgcmV0dXJuIGhlYWRUb1N0cmluZyhhd2FpdCBCb25kaW5nTWFuYWdlci5tYXhFYXJuaW5nc0NsYWltc1JvdW5kcygpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIHRoZSB1bmJvbmRpbmcgcGVyaW9kIGZvciB0cmFuc2NvZGVyc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFVuYm9uZGluZ1BlcmlvZCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0VW5ib25kaW5nUGVyaW9kKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEJvbmRpbmdNYW5hZ2VyLnVuYm9uZGluZ1BlcmlvZCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIHRoZSBudW1iZXIgb2YgYWN0aXZlIHRyYW5zY29kZXJzXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0TnVtQWN0aXZlVHJhbnNjb2RlcnMoKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldE51bUFjdGl2ZVRyYW5zY29kZXJzKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEJvbmRpbmdNYW5hZ2VyLm51bUFjdGl2ZVRyYW5zY29kZXJzKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIG1heGltdW0gZWFybmluZ3MgZm9yIGNsYWltcyByb3VuZHNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRNYXhFYXJuaW5nc0NsYWltc1JvdW5kcygpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0TWF4RWFybmluZ3NDbGFpbXNSb3VuZHMoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgQm9uZGluZ01hbmFnZXIubWF4RWFybmluZ3NDbGFpbXNSb3VuZHMoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgdG90YWwgYW1vdW50IG9mIGJvbmRlZCB0b2tlbnNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmd9XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRvdGFsQm9uZGVkKClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRUb3RhbEJvbmRlZCgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgcmV0dXJuIGhlYWRUb1N0cmluZyhhd2FpdCBCb25kaW5nTWFuYWdlci5nZXRUb3RhbEJvbmRlZCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIHRoZSB0b3RhbCBzdXBwbHkgb2YgdG9rZW4gKExUUFUpIGF2YWlsYWJsZSBpbiB0aGUgcHJvdG9jb2xcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRUb2tlblRvdGFsU3VwcGx5KClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRUb2tlblRvdGFsU3VwcGx5KCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IExpdmVwZWVyVG9rZW4udG90YWxTdXBwbHkoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyBhIHVzZXIncyB0b2tlbiBiYWxhbmNlIChMUFRVKVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0gIHtzdHJpbmd9IGFkZHIgLSB1c2VyJ3MgRVRIIGFkZHJlc3NcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRva2VuQmFsYW5jZSgnMHhmMDAuLi4nKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldFRva2VuQmFsYW5jZShhZGRyOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgcmV0dXJuIGhlYWRUb1N0cmluZyhcbiAgICAgICAgYXdhaXQgTGl2ZXBlZXJUb2tlbi5iYWxhbmNlT2YoXG4gICAgICAgICAgYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIGFkZHIpLFxuICAgICAgICApLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIGdlbmVyYWwgaW5mb3JtYXRpb24gYWJvdXQgdG9rZW5zXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYWRkciAtIHVzZXIncyBFVEggYWRkcmVzc1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8VG9rZW5JbmZvPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0VG9rZW5JbmZvKClcbiAgICAgKiAvLyA9PiBUb2tlbkluZm8geyB0b3RhbFN1cHBseTogc3RyaW5nLCBiYWxhbmNlOiBzdHJpbmcgfVxuICAgICAqL1xuICAgIGFzeW5jIGdldFRva2VuSW5mbyhhZGRyOiBzdHJpbmcpOiBQcm9taXNlPFRva2VuSW5mbz4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdG90YWxTdXBwbHk6IGF3YWl0IHJwYy5nZXRUb2tlblRvdGFsU3VwcGx5KCksXG4gICAgICAgIGJhbGFuY2U6IGF3YWl0IHJwYy5nZXRUb2tlbkJhbGFuY2UoXG4gICAgICAgICAgYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIGFkZHIpLFxuICAgICAgICApLFxuICAgICAgfVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBUcmFuc2ZlcnMgdG9rZW5zIChMUFRVKSBmcm9tIG9uZSBhY2NvdW50IHRvIGFub3RoZXJcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRvIC0gdGhlIGFjY291bnQgRVRIIGFkZHJlc3MgdG8gc2VuZCB0b2tlbnMgdG9cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gYW1vdW50IC0gdGhlIGFtb3VudCBvZiB0b2tlbiB0byBzZW5kIChMUFRVKVxuICAgICAqIEBwYXJhbSB7VHhDb25maWd9IFt0eCA9IGNvbmZpZy5kZWZhdWx0VHhdIC0gYW4gb2JqZWN0IHNwZWNpZnlpbmcgdGhlIGBmcm9tYCBhbmQgYGdhc2AgdmFsdWVzIG9mIHRoZSB0cmFuc2FjdGlvblxuICAgICAqIEByZXR1cm4ge1Byb21pc2U8VHhSZWNlaXB0Pn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMudHJhbnNmZXJUb2tlbignMHhmMDAuLi4nLCAnMTAnKVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIHRyYW5zZmVyVG9rZW4oXG4gICAgICB0bzogc3RyaW5nLFxuICAgICAgYW1vdW50OiBzdHJpbmcsXG4gICAgICB0eCA9IGNvbmZpZy5kZWZhdWx0VHgsXG4gICAgKTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIGNvbnN0IHZhbHVlID0gdG9CTihhbW91bnQpXG4gICAgICAvLyBtYWtlIHN1cmUgYmFsYW5jZSBpcyBoaWdoZXIgdGhhbiB0cmFuc2ZlclxuICAgICAgY29uc3QgYmFsYW5jZSA9IChhd2FpdCBMaXZlcGVlclRva2VuLmJhbGFuY2VPZih0eC5mcm9tKSlbMF1cbiAgICAgIGlmICghYmFsYW5jZS5ndGUodmFsdWUpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICBgQ2Fubm90IHRyYW5zZmVyICR7dG9TdHJpbmcoXG4gICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICApfSBMUFQgYmVjYXVzZSBpcyBpdCBncmVhdGVyIHRoYW4geW91ciBjdXJyZW50IGJhbGFuY2UgKCR7YmFsYW5jZX0gTFBUKS5gLFxuICAgICAgICApXG4gICAgICB9XG5cbiAgICAgIHJldHVybiBhd2FpdCB1dGlscy5nZXRUeFJlY2VpcHQoXG4gICAgICAgIGF3YWl0IExpdmVwZWVyVG9rZW4udHJhbnNmZXIoXG4gICAgICAgICAgYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIHRvKSxcbiAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICB0eCxcbiAgICAgICAgKSxcbiAgICAgICAgY29uZmlnLmV0aCxcbiAgICAgIClcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVGhlIGFtb3VudCBvZiBMUFQgdGhlIGZhdWNldCBkaXN0cmlidXRlcyB3aGVuIHRhcHBlZFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEZhdWNldEFtb3VudCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0RmF1Y2V0QW1vdW50KCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IExpdmVwZWVyVG9rZW5GYXVjZXQucmVxdWVzdEFtb3VudCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBIb3cgb2Z0ZW4gYW4gYWRkcmVzcyBjYW4gdGFwIHRoZSBmYXVjZXQgKGluIGhvdXJzKVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEZhdWNldFdhaXQoKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldEZhdWNldFdhaXQoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgTGl2ZXBlZXJUb2tlbkZhdWNldC5yZXF1ZXN0V2FpdCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBOZXh0IHRpbWVzdGFtcCBhdCB3aGljaCB0aGUgZ2l2ZW4gYWRkcmVzcyB3aWxsIGJlIGFsbG93ZWQgdG8gdGFwIHRoZSBmYXVjZXRcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBhZGRyIC0gdXNlcidzIEVUSCBhZGRyZXNzXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRGYXVjZXROZXh0KClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRGYXVjZXROZXh0KGFkZHI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKFxuICAgICAgICBhd2FpdCBMaXZlcGVlclRva2VuRmF1Y2V0Lm5leHRWYWxpZFJlcXVlc3QoXG4gICAgICAgICAgYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIGFkZHIpLFxuICAgICAgICApLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBJbmZvIGFib3V0IHRoZSBzdGF0ZSBvZiB0aGUgTFBUIGZhdWNldFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0gIHtzdHJpbmd9IGFkZHIgLSB1c2VyJ3MgRVRIIGFkZHJlc3NcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPEZhdWNldEluZm8+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRGYXVjZXRJbmZvKCcweGYwMC4uLicpXG4gICAgICogLy8gPT4gRmF1Y2V0SW5mbyB7XG4gICAgICogLy8gICBhbW91bnQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIHdhaXQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIG5leHQ6IHN0cmluZyxcbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0RmF1Y2V0SW5mbyhhZGRyOiBzdHJpbmcpOiBQcm9taXNlPEZhdWNldEluZm8+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGFtb3VudDogYXdhaXQgcnBjLmdldEZhdWNldEFtb3VudCgpLFxuICAgICAgICB3YWl0OiBhd2FpdCBycGMuZ2V0RmF1Y2V0V2FpdCgpLFxuICAgICAgICBuZXh0OiBhd2FpdCBycGMuZ2V0RmF1Y2V0TmV4dChcbiAgICAgICAgICBhd2FpdCByZXNvbHZlQWRkcmVzcyhycGMuZ2V0RU5TQWRkcmVzcywgYWRkciksXG4gICAgICAgICksXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIHBlciByb3VuZCBpbmZsYXRpb24gcmF0ZVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEluZmxhdGlvbigpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0SW5mbGF0aW9uKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IE1pbnRlci5pbmZsYXRpb24oKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgY2hhbmdlIGluIGluZmxhdGlvbiByYXRlIHBlciByb3VuZCB1bnRpbCB0aGUgdGFyZ2V0IGJvbmRpbmcgcmF0ZSBpcyBhY2hpZXZlZFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEluZmxhdGlvbkNoYW5nZSgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0SW5mbGF0aW9uQ2hhbmdlKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IE1pbnRlci5pbmZsYXRpb25DaGFuZ2UoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogSW5mbyBhYm91dCBhIGJyb2FkY2FzdGVyXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYWRkciAtIHVzZXIncyBFVEggYWRkcmVzc1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8QnJvYWRjYXN0ZXI+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRCcm9hZGNhc3RlcignMHhmMDAuLi4nKVxuICAgICAqIC8vID0+IEJyb2FkY2FzdGVyIHtcbiAgICAgKiAvLyAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGRlcG9zaXQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIHdpdGhkcmF3QmxvY2s6IHN0cmluZyxcbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0QnJvYWRjYXN0ZXIoYWRkcjogc3RyaW5nKTogUHJvbWlzZTxCcm9hZGNhc3Rlcj4ge1xuICAgICAgY29uc3QgYWRkcmVzcyA9IGF3YWl0IHJlc29sdmVBZGRyZXNzKHJwYy5nZXRFTlNBZGRyZXNzLCBhZGRyKVxuICAgICAgY29uc3QgYiA9IGF3YWl0IEpvYnNNYW5hZ2VyLmJyb2FkY2FzdGVycyhhZGRyZXNzKVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgYWRkcmVzcyxcbiAgICAgICAgZGVwb3NpdDogdG9TdHJpbmcoYi5kZXBvc2l0KSxcbiAgICAgICAgd2l0aGRyYXdCbG9jazogdG9TdHJpbmcoYi53aXRoZHJhd0Jsb2NrKSxcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVGhlIGRlbGVnYXRvciBzdGF0dXMgb2YgdGhlIGdpdmVuIGFkZHJlc3NcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBhZGRyIC0gdXNlcidzIEVUSCBhZGRyZXNzXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXREZWxlZ2F0b3JTdGF0dXMoJzB4ZjAwLi4uJylcbiAgICAgKiAvLyA9PiAnUGVuZGluZycgfCAnQm9uZGVkJyB8ICdVbmJvbmRlZCdcbiAgICAgKi9cbiAgICBhc3luYyBnZXREZWxlZ2F0b3JTdGF0dXMoYWRkcjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIGNvbnN0IHN0YXR1cyA9IGhlYWRUb1N0cmluZyhcbiAgICAgICAgYXdhaXQgQm9uZGluZ01hbmFnZXIuZGVsZWdhdG9yU3RhdHVzKFxuICAgICAgICAgIGF3YWl0IHJlc29sdmVBZGRyZXNzKHJwYy5nZXRFTlNBZGRyZXNzLCBhZGRyKSxcbiAgICAgICAgKSxcbiAgICAgIClcbiAgICAgIHJldHVybiBERUxFR0FUT1JfU1RBVFVTW3N0YXR1c11cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2VuZXJhbCBpbmZvIGFib3V0IGEgZGVsZWdhdG9yXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYWRkciAtIHVzZXIncyBFVEggYWRkcmVzc1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8RGVsZWdhdG9yPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0RGVsZWdhdG9yKCcweGYwMC4uLicpXG4gICAgICogLy8gPT4gRGVsZWdhdG9yIHtcbiAgICAgKiAvLyAgIGFsbG93YW5jZTogc3RyaW5nLFxuICAgICAqIC8vICAgYWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgYm9uZGVkQW1vdW50OiBzdHJpbmcsXG4gICAgICogLy8gICBkZWxlZ2F0ZUFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGRlbGVnYXRlQW1vdW50OiBzdHJpbmcsXG4gICAgICogLy8gICBmZWVzOiBzdHJpbmcsXG4gICAgICogLy8gICBsYXN0Q2xhaW1Sb3VuZDogc3RyaW5nLFxuICAgICAqIC8vICAgcGVuZGluZ0ZlZXM6IHN0cmluZyxcbiAgICAgKiAvLyAgIHBlbmRpbmdTdGFrZTogc3RyaW5nLFxuICAgICAqIC8vICAgc3RhcnRSb3VuZDogc3RyaW5nLFxuICAgICAqIC8vICAgc3RhdHVzOiAnUGVuZGluZycgfCAnQm9uZGVkJyB8ICdVbmJvbmRpbmcnIHwgJ1VuYm9uZGVkJyxcbiAgICAgKiAvLyAgIHdpdGhkcmF3Um91bmQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIG5leHRVbmJvbmRpbmdMb2NrSWQ6IHN0cmluZyxcbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0RGVsZWdhdG9yKGFkZHI6IHN0cmluZyk6IFByb21pc2U8RGVsZWdhdG9yPiB7XG4gICAgICBjb25zdCBhZGRyZXNzID0gYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIGFkZHIpXG4gICAgICBjb25zdCBhbGxvd2FuY2UgPSBoZWFkVG9TdHJpbmcoXG4gICAgICAgIGF3YWl0IExpdmVwZWVyVG9rZW4uYWxsb3dhbmNlKGFkZHJlc3MsIEJvbmRpbmdNYW5hZ2VyLmFkZHJlc3MpLFxuICAgICAgKVxuICAgICAgY29uc3QgY3VycmVudFJvdW5kID0gYXdhaXQgcnBjLmdldEN1cnJlbnRSb3VuZCgpXG4gICAgICBjb25zdCBwZW5kaW5nU3Rha2UgPSBoZWFkVG9TdHJpbmcoXG4gICAgICAgIGF3YWl0IEJvbmRpbmdNYW5hZ2VyLnBlbmRpbmdTdGFrZShhZGRyZXNzLCBjdXJyZW50Um91bmQpLFxuICAgICAgKVxuICAgICAgY29uc3QgcGVuZGluZ0ZlZXMgPSBoZWFkVG9TdHJpbmcoXG4gICAgICAgIGF3YWl0IEJvbmRpbmdNYW5hZ2VyLnBlbmRpbmdGZWVzKGFkZHJlc3MsIGN1cnJlbnRSb3VuZCksXG4gICAgICApXG4gICAgICBjb25zdCBkID0gYXdhaXQgQm9uZGluZ01hbmFnZXIuZ2V0RGVsZWdhdG9yKGFkZHJlc3MpXG4gICAgICBjb25zdCBib25kZWRBbW91bnQgPSB0b1N0cmluZyhkLmJvbmRlZEFtb3VudClcbiAgICAgIGNvbnN0IGZlZXMgPSB0b1N0cmluZyhkLmZlZXMpXG4gICAgICBjb25zdCBkZWxlZ2F0ZUFkZHJlc3MgPVxuICAgICAgICBkLmRlbGVnYXRlQWRkcmVzcyA9PT0gRU1QVFlfQUREUkVTUyA/ICcnIDogZC5kZWxlZ2F0ZUFkZHJlc3NcbiAgICAgIGNvbnN0IGRlbGVnYXRlZEFtb3VudCA9IHRvU3RyaW5nKGQuZGVsZWdhdGVkQW1vdW50KVxuICAgICAgY29uc3QgbGFzdENsYWltUm91bmQgPSB0b1N0cmluZyhkLmxhc3RDbGFpbVJvdW5kKVxuICAgICAgY29uc3Qgc3RhcnRSb3VuZCA9IHRvU3RyaW5nKGQuc3RhcnRSb3VuZClcbiAgICAgIGNvbnN0IG5leHRVbmJvbmRpbmdMb2NrSWQgPSB0b1N0cmluZyhkLm5leHRVbmJvbmRpbmdMb2NrSWQpXG5cbiAgICAgIGxldCB1bmJvbmRpbmdMb2NrSWQgPSB0b0JOKG5leHRVbmJvbmRpbmdMb2NrSWQpXG4gICAgICBpZiAodW5ib25kaW5nTG9ja0lkLmNtcChuZXcgQk4oMCkpID4gMCkge1xuICAgICAgICB1bmJvbmRpbmdMb2NrSWQgPSB1bmJvbmRpbmdMb2NrSWQuc3ViKG5ldyBCTigxKSlcbiAgICAgIH1cbiAgICAgIGNvbnN0IHtcbiAgICAgICAgYW1vdW50OiB3aXRoZHJhd0Ftb3VudCxcbiAgICAgICAgd2l0aGRyYXdSb3VuZCxcbiAgICAgIH0gPSBhd2FpdCBycGMuZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9jayhcbiAgICAgICAgYWRkcmVzcyxcbiAgICAgICAgdG9TdHJpbmcodW5ib25kaW5nTG9ja0lkKSxcbiAgICAgIClcbiAgICAgIGNvbnN0IHN0YXR1cyA9XG4gICAgICAgIHdpdGhkcmF3Um91bmQgIT09ICcwJyAmJiB0b0JOKGN1cnJlbnRSb3VuZCkuY21wKHRvQk4od2l0aGRyYXdSb3VuZCkpIDwgMFxuICAgICAgICAgID8gREVMRUdBVE9SX1NUQVRVUy5VbmJvbmRpbmdcbiAgICAgICAgICA6IGF3YWl0IHJwYy5nZXREZWxlZ2F0b3JTdGF0dXMoYWRkcmVzcylcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgYWRkcmVzcyxcbiAgICAgICAgYWxsb3dhbmNlLFxuICAgICAgICBib25kZWRBbW91bnQsXG4gICAgICAgIGRlbGVnYXRlQWRkcmVzcyxcbiAgICAgICAgZGVsZWdhdGVkQW1vdW50LFxuICAgICAgICBmZWVzLFxuICAgICAgICBsYXN0Q2xhaW1Sb3VuZCxcbiAgICAgICAgcGVuZGluZ0ZlZXMsXG4gICAgICAgIHBlbmRpbmdTdGFrZSxcbiAgICAgICAgc3RhcnRSb3VuZCxcbiAgICAgICAgc3RhdHVzLFxuICAgICAgICB3aXRoZHJhd1JvdW5kLFxuICAgICAgICB3aXRoZHJhd0Ftb3VudCxcbiAgICAgICAgbmV4dFVuYm9uZGluZ0xvY2tJZCxcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0IGFsbCB0aGUgdW5ib25kaW5nIGxvY2tzIGZvciBhIGRlbGVnYXRvclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBhZGRyIC0gZGVsZWdhdG9yJ3MgRVRIIGFkZHJlc3NcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPEFycmF5PFVuYm9uZGluZ0xvY2s+Pn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9ja3MoJzB4ZjAwLi4uJylcbiAgICAgKiAvLyA9PiBVbmJvbmRpbmdMb2NrIFt7XG4gICAgICogLy8gICBpZDogc3RyaW5nLFxuICAgICAqIC8vICAgZGVsZWdhdG9yOiBzdHJpbmcsXG4gICAgICogLy8gICBhbW91bnQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIHdpdGhkcmF3Um91bmQ6IHN0cmluZ1xuICAgICAqIC8vIH1dXG4gICAgICovXG4gICAgYXN5bmMgZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9ja3MoXG4gICAgICBhZGRyOiBzdHJpbmcsXG4gICAgKTogUHJvbWlzZTxBcnJheTxVbmJvbmRpbmdMb2NrPj4ge1xuICAgICAgbGV0IHsgbmV4dFVuYm9uZGluZ0xvY2tJZCB9ID0gYXdhaXQgcnBjLmdldERlbGVnYXRvcihhZGRyKVxuXG4gICAgICBsZXQgdW5ib25kaW5nTG9ja0lkID0gdG9OdW1iZXIobmV4dFVuYm9uZGluZ0xvY2tJZClcbiAgICAgIGlmICh1bmJvbmRpbmdMb2NrSWQgPiAwKSB7XG4gICAgICAgIHVuYm9uZGluZ0xvY2tJZCAtPSAxXG4gICAgICB9XG5cbiAgICAgIGxldCByZXN1bHQgPSBbXVxuXG4gICAgICB3aGlsZSAodW5ib25kaW5nTG9ja0lkID49IDApIHtcbiAgICAgICAgY29uc3QgdW5ib25kID0gYXdhaXQgcnBjLmdldERlbGVnYXRvclVuYm9uZGluZ0xvY2soXG4gICAgICAgICAgYWRkcixcbiAgICAgICAgICB0b1N0cmluZyh1bmJvbmRpbmdMb2NrSWQpLFxuICAgICAgICApXG4gICAgICAgIHJlc3VsdC5wdXNoKHVuYm9uZClcbiAgICAgICAgdW5ib25kaW5nTG9ja0lkIC09IDFcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXQgYW4gdW5ib25kaW5nIGxvY2sgZm9yIGEgZGVsZWdhdG9yXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGFkZHIgLSBkZWxlZ2F0b3IncyBFVEggYWRkcmVzc1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1bmJvbmRpbmdMb2NrSWQgLSB1bmJvbmRpbmcgbG9jayBJRFxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXREZWxlZ2F0b3JVbmJvbmRpbmdMb2NrKCcweGYwMC4uLicsIDEpXG4gICAgICogLy8gPT4gVW5ib25kaW5nTG9jayB7XG4gICAgICogLy8gICBpZDogc3RyaW5nLFxuICAgICAqIC8vICAgZGVsZWdhdG9yOiBzdHJpbmcsXG4gICAgICogLy8gICBhbW91bnQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIHdpdGhkcmF3Um91bmQ6IHN0cmluZ1xuICAgICAqIC8vIH1cbiAgICAgKi9cbiAgICBhc3luYyBnZXREZWxlZ2F0b3JVbmJvbmRpbmdMb2NrKFxuICAgICAgYWRkcjogc3RyaW5nLFxuICAgICAgdW5ib25kaW5nTG9ja0lkOiBzdHJpbmcsXG4gICAgKTogUHJvbWlzZTxVbmJvbmRpbmdMb2NrPiB7XG4gICAgICBjb25zdCBsb2NrID0gYXdhaXQgQm9uZGluZ01hbmFnZXIuZ2V0RGVsZWdhdG9yVW5ib25kaW5nTG9jayhcbiAgICAgICAgYWRkcixcbiAgICAgICAgdW5ib25kaW5nTG9ja0lkLFxuICAgICAgKVxuICAgICAgY29uc3QgYW1vdW50ID0gdG9TdHJpbmcobG9jay5hbW91bnQpXG4gICAgICBjb25zdCB3aXRoZHJhd1JvdW5kID0gdG9TdHJpbmcobG9jay53aXRoZHJhd1JvdW5kKVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaWQ6IHVuYm9uZGluZ0xvY2tJZCxcbiAgICAgICAgZGVsZWdhdG9yOiBhZGRyLFxuICAgICAgICBhbW91bnQsXG4gICAgICAgIHdpdGhkcmF3Um91bmQsXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJlYm9uZHMgTFBUIGZyb20gYW4gYWRkcmVzc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFR4UmVjZWlwdD59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLnJlYm9uZCgwKVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIHJlYm9uZChcbiAgICAgIHVuYm9uZGluZ0xvY2tJZDogc3RyaW5nLFxuICAgICAgdHggPSBjb25maWcuZGVmYXVsdFR4LFxuICAgICk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci5yZWJvbmQodW5ib25kaW5nTG9ja0lkLCB7XG4gICAgICAgICAgLi4uY29uZmlnLmRlZmF1bHRUeCxcbiAgICAgICAgICAuLi50eCxcbiAgICAgICAgfSksXG4gICAgICAgIGNvbmZpZy5ldGgsXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJlYm9uZHMgTFBUIGZyb20gYW4gYWRkcmVzc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFR4UmVjZWlwdD59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLnJlYm9uZEZyb21VbmJvbmRlZChcIjB4XCIsIDEpXG4gICAgICogLy8gPT4gVHhSZWNlaXB0IHtcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgdHJhbnNhY3Rpb25JbmRleFwiOiBCTixcbiAgICAgKiAvLyAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgY3VtdWxhdGl2ZUdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgZ2FzVXNlZDogQk4sXG4gICAgICogLy8gICBjb250cmFjdEFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxvZ3M6IEFycmF5PExvZyB7XG4gICAgICogLy8gICAgIGxvZ0luZGV4OiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSW5kZXg6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgYWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgICBkYXRhOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRvcGljczogQXJyYXk8c3RyaW5nPlxuICAgICAqIC8vICAgfT5cbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgcmVib25kRnJvbVVuYm9uZGVkKFxuICAgICAgYWRkcjogc3RyaW5nLFxuICAgICAgdW5ib25kaW5nTG9ja0lkOiBudW1iZXIsXG4gICAgICB0eCA9IGNvbmZpZy5kZWZhdWx0VHgsXG4gICAgKTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIHJldHVybiBhd2FpdCB1dGlscy5nZXRUeFJlY2VpcHQoXG4gICAgICAgIGF3YWl0IEJvbmRpbmdNYW5hZ2VyLnJlYm9uZEZyb21VbmJvbmRlZChhZGRyLCB1bmJvbmRpbmdMb2NrSWQsIHtcbiAgICAgICAgICAuLi5jb25maWcuZGVmYXVsdFR4LFxuICAgICAgICAgIC4uLnR4LFxuICAgICAgICB9KSxcbiAgICAgICAgY29uZmlnLmV0aCxcbiAgICAgIClcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogV2hldGhlciBvciBub3QgdGhlIHRyYW5zY29kZXIgaXMgYWN0aXZlXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYWRkciAtIHVzZXIncyBFVEggYWRkcmVzc1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8Ym9vbGVhbj59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRyYW5zY29kZXJJc0FjdGl2ZSgnMHhmMDAuLi4nKVxuICAgICAqIC8vID0+IGJvb2xlYW5cbiAgICAgKi9cbiAgICBhc3luYyBnZXRUcmFuc2NvZGVySXNBY3RpdmUoYWRkcjogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICByZXR1cm4gaGVhZFRvQm9vbChcbiAgICAgICAgYXdhaXQgQm9uZGluZ01hbmFnZXIuaXNBY3RpdmVUcmFuc2NvZGVyKFxuICAgICAgICAgIGF3YWl0IHJlc29sdmVBZGRyZXNzKHJwYy5nZXRFTlNBZGRyZXNzLCBhZGRyKSxcbiAgICAgICAgICBhd2FpdCBycGMuZ2V0Q3VycmVudFJvdW5kKCksXG4gICAgICAgICksXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIHN0YXR1cyBvZiBhIHRyYW5zY29kZXJcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBhZGRyIC0gdXNlcidzIEVUSCBhZGRyZXNzXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRUcmFuc2NvZGVyU3RhdHVzKCcweGYwMC4uLicpXG4gICAgICogLy8gPT4gJ05vdFJlZ2lzdGVyZWQnIHwgJ1JlZ2lzdGVyZWQnXG4gICAgICovXG4gICAgYXN5bmMgZ2V0VHJhbnNjb2RlclN0YXR1cyhhZGRyOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgY29uc3Qgc3RhdHVzID0gaGVhZFRvU3RyaW5nKFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci50cmFuc2NvZGVyU3RhdHVzKFxuICAgICAgICAgIGF3YWl0IHJlc29sdmVBZGRyZXNzKHJwYy5nZXRFTlNBZGRyZXNzLCBhZGRyKSxcbiAgICAgICAgKSxcbiAgICAgIClcbiAgICAgIHJldHVybiBUUkFOU0NPREVSX1NUQVRVU1tzdGF0dXNdXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgYSB0cmFuc2NvZGVyJ3MgdG90YWwgc3Rha2VcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBhZGRyIC0gdXNlcidzIEVUSCBhZGRyZXNzXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRUcmFuc2NvZGVyVG90YWxTdGFrZSgnMHhmMDAuLi4nKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldFRyYW5zY29kZXJUb3RhbFN0YWtlKGFkZHI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci50cmFuc2NvZGVyVG90YWxTdGFrZShcbiAgICAgICAgICBhd2FpdCByZXNvbHZlQWRkcmVzcyhycGMuZ2V0RU5TQWRkcmVzcywgYWRkciksXG4gICAgICAgICksXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgYSB0cmFuc2NvZGVyJ3MgcG9vbCBtYXggc2l6ZVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRyYW5zY29kZXJQb29sTWF4U2l6ZSgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0VHJhbnNjb2RlclBvb2xNYXhTaXplKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEJvbmRpbmdNYW5hZ2VyLmdldFRyYW5zY29kZXJQb29sTWF4U2l6ZSgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIGluZm8gYWJvdXQgYSB0cmFuc2NvZGVyXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYWRkciAtIHVzZXIncyBFVEggYWRkcmVzc1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8VHJhbnNjb2Rlcj59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRyYW5zY29kZXIoJzB4ZjAwLi4uJylcbiAgICAgKiAvLyA9PiBUcmFuc2NvZGVyIHtcbiAgICAgKiAvLyAgIGFjdGl2ZTogYm9vbGVhbixcbiAgICAgKiAvLyAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIHJld2FyZEN1dDogc3RyaW5nLFxuICAgICAqIC8vICAgZmVlU2hhcmU6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxhc3RSZXdhcmRSb3VuZDogc3RyaW5nLFxuICAgICAqIC8vICAgcGVuZGluZ1Jld2FyZEN1dCBzdHJpbmcsXG4gICAgICogLy8gICBwZW5kaW5nRmVlU2hhcmU6IHN0cmluZyxcbiAgICAgKiAvLyAgIHBlbmRpbmdQcmljZVBlclNlZ21lbnQ6IHN0cmluZyxcbiAgICAgKiAvLyAgIHByaWNlUGVyU2VnbWVudDogc3RyaW5nLFxuICAgICAqIC8vICAgc3RhdHVzOiAnTm90UmVnaXN0ZXJlZCcgfCAnUmVnaXN0ZXJlZCcsXG4gICAgICogLy8gICB0b3RhbFN0YWtlOiBzdHJpbmcsXG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIGdldFRyYW5zY29kZXIoYWRkcjogc3RyaW5nKTogUHJvbWlzZTxUcmFuc2NvZGVyPiB7XG4gICAgICBjb25zdCBhZGRyZXNzID0gYXdhaXQgcmVzb2x2ZUFkZHJlc3MocnBjLmdldEVOU0FkZHJlc3MsIGFkZHIpXG4gICAgICBjb25zdCBzdGF0dXMgPSBhd2FpdCBycGMuZ2V0VHJhbnNjb2RlclN0YXR1cyhhZGRyZXNzKVxuICAgICAgY29uc3QgYWN0aXZlID0gYXdhaXQgcnBjLmdldFRyYW5zY29kZXJJc0FjdGl2ZShhZGRyZXNzKVxuICAgICAgY29uc3QgdG90YWxTdGFrZSA9IGF3YWl0IHJwYy5nZXRUcmFuc2NvZGVyVG90YWxTdGFrZShhZGRyZXNzKVxuICAgICAgY29uc3QgdCA9IGF3YWl0IEJvbmRpbmdNYW5hZ2VyLmdldFRyYW5zY29kZXIoYWRkcmVzcylcbiAgICAgIGNvbnN0IGZlZVNoYXJlID0gdG9TdHJpbmcodC5mZWVTaGFyZSlcbiAgICAgIGNvbnN0IGxhc3RSZXdhcmRSb3VuZCA9IHRvU3RyaW5nKHQubGFzdFJld2FyZFJvdW5kKVxuICAgICAgY29uc3QgcGVuZGluZ0ZlZVNoYXJlID0gdG9TdHJpbmcodC5wZW5kaW5nRmVlU2hhcmUpXG4gICAgICBjb25zdCBwZW5kaW5nUHJpY2VQZXJTZWdtZW50ID0gdG9TdHJpbmcodC5wZW5kaW5nUHJpY2VQZXJTZWdtZW50KVxuICAgICAgY29uc3QgcGVuZGluZ1Jld2FyZEN1dCA9IHRvU3RyaW5nKHQucGVuZGluZ1Jld2FyZEN1dClcbiAgICAgIGNvbnN0IHByaWNlUGVyU2VnbWVudCA9IHRvU3RyaW5nKHQucHJpY2VQZXJTZWdtZW50KVxuICAgICAgY29uc3QgcmV3YXJkQ3V0ID0gdG9TdHJpbmcodC5yZXdhcmRDdXQpXG4gICAgICByZXR1cm4ge1xuICAgICAgICBhY3RpdmUsXG4gICAgICAgIGFkZHJlc3MsXG4gICAgICAgIGZlZVNoYXJlLFxuICAgICAgICBsYXN0UmV3YXJkUm91bmQsXG4gICAgICAgIHByaWNlUGVyU2VnbWVudCxcbiAgICAgICAgcGVuZGluZ1Jld2FyZEN1dCxcbiAgICAgICAgcGVuZGluZ0ZlZVNoYXJlLFxuICAgICAgICBwZW5kaW5nUHJpY2VQZXJTZWdtZW50LFxuICAgICAgICByZXdhcmRDdXQsXG4gICAgICAgIHN0YXR1cyxcbiAgICAgICAgdG90YWxTdGFrZSxcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0cmFuc2NvZGVyc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtBcnJheTxUcmFuc2NvZGVyPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0VHJhbnNjb2RlcnMoKVxuICAgICAqIC8vID0+IEFycmF5PFRyYW5zY29kZXI+XG4gICAgICovXG4gICAgYXN5bmMgZ2V0VHJhbnNjb2RlcnMoKTogUHJvbWlzZTxBcnJheTxUcmFuc2NvZGVyPj4ge1xuICAgICAgY29uc3QgdHJhbnNjb2RlcnMgPSBbXVxuICAgICAgbGV0IGFkZHIgPSBoZWFkVG9TdHJpbmcoYXdhaXQgQm9uZGluZ01hbmFnZXIuZ2V0Rmlyc3RUcmFuc2NvZGVySW5Qb29sKCkpXG4gICAgICB3aGlsZSAoYWRkciAhPT0gRU1QVFlfQUREUkVTUykge1xuICAgICAgICBjb25zdCB0cmFuc2NvZGVyID0gYXdhaXQgcnBjLmdldFRyYW5zY29kZXIoYWRkcilcbiAgICAgICAgdHJhbnNjb2RlcnMucHVzaCh0cmFuc2NvZGVyKVxuICAgICAgICBhZGRyID0gaGVhZFRvU3RyaW5nKGF3YWl0IEJvbmRpbmdNYW5hZ2VyLmdldE5leHRUcmFuc2NvZGVySW5Qb29sKGFkZHIpKVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRyYW5zY29kZXJzXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFdoZXRoZXIgdGhlIHByb3RvY29sIGlzIHBhdXNlZFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPGJvb2xlYW4+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRQcm90b2NvbFBhdXNlZCgpXG4gICAgICogLy8gPT4gYm9vbGVhblxuICAgICAqL1xuICAgIGFzeW5jIGdldFByb3RvY29sUGF1c2VkKCk6IFByb21pc2U8UHJvdG9jb2w+IHtcbiAgICAgIHJldHVybiBoZWFkVG9Cb29sKGF3YWl0IENvbnRyb2xsZXIucGF1c2VkKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIHByb3RvY29sXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8UHJvdG9jb2w+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRQcm90b2NvbCgpXG4gICAgICogLy8gPT4gUHJvdG9jb2wge1xuICAgICAgICBwYXVzZWRcbiAgICAgICAgdG90YWxUb2tlblN1cHBseVxuICAgICAgICB0b3RhbEJvbmRlZFRva2VuXG4gICAgICAgIHRhcmdldEJvbmRpbmdSYXRlXG4gICAgICAgIHRyYW5zY29kZXJQb29sTWF4U2l6ZVxuICAgICAgICBtYXhFYXJuaW5nc0NsYWltc1JvdW5kc1xuICAgICB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0UHJvdG9jb2woKTogUHJvbWlzZTxQcm90b2NvbD4ge1xuICAgICAgY29uc3QgcGF1c2VkID0gYXdhaXQgcnBjLmdldFByb3RvY29sUGF1c2VkKClcbiAgICAgIGNvbnN0IHRvdGFsVG9rZW5TdXBwbHkgPSBhd2FpdCBycGMuZ2V0VG9rZW5Ub3RhbFN1cHBseSgpXG4gICAgICBjb25zdCB0b3RhbEJvbmRlZFRva2VuID0gYXdhaXQgcnBjLmdldFRvdGFsQm9uZGVkKClcbiAgICAgIGNvbnN0IHRhcmdldEJvbmRpbmdSYXRlID0gYXdhaXQgcnBjLmdldFRhcmdldEJvbmRpbmdSYXRlKClcbiAgICAgIGNvbnN0IHRyYW5zY29kZXJQb29sTWF4U2l6ZSA9IGF3YWl0IHJwYy5nZXRUcmFuc2NvZGVyUG9vbE1heFNpemUoKVxuICAgICAgY29uc3QgbWF4RWFybmluZ3NDbGFpbXNSb3VuZHMgPSBhd2FpdCBycGMuZ2V0TWF4RWFybmluZ3NDbGFpbXNSb3VuZHMoKVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgcGF1c2VkLFxuICAgICAgICB0b3RhbFRva2VuU3VwcGx5LFxuICAgICAgICB0b3RhbEJvbmRlZFRva2VuLFxuICAgICAgICB0YXJnZXRCb25kaW5nUmF0ZSxcbiAgICAgICAgdHJhbnNjb2RlclBvb2xNYXhTaXplLFxuICAgICAgICBtYXhFYXJuaW5nc0NsYWltc1JvdW5kcyxcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyB0aGUgbGVuZ3RoIG9mIGEgcm91bmQgKGluIGJsb2NrcylcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRSb3VuZExlbmd0aCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Um91bmRMZW5ndGgoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgUm91bmRzTWFuYWdlci5yb3VuZExlbmd0aCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIHRoZSBlc3RpbWF0ZWQgbnVtYmVyIG9mIHJvdW5kcyBwZXIgeWVhclxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFJvdW5kc1BlclllYXIoKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldFJvdW5kc1BlclllYXIoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgUm91bmRzTWFuYWdlci5yb3VuZHNQZXJZZWFyKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIG51bWJlciBvZiB0aGUgY3VycmVudCByb3VuZFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEN1cnJlbnRSb3VuZCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Q3VycmVudFJvdW5kKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IFJvdW5kc01hbmFnZXIuY3VycmVudFJvdW5kKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFdoZXRoZXIgb3Igbm90IHRoZSBjdXJyZW50IHJvdW5kIGlzIGluaXRhbGl6ZWRcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxib29sZWFuPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0Q3VycmVudFJvdW5kSXNJbml0aWFsaXplZCgpXG4gICAgICogLy8gPT4gYm9vbGVhblxuICAgICAqL1xuICAgIGFzeW5jIGdldEN1cnJlbnRSb3VuZElzSW5pdGlhbGl6ZWQoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICByZXR1cm4gaGVhZFRvQm9vbChhd2FpdCBSb3VuZHNNYW5hZ2VyLmN1cnJlbnRSb3VuZEluaXRpYWxpemVkKCkpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFRoZSBibG9jayBhdCB3aGljaCB0aGUgY3VycmVudCByb3VuZCBzdGFydGVkXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0Q3VycmVudFJvdW5kU3RhcnRCbG9jaygpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Q3VycmVudFJvdW5kU3RhcnRCbG9jaygpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgcmV0dXJuIGhlYWRUb1N0cmluZyhhd2FpdCBSb3VuZHNNYW5hZ2VyLmN1cnJlbnRSb3VuZFN0YXJ0QmxvY2soKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVGhlIHByZXZpb3VzbHkgaW50aXRpYWxpemVkIHJvdW5kXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0TGFzdEluaXRpYWxpemVkUm91bmQoKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldExhc3RJbml0aWFsaXplZFJvdW5kKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IFJvdW5kc01hbmFnZXIubGFzdEluaXRpYWxpemVkUm91bmQoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0cyBnZW5lcmFsIGluZm9ybWF0aW9uIGFib3V0IHRoZSByb3VuZHMgaW4gdGhlIHByb3RvY29sXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8Um91bmRJbmZvPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0Q3VycmVudFJvdW5kSW5mbygpXG4gICAgICogLy8gPT4gUm91bmRJbmZvIHtcbiAgICAgKiAvLyAgIGlkOiBzdHJpbmcsXG4gICAgICogLy8gICBpbml0aWFsaXplZDogYm9vbGVhbixcbiAgICAgKiAvLyAgIHN0YXJ0QmxvY2s6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxhc3RJbml0aWFsaXplZFJvdW5kOiBzdHJpbmcsXG4gICAgICogLy8gICBsZW5ndGg6IHN0cmluZyxcbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0Q3VycmVudFJvdW5kSW5mbygpOiBQcm9taXNlPFJvdW5kSW5mbz4ge1xuICAgICAgY29uc3QgbGVuZ3RoID0gYXdhaXQgcnBjLmdldFJvdW5kTGVuZ3RoKClcbiAgICAgIGNvbnN0IGlkID0gYXdhaXQgcnBjLmdldEN1cnJlbnRSb3VuZCgpXG4gICAgICBjb25zdCBpbml0aWFsaXplZCA9IGF3YWl0IHJwYy5nZXRDdXJyZW50Um91bmRJc0luaXRpYWxpemVkKClcbiAgICAgIGNvbnN0IGxhc3RJbml0aWFsaXplZFJvdW5kID0gYXdhaXQgcnBjLmdldExhc3RJbml0aWFsaXplZFJvdW5kKClcbiAgICAgIGNvbnN0IHN0YXJ0QmxvY2sgPSBhd2FpdCBycGMuZ2V0Q3VycmVudFJvdW5kU3RhcnRCbG9jaygpXG4gICAgICByZXR1cm4ge1xuICAgICAgICBpZCxcbiAgICAgICAgaW5pdGlhbGl6ZWQsXG4gICAgICAgIGxhc3RJbml0aWFsaXplZFJvdW5kLFxuICAgICAgICBsZW5ndGgsXG4gICAgICAgIHN0YXJ0QmxvY2ssXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8vIEpvYnNcblxuICAgIC8qKlxuICAgICAqIFRvdGFsIGpvYnMgdGhhdCBoYXZlIGJlZW4gY3JlYXRlZFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldFRvdGFsSm9icygpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0VG90YWxKb2JzKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEpvYnNNYW5hZ2VyLm51bUpvYnMoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVmVyaWZpY2F0aW9uIHJhdGUgZm9yIGpvYnNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRKb2JWZXJpZmljYXRpb25SYXRlKClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBnZXRKb2JWZXJpZmljYXRpb25SYXRlKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEpvYnNNYW5hZ2VyLnZlcmlmaWNhdGlvblJhdGUoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVmVyaWZpY2F0aW9uIHBlcmlvZCBmb3Igam9ic1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEpvYlZlcmlmaWNhdGlvblBlcmlvZCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Sm9iVmVyaWZpY2F0aW9uUGVyaW9kKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEpvYnNNYW5hZ2VyLnZlcmlmaWNhdGlvblBlcmlvZCgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBTbGFzaGluZyBwZXJpb2QgZm9yIGpvYnNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxzdHJpbmc+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5nZXRKb2JWZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZCgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Sm9iVmVyaWZpY2F0aW9uU2xhc2hpbmdQZXJpb2QoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgIHJldHVybiBoZWFkVG9TdHJpbmcoYXdhaXQgSm9ic01hbmFnZXIudmVyaWZpY2F0aW9uU2xhc2hpbmdQZXJpb2QoKSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogRmluZGVyIGZlZSBmb3Igam9ic1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPHN0cmluZz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEpvYkZpbmRlckZlZSgpXG4gICAgICogLy8gPT4gc3RyaW5nXG4gICAgICovXG4gICAgYXN5bmMgZ2V0Sm9iRmluZGVyRmVlKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IEpvYnNNYW5hZ2VyLmZpbmRlckZlZSgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXRzIGdlbmVyYWwgaW5mbyBhYm91dCB0aGUgc3RhdGUgb2Ygam9icyBpbiB0aGUgcHJvdG9jb2xcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxKb2JzSW5mbz59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEpvYnNJbmZvKClcbiAgICAgKiAvLyA9PiBKb2JzSW5mbyB7XG4gICAgICogLy8gICB0b3RhbDogc3RyaW5nLFxuICAgICAqIC8vICAgdmVyaWZpY2F0aW9uUmF0ZTogc3RyaW5nLFxuICAgICAqIC8vICAgdmVyaWZpY2F0aW9uUGVyaW9kOiBzdHJpbmcsXG4gICAgICogLy8gICB2ZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZDogc3RyaW5nLFxuICAgICAqIC8vICAgZmluZGVyRmVlOiBzdHJpbmcsXG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIGdldEpvYnNJbmZvKCk6IFByb21pc2U8Sm9ic0luZm8+IHtcbiAgICAgIGNvbnN0IHRvdGFsID0gYXdhaXQgcnBjLmdldFRvdGFsSm9icygpXG4gICAgICBjb25zdCB2ZXJpZmljYXRpb25SYXRlID0gYXdhaXQgcnBjLmdldEpvYlZlcmlmaWNhdGlvblJhdGUoKVxuICAgICAgY29uc3QgdmVyaWZpY2F0aW9uUGVyaW9kID0gYXdhaXQgcnBjLmdldEpvYlZlcmlmaWNhdGlvblBlcmlvZCgpXG4gICAgICBjb25zdCB2ZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZCA9IGF3YWl0IHJwYy5nZXRKb2JWZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZCgpXG4gICAgICBjb25zdCBmaW5kZXJGZWUgPSBhd2FpdCBycGMuZ2V0Sm9iRmluZGVyRmVlKClcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRvdGFsLFxuICAgICAgICB2ZXJpZmljYXRpb25SYXRlLFxuICAgICAgICB2ZXJpZmljYXRpb25QZXJpb2QsXG4gICAgICAgIHZlcmlmaWNhdGlvblNsYXNoaW5nUGVyaW9kLFxuICAgICAgICBmaW5kZXJGZWUsXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgYSBqb2IgYnkgaWRcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBpZCAtIHRoZSBqb2IgaWRcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPEpvYj59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEpvYignMTMzNycpXG4gICAgICogLy8gPT4gSm9iIHtcbiAgICAgKiAvLyAgIGlkOiBzdHJpbmcsXG4gICAgICogLy8gICBzdHJlYW1JZDogc3RyaW5nLFxuICAgICAqIC8vICAgdHJhbnNjb2RpbmdPcHRpb25zOiBBcnJheTxKb2JQcm9maWxlPlxuICAgICAqIC8vICAgdHJhbnNjb2Rlcjogc3RyaW5nLFxuICAgICAqIC8vICAgYnJvYWRjYXN0ZXI6IHN0cmluZyxcbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgZ2V0Sm9iKGlkOiBzdHJpbmcpOiBQcm9taXNlPEpvYj4ge1xuICAgICAgY29uc3QgeCA9IGF3YWl0IEpvYnNNYW5hZ2VyLmdldEpvYihpZClcbiAgICAgIC8vIENvbXB1dGVyIHRyYW5zY29kZXIncyBhZGRyZXNzIGlmIG5vbmUgd2FzIHJldHVybmVkXG4gICAgICBpZiAoXG4gICAgICAgIHgudHJhbnNjb2RlckFkZHJlc3MgPT09IG51bGwgfHxcbiAgICAgICAgeC50cmFuc2NvZGVyQWRkcmVzcyA9PT0gRU1QVFlfQUREUkVTU1xuICAgICAgKSB7XG4gICAgICAgIGNvbnN0IHsgaGFzaCB9ID0gYXdhaXQgcnBjLmdldEJsb2NrKHguY3JlYXRpb25CbG9jaylcbiAgICAgICAgeC50cmFuc2NvZGVyQWRkcmVzcyA9IGhlYWRUb1N0cmluZyhcbiAgICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci5lbGVjdEFjdGl2ZVRyYW5zY29kZXIoXG4gICAgICAgICAgICB4Lm1heFByaWNlUGVyU2VnbWVudCxcbiAgICAgICAgICAgIGhhc2gsXG4gICAgICAgICAgICB4LmNyZWF0aW9uUm91bmQsXG4gICAgICAgICAgKSxcbiAgICAgICAgKVxuICAgICAgfVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaWQ6IHRvU3RyaW5nKGlkKSxcbiAgICAgICAgc3RyZWFtSWQ6IHguc3RyZWFtSWQsXG4gICAgICAgIHRyYW5zY29kaW5nT3B0aW9uczogdXRpbHMucGFyc2VUcmFuc2NvZGluZ09wdGlvbnMoeC50cmFuc2NvZGluZ09wdGlvbnMpLFxuICAgICAgICB0cmFuc2NvZGVyOiB4LnRyYW5zY29kZXJBZGRyZXNzLFxuICAgICAgICBicm9hZGNhc3RlcjogeC5icm9hZGNhc3RlckFkZHJlc3MsXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgYSBsaXN0IG9mIGpvYnNcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMuZnJvbSAtIGJsb2NrIHRvIHNlYXJjaCBmcm9tXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMudG8gLSBibG9jayB0byBzZWFyY2ggdG9cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gb3B0cy5ibG9ja3NBZ28gLSBoZWxwcyBkZXRlcm1pbmUgZGVmYXVsdCBmcm9tIGJsb2NrXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMuYnJvYWRjYXN0ZXIgLSBmaWx0ZXIgdG8gb25seSBqb2JzIGNyZWF0ZWQgYnkgdGhpcyBicm9hZGNhc3RlclxuICAgICAqIEByZXR1cm4ge0FycmF5PEpvYj59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmdldEpvYnMoKVxuICAgICAqIC8vID0+IEFycmF5PEpvYj5cbiAgICAgKi9cbiAgICBhc3luYyBnZXRKb2JzKHtcbiAgICAgIHRvLFxuICAgICAgZnJvbSxcbiAgICAgIGJsb2Nrc0FnbyA9IDEwMCAqIDEwMDAwLFxuICAgICAgLi4uZmlsdGVyc1xuICAgIH0gPSB7fSk6IEFycmF5PEpvYj4ge1xuICAgICAgY29uc3QgZXZlbnQgPSBldmVudHMuTmV3Sm9iXG4gICAgICBjb25zdCBoZWFkID0gYXdhaXQgY29uZmlnLmV0aC5ibG9ja051bWJlcigpXG4gICAgICBjb25zdCBmcm9tQmxvY2sgPSBmcm9tIHx8IE1hdGgubWF4KDAsIGhlYWQgLSBibG9ja3NBZ28pXG4gICAgICBjb25zdCB0b0Jsb2NrID0gdG8gfHwgJ2xhdGVzdCdcbiAgICAgIGNvbnN0IHRvcGljcyA9IHV0aWxzLmVuY29kZUV2ZW50VG9waWNzKFxuICAgICAgICBldmVudCxcbiAgICAgICAgZmlsdGVycy5icm9hZGNhc3RlclxuICAgICAgICAgID8ge1xuICAgICAgICAgICAgICAuLi5maWx0ZXJzLFxuICAgICAgICAgICAgICBicm9hZGNhc3RlcjogYXdhaXQgcmVzb2x2ZUFkZHJlc3MoXG4gICAgICAgICAgICAgICAgcnBjLmdldEVOU0FkZHJlc3MsXG4gICAgICAgICAgICAgICAgZmlsdGVycy5icm9hZGNhc3RlcixcbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICA6IGZpbHRlcnMsXG4gICAgICApXG4gICAgICBjb25zdCBwYXJhbXMgPSB7XG4gICAgICAgIGFkZHJlc3M6IEpvYnNNYW5hZ2VyLmFkZHJlc3MsXG4gICAgICAgIGZyb21CbG9jayxcbiAgICAgICAgdG9CbG9jayxcbiAgICAgICAgdG9waWNzLFxuICAgICAgfVxuICAgICAgY29uc3QgcmVzdWx0cyA9XG4gICAgICAgIC8vIGNhY2hlW2tleV0gfHxcbiAgICAgICAgKGF3YWl0IGNvbmZpZy5ldGguZ2V0TG9ncyhwYXJhbXMpKS5tYXAoXG4gICAgICAgICAgY29tcG9zZShcbiAgICAgICAgICAgIHggPT4gKHtcbiAgICAgICAgICAgICAgaWQ6IHRvU3RyaW5nKHguam9iSWQpLFxuICAgICAgICAgICAgICBzdHJlYW1JZDogeC5zdHJlYW1JZCxcbiAgICAgICAgICAgICAgdHJhbnNjb2RpbmdPcHRpb25zOiB1dGlscy5wYXJzZVRyYW5zY29kaW5nT3B0aW9ucyhcbiAgICAgICAgICAgICAgICB4LnRyYW5zY29kaW5nT3B0aW9ucyxcbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgYnJvYWRjYXN0ZXI6IHguYnJvYWRjYXN0ZXIsXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIHV0aWxzLmRlY29kZUV2ZW50KGV2ZW50KSxcbiAgICAgICAgICApLFxuICAgICAgICApXG4gICAgICAvLyBjYWNoZVtrZXldID0gcmVzdWx0c1xuICAgICAgcmV0dXJuIHJlc3VsdHMucmV2ZXJzZSgpXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgTFBUIGZyb20gdGhlIGZhdWNldFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFR4UmVjZWlwdD59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLnRhcEZhdWNldCgnMTMzNycpXG4gICAgICogLy8gPT4gVHhSZWNlaXB0IHtcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgdHJhbnNhY3Rpb25JbmRleFwiOiBCTixcbiAgICAgKiAvLyAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgY3VtdWxhdGl2ZUdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgZ2FzVXNlZDogQk4sXG4gICAgICogLy8gICBjb250cmFjdEFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxvZ3M6IEFycmF5PExvZyB7XG4gICAgICogLy8gICAgIGxvZ0luZGV4OiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSW5kZXg6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgYWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgICBkYXRhOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRvcGljczogQXJyYXk8c3RyaW5nPlxuICAgICAqIC8vICAgfT5cbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgdGFwRmF1Y2V0KHR4ID0gY29uZmlnLmRlZmF1bHRUeCk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICAvLyBjb25zdCBtcyA9IGF3YWl0IHJwYy5nZXRGYXVjZXRUYXBJbih0eC5mcm9tKVxuICAgICAgLy8gaWYgKG1zID4gMClcbiAgICAgIC8vICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgLy8gICAgIGBDYW4ndCB0YXAgdGhlIGZhdWNldCByaWdodCBub3cuIFlvdXIgbmV4dCB0YXAgaXMgaW4gJHtmb3JtYXREdXJhdGlvbihcbiAgICAgIC8vICAgICAgIG1zLFxuICAgICAgLy8gICAgICl9LmAsXG4gICAgICAvLyAgIClcbiAgICAgIC8vIHRhcCB0aGUgZmF1Y2V0XG4gICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICBhd2FpdCBMaXZlcGVlclRva2VuRmF1Y2V0LnJlcXVlc3QodHgpLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyB0aGUgcm91bmRcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtUeENvbmZpZ30gW3R4ID0gY29uZmlnLmRlZmF1bHRUeF0gLSBhbiBvYmplY3Qgc3BlY2lmeWluZyB0aGUgYGZyb21gIGFuZCBgZ2FzYCB2YWx1ZXMgb2YgdGhlIHRyYW5zYWN0aW9uXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxUeFJlY2VpcHQ+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5pbml0aWFsaXplUm91bmQoKVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIGluaXRpYWxpemVSb3VuZCh0eCA9IGNvbmZpZy5kZWZhdWx0VHgpOiBQcm9taXNlPFR4UmVjZWlwdD4ge1xuICAgICAgdHJ5IHtcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSByb3VuZFxuICAgICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICAgIGF3YWl0IFJvdW5kc01hbmFnZXIuaW5pdGlhbGl6ZVJvdW5kKHR4KSxcbiAgICAgICAgICBjb25maWcuZXRoLFxuICAgICAgICApXG4gICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgZXJyLm1lc3NhZ2UgPSAnRXJyb3I6IGluaXRpYWxpemVSb3VuZFxcbicgKyBlcnIubWVzc2FnZVxuICAgICAgICB0aHJvdyBlcnJcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2xhaW1zIHRva2VuIGFuZCBldGggZWFybmluZ3MgZnJvbSB0aGUgc2VuZGVyJ3MgYGxhc3RDbGFpbVJvdW5kICsgMWAgdGhyb3VnaCBhIGdpdmVuIGBlbmRSb3VuZGBcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGVuZFJvdW5kIC0gdGhlIHJvdW5kIHRvIGNsYWltIGVhcm5pbmdzIHVudGlsXG4gICAgICogQHBhcmFtIHtUeENvbmZpZ30gW3R4ID0gY29uZmlnLmRlZmF1bHRUeF0gLSBhbiBvYmplY3Qgc3BlY2lmeWluZyB0aGUgYGZyb21gIGFuZCBgZ2FzYCB2YWx1ZXMgb2YgdGhlIHRyYW5zYWN0aW9uXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5jbGFpbUVhcm5pbmdzKClcbiAgICAgKiAvLyA9PiBzdHJpbmdcbiAgICAgKi9cbiAgICBhc3luYyBjbGFpbUVhcm5pbmdzKFxuICAgICAgZW5kUm91bmQ6IHN0cmluZyxcbiAgICAgIHR4ID0gY29uZmlnLmRlZmF1bHRUeCxcbiAgICApOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgcmV0dXJuIGF3YWl0IEJvbmRpbmdNYW5hZ2VyLmNsYWltRWFybmluZ3MoZW5kUm91bmQsIHtcbiAgICAgICAgLi4uY29uZmlnLmRlZmF1bHRUeCxcbiAgICAgICAgLi4udHgsXG4gICAgICB9KVxuICAgIH0sXG5cbiAgICBhc3luYyBhcHByb3ZlVG9rZW5Cb25kQW1vdW50KFxuICAgICAgYW1vdW50OiBzdHJpbmcsXG4gICAgICB0eDogVHhPYmplY3QsXG4gICAgKTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIGNvbnN0IHRva2VuID0gdG9CTihhbW91bnQpXG4gICAgICAvLyBUT0RPOiAtIGNoZWNrIHRva2VuIGJhbGFuY2VcbiAgICAgIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgYXdhaXQgTGl2ZXBlZXJUb2tlbi5hcHByb3ZlKEJvbmRpbmdNYW5hZ2VyLmFkZHJlc3MsIHRva2VuLCB7XG4gICAgICAgICAgLi4uY29uZmlnLmRlZmF1bHRUeCxcbiAgICAgICAgICAuLi50eCxcbiAgICAgICAgfSksXG4gICAgICAgIGNvbmZpZy5ldGgsXG4gICAgICApXG4gICAgfSxcblxuICAgIGFzeW5jIGJvbmRBcHByb3ZlZFRva2VuQW1vdW50KFxuICAgICAgdG86IHN0cmluZyxcbiAgICAgIGFtb3VudDogc3RyaW5nLFxuICAgICAgdHg6IFR4T2JqZWN0LFxuICAgICk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICBjb25zdCB0b2tlbiA9IHRvQk4oYW1vdW50KVxuICAgICAgLy8gVE9ETzogY2hlY2sgZm9yIGV4aXN0aW5nIGFwcHJvdmFsIC8gcm91bmQgaW5pdGlhbGl6YXRpb24gLyB0b2tlbiBiYWxhbmNlXG4gICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci5ib25kKFxuICAgICAgICAgIHRva2VuLFxuICAgICAgICAgIGF3YWl0IHJlc29sdmVBZGRyZXNzKHJwYy5nZXRFTlNBZGRyZXNzLCB0byksXG4gICAgICAgICAge1xuICAgICAgICAgICAgLi4uY29uZmlnLmRlZmF1bHRUeCxcbiAgICAgICAgICAgIC4uLnR4LFxuICAgICAgICAgIH0sXG4gICAgICAgICksXG4gICAgICAgIGNvbmZpZy5ldGgsXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldHMgdGhlIGVzdGltYXRlZCBhbW91bnQgb2YgZ2FzIHRvIGJlIHVzZWQgYnkgYSBzbWFydCBjb250cmFjdFxuICAgICAqIG1ldGhvZC5cbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtXG4gICAgICogIGNvbnRyYWN0TmFtZTogbmFtZSBvZiBjb250cmFjdCBjb250YWluaW5nIG1ldGhvZCB5b3Ugd2lzaCB0byBmaW5kIGdhcyBwcmljZSBmb3IuXG4gICAgICogIG1ldGhvZE5hbWU6IG5hbWUgb2YgbWV0aG9kIG9uIGNvbnRyYWN0LlxuICAgICAqICBtZXRob2RBcmdzOiBhcnJheSBvZiBhcmd1bWVudCB0byBiZSBwYXNzZWQgdG8gdGhlIGNvbnRyYWN0IGluIHNwZWNpZmllZCBvcmRlci5cbiAgICAgKiAgdHg6IChvcHRpb2FubCl7XG4gICAgICogICAgZnJvbTogYWRkcmVzcyAtIDB4Li4uLFxuICAgICAqICAgIGdhczogbnVtYmVyLFxuICAgICAqICAgIHZhbHVlOiAob3B0aW9uYWwpIG51bWJlciBvciBzdHJpbmcgY29udGFpbmluZyBudW1iZXJcbiAgICAgKiAgfVxuICAgICAqXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxudW1iZXI+fSBjb250YWluaW5nIGVzdGltYXRlZCBnYXMgcHJpY2VcbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZXN0aW1hdGVHYXMoXG4gICAgICogICdCb25kaW5nTWFuYWdlcicsXG4gICAgICogICdib25kJyxcbiAgICAgKiAgWzEwLCAnMHgwMC4uLi4uJ11cbiAgICAgKiApXG4gICAgICogLy8gPT4gMzM0NTRcbiAgICAgKi9cbiAgICBhc3luYyBlc3RpbWF0ZUdhcyhcbiAgICAgIGNvbnRyYWN0TmFtZTogc3RyaW5nLFxuICAgICAgbWV0aG9kTmFtZTogc3RyaW5nLFxuICAgICAgbWV0aG9kQXJnczogQXJyYXksXG4gICAgICB0eCA9IGNvbmZpZy5kZWZhdWx0VHgsXG4gICAgKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgIHR4LnZhbHVlID0gdHgudmFsdWUgPyB0eC52YWx1ZSA6ICcwJ1xuICAgICAgY29uc3QgZ2FzUmF0ZSA9IDEuMlxuICAgICAgY29uc3QgY29udHJhY3RBQkkgPSBjb25maWcuYWJpc1tjb250cmFjdE5hbWVdXG4gICAgICBjb25zdCBtZXRob2RBQkkgPSB1dGlscy5maW5kQWJpQnlOYW1lKGNvbnRyYWN0QUJJLCBtZXRob2ROYW1lKVxuICAgICAgY29uc3QgZW5jb2RlZERhdGEgPSB1dGlscy5lbmNvZGVNZXRob2RQYXJhbXMobWV0aG9kQUJJLCBtZXRob2RBcmdzKVxuICAgICAgcmV0dXJuIE1hdGgucm91bmQoXG4gICAgICAgIHRvTnVtYmVyKFxuICAgICAgICAgIGF3YWl0IGNvbmZpZy5ldGguZXN0aW1hdGVHYXMoe1xuICAgICAgICAgICAgdG86IGNvbmZpZy5jb250cmFjdHNbY29udHJhY3ROYW1lXS5hZGRyZXNzLFxuICAgICAgICAgICAgZnJvbTogY29uZmlnLmRlZmF1bHRUeC5mcm9tLFxuICAgICAgICAgICAgdmFsdWU6IHR4LnZhbHVlLFxuICAgICAgICAgICAgZGF0YTogZW5jb2RlZERhdGEsXG4gICAgICAgICAgfSksXG4gICAgICAgICkgKiBnYXNSYXRlLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBVbmJvbmRzIExQVCBmcm9tIGFuIGFkZHJlc3NcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHtUeENvbmZpZ30gW3R4ID0gY29uZmlnLmRlZmF1bHRUeF0gLSBhbiBvYmplY3Qgc3BlY2lmeWluZyB0aGUgYGZyb21gIGFuZCBgZ2FzYCB2YWx1ZXMgb2YgdGhlIHRyYW5zYWN0aW9uXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxUeFJlY2VpcHQ+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy51bmJvbmQoYW1vdW50KVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIHVuYm9uZChhbW91bnQ6IHN0cmluZywgdHggPSBjb25maWcuZGVmYXVsdFR4KTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIGNvbnN0IHsgc3RhdHVzLCBwZW5kaW5nU3Rha2UsIGJvbmRlZEFtb3VudCB9ID0gYXdhaXQgcnBjLmdldERlbGVnYXRvcihcbiAgICAgICAgdHguZnJvbSxcbiAgICAgIClcbiAgICAgIC8vIHBlbmRpbmdTdGFrZSA9IDAgaWYgZGVsZWdhdG9yIGhhcyBjbGFpbWVkIGVhcm5pbmdzIHRocm91Z2ggdGhlIGN1cnJlbnQgcm91bmRcbiAgICAgIC8vIEluIHRoaXMgY2FzZSwgYm9uZGVkQW1vdW50IGlzIHVwIHRvIGRhdGVcbiAgICAgIGNvbnN0IHRvdGFsU3Rha2UgPVxuICAgICAgICB0b0JOKHBlbmRpbmdTdGFrZSkuY21wKHRvQk4oYm9uZGVkQW1vdW50KSkgPCAwXG4gICAgICAgICAgPyBib25kZWRBbW91bnRcbiAgICAgICAgICA6IHBlbmRpbmdTdGFrZVxuICAgICAgLy8gT25seSB1bmJvbmQgaWYgYW1vdW50IGRvZXNuJ3QgZXhjZWVkIHlvdXIgY3VycmVudCBzdGFrZVxuICAgICAgaWYgKHRvQk4odG90YWxTdGFrZSkuY21wKHRvQk4oYW1vdW50KSkgPCAwKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICBgQ2Fubm90IHVuYm9uZCBhIHBvcnRpb24gb2YgdG9rZW5zIGdyZWF0ZXIgdGhhbiB5b3VyIHRvdGFsIHN0YWtlIG9mICR7dG90YWxTdGFrZX0gTFBUYCxcbiAgICAgICAgKVxuICAgICAgfVxuXG4gICAgICAvLyBVbmJvbmQgdG90YWwgc3Rha2UgaWYgYSB6ZXJvIG9yIG5lZ2F0aXZlIGFtb3VudCBpcyBwYXNzZWRcbiAgICAgIGFtb3VudCA9IGFtb3VudCA8PSAwID8gdG90YWxTdGFrZSA6IGFtb3VudFxuXG4gICAgICAvLyBDYW4gb25seSB1bmJvbmQgc3VjY2Vzc2Z1bGx5IHdoZW4gbm90IGFscmVhZHkgXCJVbmJvbmRlZFwiXG4gICAgICBpZiAoc3RhdHVzID09PSBERUxFR0FUT1JfU1RBVFVTLlVuYm9uZGVkKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhpcyBhY2NvdW50IGlzIGFscmVhZHkgdW5ib25kZWQuJylcbiAgICAgIH1cblxuICAgICAgdHguZ2FzID0gYXdhaXQgcnBjLmVzdGltYXRlR2FzKCdCb25kaW5nTWFuYWdlcicsICd1bmJvbmQnLCBbYW1vdW50XSlcblxuICAgICAgcmV0dXJuIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgYXdhaXQgQm9uZGluZ01hbmFnZXIudW5ib25kKGFtb3VudCwge1xuICAgICAgICAgIC4uLmNvbmZpZy5kZWZhdWx0VHgsXG4gICAgICAgICAgLi4udHgsXG4gICAgICAgIH0pLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRyYW5zY29kZXIgcGFyYW1ldGVyc1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmV3YXJkQ3V0IC0gdGhlIGJsb2NrIHJld2FyZCBjdXQgeW91IHdpc2ggdG8gc2V0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZlZVNoYXJlIC0gdGhlIGZlZSBzaGFyZSB5b3Ugd2lzaCB0byBzZXRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcHJpY2VQZXJTZWdtZW50IC0gdGhlIHByaWNlIHBlciBzZWdtZW50IHlvdSB3aXNoIHRvIHNldFxuICAgICAqIEBwYXJhbSB7VHhDb25maWd9IFt0eCA9IGNvbmZpZy5kZWZhdWx0VHhdIC0gYW4gb2JqZWN0IHNwZWNpZnlpbmcgdGhlIGBmcm9tYCBhbmQgYGdhc2AgdmFsdWVzIG9mIHRoZSB0cmFuc2FjdGlvblxuICAgICAqIEByZXR1cm4ge1Byb21pc2U8VHhSZWNlaXB0Pn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuc2V0dXBUcmFuc2NvZGVyKCcxMCcsICcxMCcsICc1JylcbiAgICAgKiAvLyA9PiBUeFJlY2VpcHQge1xuICAgICAqIC8vICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICB0cmFuc2FjdGlvbkluZGV4XCI6IEJOLFxuICAgICAqIC8vICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICBjdW11bGF0aXZlR2FzVXNlZDogQk4sXG4gICAgICogLy8gICBnYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGNvbnRyYWN0QWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgbG9nczogQXJyYXk8TG9nIHtcbiAgICAgKiAvLyAgICAgbG9nSW5kZXg6IEJOLFxuICAgICAqIC8vICAgICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25JbmRleDogc3RyaW5nLFxuICAgICAqIC8vICAgICBhZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICAgIGRhdGE6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdG9waWNzOiBBcnJheTxzdHJpbmc+XG4gICAgICogLy8gICB9PlxuICAgICAqIC8vIH1cbiAgICAgKi9cbiAgICBhc3luYyBzZXR1cFRyYW5zY29kZXIoXG4gICAgICByZXdhcmRDdXQ6IHN0cmluZywgLy8gcGVyY2VudGFnZVxuICAgICAgZmVlU2hhcmU6IHN0cmluZywgLy8gcGVyY2VudGFnZVxuICAgICAgcHJpY2VQZXJTZWdtZW50OiBzdHJpbmcsIC8vIGxwdFxuICAgICAgdHggPSBjb25maWcuZGVmYXVsdFR4LFxuICAgICk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICAvLyBiZWNvbWUgYSB0cmFuc2NvZGVyXG4gICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci50cmFuc2NvZGVyKFxuICAgICAgICAgIHRvQk4ocmV3YXJkQ3V0KSxcbiAgICAgICAgICB0b0JOKGZlZVNoYXJlKSxcbiAgICAgICAgICB0b0JOKHByaWNlUGVyU2VnbWVudCksXG4gICAgICAgICAgdHgsXG4gICAgICAgICksXG4gICAgICAgIGNvbmZpZy5ldGgsXG4gICAgICApXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldCB0YXJnZXQgYm9uZGluZyByYXRlXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMuZ2V0VGFyZ2V0Qm9uZGluZ1JhdGUoKVxuICAgICAqIC8vID0+IHN0cmluZ1xuICAgICAqL1xuICAgIGFzeW5jIGdldFRhcmdldEJvbmRpbmdSYXRlKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gaGVhZFRvU3RyaW5nKGF3YWl0IE1pbnRlci50YXJnZXRCb25kaW5nUmF0ZSgpKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBEZXBvc2l0cyBFVEggZm9yIGJyb2FkY2FzdGluZ1xuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gYW1vdW50IC0gYW1vdW50IG9mIEVUSCB0byBkZXBvc2l0XG4gICAgICogQHBhcmFtIHtUeENvbmZpZ30gW3R4ID0gY29uZmlnLmRlZmF1bHRUeF0gLSBhbiBvYmplY3Qgc3BlY2lmeWluZyB0aGUgYGZyb21gIGFuZCBgZ2FzYCB2YWx1ZXMgb2YgdGhlIHRyYW5zYWN0aW9uXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxUeFJlY2VpcHQ+fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy5kZXBvc2l0KCcxMDAnKVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIGRlcG9zaXQoYW1vdW50LCB0eCA9IGNvbmZpZy5kZWZhdWx0VHgpOiBQcm9taXNlPFR4UmVjZWlwdD4ge1xuICAgICAgLy8gbWFrZSBzdXJlIGJhbGFuY2UgaXMgaGlnaGVyIHRoYW4gZGVwb3NpdFxuICAgICAgY29uc3QgYmFsYW5jZSA9IHRvQk4oYXdhaXQgcnBjLmdldEV0aEJhbGFuY2UodHguZnJvbSkpXG4gICAgICBjb25zdCB2YWx1ZSA9IHRvQk4oYW1vdW50KVxuICAgICAgaWYgKCFiYWxhbmNlLmd0ZSh2YWx1ZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgIGBDYW5ub3QgZGVwb3NpdCAke3RvU3RyaW5nKFxuICAgICAgICAgICAgdG9CTih2YWx1ZS50b1N0cmluZygxMCkpLFxuICAgICAgICAgICl9IExQVCBiZWNhdXNlIGlzIGl0IGdyZWF0ZXIgdGhhbiB5b3VyIGN1cnJlbnQgYmFsYW5jZSAoJHtiYWxhbmNlfSBMUFQpLmAsXG4gICAgICAgIClcbiAgICAgIH1cbiAgICAgIC8vIC4uLmFhYWFuZCBkZXBvc2l0IVxuICAgICAgcmV0dXJuIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgYXdhaXQgSm9ic01hbmFnZXIuZGVwb3NpdCh7XG4gICAgICAgICAgLi4udHgsXG4gICAgICAgICAgdmFsdWUsXG4gICAgICAgIH0pLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBXaXRoZHJhd3MgZGVwb3NpdGVkIEVUSFxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFR4UmVjZWlwdD59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLndpdGhkcmF3KClcbiAgICAgKiAvLyA9PiBUeFJlY2VpcHQge1xuICAgICAqIC8vICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICB0cmFuc2FjdGlvbkluZGV4XCI6IEJOLFxuICAgICAqIC8vICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICBjdW11bGF0aXZlR2FzVXNlZDogQk4sXG4gICAgICogLy8gICBnYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGNvbnRyYWN0QWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgbG9nczogQXJyYXk8TG9nIHtcbiAgICAgKiAvLyAgICAgbG9nSW5kZXg6IEJOLFxuICAgICAqIC8vICAgICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25JbmRleDogc3RyaW5nLFxuICAgICAqIC8vICAgICBhZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICAgIGRhdGE6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdG9waWNzOiBBcnJheTxzdHJpbmc+XG4gICAgICogLy8gICB9PlxuICAgICAqIC8vIH1cbiAgICAgKi9cbiAgICBhc3luYyB3aXRoZHJhdyh0eCA9IGNvbmZpZy5kZWZhdWx0VHgpOiBQcm9taXNlPFR4UmVjZWlwdD4ge1xuICAgICAgcmV0dXJuIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgYXdhaXQgSm9ic01hbmFnZXIud2l0aGRyYXcodHgpLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgICAgLy8gd2l0aGRyYXcgYWxsIChkZWZhdWx0KVxuICAgICAgLy8gaWYgKCd1bmRlZmluZWQnID09PSB0eXBlb2YgYW1vdW50KSB7XG4gICAgICAvLyAgIGF3YWl0IEpvYnNNYW5hZ2VyLndpdGhkcmF3KClcbiAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAvLyAgIC8vIHdpdGhkcmF3IHNwZWNpZmljIGFtb3VudFxuICAgICAgLy8gICBjb25zdCBhID0gdG9CTihhbW91bnQpXG4gICAgICAvLyAgIGNvbnN0IGIgPSAoYXdhaXQgSm9ic01hbmFnZXIuYnJvYWRjYXN0ZXJzKHR4LmZyb20pKS5kZXBvc2l0XG4gICAgICAvLyAgIGNvbnN0IGMgPSBiLnN1YihhKVxuICAgICAgLy8gICBhd2FpdCBKb2JzTWFuYWdlci53aXRoZHJhdygpXG4gICAgICAvLyAgIGF3YWl0IHJwYy5kZXBvc2l0KGMsIHR4KVxuICAgICAgLy8gfVxuICAgICAgLy8gLy8gdXBkYXRlZCB0b2tlbiBpbmZvXG4gICAgICAvLyByZXR1cm4gYXdhaXQgcnBjLmdldFRva2VuSW5mbyh0eC5mcm9tKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBXaXRoZHJhd3MgZWFybmVkIHRva2VuIChUcmFuc2ZlcnMgYSBzZW5kZXIncyBkZWxlZ2F0b3IgYGJvbmRlZEFtb3VudGAgdG8gdGhlaXIgYHRva2VuQmFsYW5jZWApXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSB7VHhDb25maWd9IFt0eCA9IGNvbmZpZy5kZWZhdWx0VHhdIC0gYW4gb2JqZWN0IHNwZWNpZnlpbmcgdGhlIGBmcm9tYCBhbmQgYGdhc2AgdmFsdWVzIG9mIHRoZSB0cmFuc2FjdGlvblxuICAgICAqIEByZXR1cm4ge1R4UmVjZWlwdH1cbiAgICAgKlxuICAgICAqIEBleGFtcGxlXG4gICAgICpcbiAgICAgKiBhd2FpdCBycGMud2l0aGRyYXdTdGFrZSgpXG4gICAgICogLy8gPT4gVHhSZWNlaXB0IHtcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgdHJhbnNhY3Rpb25JbmRleFwiOiBCTixcbiAgICAgKiAvLyAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgY3VtdWxhdGl2ZUdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgZ2FzVXNlZDogQk4sXG4gICAgICogLy8gICBjb250cmFjdEFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxvZ3M6IEFycmF5PExvZyB7XG4gICAgICogLy8gICAgIGxvZ0luZGV4OiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSW5kZXg6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgYWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgICBkYXRhOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRvcGljczogQXJyYXk8c3RyaW5nPlxuICAgICAqIC8vICAgfT5cbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgd2l0aGRyYXdTdGFrZShcbiAgICAgIHR4ID0gY29uZmlnLmRlZmF1bHRUeCxcbiAgICAgIHVuYm9uZGluZ0xvY2tJZCA9IG51bGwsXG4gICAgKTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIGNvbnN0IHtcbiAgICAgICAgc3RhdHVzLFxuICAgICAgICB3aXRoZHJhd0Ftb3VudCxcbiAgICAgICAgbmV4dFVuYm9uZGluZ0xvY2tJZCxcbiAgICAgIH0gPSBhd2FpdCBycGMuZ2V0RGVsZWdhdG9yKHR4LmZyb20pXG5cbiAgICAgIGlmIChzdGF0dXMgPT09IERFTEVHQVRPUl9TVEFUVVMuVW5ib25kaW5nICYmICF1bmJvbmRpbmdMb2NrSWQpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdEZWxlZ2F0b3IgbXVzdCB3YWl0IHRocm91Z2ggdW5ib25kaW5nIHBlcmlvZCcpXG4gICAgICB9IGVsc2UgaWYgKHdpdGhkcmF3QW1vdW50ID09PSAnMCcpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdEZWxlZ2F0b3IgZG9lcyBub3QgaGF2ZSBhbnl0aGluZyB0byB3aXRoZHJhdycpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB1bmJvbmRpbmdMb2NrSWQgPSB0b0JOKG5leHRVbmJvbmRpbmdMb2NrSWQpXG4gICAgICAgIGlmICh1bmJvbmRpbmdMb2NrSWQuY21wKG5ldyBCTigwKSkgPiAwKSB7XG4gICAgICAgICAgdW5ib25kaW5nTG9ja0lkID0gdW5ib25kaW5nTG9ja0lkLnN1YihuZXcgQk4oMSkpXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci53aXRoZHJhd1N0YWtlKHRvU3RyaW5nKHVuYm9uZGluZ0xvY2tJZCksIHR4KSxcbiAgICAgICAgICBjb25maWcuZXRoLFxuICAgICAgICApXG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFdpdGhkcmF3cyBlYXJuZWQgdG9rZW4gKFRyYW5zZmVycyBhIHNlbmRlcidzIGRlbGVnYXRvciBgYm9uZGVkQW1vdW50YCB0byB0aGVpciBgdG9rZW5CYWxhbmNlYClcbiAgICAgKiBAbWVtYmVyb2YgbGl2ZXBlZXJ+cnBjXG4gICAgICogQHBhcmFtIHt9IFt1bmJvbmRsb2NrXSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSB1bmJvbmRsb2NrIGlkLCBhbW91bnQgJiB3aXRoZHJhd1JvdW5kXG4gICAgICogQHBhcmFtIHtUeENvbmZpZ30gW3R4ID0gY29uZmlnLmRlZmF1bHRUeF0gLSBhbiBvYmplY3Qgc3BlY2lmeWluZyB0aGUgYGZyb21gIGFuZCBgZ2FzYCB2YWx1ZXMgb2YgdGhlIHRyYW5zYWN0aW9uXG4gICAgICogQHJldHVybiB7VHhSZWNlaXB0fVxuICAgICAqXG4gICAgICogQGV4YW1wbGVcbiAgICAgKlxuICAgICAqIGF3YWl0IHJwYy53aXRoZHJhd1N0YWtlV2l0aFVuYm9uZExvY2sodW5ib25kbG9jaylcbiAgICAgKiAvLyA9PiBUeFJlY2VpcHQge1xuICAgICAqIC8vICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICB0cmFuc2FjdGlvbkluZGV4XCI6IEJOLFxuICAgICAqIC8vICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICBjdW11bGF0aXZlR2FzVXNlZDogQk4sXG4gICAgICogLy8gICBnYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGNvbnRyYWN0QWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgbG9nczogQXJyYXk8TG9nIHtcbiAgICAgKiAvLyAgICAgbG9nSW5kZXg6IEJOLFxuICAgICAqIC8vICAgICBibG9ja051bWJlcjogQk4sXG4gICAgICogLy8gICAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25JbmRleDogc3RyaW5nLFxuICAgICAqIC8vICAgICBhZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICAgIGRhdGE6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdG9waWNzOiBBcnJheTxzdHJpbmc+XG4gICAgICogLy8gICB9PlxuICAgICAqIC8vIH1cbiAgICAgKi9cblxuICAgIGFzeW5jIHdpdGhkcmF3U3Rha2VXaXRoVW5ib25kTG9jayhcbiAgICAgIHVuYm9uZGxvY2s6IHsgaWQ6IHN0cmluZywgYW1vdW50OiBzdHJpbmcsIHdpdGhkcmF3Um91bmQ6IHN0cmluZyB9LFxuICAgICAgdHggPSBjb25maWcuZGVmYXVsdFR4LFxuICAgICk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICBjb25zdCB7IGlkLCBhbW91bnQsIHdpdGhkcmF3Um91bmQgfSA9IHVuYm9uZGxvY2tcblxuICAgICAgY29uc3QgY3VycmVudFJvdW5kID0gYXdhaXQgcnBjLmdldEN1cnJlbnRSb3VuZCgpXG5cbiAgICAgIC8vIGVuc3VyZSB0aGUgdW5ib25kaW5nIHBlcmlvZCBpcyBvdmVyXG4gICAgICBpZiAod2l0aGRyYXdSb3VuZCA+IGN1cnJlbnRSb3VuZCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0RlbGVnYXRvciBtdXN0IHdhaXQgdGhyb3VnaCB1bmJvbmRpbmcgcGVyaW9kJylcbiAgICAgIH0gZWxzZSBpZiAoYW1vdW50ID09PSAnMCcpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdEZWxlZ2F0b3IgZG9lcyBub3QgaGF2ZSBhbnl0aGluZyB0byB3aXRoZHJhdycpXG4gICAgICB9IGVsc2UgaWYgKGFtb3VudCA8IDApIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBbW91bnQgY2Fubm90IGJlIG5lZ2F0aXZlJylcbiAgICAgIH1cblxuICAgICAgbGV0IHVuYm9uZGluZ0xvY2tJZCA9IHRvQk4oaWQpXG5cbiAgICAgIHJldHVybiBhd2FpdCB1dGlscy5nZXRUeFJlY2VpcHQoXG4gICAgICAgIGF3YWl0IEJvbmRpbmdNYW5hZ2VyLndpdGhkcmF3U3Rha2UodG9TdHJpbmcodW5ib25kaW5nTG9ja0lkKSwgdHgpLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBXaXRoZHJhd3MgZWFybmVkIGZlZXMgKFRyYW5zZmVycyBhIHNlbmRlcidzIGRlbGVnYXRvciBgZmVlc2AgdG8gdGhlaXIgYGV0aEJhbGFuY2VgKVxuICAgICAqIEBtZW1iZXJvZiBsaXZlcGVlcn5ycGNcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtUeFJlY2VpcHR9XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLndpdGhkcmF3RmVlcygpXG4gICAgICogLy8gPT4gVHhSZWNlaXB0IHtcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgdHJhbnNhY3Rpb25JbmRleFwiOiBCTixcbiAgICAgKiAvLyAgIGJsb2NrSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgY3VtdWxhdGl2ZUdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgZ2FzVXNlZDogQk4sXG4gICAgICogLy8gICBjb250cmFjdEFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgIGxvZ3M6IEFycmF5PExvZyB7XG4gICAgICogLy8gICAgIGxvZ0luZGV4OiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tOdW1iZXI6IEJOLFxuICAgICAqIC8vICAgICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSW5kZXg6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgYWRkcmVzczogc3RyaW5nLFxuICAgICAqIC8vICAgICBkYXRhOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRvcGljczogQXJyYXk8c3RyaW5nPlxuICAgICAqIC8vICAgfT5cbiAgICAgKiAvLyB9XG4gICAgICovXG4gICAgYXN5bmMgd2l0aGRyYXdGZWVzKHR4ID0gY29uZmlnLmRlZmF1bHRUeCk6IFByb21pc2U8VHhSZWNlaXB0PiB7XG4gICAgICByZXR1cm4gYXdhaXQgdXRpbHMuZ2V0VHhSZWNlaXB0KFxuICAgICAgICBhd2FpdCBCb25kaW5nTWFuYWdlci53aXRoZHJhd0ZlZXModHgpLFxuICAgICAgICBjb25maWcuZXRoLFxuICAgICAgKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgam9iXG4gICAgICogQG1lbWJlcm9mIGxpdmVwZWVyfnJwY1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzdHJlYW1JZCAtIHRoZSBzdHJlYW0gaWQgZm9yIHRoZSBqb2JcbiAgICAgKiBAcGFyYW0ge0FycmF5PHN0cmluZz59IHByb2ZpbGVzIC0gYSBsaXN0IG9mIHByb2ZpbGVzIHRvIHRyYW5zY29kZSB0aGUgam9iIGludG9cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbWF4UHJpY2VQZXJTZWdtZW50IC0gdGhlIG1heGltdW0gTFBUVSBwcmljZSB0aGUgYnJvYWRjYXN0ZXIgaXMgd2lsbGluZyB0byBwYXkgcGVyIHNlZ21lbnRcbiAgICAgKiBAcGFyYW0ge1R4Q29uZmlnfSBbdHggPSBjb25maWcuZGVmYXVsdFR4XSAtIGFuIG9iamVjdCBzcGVjaWZ5aW5nIHRoZSBgZnJvbWAgYW5kIGBnYXNgIHZhbHVlcyBvZiB0aGUgdHJhbnNhY3Rpb25cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFR4UmVjZWlwdD59XG4gICAgICpcbiAgICAgKiBAZXhhbXBsZVxuICAgICAqXG4gICAgICogYXdhaXQgcnBjLmNyZWF0ZUpvYignZm9vJywgW1AyNDBwMzBmcHM0eDMnLCAnUDM2MHAzMGZwczE2eDknXSwgJzUnKVxuICAgICAqIC8vID0+IFR4UmVjZWlwdCB7XG4gICAgICogLy8gICB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIHRyYW5zYWN0aW9uSW5kZXhcIjogQk4sXG4gICAgICogLy8gICBibG9ja0hhc2g6IHN0cmluZyxcbiAgICAgKiAvLyAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgIGN1bXVsYXRpdmVHYXNVc2VkOiBCTixcbiAgICAgKiAvLyAgIGdhc1VzZWQ6IEJOLFxuICAgICAqIC8vICAgY29udHJhY3RBZGRyZXNzOiBzdHJpbmcsXG4gICAgICogLy8gICBsb2dzOiBBcnJheTxMb2cge1xuICAgICAqIC8vICAgICBsb2dJbmRleDogQk4sXG4gICAgICogLy8gICAgIGJsb2NrTnVtYmVyOiBCTixcbiAgICAgKiAvLyAgICAgYmxvY2tIYXNoOiBzdHJpbmcsXG4gICAgICogLy8gICAgIHRyYW5zYWN0aW9uSGFzaDogc3RyaW5nLFxuICAgICAqIC8vICAgICB0cmFuc2FjdGlvbkluZGV4OiBzdHJpbmcsXG4gICAgICogLy8gICAgIGFkZHJlc3M6IHN0cmluZyxcbiAgICAgKiAvLyAgICAgZGF0YTogc3RyaW5nLFxuICAgICAqIC8vICAgICB0b3BpY3M6IEFycmF5PHN0cmluZz5cbiAgICAgKiAvLyAgIH0+XG4gICAgICogLy8gfVxuICAgICAqL1xuICAgIGFzeW5jIGNyZWF0ZUpvYihcbiAgICAgIHN0cmVhbUlkOiBzdHJpbmcsXG4gICAgICBwcm9maWxlczogQXJyYXk8c3RyaW5nPiA9IFtcbiAgICAgICAgLy8gZGVmYXVsdCBwcm9maWxlc1xuICAgICAgICAnUDI0MHAzMGZwczR4MycsXG4gICAgICAgICdQMzYwcDMwZnBzMTZ4OScsXG4gICAgICBdLFxuICAgICAgbWF4UHJpY2VQZXJTZWdtZW50OiBzdHJpbmcsXG4gICAgICB0eCA9IGNvbmZpZy5kZWZhdWx0VHgsXG4gICAgKTogUHJvbWlzZTxUeFJlY2VpcHQ+IHtcbiAgICAgIC8vIGVuc3VyZSB0aGVyZSBhcmUgYWN0aXZlIHRyYW5zY29kZXJzXG4gICAgICAvLyAuLi5tYXliZSB3ZSBzaG91bGQgYWxzbyB0aHJvdyBpZiBkZXBvc2l0IGlzIGJlbG93IHNvbWUgdGhlc2hvbGQ/XG4gICAgICAvLyBpZiAoKGF3YWl0IHJwYy5nZXRUb3RhbEFjdGl2ZVRyYW5zY29kZXJzKCkpIDwgMSkge1xuICAgICAgLy8gICB0aHJvdyBFcnJvcihcbiAgICAgIC8vICAgICAnQ2Fubm5vdCBjcmVhdGUgYSBqb2IgYXQgdGhpcyB0aW1lIHNpbmNlIHRoZXJlIGFyZSBubyBhY3RpdmUgdHJhbnNjb2RlcnMnLFxuICAgICAgLy8gICApXG4gICAgICAvLyB9XG4gICAgICAvLyBjcmVhdGUgam9iIVxuICAgICAgcmV0dXJuIGF3YWl0IHV0aWxzLmdldFR4UmVjZWlwdChcbiAgICAgICAgYXdhaXQgSm9ic01hbmFnZXIuam9iKFxuICAgICAgICAgIHN0cmVhbUlkLFxuICAgICAgICAgIHV0aWxzLnNlcmlhbGl6ZVRyYW5zY29kaW5nUHJvZmlsZXMocHJvZmlsZXMpLFxuICAgICAgICAgIHRvQk4obWF4UHJpY2VQZXJTZWdtZW50KSxcbiAgICAgICAgICB0eCxcbiAgICAgICAgKSxcbiAgICAgICAgY29uZmlnLmV0aCxcbiAgICAgIClcbiAgICAgIC8vIGxhdGVzdCBqb2JcbiAgICAgIC8vIHJldHVybiAoYXdhaXQgcnBjLmdldEpvYnMoeyBicm9hZGNhc3RlcjogdHguZnJvbSwgYmxvY2tzQWdvOiAwIH0pKVswXVxuICAgIH0sXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGNyZWF0ZTogY3JlYXRlTGl2ZXBlZXJTREssXG4gICAgY29uZmlnLFxuICAgIHJwYyxcbiAgICB1dGlscyxcbiAgICBldmVudHMsXG4gICAgY29uc3RhbnRzOiB7XG4gICAgICBBRERSRVNTX1BBRCxcbiAgICAgIEVNUFRZX0FERFJFU1MsXG4gICAgICBERUxFR0FUT1JfU1RBVFVTLFxuICAgICAgVFJBTlNDT0RFUl9TVEFUVVMsXG4gICAgICBWSURFT19QUk9GSUxFX0lEX1NJWkUsXG4gICAgICBWSURFT19QUk9GSUxFUyxcbiAgICB9LFxuICB9XG5cbiAgLy8gS2VlcGluZyB0eXBlZGVmcyBkb3duIGhlcmUgc28gdGhleSBzaG93IHVwIGxhc3QgaW4gdGhlIGdlbmVyYXRlZCBBUEkgdGFibGUgb2YgY29udGVudHNcblxuICAvKipcbiAgICogQUJJIHByb3BlcnR5IGRlc2NyaXB0b3JcbiAgICogQHR5cGVkZWYge09iamVjdH0gQUJJUHJvcERlc2NyaXB0b3JcbiAgICogQHByb3Age2Jvb2xlYW59IGNvbnN0YW50cyAtIGlzIHRoZSBtZXRob2QgY29uc3RhbnQ/XG4gICAqIEBwcm9wIHtBcnJheTx7IG5hbWU6IHN0cmluZywgdHlwZTogc3RyaW5nIH0+fSBpbnB1dHMgLSB0aGUgbWV0aG9kIHBhcmFtc1xuICAgKiBAcHJvcCB7QXJyYXk8eyBuYW1lOiBzdHJpbmcsIHR5cGU6IHN0cmluZyB9Pn0gb3V0cHV0cyAtIG1ldGhvZCByZXR1cm4gdmFsdWVzXG4gICAqIEBwcm9wIHtib29sZWFufSBwYXlhYmxlIC0gaXMgdGhlIG1ldGhvZCBwYXlhYmxlP1xuICAgKiBAcHJvcCB7c3RyaW5nfSBzdGF0ZU11dGFiaWxpdHkgLSB0eXBlIG9mIHN0YXRlIG11dGFiaWxpdHlcbiAgICogQHByb3Age3N0cmluZ30gdHlwZSAtIHR5cGUgb2YgY29udHJhY3QgcHJvcGVydHlcbiAgICovXG5cbiAgLyoqXG4gICAqIE1vc3RseSBcImB0cnVmZmxlYC1zdHlsZVwiIEFCSSBhcnRpZmFjdHMgYnV0IG5vIGJ5dGVjb2RlL25ldHdvcmsgcHJvcGVydGllcyByZXF1aXJlZFxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBDb250cmFjdEFydGlmYWN0XG4gICAqIEBwcm9wIHtzdHJpbmd9IG5hbWUgLSBuYW1lIG9mIHRoZSBjb250cmFjdFxuICAgKiBAcHJvcCB7QXJyYXk8QUJJUHJvcERlc2NyaXB0b3I+fSBhYmkgLSBsaXN0cyBpbmZvIGFib3V0IGNvbnRyYWN0IHByb3BlcnRpZXNcbiAgICovXG5cbiAgLyoqXG4gICAqIFNESyBjb25maWd1cmF0aW9uIG9wdGlvbnNcbiAgICogQHR5cGVkZWYge09iamVjdH0gTGl2ZXBlZXJTREtPcHRpb25zXG4gICAqIEBwcm9wIHtzdHJpbmd9IFtjb250cm9sbGVyQWRkcmVzcyA9ICcweDM3ZEM3MTM2NkVjNjU1MDkzYjk5MzBiYzgxNkUxNmU2YjU4N0Y5NjgnXSAtIFRoZSBhZGRyZXNzIG9mIHRoZSBkZWxwb3llZCBDb250cm9sbGVyIGNvbnRyYWN0XG4gICAqIEBwcm9wIHtzdHJpbmd9IFtwcm92aWRlciA9ICdodHRwczovL3JpbmtlYnkuaW5mdXJhLmlvL3NyRmFXZzBTbGxqZEpBb0NsWDNCJ10gLSBUaGUgRVRIIGh0dHAgcHJvdmlkZXIgZm9yIHJwYyBtZXRob2RzXG4gICAqIEBwcm9wIHtudW1iZXJ9IFtnYXMgPSAwXSAtIHRoZSBhbW91bnQgb2YgZ2FzIHRvIGluY2x1ZGUgd2l0aCB0cmFuc2FjdGlvbnMgYnkgZGVmYXVsdFxuICAgKiBAcHJvcCB7T2JqZWN0PHN0cmluZywgQ29udHJhY3RBcnRpZmFjdD59IGFydGlmYWN0cyAtIGFuIG9iamVjdCBjb250YWluaW5nIGNvbnRyYWN0IG5hbWUgLT4gQ29udHJhY3RBcnRpZmFjdCBtYXBwaW5nc1xuICAgKiBAcHJvcCB7T2JqZWN0PHN0cmluZywgc3RyaW5nPn0gcHJpdmF0ZUtleXMgLSBhbiBvYmplY3QgY29udGFpbmluZyBwdWJsaWMgLT4gcHJpdmF0ZSBrZXkgbWFwcGluZ3MuIFNob3VsZCBiZSBzcGVjaWZpZWQgaWYgdXNpbmcgdGhlIFNESyBmb3IgdHJhbnNhY3Rpb25zIHdpdGhvdXQgTWV0YU1hc2sgKHZpYSBDTEksIGV0YylcbiAgICogQHByb3Age3N0cmluZ3xudW1iZXJ9IGFjY291bnQgLSB0aGUgYWNjb3VudCB0aGF0IHdpbGwgYmUgdXNlZCBmb3IgdHJhbnNhY3RpbmcgYW5kIGRhdGEtZmV0Y2hpbmcuIENhbiBiZSBvbmUgb2YgdGhlIHB1YmxpY0tleXMgc3BlY2lmaWVkIGluIHRoZSBgcHJpdmF0ZUtleXNgIG9wdGlvbiBvciBhbiBpbmRleCBvZiBhbiBhY2NvdW50IGF2YWlsYWJsZSB2aWEgTWV0YU1hc2tcbiAgICovXG5cbiAgLyoqXG4gICAqIEFuIG9iamVjdCBjb250YWluaW5nIGNvbnRyYWN0IGluZm8gYW5kIHV0aWxpdHkgbWV0aG9kcyBmb3IgaW50ZXJhY3Rpbmcgd2l0aCB0aGUgTGl2ZXBlZXIgcHJvdG9jb2wncyBzbWFydCBjb250cmFjdHNcbiAgICogQHR5cGVkZWYge09iamVjdH0gTGl2ZXBlZXJTREtcbiAgICogQHByb3Age09iamVjdDxzdHJpbmcsIGFueT59IGNvbmZpZyAtIHRoaXMgcHJvcCBpcyBtb3N0bHkgZm9yIGRlYnVnZ2luZyBwdXJwb3NlcyBhbmQgY291bGQgY2hhbmdlIGEgbG90IGluIHRoZSBmdXR1cmUuIEN1cnJlbnRseSwgaXQgY29udGFpbnMgdGhlIGZvbGxvd2luZyBwcm9wczogYGFiaXNgLCBgYWNjb3VudHNgLCBgY29udHJhY3RzYCwgYGRlZmF1bHRUeGAsIGBldGhgXG4gICAqIEBwcm9wIHtPYmplY3Q8c3RyaW5nLCBhbnk+fSBjb25zdGFudHMgLSBFeHBvc2VzIHNvbWUgY29uc3RhbnQgdmFsdWVzLiBDdXJyZW50bHksIGl0IGNvbnRhaW5zIHRoZSBmb2xsb3dpbmcgcHJvcHM6IGBBRERSRVNTX1BBRGAsIGBERUxFR0FUT1JfU1RBVFVTYCwgYEVNUFRZX0FERFJFU1NgLCBgVFJBTlNDT0RFUl9TVEFUVVNgLCBgVklERU9fUFJPRklMRVNgLCBgVklERU9fUFJPRklMRV9JRF9TSVpFYFxuICAgKiBAcHJvcCB7RnVuY3Rpb259IGNyZWF0ZSAtIHNhbWUgYXMgdGhlIGBjcmVhdGVMaXZlcGVlclNES2AgZnVuY3Rpb25cbiAgICogQHByb3Age09iamVjdDxzdHJpbmcsIE9iamVjdD59IGV2ZW50cyAtIE9iamVjdCBtYXBwaW5nIGFuIGV2ZW50IG5hbWUgLT4gY29udHJhY3QgZXZlbnQgZGVzY3JpcHRvciBvYmplY3RcbiAgICogQHByb3Age09iamVjdDxzdHJpbmcsIEZ1bmN0aW9uPn0gcnBjIC0gY29udGFpbnMgYWxsIG9mIHRoZSBycGMgbWV0aG9kcyBhdmFpbGFibGUgZm9yIGludGVyYWN0aW5nIHdpdGggdGhlIExpdmVwZWVyIHByb3RvY29sXG4gICAqIEBwcm9wIHtPYmplY3Q8c3RyaW5nLCBGdW5jdGlvbj59IHV0aWxzIC0gY29udGFpbnMgdXRpbGl0eSBtZXRob2RzLiBNb3N0bHkgaGVyZSBqdXN0IGJlY2F1c2UuIENvdWxkIHBvc3NpYmx5IGJlIHJlbW92ZWQgb3IgbW92ZWQgaW50byBpdHMgb3duIG1vZHVsZSBpbiB0aGUgZnV0dXJlXG4gICAqL1xuXG4gIC8qKlxuICAgKiBBbiBvYmplY3QgY29udGFpbmluZyB0aGUgdG90YWwgdG9rZW4gc3VwcGx5IGFuZCBhIHVzZXIncyBhY2NvdW50IGJhbGFuY2UuXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IFRva2VuSW5mb1xuICAgKiBAcHJvcCB7c3RyaW5nfSB0b3RhbFN1cHBseSAtIHRvdGFsIHN1cHBseSBvZiB0b2tlbiBhdmFpbGFibGUgaW4gdGhlIHByb3RvY29sIChMUFRVKVxuICAgKiBAcHJvcCB7c3RyaW5nfSBiYWxhbmNlIC0gdXNlcidzIHRva2VuIGJhbGFuY2UgKExQVFUpXG4gICAqL1xuXG4gIC8qKlxuICAgKiBUcmFuc2FjdGlvbiBjb25maWcgb2JqZWN0XG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IFR4Q29uZmlnXG4gICAqIEBwcm9wIHtzdHJpbmd9IGZyb20gLSB0aGUgRVRIIGFjY291bnQgYWRkcmVzcyB0byBzaWduIHRoZSB0cmFuc2FjdGlvbiBmcm9tXG4gICAqIEBwcm9wIHtudW1iZXJ9IGdhcyAtIHRoZSBhbW91bnQgb2YgZ2FzIHRvIGluY2x1ZGUgaW4gdGhlIHRyYW5zYWN0aW9uXG4gICAqL1xuXG4gIC8qKlxuICAgKiBUcmFuc2FjdGlvbiByZWNlaXB0XG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IFR4UmVjZWlwdFxuICAgKiBAcHJvcCB7c3RyaW5nfSB0cmFuc2FjdGlvbkhhc2ggLSB0aGUgdHJhbnNhY3Rpb24gaGFzaFxuICAgKiBAcHJvcCB7Qk59IHRyYW5zYWN0aW9uSW5kZXggLSB0aGUgdHJhbnNhY3Rpb24gaW5kZXhcbiAgICogQHByb3Age3N0cmluZ30gYmxvY2tIYXNoIC0gdGhlIHRyYW5zYWN0aW9uIGJsb2NrIGhhc2hcbiAgICogQHByb3Age0JOfSBibG9ja051bWJlciAtIHRoZSB0cmFuc2FjdGlvbiBibG9jayBudW1iZXJcbiAgICogQHByb3Age0JOfSBjdW11bGF0aXZlR2FzVXNlZCAtIHRoZSBjdW11bGF0aXZlIGdhcyB1c2VkIGluIHRoZSB0cmFuc2FjdGlvblxuICAgKiBAcHJvcCB7Qk59IGdhc1VzZWQgLSB0aGUgZ2FzIHVzZWQgaW4gdGhlIHRyYW5zYWN0aW9uXG4gICAqIEBwcm9wIHtzdHJpbmd9IGNvbnRyYWN0QWRkcmVzcyAtIHRoZSBjb250cmFjdCBhZGRyZXNzIG9mIHRoZSB0cmFuc2FjdGlvbiBtZXRob2RcbiAgICogQHByb3Age0FycmF5PExvZz59IGxvZ3MgLSBhbiBvYmplY3QgY29udGFpbmluZyBsb2dzIHRoYXQgd2VyZSBmaXJlZCBkdXJpbmcgdGhlIHRyYW5zYWN0aW9uXG4gICAqL1xuXG4gIC8qKlxuICAgKiBBbiBvYmplY3QgcmVwcmVzZW50aW5nIGEgY29udHJhY3QgbG9nXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IExvZ1xuICAgKiBAcHJvcCB7Qk59IGxvZ0luZGV4IC0gdGhlIGxvZyBpbmRleFxuICAgKiBAcHJvcCB7Qk59IGJsb2NrTnVtYmVyIC0gdGhlIGxvZyBibG9jayBudW1iZXJcbiAgICogQHByb3Age3N0cmluZ30gYmxvY2tIYXNoIC0gdGhlIGxvZyBibG9jayBoYXNoXG4gICAqIEBwcm9wIHtzdHJpbmd9IHRyYW5zYWN0aW9uSGFzaCAtIHRoZSBsb2cncyB0cmFuc2FjdGlvbiBoYXNoXG4gICAqIEBwcm9wIHtCTn0gdHJhbnNhY3Rpb25JbmRleCAtIHRoZSBsb2cncyB0cmFuc2FjdGlvbiBpbmRleFxuICAgKiBAcHJvcCB7c3RyaW5nfSBhZGRyZXNzIC0gdGhlIGxvZydzIGFkZHJlc3NcbiAgICogQHByb3Age3N0cmluZ30gZGF0YSAtIHRoZSBsb2cncyBkYXRhXG4gICAqIEBwcm9wIHtBcnJheTxzdHJpbmc+fSB0b3BpY3MgLSB0aGUgbG9nJ3MgdG9waWNzXG4gICAqL1xuXG4gIC8qKlxuICAgKiBJbmZvcm1hdGlvbiBhYm91dCB0aGUgc3RhdHVzIG9mIHRoZSBMUFQgZmF1Y2V0XG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IEZhdWNldEluZm9cbiAgICogQHByb3Age3N0cmluZ30gYW1vdW50IC0gdGhlIGFtb3VudCBkaXN0cmlidXRlZCBieSB0aGUgZmF1Y2V0XG4gICAqIEBwcm9wIHtzdHJpbmd9IHdhaXQgLSB0aGUgZmF1Y2V0IHJlcXVlc3QgY29vbGRvd24gdGltZVxuICAgKiBAcHJvcCB7c3RyaW5nfSBuZXh0IC0gdGhlIG5leHQgdGltZSBhIHZhbGlkIGZhdWNldCByZXF1ZXN0IG1heSBiZSBtYWRlXG4gICAqL1xuXG4gIC8qKlxuICAgKiBBIEJyb2FkY2FzdGVyIHN0cnVjdFxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBCcm9hZGNhc3RlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBhZGRyZXNzIC0gdGhlIEVUSCBhZGRyZXNzIG9mIHRoZSBicm9hZGNhc3RlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBkZXBvc2l0IC0gdGhlIGFtb3VudCBvZiBMUFQgdGhlIGJyb2FkY2FzdGVyIGhhcyBkZXBvc2l0ZWRcbiAgICogQHByb3Age3N0cmluZ30gd2l0aGRyYXdCbG9jayAtIHRoZSBuZXh0IGJsb2NrIGF0IHdoaWNoIGEgYnJvYWRjYXN0ZXIgbWF5IHdpdGhkcmF3IHRoZWlyIGRlcG9zaXRcbiAgICovXG5cbiAgLyoqXG4gICAqIEEgRGVsZWdhdG9yIHN0cnVjdFxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBEZWxlZ2F0b3JcbiAgICogQHByb3Age3N0cmluZ30gYWxsb3dhbmNlIC0gdGhlIGRlbGVnYXRvcidzIExpdmVwZWVyVG9rZW4gYXBwcm92ZWQgYW1vdW50IGZvciB0cmFuc2ZlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBhZGRyZXNzIC0gdGhlIGRlbGVnYXRvcidzIEVUSCBhZGRyZXNzXG4gICAqIEBwcm9wIHtzdHJpbmd9IGJvbmRlZEFtb3VudCAtIFRoZSBhbW91bnQgb2YgTFBUVSBhIGRlbGVnYXRvciBoYXMgYm9uZGVkXG4gICAqIEBwcm9wIHtzdHJpbmd9IGRlbGVnYXRlQWRkcmVzcyAtIHRoZSBFVEggYWRkcmVzcyBvZiB0aGUgZGVsZWdhdG9yJ3MgZGVsZWdhdGVcbiAgICogQHByb3Age3N0cmluZ30gZGVsZWdhdGVkQW1vdW50IC0gdGhlIGFtb3VudCBvZiBMUFRVIHRoZSBkZWxlZ2F0b3IgaGFzIGRlbGVnYXRlZFxuICAgKiBAcHJvcCB7c3RyaW5nfSBmZWVzIC0gdGhlIGFtb3VudCBvZiBMUFRVIGEgZGVsZWdhdG9yIGhhcyBjb2xsZWN0ZWRcbiAgICogQHByb3Age3N0cmluZ30gbGFzdENsYWltUm91bmQgLSB0aGUgbGFzdCByb3VuZCB0aGF0IHRoZSBkZWxlZ2F0b3IgY2xhaW1lZCByZXdhcmQgYW5kIGZlZSBwb29sIHNoYXJlc1xuICAgKiBAcHJvcCB7c3RyaW5nfSBwZW5kaW5nRmVlcyAtIHRoZSBhbW91bnQgb2YgRVRIIHRoZSBkZWxlZ2F0b3IgaGFzIGVhcm5lZCB1cCB0byB0aGUgY3VycmVudCByb3VuZFxuICAgKiBAcHJvcCB7c3RyaW5nfSBwZW5kaW5nU3Rha2UgLSB0aGUgYW1vdW50IG9mIHRva2VuIHRoZSBkZWxlZ2F0b3IgaGFzIGVhcm5lZCB1cCB0byB0aGUgY3VycmVudCByb3VuZFxuICAgKiBAcHJvcCB7c3RyaW5nfSBzdGFydFJvdW5kIC0gdGhlIHJvdW5kIHRoZSBkZWxlZ2F0b3IgYmVjb21lcyBib25kZWQgYW5kIGRlbGVnYXRlZCB0byBpdHMgZGVsZWdhdGVcbiAgICogQHByb3Age3N0cmluZ30gc3RhdHVzIC0gdGhlIGRlbGVnYXRvcidzIHN0YXR1c1xuICAgKiBAcHJvcCB7c3RyaW5nfSB3aXRoZHJhd2FibGVBbW91bnQgLSB0aGUgYW1vdW50IG9mIExQVFUgYSBkZWxlZ2F0b3IgY2FuIHdpdGhkcmF3XG4gICAqIEBwcm9wIHtzdHJpbmd9IHdpdGhkcmF3Um91bmQgLSB0aGUgcm91bmQgdGhlIGRlbGVnYXRvciBjYW4gd2l0aGRyYXcgaXRzIHN0YWtlXG4gICAqIEBwcm9wIHtzdHJpbmd9IG5leHRVbmJvbmRpbmdMb2NrSWQgLSB0aGUgbmV4dCB1bmJvbmRpbmcgbG9jayBJRCBmb3IgdGhlIGRlbGVnYXRvclxuICAgKi9cblxuICAvKipcbiAgICogQSBUcmFuc2NvZGVyIHN0cnVjdFxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBUcmFuc2NvZGVyXG4gICAqIEBwcm9wIHtib29sZWFufSBhY3RpdmUgLSB3aGV0aGVyIG9yIG5vdCB0aGUgdHJhbnNjb2RlciBpcyBhY3RpdmVcbiAgICogQHByb3Age3N0cmluZ30gYWRkcmVzcyAtIHRoZSB0cmFuc2NvZGVyJ3MgRVRIIGFkZHJlc3NcbiAgICogQHByb3Age3N0cmluZ30gcmV3YXJkQ3V0IC0gJSBvZiBibG9jayByZXdhcmQgY3V0IHBhaWQgdG8gdHJhbnNjb2RlciBieSBhIGRlbGVnYXRvclxuICAgKiBAcHJvcCB7c3RyaW5nfSBmZWVTaGFyZSAtICUgb2YgZmVlcyBwYWlkIHRvIGRlbGVnYXRvcnMgYnkgdHJhbnNjb2RlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBsYXN0UmV3YXJkUm91bmQgLSBsYXN0IHJvdW5kIHRoYXQgdGhlIHRyYW5zY29kZXIgY2FsbGVkIHJld2FyZFxuICAgKiBAcHJvcCB7c3RyaW5nfSBwZW5kaW5nUmV3YXJkQ3V0IC0gcGVuZGluZyBibG9jayByZXdhcmQgY3V0IGZvciBuZXh0IHJvdW5kIGlmIHRoZSB0cmFuc2NvZGVyIGlzIGFjdGl2ZVxuICAgKiBAcHJvcCB7c3RyaW5nfSBwZW5kaW5nRmVlU2hhcmUgLSBwZW5kaW5nIGZlZSBzaGFyZSBmb3IgbmV4dCByb3VuZCBpZiB0aGUgdHJhbnNjb2RlciBpcyBhY3RpdmVcbiAgICogQHByb3Age3N0cmluZ30gcGVuZGluZ1ByaWNlUGVyU2VnbWVudCAtIHBlbmRpbmcgcHJpY2UgcGVyIHNlZ21lbnQgZm9yIG5leHQgcm91bmQgaWYgdGhlIHRyYW5zY29kZXIgaXMgYWN0aXZlXG4gICAqIEBwcm9wIHtzdHJpbmd9IHByaWNlUGVyU2VnbWVudCAtIHByaWNlIHBlciBzZWdtZW50IGZvciBhIHN0cmVhbSAoTFBUVSlcbiAgICogQHByb3Age3N0cmluZ30gc3RhdHVzIC0gdGhlIHRyYW5zY29kZXIncyBzdGF0dXNcbiAgICogQHByb3Age3N0cmluZ30gdG90YWxTdGFrZSAtIHRvdGFsIHRva2VucyBkZWxlZ2F0ZWQgdG93YXJkIGEgdHJhbnNjb2RlciAoaW5jbHVkaW5nIHRoZWlyIG93bilcbiAgICovXG5cbiAgLyoqXG4gICAqIEFuIFVuYm9uZGluZ0xvY2sgc3RydWN0XG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IFVuYm9uZGluZ0xvY2tcbiAgICogQHByb3Age3N0cmluZ30gaWQgLSB0aGUgdW5ib25kaW5nIGxvY2sgSURcbiAgICogQHByb3Age3N0cmluZ30gZGVsZWdhdG9yIC0gdGhlIGRlbGVnYXRvcidzIEVUSCBhZGRyZXNzXG4gICAqIEBwcm9wIHtzdHJpbmd9IGFtb3VudCAtIHRoZSBhbW91bnQgb2YgdG9rZW5zIGJlaW5nIHVuYm9uZGVkXG4gICAqIEBwcm9wIHtzdHJpbmd9IHdpdGhkcmF3Um91bmQgLSB0aGUgcm91bmQgYXQgd2hpY2ggdW5ib25kaW5nIHBlcmlvZCBpcyBvdmVyIGFuZCB0b2tlbnMgY2FuIGJlIHdpdGhkcmF3blxuICAgKi9cblxuICAvKipcbiAgICogQW4gb2JqZWN0IGNvbnRhaW5pbmcgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGN1cnJlbnQgcm91bmRcbiAgICogQHR5cGVkZWYge09iamVjdH0gUm91bmRJbmZvXG4gICAqIEBwcm9wIHtzdHJpbmd9IGlkIC0gdGhlIG51bWJlciBvZiB0aGUgY3VycmVudCByb3VuZFxuICAgKiBAcHJvcCB7Ym9vbGVhbn0gaW5pdGlhbGl6ZWQgLSB3aGV0aGVyIG9yIG5vdCB0aGUgY3VycmVudCByb3VuZCBpcyBpbml0aWFsaXplZFxuICAgKiBAcHJvcCB7c3RyaW5nfSBzdGFydEJsb2NrIC0gdGhlIHN0YXJ0IGJsb2NrIG9mIHRoZSBjdXJyZW50IHJvdW5kXG4gICAqIEBwcm9wIHtzdHJpbmd9IGxhc3RJbml0aWFsaXplZFJvdW5kIC0gdGhlIGxhc3Qgcm91bmQgdGhhdCB3YXMgaW5pdGlhbGl6ZWQgcHJpb3IgdG8gdGhlIGN1cnJlbnRcbiAgICogQHByb3Age3N0cmluZ30gbGVuZ3RoIC0gdGhlIGxlbmd0aCBvZiByb3VuZHNcbiAgICovXG5cbiAgLyoqXG4gICAqIEFuIG9iamVjdCBjb250YWluaW5nIGluZm9ybWF0aW9uIGFib3V0IGFuIEV0aGVyZXVtIGJsb2NrXG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IEJsb2NrXG4gICAqIEBwcm9wIHtzdHJpbmd9IG51bWJlciAtIGJsb2NrIG51bWJlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBoYXNoIC0gYmxvY2sgaGFzaFxuICAgKiBAcHJvcCB7c3RyaW5nfSBwYXJlbnRIYXNoIC0gcGFyZW50IGhhcyBvZiB0aGUgYmxvY2tcbiAgICogQHByb3Age3N0cmluZ30gbm9uY2UgLSBibG9jayBub25jZVxuICAgKiBAcHJvcCB7c3RyaW5nfSBzaGEzVW5jbGVzIC0gYmxvY2sgc2hhMyB1bmNsZXNcbiAgICogQHByb3Age3N0cmluZ30gbG9nc0Jsb29tIC0gbG9nc3MgYmxvb20gZm9yIHRoZSBibG9ja1xuICAgKiBAcHJvcCB7c3RyaW5nfSB0cmFuc2FjdGlvbnNSb290IC0gYmxvY2sgdHJhbnNhY3Rpb24gcm9vdCBoYXNoXG4gICAqIEBwcm9wIHtzdHJpbmd9IHN0YXRlUm9vdCAtIGJsb2NrIHN0YXRlIHJvb3QgaGFzaFxuICAgKiBAcHJvcCB7c3RyaW5nfSByZWNlaXB0c1Jvb3QgLSBibG9jayByZWNlaXB0cyByb290IGhhc2hcbiAgICogQHByb3Age3N0cmluZ30gbWluZXIgLSBibG9jayBtaW5lciBoYXNoXG4gICAqIEBwcm9wIHtzdHJpbmd9IG1peEhhc2ggLSBibG9jayBtaXhIYXNoXG4gICAqIEBwcm9wIHtzdHJpbmd9IGRpZmZpY3VsdHkgLSBkaWZmaWN1bHR5IGludFxuICAgKiBAcHJvcCB7c3RyaW5nfSB0b3RhbERpZmZpY3VsdHkgLSB0b3RhbCBkaWZmaWN1bHR5IGludFxuICAgKiBAcHJvcCB7c3RyaW5nfSBleHRyYURhdGEgLSBoYXNoIG9mIGV4dHJhIGRhdGFcbiAgICogQHByb3Age3N0cmluZ30gc2l6ZSAtIGJsb2NrIHNpemVcbiAgICogQHByb3Age3N0cmluZ30gZ2FzTGltaXQgLSBibG9jayBnYXMgbGltaXRcbiAgICogQHByb3Age3N0cmluZ30gZ2FzVXNlZCAtIGdhcyB1c2VkIGluIGJsb2NrXG4gICAqIEBwcm9wIHtudW1iZXJ9IHRpbWVzdGFtcCAtIGJsb2NrIHRpbWVzdGFtcFxuICAgKiBAcHJvcCB7c3RyaW5nfSB0cmFuc2FjdGlvbnMgLSBibG9jayB0cmFuc2FjdGlvbnMgaGFzaFxuICAgKiBAcHJvcCB7c3RyaW5nfSB1bmNsZXMgLSBibG9jayB1bmNsZXMgaGFzaFxuICAgKiBAcHJvcCB7QXJyYXk8VHJhbnNhY3Rpb24+fSB0cmFuc2FjdGlvbnMgLSB0cmFuc2FjdGlvbnMgaW4gdGhlIGJsb2NrXG4gICAqIEBwcm9wIHtzdHJpbmd9IHRyYW5zYWN0aW9uc1Jvb3QgLSByb290IHRyYW5zYWN0aW9uIGhhc2hcbiAgICogQHByb3Age0FycmF5PFVuY2xlPn0gdW5jbGVzIC0gYmxvY2sgdW5jbGVzXG4gICAqL1xuXG4gIC8qKlxuICAgKiBBbiBvYmplY3QgY29udGFpbmluZyBvdmVydmlldyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgam9icyBpbiB0aGUgcHJvdG9jb2xcbiAgICogQHR5cGVkZWYge09iamVjdH0gSm9ic0luZm9cbiAgICogQHByb3Age3N0cmluZ30gdG90YWwgLSB0aGUgdG90YWwgbnVtYmVyIG9mIGpvYnMgY3JlYXRlZFxuICAgKiBAcHJvcCB7Ym9vbGVhbn0gdmVyaWZpY2F0aW9uUmF0ZSAtIHRoZSB2ZXJpZmljYXRpb24gcmF0ZSBmb3Igam9ic1xuICAgKiBAcHJvcCB7c3RyaW5nfSB2ZXJpZmljYXRpb25QZXJpb2QgLSB0aGUgdmVyaWZpY2F0aW9uIHBlcmlvZCBmb3Igam9ic1xuICAgKiBAcHJvcCB7c3RyaW5nfSB2ZXJpZmljYXRpb25TbGFzaGluZ1BlcmlvZCAtIHRoZSBzbGFzaGluZyBwZXJpb2QgZm9yIGpvYnNcbiAgICogQHByb3Age3N0cmluZ30gZmluZGVyRmVlIC0gdGhlIGZpbmRlciBmZWUgZm9yIGpvYnNcbiAgICovXG5cbiAgLyoqXG4gICAqIEEgSm9iIHN0cnVjdFxuICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBKb2JcbiAgICogQHByb3Age3N0cmluZ30gam9iSWQgLSB0aGUgaWQgb2YgdGhlIGpvYlxuICAgKiBAcHJvcCB7c3RyaW5nfSBzdHJlYW1JZCAtIHRoZSBqb2IncyBzdHJlYW0gaWRcbiAgICogQHByb3Age0FycmF5PFRyYW5zY29kaW5nUHJvZmlsZT59IHRyYW5zY29kaW5nT3B0aW9ucyAtIHRyYW5zY29kaW5nIHByb2ZpbGVzXG4gICAqIEBwcm9wIHtzdHJpbmd9IFt0cmFuc2NvZGVyXSAtIHRoZSBFVEggYWRkcmVzcyBvZiB0aGUgYXNzaWduZWQgdHJhbnNjb2RlclxuICAgKiBAcHJvcCB7c3RyaW5nfSBicm9hZGNhc3RlciAtIHRoZSBFVEggYWRkcmVzcyBvZiB0aGUgYnJvYWRjYXN0ZXIgd2hvIGNyZWF0ZWQgdGhlIGpvYlxuICAgKi9cblxuICAvKipcbiAgICogQSBQcm90b2NvbCBzdHJ1Y3RcbiAgICogQHR5cGVkZWYge09iamVjdH0gUHJvdG9jb2xcbiAgICogQHByb3Age2Jvb2xlYW59IHBhdXNlZCAtIHRoZSBwcm90b2NvbCBwYXVzZWQgb3Igbm90XG4gICAqIEBwcm9wIHtzdHJpbmd9IHRvdGFsVG9rZW5TdXBwbHkgLSB0b3RhbCB0b2tlbiBzdXBwbHkgZm9yIHByb3RvY29sXG4gICAqIEBwcm9wIHtzdHJpbmd9IHRvdGFsQm9uZGVkVG9rZW4gLSB0b3RhbCBib25kZWQgdG9rZW4gZm9yIHByb3RvY29sXG4gICAqIEBwcm9wIHtzdHJpbmd9IHRhcmdldEJvbmRpbmdSYXRlIC0gdGFyZ2V0IGJvbmRpbmcgcmF0ZSBmb3IgcHJvdG9jb2xcbiAgICogQHByb3Age3N0cmluZ30gdHJhbnNjb2RlclBvb2xNYXhTaXplIC0gdHJhbnNjb2RlciBwb29sIG1heCBzaXplXG4gICAqL1xufVxuXG5leHBvcnQgeyBjcmVhdGVMaXZlcGVlclNESyBhcyBMaXZlcGVlclNESywgY3JlYXRlTGl2ZXBlZXJTREsgYXMgZGVmYXVsdCB9XG4iXX0=